import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import swal from 'sweetalert2';

import { OrganizationService } from '../shared/data/organization.service';
import { SubscriptionService } from '../shared/data/subscription.service';
import { SettingsService } from '../shared/data/settings.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {
	organizationForm: FormGroup;
	public showOrg: boolean = false;
	public showForm: boolean = false;
	public submitted: boolean = false;
	//public organizations = [];
	public organizations;
	public rows;
	public rows1;
	public newaddress;
	public subscriptionList;
	public subscribers = [];
	public admins = [];
	public save;
  public userData;
  options = {
    types: ['geocode'],
    componentRestrictions: { country: 'AU' }
  }
	columns = [
        { prop: 'firstName' },
        { prop: 'lastEmail' },
        { prop: 'email' },
    	{ prop: 'role' }
    ];
    columns1 = [
        { prop: 'Organization' },
        { prop: 'subscriptionType' },
        { prop: 'Count' }
    ];
    organization = {
        'id': '',
        'orgName': '',
        'orgEmail': '',
        'orgPhone': '',
        'orgAddress': ''
    };
    data = {'status': ''};

    style = 'material';
    title = 'Success';
    body = 'Organization created successfully!';
    timeout = 3000;
    position: SnotifyPosition = SnotifyPosition.centerTop;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;
    public status;

    public timestamp = new Date().getTime();
    module = {
      'emailManagement': false,
      'loyalty': false,
      'survey': false,
      'membership': false,
      'webPush': false,
      'events': false,
      'emailManagementSub': [],
      'loyaltySub': [],
      'membersSub': [],
      'surveySub': [],
      'webPushSub': [],
      'eventsSub': []
    };
    emailManagement = {
      'newsletter': false,
      'reports': false,
      'emailTemplate': false
    };
    loyalty = {
      'memPlans': false,
      'renewals': false,
      'transactions': false,
      'loyaltypoints': false,
      'redeemloyalty': false,
      'loyaltyProgram': false,
      'rewards': false
    };
    members = {
      'members': false,
      'subscribers': false,
      'management': false,
      'list': false,
      'segments' : false
    };
    survey = {
      'createForm': false,
      'formmanager': false,
      'tablemapping': false,
      'chartsreport': false
    };
    webPushDashboard = {
      'webPushDashboard': false,
      'webEngagement': false,
      'segments': false,
      'tokens': false,
      'webtemplate': false,
      'webnotification': false,
      'safaripushconfig': false
    }
    events = {
      'events': false
    };
    modulesettings = false;

	@ViewChild(DatatableComponent) table: DatatableComponent;

  	constructor(public organizationservice: OrganizationService, public settingsservice: SettingsService, public subscriptionService: SubscriptionService, private snotifyService: SnotifyService) {
  		this.showOrg = false;
  		//this.status = false;
		/*this.organizationservice.getOrganization().then(data => {
			if(data['organization']){
				console.log('ORG data - ' + JSON.stringify(data['organization']));
				for (let i = 0; i < data['organization'].length; i++) {
					let array = {
						organizationName: data['organization'][i]['name'],
						organizationEmail: data['organization'][i]['email'],
						organizationPhone: data['organization'][i]['phone']
					}
					this.organizations.push(array);
				}
				this.rows = this.organizations;
			}
		});*/
  	}

  	ngOnInit() {
      this.save = true;
  		this.organizationForm = new FormGroup({
  			'orgName': new FormControl(null, [Validators.required]),
  			'orgEmail': new FormControl(null, [Validators.required]),
  			'orgPhone': new FormControl(null, [Validators.required]),
  			'orgAddress': new FormControl(null, [Validators.required])
	    });
	    this.showOrg = false;
	    console.log('timestamp- ' + this.timestamp);
	    this.organizationservice.getCharts().then(data => {
	    	console.log('getCharts - ' + JSON.stringify(data));
	    });
      this.organizationservice.getOrganization(new Date().getTime()).then(data => {
			     if(data['organization']){
    				this.organizations = data['organization'];
    				for (let i = 0; i < data['organization'].length; i++) {
    					this.status = data['organization'][i]['status'];
    				}
				    console.log('ORG data - ' + JSON.stringify(data['organization']));
				// for (let i = 0; i < data['organization'].length; i++) {
				// 	let array = {
				// 		organizationName: data['organization'][i]['name'],
				// 		organizationEmail: data['organization'][i]['email'],
				// 		organizationPhone: data['organization'][i]['phone'],
				// 		organizationAddress: data['organization'][i]['address']
				// 	}
				// 	this.organizations.push(array);
				// }
				// this.rows = this.organizations;
			}
		});
}



  	handleAddressChange(address) {
		// Do some stuff
		this.newaddress = address.formatted_address;
	}

  	getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
    }

  	saveOrganization(){
  		this.submitted = true;
  		if(this.save){
		  	let organizationDetail = {
				'orgName': this.organizationForm.controls['orgName'].value,
				'orgEmail': this.organizationForm.controls['orgEmail'].value,
				'orgPhone': this.organizationForm.controls['orgPhone'].value,
				'orgAddress': this.newaddress
			}

      this.module.emailManagementSub.push(this.emailManagement);
      this.module.loyaltySub.push(this.loyalty);
      this.module.membersSub.push(this.members);
      this.module.surveySub.push(this.survey);
      this.module.webPushSub.push(this.webPushDashboard);

      this.userData = {
        username: '',
        password: '123456',
        email: organizationDetail['orgEmail'],
        status: 'y'
      }
		  	this.organizationservice.saveOrganization(organizationDetail,this.module,this.userData).then(data => {
          if(data['status'] == 'success'){
            this.menuhidefunction(data['orgId']);
  		  		const { timeout, closeOnClick, ...config } = this.getConfig();
  		  		this.snotifyService.confirm(this.body, this.title, {
  		            ...config,
  		            buttons: [
  		                { text: 'Ok', action: (toast) => {
  		                	this.snotifyService.remove(toast.id);
  		                	this.submitted = false;
  		                	this.showOrg = false;
  		                	this.showForm = false;
  		                	this.organizationservice.getOrganization(new Date().getTime()).then(data => {
  		                		this.organizations = [];
          								if(data['organization']){
          									this.organizations = data['organization'];
          								}
          							});
                        this.organizationForm.reset();
  		                }
                    }
  		            ]
  	        	});
            }
		  	});
		}
		else if(!this.save){
			let organizationDetail = {
				'id': this.organization.id,
				'orgName': this.organizationForm.controls['orgName'].value,
				'orgEmail': this.organizationForm.controls['orgEmail'].value,
				'orgPhone': this.organizationForm.controls['orgPhone'].value,
				'orgAddress': this.newaddress
			}
		  	this.organizationservice.updateOrganization(organizationDetail).then(data => {
		  		const { timeout, closeOnClick, ...config } = this.getConfig();
		  		this.snotifyService.confirm(this.body, this.title, {
		            ...config,
		            buttons: [
		                { text: 'Ok', action: (toast) => {
		                	this.snotifyService.remove(toast.id);
		                	this.submitted = false;
		                	this.showOrg = false;
		                	this.showForm = false;
		                	this.organizationservice.getOrganization(new Date().getTime()).then(data => {
		                		this.organizations = [];
								if(data['organization']){
									this.organizations = data['organization'];
								}
							});
		                } }
		            ]
	        	});
		  	});
		}
  	}

    menuhidefunction(orgId){
      //user Menu
      let userMenu = [
        {
          'path': '/users/profile', 'title': 'User Profile', 'icon': 'fas fa-user', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': []
        }
      ];
      //Main menu
      let mainMenu;
      // let   mainMenu = [{
      //     'path': '/dashboard/charts', 'title': 'Home', 'icon': 'icon-home', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': []
      //   }];

      if(this.module.loyalty){
        let loyaltymenu = {'path': '', 'title': 'Loyalty', 'icon': 'icon-present', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false,'submenu': []};

        if(this.loyalty.memPlans){
          loyaltymenu.submenu.push({ 'path': '/loyalty/membership', 'title': 'Membership', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.loyalty.renewals){
          loyaltymenu.submenu.push({ 'path': '/loyalty/renewals', 'title': 'Renewals', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.loyalty.transactions){
          loyaltymenu.submenu.push({ 'path': '/loyalty/transactions', 'title': 'Transactions', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
          userMenu.push({ 'path': '/loyalty/transactions', 'title': 'Transactions', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.loyalty.loyaltypoints){
          loyaltymenu.submenu.push({ 'path': '/loyalty/loyalty-points', 'title': 'Loyalty Points', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.loyalty.redeemloyalty){
          loyaltymenu.submenu.push({ 'path': '/loyalty/redeem-loyalty-display', 'title': 'Redeem Loyalty ', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
          userMenu.push({ 'path': '/loyalty/redeem-loyalty-display', 'title': 'Redeem Loyalty ', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.loyalty.loyaltyProgram){
          loyaltymenu.submenu.push({ 'path': '/loyalty/loyalty-display', 'title': 'Loyalty Program', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
          userMenu.push({ 'path': '/loyalty/loyalty-display', 'title': 'Loyalty Program', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.loyalty.rewards){
          loyaltymenu.submenu.push({ 'path': '/loyalty/redeem-reward', 'title': 'Rewards', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
          userMenu.push({ 'path': '/loyalty/redeem', 'title': 'Rewards', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        mainMenu.push(loyaltymenu);
      }

      if(this.module.membership){
        let membermenu = {'path': '', 'title': 'Membership', 'icon': 'ft-users', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': []};

        if(this.members.members){
          membermenu.submenu.push({ 'path': '/users/users', 'title': 'Members', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.members.subscribers){
          membermenu.submenu.push({ 'path': '/users/subscribers', 'title': 'Subscriber', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.members.management){
          membermenu.submenu.push({ 'path': '/users/move-users', 'title': 'Management', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.members.list){
          membermenu.submenu.push({ 'path': '/users/lists', 'title': 'Lists', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.members.segments){
          membermenu.submenu.push({ 'path': '/users/segments', 'title': 'Segments', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        mainMenu.push(membermenu);
      }

      if(this.module.survey){
        let surveymenu = {'path': '', 'title': 'Feedback', 'icon': 'icon-envelope', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false,'submenu': []};

        if(this.survey.createForm){
          surveymenu.submenu.push({ 'path': '/feedback/createform', 'title': 'Create Form', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },);
        }
        if(this.survey.formmanager){
          surveymenu.submenu.push({ 'path': '/feedback/formmanager', 'title': 'Form Manager', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },);
        }
        if(this.survey.tablemapping){
          surveymenu.submenu.push({ 'path': '/feedback/dbmaping', 'title': 'Table Maping', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },);
        }
        if(this.survey.chartsreport){
          surveymenu.submenu.push({ 'path': '/feedback/chartsreport', 'title': 'Charts Report', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },);
        }
        mainMenu.push(surveymenu);
      }

      if(this.module.emailManagement){
        let campaignmenu = {'path': '', 'title': 'Campaign', 'icon': 'icon-grid', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false,'submenu': []}

        if(this.emailManagement.newsletter){
          campaignmenu.submenu.push({ 'path': '/campaign/newsletter', 'title': 'Newsletter', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.emailManagement.reports){
          campaignmenu.submenu.push({ 'path': '/campaign/reports', 'title': 'Reports', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        if(this.emailManagement.emailTemplate){
          campaignmenu.submenu.push({ 'path': '/campaign/emailtemplate', 'title': 'Email Template', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        }
        mainMenu.push(campaignmenu);
      }

      if(this.module.webPush){
        let webPushmenu = {"path": "", "title": "Web Push", "icon": "", "class": "has-sub", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []};

        if(this.webPushDashboard.webPushDashboard){
          webPushmenu.submenu.push({"path": "/web-dashboard/dashboard", "title": "Dashboard", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
        }
        if(this.webPushDashboard.webEngagement){
          webPushmenu.submenu.push({"path": "/web-dashboard/web-engagement", "title": "Campaign", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
        }
        if(this.webPushDashboard.segments){
          webPushmenu.submenu.push({"path": "/web-dashboard/segments", "title": "Segments", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
        }
        if(this.webPushDashboard.tokens){
          webPushmenu.submenu.push({"path": "/web-dashboard/tokens", "title": "Tokens", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
        }
        if(this.webPushDashboard.webtemplate){
          webPushmenu.submenu.push({"path": "/web-dashboard/web-template", "title": "Web-Template", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
        }
        if(this.webPushDashboard.webnotification){
          webPushmenu.submenu.push({"path": "/web-dashboard/web-notification", "title": "Web Notifications", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
        }
        if(this.webPushDashboard.safaripushconfig){
          webPushmenu.submenu.push({"path": "/web-dashboard/safaripushconfig", "title": "Safari Push Config", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
        }
        mainMenu.push(webPushmenu);
      }

      if(this.module.events){
        let eventsmenu = {'path': '/events', 'title': 'Events', 'icon': 'icon-list', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': []};

        mainMenu.push(eventsmenu);
      }
      this.settingsservice.addMenuOrgId(mainMenu, userMenu, 'saveModule', orgId).then(data => {
        console.log('mainMenu - '+JSON.stringify(mainMenu));
      });
    }

  	editOrganization(id){
  		this.organizationservice.getOrganizationById(id).then(data => {
  			//console.log('getOrganizationById - ' + JSON.stringify(data));
            this.organization.id = data['organization'][0].id;
            this.organization.orgName = data['organization'][0].name;
            this.organization.orgEmail = data['organization'][0].email;
            this.organization.orgPhone = data['organization'][0].phone;
            this.organization.orgAddress = data['organization'][0].address;

            this.showOrg = true;
			this.save = false;
        },
        error => {
        });

        this.organizationservice.getAdminsByOrgId(id).then(data => {
        	if(data['organization']){
				//console.log('ORG data - ' + JSON.stringify(data['organization']));
				for (let i = 0; i < data['organization'].length; i++) {
					let array = {
						firstName: data['organization'][i]['FirstName'],
						lastEmail: data['organization'][i]['LastName'],
						email: data['organization'][i]['email'],
						role: data['organization'][i]['role']
					}
					this.admins.push(array);
				}
				this.rows = this.admins;
			}
    	});

    	this.subscriptionService.getSubscribers().then(data => {
    		console.log('getSubscribers - ' + JSON.stringify(data));
			this.subscriptionList = data['rows'];
			if(data['rows']){
				//console.log('ORG data - ' + JSON.stringify(data['organization']));
				for (let i = 0; i < data['rows'].length; i++) {
					let array = {
						Organization: data['rows'][i]['Organization'],
						subscriptionType: data['rows'][i]['Type'],
						Count: data['rows'][i]['Count']
					}
					this.subscribers.push(array);
				}
				this.rows1 = this.subscribers;
			}
		});
    this.organizationservice.ModuleSettings(this.module, 'getModule', id).then(data => {
      if(data['status'] == 'success'){
        this.modulesettings = true;
        this.module.emailManagement = data['module'][0]['emailManagement'];
        this.module.loyalty = data['module'][0]['loyalty'];
        this.module.survey = data['module'][0]['survey'];
        this.module.membership = data['module'][0]['membership'];
        this.module.webPush = data['module'][0]['webPush'];
        this.emailManagement.newsletter = data['module'][0]['emailManagementSub'][0]['newsletter'];
        this.emailManagement.reports = data['module'][0]['emailManagementSub'][0]['reports'];
        this.emailManagement.emailTemplate = data['module'][0]['emailManagementSub'][0]['emailTemplate'];
        this.loyalty.memPlans = data['module'][0]['loyaltySub'][0]['memPlans'];
        this.loyalty.renewals = data['module'][0]['loyaltySub'][0]['renewals'];
        this.loyalty.transactions = data['module'][0]['loyaltySub'][0]['transactions'];
        this.loyalty.loyaltypoints = data['module'][0]['loyaltySub'][0]['loyaltypoints'];
        this.loyalty.redeemloyalty = data['module'][0]['loyaltySub'][0]['redeemloyalty'];
        this.loyalty.loyaltyProgram = data['module'][0]['loyaltySub'][0]['loyaltyProgram'];
        this.loyalty.rewards = data['module'][0]['loyaltySub'][0]['rewards'];
        this.members.members = data['module'][0]['membersSub'][0]['members'];
        this.members.subscribers = data['module'][0]['membersSub'][0]['subscribers'];
        this.members.management = data['module'][0]['membersSub'][0]['management'];
        this.members.list = data['module'][0]['membersSub'][0]['list'];
        this.members.segments = data['module'][0]['membersSub'][0]['segments'];
        this.survey.createForm = data['module'][0]['surveySub'][0]['createForm'];
        this.survey.formmanager = data['module'][0]['surveySub'][0]['formmanager'];
        this.survey.tablemapping = data['module'][0]['surveySub'][0]['tablemapping'];
        this.survey.chartsreport = data['module'][0]['surveySub'][0]['chartsreport'];
        this.webPushDashboard.webPushDashboard = data['module'][0]['webPushSub'][0]['webPushDashboard'];
        this.webPushDashboard.webEngagement = data['module'][0]['webPushSub'][0]['webEngagement'];
        this.webPushDashboard.segments = data['module'][0]['webPushSub'][0]['segments'];
        this.module.emailManagementSub = [];
        this.module.loyaltySub = [];
        this.module.membersSub = [];
        this.module.surveySub = [];
        this.module.webPushSub = [];
      }
      else{
        this.modulesettings = false;
      }
    });

    }

    viewOrganization(id) {
    	this.organizationservice.getOrganizationById(id).then(data => {
    		swal({
		        title: '<b>Organization Detail</b>',
		        type: 'info',
		        html:
		        '<table class="viewOrg"><tbody><tr></tr>' +
		        '<tr><th>ORGANIZATION NAME</th><td>'+data['organization'][0].name+'</td></tr>' +
		        '<tr><th>EMAIL</th><td>'+data['organization'][0].email+'</td></tr>' +
		        '<tr><th>PHONE</th><td>'+data['organization'][0].phone+'</td></tr>' +
		        '<tr><th>ADDRESS</th><td>'+data['organization'][0].address+'</td></tr>' +
		        '</tbody></table><br />',
		        showCloseButton: true,
		        confirmButtonText:
		        '<i class="fa fa-thumbs-up"></i> Okay!'
		    })
    	});
    }

  	onChange(status,id) {
  		this.organizationservice.updateStatus(status.toString(),id).then(data => {
  			console.log('updateStatus - ' + JSON.stringify(data));
  		});
  	}

  	createOrg(){
        this.save =  true;
      	this.showOrg = false;
      	this.showForm = true;
  	}

    cancel(){
      this.showOrg = false;
      this.showForm = false;
      this.organizationForm.reset();
    }
}
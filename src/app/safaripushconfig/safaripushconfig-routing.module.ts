import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SafaripushconfigComponent } from "./safaripushconfig.component";

const routes: Routes = [
    {
        // path: '',
        // children: [{
        //     path: 'cards',
        //     component: CardsComponent
        // }
        // ]
        path: '',
        component: SafaripushconfigComponent,
       data: {
         title: 'Statistics'
       },
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SafaripushconfigRoutingModule { }

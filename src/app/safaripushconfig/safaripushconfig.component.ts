import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import { SettingsService } from '../shared/data/settings.service';
import { FormControl,FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-safaripushconfig',
  templateUrl: './safaripushconfig.component.html',
  styleUrls: ['./safaripushconfig.component.scss']
})
export class SafaripushconfigComponent implements OnInit {
  @ViewChild('uploadLabel') uploadLabel : ElementRef;
  @ViewChild('uploadLabel1') uploadLabel1 : ElementRef;
  @ViewChild('uploadLabel2') uploadLabel2 : ElementRef;
  @ViewChild('uploadLabel3') uploadLabel3 : ElementRef;

    safariForm:FormGroup;
    //formData: FormGroup;
    files = [];
    p12;
    pem;
    cer;
    img;

    url;
    water;
    sitedir;
    loader:boolean = false;
    serve = '';
    file = '';
    apple = '';
    p12File='';
    wwdrcaFile='';
    pushCertificate='';
    icon='';
    safariId='';
    URL='';
    form = {
    'image': '',
    };
    formData;
    action='Add';
    public file1;
    public file2;
    public file3;
    public imageIcon;
  constructor(private _FB: FormBuilder,public settingsservice: SettingsService, private http: HttpClient) {
}
  ngOnInit() {
    this.safariForm = new FormGroup({
          'upload': new FormControl(null, [Validators.required]),
          'upload1': new FormControl(null, [Validators.required]),
          'uploadFile': new FormControl(null, [Validators.required]),
          'upload3': new FormControl(null, [Validators.required]),
          'safari': new FormControl(null, [Validators.required]),
          'push': new FormControl(null, [Validators.required]),
          'token': new FormControl(null, [Validators.required]),
          'pwd': new FormControl(null, [Validators.required]),
          'domains': new FormControl(null, [Validators.required])
    	});

      this.settingsservice.getSafariConfig().then(data => {
    // alert(JSON.stringify(data));
          console.log('water - ' + JSON.stringify(data));
      if(data['data'].length > 0)
      {
        this.action='Update';
        let pushid = data['data'][0]['safari_push_id'];
        (<HTMLInputElement>document.getElementById('safari')).value = pushid;
        this.uploadLabel.nativeElement.innerHTML=data['data'][0]['p12_file'];
        this.uploadLabel1.nativeElement.innerHTML=data['data'][0]['wwdrca_file'];
        this.uploadLabel2.nativeElement.innerHTML=data['data'][0]['push_certificate'];
        this.uploadLabel3.nativeElement.innerHTML=data['data'][0]['icon_file'];
        (<HTMLInputElement>document.getElementById('allowedDomains')).value=data['data'][0]['allowedDomains'];
        (<HTMLInputElement>document.getElementById('password')).value=data['data'][0]['password'];
        (<HTMLInputElement>document.getElementById('url')).value=data['data'][0]['safari_push_url'];
        (<HTMLInputElement>document.getElementById('authenticationToken')).value=data['data'][0]['token'];
      }
  });

    // this.settingsservice.getSettings().then(data => {
    //    console.log('data - ' + data['settings'][0]['sitedir']);
    //    if(data['status'] == 'success'){
    //      this.sitedir = data['settings'][0]['sitedir'];
    //    }
    // });
  }

  /*upload(files: File[]){
  this.uploadLabel.nativeElement.innerHTML = files[0].name;
    this.loader = true;
    this.url = 'https://click365.com.au/usermanagement/safaripushtemp/'+files[0].name;
    console.log('dxcvdx - ' + this.url);
    this.uploadFileProgress(files);
  }

  uploadFileProgress(files: File[]){
    console.log('formData - ' + files);
    var formData = new FormData();
    console.log('formData - ' + formData);
    Array.from(files).forEach(f => formData.append('file',f));
    this.http.post('https:/click365.com.au/usermanagement/uploadSafariFile.php?fld=safaripushtemp', formData)
      .subscribe(event => {
        this.loader = false;
    });
  }

  upload1(files: File[]){
  this.uploadLabel1.nativeElement.innerHTML = files[0].name;
    this.loader = true;
    this.url = 'https://click365.com.au/usermanagement/safaripushtemp/'+files[0].name;
    console.log('dxcvdx - ' + this.url);
    this.GetUploadFileProgress(files);
  }

  GetUploadFileProgress(files: File[]){
    console.log('formData - ' + files);
    var formData = new FormData();
    console.log('formData - ' + formData);
    Array.from(files).forEach(f => formData.append('file',f));
    this.http.post('https:/click365.com.au/usermanagement/uploadSafariFile.php?fld=safaripushtemp', formData)
      .subscribe(event => {
        this.loader = false;
    });
  }*/
  uploadFiles(files: File[],fileType,extension){
    console.log('File - ' + fileType)
    var ext = (<HTMLInputElement>document.getElementById(fileType)).value.match(/\.([^\.]+)$/)[1];

    switch (ext) {
      case extension:
        break;
      default:
        swal('Error!','Only ' + extension + ' extension is allowed','error');
        (<HTMLInputElement>document.getElementById(fileType)).value = '';
        return;
    }
    this.loader = true;
    if(fileType == 'upload'){
      this.uploadLabel.nativeElement.innerHTML = files[0].name;
      this.file1 = files[0].name;//'https://click365.com.au/usermanagement/safari_push/' + this.safariForm.controls['safari'].value + '/' + files[0].name;
      this.p12 = files;

      //this.safariForm.controls['upload'].setValue('https://click365.com.au/usermanagement/safaripushtemp/'+files[0].name);
    }
    if(fileType == 'upload1'){
      this.uploadLabel1.nativeElement.innerHTML = files[0].name;
      this.file2 = files[0].name;//'https://click365.com.au/usermanagement/safari_push/' + this.safariForm.controls['safari'].value + '/' + files[0].name;
      this.pem = files;
      //this.safariForm.controls['upload1'].setValue('https://click365.com.au/usermanagement/safaripushtemp/'+files[0].name);
    }
    if(fileType == 'uploadFile'){
      this.uploadLabel2.nativeElement.innerHTML = files[0].name;
      this.file3 = files[0].name;//'https://click365.com.au/usermanagement/safari_push/' + this.safariForm.controls['safari'].value + '/' + files[0].name;
      //this.safariForm.controls['uploadFile'].setValue('https://click365.com.au/usermanagement/safaripushtemp/'+files[0].name);  this.files = files;
      this.cer = files;
    }
    // this.uploadFileAndProgress(files);
  }

  uploadFileAndProgress(files: File[]){
    console.log('formData - ' + files);
    var formData = new FormData();
    console.log('formData - ' + formData);
    Array.from(files).forEach(f => formData.append('file',f));
    this.http.post('https:/click365.com.au/usermanagement/upload_safari_push_files.php?safariId='+this.formData.safariId, formData)
      .subscribe(event => {
        this.loader = false;
    });
  }
  upload3(files: File[]){
    var ext = (<HTMLInputElement>document.getElementById('upload3')).value.match(/\.([^\.]+)$/)[1];

    switch (ext) {
      case 'png':
      // case 'gif':
      // case 'jpg':
      // case 'jpeg':
      // case 'tiff':
      // case 'tif':
        break;
      default:
        swal('Error!','Only .png extension allowed','error');
        (<HTMLInputElement>document.getElementById('upload3')).value = '';
        return;
      }

  this.uploadLabel3.nativeElement.innerHTML = files[0].name;
    this.loader = true;
    this.img = files;

    this.imageIcon = files[0].name;//'https://click365.com.au/usermanagement/safari_push/' + this.safariForm.controls['safari'].value + '/' + files[0].name;
    //this.safariForm.controls['upload3'].setValue('https://click365.com.au/usermanagement/images/'+files[0].name);
    console.log('dxcvdx - ' + this.url);
    // this.uploadPushProgress(files);
  }


  uploadPushProgress(files: File[]){
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f));

    this.http.post('https:/click365.com.au/usermanagement/upload_safari_push_iconset.php?safariId='+this.formData.safariId, formData)
      .subscribe(event => {
        this.loader = false;
        if(event && event['status'] == "Sorry, your file is too large."){
          alert('Sorry, your file is too large. Recommended Size: 192x192.');
          this.uploadLabel3.nativeElement.innerHTML = '';
        }
    });
  }

  isURL()
  {
    if (this.url == null) {
      return false;
    }
    // Assigning the url format regular expression
      let urlPattern = "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
    return this.url.match(urlPattern);
  }
  save() {
    let pushid = this.safariForm.controls['safari'].value;
    this.formData = {
    safariId: pushid,
    p12File: '/home/click365com/public_html/usermanagement/safari_push/' + pushid + '/' + this.file1,//this.file1,
    wwdrcaFile: '/home/click365com/public_html/usermanagement/safari_push/' + pushid + '/' + this.file2,//this.file2,
    pushCertificate: '/home/click365com/public_html/usermanagement/safari_push/' + pushid + '/' + this.file3,////this.file3,
    icon: '/home/click365com/public_html/usermanagement/safari_push/' + pushid + '/' + this.imageIcon,//this.imageIcon,
    URL: this.safariForm.controls['push'].value,
    allowedDomains: this.safariForm.controls['domains'].value,
    password: this.safariForm.controls['pwd'].value,
    token: this.safariForm.controls['token'].value,
    org_id: localStorage.getItem('oi')//,
    // 'file':{'tmp_name':this.img}
    }

this.uploadPushProgress(this.img);

this.uploadFileAndProgress(this.p12);
this.uploadFileAndProgress(this.pem);
this.uploadFileAndProgress(this.cer);
this.uploadFileAndProgress(this.img);
// this.formData.set('file',fileArray);

  // Array.from(this.p12).forEach(f => this.formData.append('file',f));
  // Array.from(this.pem).forEach(f => this.formData.append('file',f));

// alert(JSON.stringify(this.formData));
  console.log('push-' + JSON.stringify(this.formData));
  this.settingsservice.saveSafariConfig(this.formData,this.action).then(data => {
swal('Success!','Saved Successfully','success');
    console.log('water - ' + JSON.stringify(data));
});
}
}

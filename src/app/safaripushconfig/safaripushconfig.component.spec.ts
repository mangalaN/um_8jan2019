import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafaripushconfigComponent } from './safaripushconfig.component';

describe('SafaripushconfigComponent', () => {
  let component: SafaripushconfigComponent;
  let fixture: ComponentFixture<SafaripushconfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafaripushconfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafaripushconfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

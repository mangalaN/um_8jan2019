import { Component, OnInit, ViewChild } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist";
import { Router, ActivatedRoute } from '@angular/router';
import * as Highcharts from 'highcharts';
import { StatisticsService } from '../shared/data/statistics.service';

declare var require: any;

//const data: any = require('../../shared/data/chartist.json');

export interface Chart {
    type: ChartType;
    data: Chartist.IChartistData;
    options?: any;
    responsiveOptions?: any;
    events?: ChartEvent;
}

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})


export class StatisticsComponent implements OnInit {
  Highcharts = Highcharts;
  newsletterDate;
  newsletterId;
  newsletterDetail;
  subject;
  list;
  totalSent;
  notSent;
  sent;
  totalSentPerc;
  notSentPerc;
  clickCount;
  openCount;
  chartData;
  chartLabels;
  links;
  domains;
  openSubscribers;
  clickSubscribers;
  campaignUnsubscribers;
  numberUsers;
  openRate;
  clickCountTable: boolean;
  openCountTable: boolean = true;
  unsubscriberTable: boolean;
  chartOptions;

  constructor(public statisticsService: StatisticsService, private route: ActivatedRoute, public router: Router) {
    this.route.queryParams.subscribe(params => {
      this.newsletterId = window.atob(params['id']).split("$")[0];
      //this.newsletterId = window.atob(this.newsletterId);//.split("$")[0];
      this.statisticsService.gettimeline(this.newsletterId).then(data => {
        if(data['status']){
            //console.log(data['reports']);
            let opencount = [];
            let clickcount = [];
            let graphdata = [];
            let graphlabels = [];
            for(let i = 0; i < data['reports']['open'].length; i++){
                opencount.push(data['reports']['open'][i]['opencount']);
                graphlabels.push(data['reports']['open'][i]['hr']);
            }
            opencount.push(1);
            for(let i = 0; i < data['reports']['click'].length; i++){
                clickcount.push(data['reports']['click'][i]['clickcount']);
                graphlabels.push(data['reports']['open'][i]['hr']);
            }
            clickcount.push(6);
            graphdata.push({data:opencount, label:'Opened'},{data:clickcount, label:'Clicked'});
            //console.log(graphdata);
            this.chartData = graphdata;
            console.log(this.chartData);
            console.log(typeof(this.chartData));
            this.chartLabels = graphlabels;
        }
      },
      error => {
      });
    });
  }

  ngOnInit() {
    this.statisticsService.getReportById(this.newsletterId).then(data => {
        if(data['status']){
            //document.getElementById('messageTemplate').innerHTML = JSON.parse(data['reports'][0]['message']);
            this.newsletterDetail = JSON.parse(data['reports'][0]['message']);
            this.newsletterDate = data['reports'][0]['date'];
            this.subject = data['reports'][0]['subject'];
            this.list = data['reports'][0]['list'];
            this.totalSent = data['reports'][0]['totalSent'];
            this.notSent = data['reports'][0]['notSent'];
            this.numberUsers = data['reports'][0]['numberUsers'];
            this.sent = data['reports'][0]['numberUsers'] - data['reports'][0]['notSent'];
            this.totalSentPerc = ((data['reports'][0]['totalSent'] / data['reports'][0]['numberUsers']) * 100);
            this.notSentPerc = ((data['reports'][0]['notSent'] / data['reports'][0]['numberUsers']) * 100);
            this.clickCount = data['reports'][0]['clickCount'];
            console.log(data['reports']);
            if(this.newsletterDate != ''){
              let sendDate = this.newsletterDate.split('-');
              let month = new Date(this.newsletterDate).getUTCMonth();
              this.statisticsService.getnormaltimeline(this.newsletterId).then(data => {
                if(data['status']){
                  let openRates = data['reports']['open'];
                  let openData = [];
                  for(let i=0; i<openRates.length;i++){
                    if(openRates[i].hasOwnProperty('opencount')){
                      openData.push([openRates[i].hr, parseInt(openRates[i].opencount)]);
                    }
                    else{
                      openData.push(["00:00",0]);
                    }
                  }
                  let clickRates = data['reports']['click'];
                  let clickData = [];
                  for(let i=0; i<clickRates.length;i++){
                    if(clickRates[i].hasOwnProperty('clickcount')){
                      clickData.push([clickRates[i].hr, parseInt(clickRates[i].clickcount)]);
                    }
                    else{
                      clickData.push(["00:00",0]);
                    }
                  }
                  let deliveredData = [];
                  deliveredData.push([data['reports']['delivered'][0]['hr'],parseInt(data['reports']['delivered'][0]['deliveredCount'])]);

                  let bouncedData = [];
                  bouncedData.push([data['reports']['delivered'][0]['hr'],parseInt(data['reports']['delivered'][0]['bouncedCount'])]);

                  Highcharts.setOptions({
                      time: {
                          timezoneOffset: parseInt("10") * 60
                      }
                  });
                  this.chartOptions = {
                    title: {
                        text: 'Newsletter Overview'
                    },
                    chart: {
                      type: 'spline'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    series: [{
                        data: openData,
                        pointInterval: 10000 * 60,
                        name:'Open Count'
                    },
                    {
                        data: clickData,
                        pointInterval: 10000 * 60,
                        name:'Click Count'
                    },
                    {
                        data: deliveredData,
                        pointInterval: 10000 * 60,
                        name:'Delivered Count'
                    },
                    {
                        data: bouncedData,
                        pointInterval: 10000 * 60,
                        name:'Bounced Count'
                    }]
                  };
                }
              });
            }
        }
    },
    error => {
    });

    this.statisticsService.getclickCounts(this.newsletterId).then(data => {
      if(data['status'] = 'success'){
        this.links = data['reports'];
        this.openCount = data['reports'][0]['openCount'];
        if(this.openCount > 0){
          this.openRate = (this.openCount/(this.totalSent - this.notSent)) * 100;
        }
      }
    },
    error => {
    });

    this.statisticsService.getEmailDomain(this.newsletterId).then(data => {
      if(data['status'] = 'success'){
        this.domains = data['reports'];
        console.log(this.domains);
      }
    },
    error => {
    });

    this.statisticsService.getOpenSubscriberList(this.newsletterId).then(data => {
      if(data['status'] = 'success'){
        this.openSubscribers = data['reports'];
      }
    },
    error => {
    });

    this.statisticsService.getClickSubscriberList(this.newsletterId).then(data => {
      if(data['status'] = 'success'){
        this.clickSubscribers = data['reports'];
      }
    },
    error => {
    });

    this.statisticsService.getUnSubscriberList(this.newsletterId).then(data => {
      if(data['status'] = 'success'){
        this.campaignUnsubscribers = data['reports'];
      }
    },
    error => {
    });
  }

  openSubscriberList () {
    this.clickCountTable=false;
    this.unsubscriberTable=false;
    this.openCountTable=true;
  }

  clickSubscriberList () {
    this.clickCountTable=true;
    this.openCountTable=false;
    this.unsubscriberTable=false;
  }

  unSubscriberList () {
    this.unsubscriberTable=true;
    this.clickCountTable=false;
    this.openCountTable=false;
  }
}

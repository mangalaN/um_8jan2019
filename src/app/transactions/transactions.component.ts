import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import { TransactionsService } from '../shared/data/transactions.service';
//import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
//import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';

declare var require: any;
//const data: any = require('../shared/data/company.json');

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})

export class TransactionsComponent implements OnInit {
	str = [];
  columns;
  option;
  title;
  saveTxt = 'Save';
  IsSchedule = false;
  every = 1;
  weekday = 0;
  month = 1;
  day = 1;
  hour = 0;
  minute = 0;
  //dayValues = [0, 1, 2, 3, 4, 5, 6];
  days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
  dayOfMonthValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
  //months = ["January", "February","March","April","May","June","July","August","September","October","November","December"];
  months = [
      { name: 'January', value: 1},
      { name: 'February', value: 2},
      { name: 'March', value: 3},
      { name: 'April', value: 4},
      { name: 'May', value: 5},
      { name: 'June', value: 6},
      { name: 'July', value: 7},
      { name: 'August', value: 8},
      { name: 'September', value: 9},
      { name: 'October', value: 10},
      { name: 'November', value: 11},
      { name: 'December', value: 12}
  ];

  minuteValues = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
  hourValues = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
  // p: number = 1;
  // pageSize: number = 2;
  // collectionSize=0;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('uploadLabel') uploadLabel : ElementRef;

  fileName = '';
  selectedValue = 'local';
  linkVal = null;
  serverVal = null;
  usernameVal = null;
  passwordVal = null;
  headernameVal = null;
  headervalueVal = null;
  files = [];
  //flag=false;
  json;
  selectedTable = 'transaction';

  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  Form: FormGroup;

	constructor(
    //private spinnerService: NgxSpinnerService,
    public router: Router,private http: HttpClient,public transactionsservice: TransactionsService) {
    // this.temp = [...data];
		// this.rows = data;
    this.router.navigate(['/configurations/import']);

	}

convert(input)
{
  switch (input) {
          case 1:
              return "1st";
          case 2:
              return "2nd";
          case 3:
              return "3rd";
          case 21:
              return "21st";
          case 22:
              return "22nd";
          case 23:
              return "23rd";
          case 31:
              return "31st";
          case null:
              return null;
          default:
              return input + "th";
      }
}

SaveTask(task,start_dt=null,end_dt=null)
{
  if(this.title.trim() == '' || this.title == null)
  {
    alert("Please Enter Title");
    return;
  }
  var user_id = localStorage.getItem('currentUserId');
  var parameters;
  if(this.selectedValue == 'local')
  {
      alert("Schedule cannot be created for Local File.");
      return;
  }
  else if(this.selectedValue == 'link')
  {
    let link = this.f.link.value;
    if(link == null || link.trim() == "")
    {
      alert("Please Insert Link");
      return;
    }
    parameters = {'link':link};
  }
  else if(this.selectedValue == 'ftp')
  {
    if(this.f.link.value == null || this.f.server.value== null || this.f.username.value==null || this.f.password.value==null || this.f.link.value.trim() == "" || this.f.server.value.trim() == "" || this.f.username.value.trim() == "" || this.f.password.value.trim() == "")
    {
      alert("Any FTP Detail should not be emply.");
      return;
    }
    parameters = {'link': this.f.link.value,'server': this.f.server.value,'username': this.f.username.value,'password': this.f.password.value};
  }
  else if(this.selectedValue == 'api')
  {
    if(this.f.link.value == null || this.f.link.value.trim() == "")
    {
      alert("Please Insert API URL");
      return;
    }
    parameters = {'link':this.f.link.value};
  }
  var minute1="";
  var hour1="";
  var day1="";
  var month1="";
  var weekday1="";
  if(task == 'schedule')
  {
    if(this.every == 1)
    {
      minute1="*";
      hour1="*";
      day1="*";
      month1="*";
      weekday1="*";
    }
    else if(this.every == 2)
    {
      minute1 =this.minute + "";
      hour1="*";
      day1="*";
      month1="*";
      weekday1="*";
    }
    else if(this.every == 3)
    {
      minute1=this.minute + "";
      hour1=this.hour + "";
      day1="*";
      month1="*";
      weekday1="*";
    }
    else if(this.every == 4)
    {
      minute1=this.minute + "";
      hour1=this.hour + "";
      day1="*";
      month1="*";
      weekday1=this.weekday + "";
    }
    else if(this.every == 5)
    {
      minute1=this.minute + "";
      hour1=this.hour + "";
      day1=this.day + "";
      month1="*";
      weekday1="*";
    }
    else if(this.every == 6)
    {
      minute1=this.minute + "";
      hour1=this.hour + "";
      day1=this.day + "";
      month1=this.month + "";
      weekday1="*";
    }
  }
  let src_cols = [];
  let dest_cols = [];
  for (var j = 0; j < this.columns.length; j++) {
    if((<HTMLInputElement>document.getElementById("c" + j)).value != 'Nothing(skip)')
    {
      src_cols.push(this.columns[j]);
      dest_cols.push((<HTMLInputElement>document.getElementById("c" + j)).value);
    }
  }

  this.transactionsservice.saveSchedule(user_id,src_cols,dest_cols,this.selectedTable,this.selectedValue,parameters,minute1,hour1,day1,month1,weekday1,this.title,task)
  .then(data => {
    if(task == 'schedule')
      alert("Schedule Submitted Successfully!");
    else
    {
      //alert(start_dt);
      this.transactionsservice.saveImportLog(data['id'],start_dt,end_dt,'Succeeded')
      .then(data => {
        alert("Saved Successfully!");
      });
    }
  });
}

ngOnInit() {
		//this.users = JSON.parse(localStorage.getItem('currentUser'));
    	//console.log(this.users);
      this.Form = new FormGroup({
          'link': new FormControl(null, [Validators.required]),
          'server': new FormControl(null, [Validators.required]),
          'username': new FormControl(null, [Validators.required]),
          'password': new FormControl(null, [Validators.required]),
          'headername': new FormControl(null, [Validators.required]),
          'headervalue': new FormControl(null, [Validators.required])
      }, {updateOn: 'blur'});
	}

  select()
  {
    this.saveTxt="Save";
    this.linkVal = null;
    this.serverVal = null;
    this.usernameVal = null;
    this.passwordVal = null;
    this.headernameVal = null;
    this.headervalueVal = null;

    // this.f.link.value = null;
    // this.f.server.value = null;
    // this.f.username.value = null;
    // this.f.password.value = null;
    // this.f.headername.value = null;
    // this.f.headervalue.value = null;
  }
  getCol()
  {
    this.saveTxt = "Save";
    this.CreateTableFromJSON(this.json);
  }

  upload(files: File[]){
	    //pick from one of the 4 styles of file uploads below
	    //this.uploadAndProgress(files);
	    //this.settingDetail.companylogo = files[0].name;
      this.uploadLabel.nativeElement.innerHTML = files[0].name;
      this.fileName = files[0].name;
      console.log(this.uploadLabel.nativeElement);
      //alert(files[0].name);
      this.files = files;
      //this.uploadAndProgress(files);
	}

  uploadAndProgress(files: File[]){
    console.log(files)
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f))

    this.http.post('https:/click365.com.au/usermanagement/uploadCsv.php?fld=csv', formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          //this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          //this.uploadSuccess = true;
        }
    });
  }

  get f() { return this.Form.controls; }

  import()
  {
    //this.spinnerService.show();
    //this.flag = true;
    if(this.title.trim() == '' || this.title == null)
    {
      alert("Please Enter Title");
      return;
    }
    let link = "";
    if(this.selectedValue == 'local')
    {
      this.uploadAndProgress(this.files);
      link = "uploads/csv/" + this.fileName;
      if(this.fileName == '')
      {
        alert("Please select File");
        return;
      }
    }
    else if(this.selectedValue == 'link')
    {
      link = this.f.link.value;
      if(link == null || link.trim() == "")
      {
        alert("Please Insert Link");
        return;
      }
    }
    if(this.selectedValue == 'local' || this.selectedValue == 'link')
    {
      this.transactionsservice.getByCsv(link).then(data => {
        this.json = data["data"];
        this.CreateTableFromJSON(data["data"]);
      });
    }
    else if(this.selectedValue == 'ftp')
    {
      if(this.f.link.value == null || this.f.server.value== null || this.f.username.value==null || this.f.password.value==null || this.f.link.value.trim() == "" || this.f.server.value.trim() == "" || this.f.username.value.trim() == "" || this.f.password.value.trim() == "")
      {
        alert("Any FTP Detail should not be emply.");
        return;
      }

        this.transactionsservice.getByFTP(this.f.link.value,this.f.server.value,this.f.username.value,this.f.password.value).then(data => {
          this.json = data["data"];
          this.CreateTableFromJSON(data["data"]);
      });
    }
    else if(this.selectedValue == 'api')
    {
      if(this.f.link.value == null || this.f.link.value.trim() == "")
      {
        alert("Please Insert API URL");
        return;
      }
      this.transactionsservice.getByAPI(this.f.link.value,this.f.headername.value,this.f.headervalue.value).then(data => {
        this.json = data["data"];
          this.CreateTableFromJSON(data);
      });
    }
    //this.spinnerService.hide();
}

// CreateTableFromAPI(json) {
//   this.json= json;
//   let key2, key1;
//   var col = [];
//
//   for (key1 in json)
//   {
//     key2 = key1;
//     break;
//   }
//   // EXTRACT VALUE FOR HTML HEADER.
//     {
//       for (var i = 0; i < json[key2].length; i++) {
//           for (var key in json[key2][i]) {
//               if (col.indexOf(key) === -1) {
//                   col.push(key);
//               }
//           }
//       }
//       this.columns = col;
// alert(JSON.stringify(this.columns));
//       // CREATE DYNAMIC TABLE.
//       var table = document.createElement("table");
//
//       // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
//
//       var tr = table.insertRow(-1);                   // TABLE ROW.
//
//       for (var i = 0; i < col.length; i++) {
//           var th = document.createElement("th");      // TABLE HEADER.
//           th.innerHTML = col[i] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//           tr.appendChild(th);
//       }
//       this.transactionsservice.getColNames(this.selectedTable).then(data => {
//         //alert(JSON.stringify(data));
//         tr = table.insertRow(-1);
//
//         let start = '';
//         let option = "<option value='Nothing(skip)'>Nothing(skip)</option>";
//         let end = '';
//         for (var j = 0; j < data["cols"].length; j++) {
//             option = option + "<option value='" + data["cols"][j] + "'>" + data["cols"][j] + "</option>";
//         }
//
//         for (var i = 0; i < col.length; i++) {
//             var th = document.createElement("th");
//             //th.innerHTML = "<input type='text' value='" + col[i] + "' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//             start = "<select id='c" + i + "'>";
//             end = "</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//             th.innerHTML = start + option + end;
//             tr.appendChild(th);
//         }
//
//         // ADD JSON DATA TO THE TABLE AS ROWS.
//         for (var i = 0; i < json[key2].length; i++) {
//
//             tr = table.insertRow(-1);
//
//             for (var j = 0; j < col.length; j++) {
//                 var tabCell = tr.insertCell(-1);
//                 tabCell.innerHTML = json[key2][i][col[j]] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//                 //this.row.push(json[0][i][col[j]]);
//             }
//         }
//         var divContainer = document.getElementById("showData");
//         divContainer.innerHTML = "";
//         divContainer.appendChild(table);
//     });
//
//               //alert(JSON.stringify(this.row));
//       // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
//       // var divContainer = document.getElementById("showData");
//       // divContainer.innerHTML = "";
//       // divContainer.appendChild(table);
//   }

  CreateTableFromJSON(json) {
    this.json= json;
    //alert(JSON.stringify(json));
    //this.collectionSize = json.length;

   //alert(JSON.stringify(this.json));

        // EXTRACT VALUE FOR HTML HEADER.
        var col = [];
        for (var i = 0; i < json.length; i++) {
            for (var key in json[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }
        this.columns = col;
//alert(JSON.stringify(this.columns));
        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");

        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

        var tr = table.insertRow(-1);                   // TABLE ROW.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            tr.appendChild(th);
        }
        this.transactionsservice.getColNames(this.selectedTable).then(data => {
          //alert(JSON.stringify(data));
          tr = table.insertRow(-1);

          let start = '';
          this.option = "<option value='Nothing(skip)'>Nothing(skip)</option>";
          let end = '';
          this.str = [];
          for (var j = 0; j < data["cols"].length; j++) {
              this.option = this.option + "<option value='" + data["cols"][j] + "'>" + data["cols"][j] + "</option>";
              this.str.push(data["cols"][j]);
          }
          //alert(JSON.stringify(this.str));

          for (var i = 0; i < col.length; i++) {
              var th = document.createElement("th");
              //th.innerHTML = "<input type='text' value='" + col[i] + "' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
              start = "<select id='c" + i + "'>";
              end = "</select>";//"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
              th.innerHTML = start + this.option + end;
              // let s = start + option + end;
              // this.str.push(s);// = start + option + end;
              tr.appendChild(th);
              // var divContainer = document.getElementById("s");
              // divContainer.innerHTML = "";
              // divContainer.appendChild(table);
          }
          // ADD JSON DATA TO THE TABLE AS ROWS.
          for (var i = 0; i < json.length; i++) {

              tr = table.insertRow(-1);

              for (var j = 0; j < col.length; j++) {
                  var tabCell = tr.insertCell(-1);
                  tabCell.innerHTML = json[i][col[j]] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                  //this.row.push(json[i][col[j]]);
              }
          }
          // var divContainer = document.getElementById("showData");
          // divContainer.innerHTML = "";
          // divContainer.appendChild(table);
      });

                //alert(JSON.stringify(this.row));
        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        // var divContainer = document.getElementById("showData");
        // divContainer.innerHTML = "";
        // divContainer.appendChild(table);
    }
    save()
    {
      this.saveTxt = "Processing...";
      var row = '';
      // var col = '';
      //
      // for (var i1 = 0; i1 < this.columns.length; i1++) {
      //   if((<HTMLInputElement>document.getElementById("c" + i1)).value != 'Nothing(skip)')
      //   {
      //       if(col.includes((<HTMLInputElement>document.getElementById("c" + i1)).value))
      //       {
      //         alert('Same column cannot be selected multiple times.');
      //         this.saveTxt = "Save";
      //         return;
      //       }
      //     //col = col + "," + document.getElementById("c" + i).value;
      //       col = col + "," + (<HTMLInputElement>document.getElementById("c" + i1)).value;
      //   }
      // }
      // col = col.substring(1, col.length);
      // if(col == '')
      // {
      //   alert("PLease select atleast one column");
      //   this.saveTxt = "Save";
      //   return;
      // }
      let col = '';
      col = this.validate();
      if(col == '')
      {
        this.saveTxt = "Save";
        return;
      }
      let cnt = 0;
      for (var i = 0; i < this.json.length; i++) {
          row = '';
          for (var j = 0; j < this.columns.length; j++) {
            if((<HTMLInputElement>document.getElementById("c" + j)).value != 'Nothing(skip)')
            //    row = row + ",'" + this.columns[j] + "': '" + this.json[i][this.columns[j]] + "'";
                row = row + ",'" + this.json[i][this.columns[j]] + "'";
          }
          row = row.substring(1, row.length);
        //alert(JSON.stringify(row));

          var start_dt = new Date().toISOString();
          this.transactionsservice.saveTable(this.selectedTable,col,row).then(data => {
            cnt = cnt + 1;
          if(cnt == this.json.length)
          {
            var end_dt = new Date().toISOString();
            this.saveTxt = "Saved";
            //alert("Saved Successfully!");
            this.SaveTask('manual',start_dt,end_dt);
          }

        });
      }
      // else
      //  alert("Something went wrong.");
    }
    validate()
    {
      var col = '';

      for (var i1 = 0; i1 < this.columns.length; i1++) {
        if((<HTMLInputElement>document.getElementById("c" + i1)).value != 'Nothing(skip)')
        {
            if(col.includes((<HTMLInputElement>document.getElementById("c" + i1)).value))
            {
              alert('Same column cannot be selected multiple times.');
              col = '';
              return col;
            }
          //col = col + "," + document.getElementById("c" + i).value;
            col = col + "," + (<HTMLInputElement>document.getElementById("c" + i1)).value;
        }
      }
      col = col.substring(1, col.length);
      if(col == '')
      {
        alert("Please select atleast one column");
        return col;
      }
      return col;
    }
    schedule()
    {
      let col = '';
      col = this.validate();
      if(col != '')
      {
        this.IsSchedule = true;
      }
  }
}

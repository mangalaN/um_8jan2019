import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TransactionsRoutingModule } from "./transactions-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { TransactionsComponent } from "./transactions.component";

@NgModule({
    imports: [
        CommonModule,
        TransactionsRoutingModule,
        NgxChartsModule,
        NgbModule,
        MatchHeightModule,
        NgxDatatableModule
    ],
    declarations: [
        TransactionsComponent,
   ]
})
export class TransactionsModule { }

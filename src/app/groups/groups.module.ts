import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { GroupsRoutingModule } from "./groups-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { GroupsComponent } from "./groups.component";
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { UiSwitchModule } from 'ngx-ui-switch';
import { GroupDetailComponent } from './group-detail/group-detail.component';
import { GroupMembersComponent } from './group-members/group-members.component';

@NgModule({
    imports: [
        CommonModule,
        GroupsRoutingModule,
        NgxChartsModule,
        NgbModule,
        MatchHeightModule,
        FormsModule,
        ReactiveFormsModule,
        NgxDatatableModule,
        NgxPaginationModule,
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        UiSwitchModule
    ],
    declarations: [
        GroupsComponent,
        GroupDetailComponent,
        GroupMembersComponent
   ]
})
export class GroupsModule { }

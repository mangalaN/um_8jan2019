import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, NgForm } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { GroupsService } from '../shared/data/groups.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  @ViewChild("groupsForm") groupsForm: NgForm;
  //groups: FormGroup;
  groupData = {
    'groupName':'',
    'groupDescription':'',
    'groupControl':'',
    'user_id':localStorage.getItem('currentUserId')
  };
  addGroup = false;
  groupList = [];
  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  columns = [
    { name: 'Name' },
    { name: 'Description' },
    { name: 'Access' },
    { name: 'Date' }
  ];

  constructor(private groupsService:GroupsService, private formBuilder: FormBuilder, private snotifyService: SnotifyService,) { }

  ngOnInit() {
    this.groupsService.getGroups().then(data =>{
      if(data['status'] == 'success'){
        this.groupList = data['groups'];
      }
      else{
        this.groupList = [];
      }
    })
  }

  saveGroup(){
    this.groupsService.addGroups(this.groupData).then(data =>{
      if(data['status'] == 'success'){
        //
      }
      else{
        //
      }
    });
  }
editGroup(id){
    this.groupsService.getGroups().then(data =>{
  if(data['status'] == 'success'){
    this.groupData.groupName = data['groups'][0]['name'];
      this.groupData.groupDescription = data['groups'][0]['description'];
        this.groupData.groupControl = data['groups'][0]['groupControl'];
    }
  });
}
deleteGroup(id){
  this.groupsService.deleteGroup(id).then(data => {
    if(data['text'] == "true"){
      this.snotifyService.success('Deleted Successfully', '', this.getConfig());
        this.groupList = this.groupList.filter(h => h.id != id);
}
    else if(data['text'] == 'cannot delete'){
      this.snotifyService.success('Connot delete groups as it is used in loyalty program', '', this.getConfig());
    }
  });
}
  trimLongText(name){
    if (!name) return '';
    if (name.length <= 20) {
        return name;
    }
    return name.substr(0, 20) + '...';
  }
  getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
      }


}

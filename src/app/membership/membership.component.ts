import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';

import { MembershipService } from '../shared/data/membership.service';
import { EmailTemplateService } from '../shared/data/email-template.service';

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.scss']
})
export class MembershipComponent implements OnInit {
  members = [];
  showMembership = false;
  memDetail;

  @ViewChild('membershipIconLabel') membershipIconLabel : ElementRef;
  membershipForm: FormGroup;

  p: number = 1;
  totalList;
  previousPage: any;
  pageSize: number = 5;
  collectionSize;
  editMem = false;
  memId = 0;
  selectval = '';
  memLogo = '';
  memShowLogo = false;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  /*columns = [
    { name: 'Membership Name' },
    { name: 'Price' },
    { name: 'Duration' },
    { name: 'Action' }
  ];*/
  columns = [
    { name: 'Plans' },
    { name: 'Price' },
    { name: 'Duration'}

  ];
  allColumns = [
    { name: 'Plans' },
    { name: 'Price' },
    { name: 'Duration' }
  ];
  isCollapsed = false;

  templates;
  actionsetIndex = 0;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private http: HttpClient, private snotifyService: SnotifyService, public membershipservice: MembershipService, public emailTemplateService: EmailTemplateService, public router: Router, private _FB: FormBuilder) {

  }

  ngOnInit() {
    this.membershipForm = new FormGroup({
        'membershipName': new FormControl(null, [Validators.required]),
        'textArea': new FormControl(null, [Validators.required]),
        'price': new FormControl(null, [Validators.required]),
        'duration': new FormControl(null, [Validators.required]),
        'durationIn': new FormControl(null, [Validators.required]),
        'membershipIcon': new FormControl(null, null),
        'actionset' : this._FB.array([
        ]),
    }, {updateOn: 'blur'});
    this.membershipservice.getMembership().then(data => {
      if(data['membership']){
        this.members = data['membership'];
        this.collectionSize = data['membership'].length;
        this.totalList = data['membership'].length;
      }
    },
    error => {
    });
    this.emailTemplateService.getNewsTemp(new Date()).then(data => {
      if(data['status'] == 'success'){
        this.templates = data['emailTemplate'];
        // this.templates = [...this.templates];
        //this.addSetCondition(0);
      }
      else{
        this.templates = [];
      }
    });
  }

   get f() { return this.membershipForm.controls; }

   toggle(col) {
    const isChecked = this.isChecked(col);
    console.log('isChecked value - ' + isChecked);
    if(isChecked) {
      this.columns = this.columns.filter(c => {
        console.log('Name - ' + c.name);
        return c.name !== col.name;
      });
    } else {
      this.columns = [...this.columns, col];
      console.log('columns - ' + this.columns);
    }
  }

  isChecked(col) {
    return this.columns.find(c => {
      return c.name === col.name;
    });
  }

   memToggle(){
     this.showMembership = true;
   }

   initMapFields() : FormGroup{
    //  let f = 'days'+index;
    //  let m = 'template'+index;
    //  return this._FB.group({
    //    f : '',
    //    m: ''
    //  });
     return this._FB.group({
       days   : [],
       template   : []
     });
   }

   addSetCondition(){
    //  let fg = this._FB.group(new FormControl());
    //  this.particularsArray.push(this.initMapFields(this.actionsetIndex));
    //  this.actionsetIndex ++;
    const control = <FormArray>this.membershipForm.controls.actionset;
    control.push(this.initMapFields());
    // this.actionsetIndex ++;
   }

   removeSetCondition(i : number) : void{
     const control = <FormArray>this.membershipForm.controls.actionset;
     control.removeAt(i);
   }

   addSetConditionEdit(myobj){
     const control = <FormArray>this.membershipForm.controls.actionset;
     control.push(this.initMapFieldsEdit(myobj));
    }

    initMapFieldsEdit(myobj) : FormGroup{
      return this._FB.group({
        days   : [myobj['days']],
        template   : [myobj['template']]
      });
    }

  //  addSetConditionEdit(myobj, index){
  //    let fg = this._FB.group(new FormControl());
  //    this.particularsArray.push(this.initMapFieldsEdit(myobj, index));
  //   //  const control = <FormArray>this.membershipForm.controls.actionset;
  //   //  control.push(this.initMapFieldsEdit(myobj, index));
  //   }
   //
  //   initMapFieldsEdit(myobj, index) : FormGroup{
  //     let f = 'days'+index;
  //     let m = 'template'+index;
  //     return this._FB.group({
  //       f   : [myobj[f]],
  //       m   : [myobj[m]]
  //     });
  //   }


    cancel(){
      //window.location.reload();
      this.showMembership = false;
      this.membershipForm.reset();
      this.memLogo = '';
      this.memShowLogo = false;
      this.selectval = '';
      this.memId = 0;
      this.editMem = false;
      this.membershipForm = new FormGroup({
          'membershipName': new FormControl(null, [Validators.required]),
          'textArea': new FormControl(null, [Validators.required]),
          'price': new FormControl(null, [Validators.required]),
          'duration': new FormControl(null, [Validators.required]),
          'durationIn': new FormControl(null, [Validators.required]),
          'membershipIcon': new FormControl(null, null),
          'actionset' : this._FB.array([
          ]),
      }, {updateOn: 'blur'});
      this.membershipservice.getMembershipTime(new Date().getTime()).then(data => {
        if(data['membership']){
          this.members = data['membership'];
          this.collectionSize = data['membership'].length;
          this.totalList = data['membership'].length;
        }
      },
      error => {
      });
    }

    addMembership(){
      if(!this.editMem){
        this.memDetail = {
          memname: this.f.membershipName.value,
          description: this.f.textArea.value,
          duration: this.f.duration.value,
          price: this.f.price.value,
          durationIn: this.f.durationIn.value,
          membershipIcon: this.memLogo,
          actionset: this.f.actionset.value
        }
        this.membershipservice.addMembership(this.memDetail).then(data => {
          if(data['status']){
            this.actionsetIndex = 0;
            this.memId = 0;
            this.snotifyService.success('Saved Successfully', '', this.getConfig());
              this.showMembership = false;
              this.membershipForm.reset();
              this.membershipForm = new FormGroup({
                  'membershipName': new FormControl(null, [Validators.required]),
                  'textArea': new FormControl(null, [Validators.required]),
                  'price': new FormControl(null, [Validators.required]),
                  'duration': new FormControl(null, [Validators.required]),
                  'durationIn': new FormControl(null, [Validators.required]),
                  'membershipIcon': new FormControl(null, null),
                  'actionset' : this._FB.array([
                  ]),
              }, {updateOn: 'blur'});
              this.membershipservice.getMembershipTime(new Date().getTime()).then(data => {
                if(data['membership']){
                  this.members = data['membership'];
                  this.collectionSize = data['membership'].length;
                  this.totalList = data['membership'].length;
                }
              },
              error => {
              });
              //this.router.navigate(['/membership']);
          }
        });
      }
      else{
        let pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
                      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
                      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
                      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        //if(pattern.test(this.memLogo)){
          this.memLogo = this.memLogo.substring(this.memLogo.lastIndexOf('/')+1);
        //}
        console.log('memLogo 1- '+ this.memLogo.substring(this.memLogo.lastIndexOf('/')+1));
        console.log('pattern.test(this.memLogo) - '+ pattern.test(this.memLogo));
        this.memDetail = {
          memname: this.f.membershipName.value,
          description: this.f.textArea.value,
          duration: this.f.duration.value,
          price: this.f.price.value,
          durationIn: this.f.durationIn.value,
          membershipIcon: this.memLogo,
          id: this.memId,
          actionset: this.f.actionset.value
        };
        console.log('memLogo 2- '+ this.memLogo);
        //console.log('memDetail - '+this.memDetail);
        this.membershipservice.updateMembership(this.memDetail).then(data => {
          if(data['status']){
              console.log(data['membership']);
              this.showMembership = false;
              this.membershipForm.reset();
              this.membershipForm = new FormGroup({
                  'membershipName': new FormControl(null, [Validators.required]),
                  'textArea': new FormControl(null, [Validators.required]),
                  'price': new FormControl(null, [Validators.required]),
                  'duration': new FormControl(null, [Validators.required]),
                  'durationIn': new FormControl(null, [Validators.required]),
                  'membershipIcon': new FormControl(null, null),
                  'actionset' : this._FB.array([
                  ]),
              }, {updateOn: 'blur'});
              this.memLogo = '';
              this.memShowLogo = false;
              this.selectval = '';
              this.membershipservice.getMembershipTime(new Date().getTime()).then(data => {
                if(data['membership']){
                  this.memId = 0;
                  this.editMem = false;
                  this.snotifyService.success('Updted Successfully', '', this.getConfig());
                  this.members = data['membership'];
                  this.collectionSize = data['membership'].length;
                  this.totalList = data['membership'].length;
                }
              },
              error => {
              });
          }
        });
      }
    }

    editmembership(id){
      this.membershipservice.getMembershipById(id).then(data => {
        if(data['status'] == 'success'){
            this.membershipForm = new FormGroup({
              'membershipName': new FormControl(null, [Validators.required]),
              'textArea': new FormControl(null, [Validators.required]),
              'price': new FormControl(null, [Validators.required]),
              'duration': new FormControl(null, [Validators.required]),
              'durationIn': new FormControl(null, [Validators.required]),
              'membershipIcon': new FormControl(null, null),
              'actionset' : this._FB.array([
              ]),
            }, {updateOn: 'blur'});
            this.actionsetIndex = 0;

            this.membershipForm.controls['membershipName'].setValue(data['membership'][0]['plans']);
            this.membershipForm.controls['textArea'].setValue(data['membership'][0]['description']);
            this.membershipForm.controls['price'].setValue(data['membership'][0]['price']);
            this.membershipForm.controls['duration'].setValue(data['membership'][0]['terms']);
            this.membershipForm.controls['durationIn'].setValue(data['membership'][0]['duration']);
            this.memLogo = data['membership'][0]['membershipIcon'];
            //this.membershipForm.controls['membershipIcon'].setValue(data['membership'][0]['membershipIcon']);
            //this.membershipIconLabel.nativeElement.innerHTML = data['membership'][0]['membershipIcon'];
            this.selectval = data['membership'][0]['duration'];
            this.memId = data['membership'][0]['id'];
            this.showMembership = true;
            this.editMem = true;
            this.memShowLogo = true;
            let actionset = data['membership'][0]['templateMapping'];
            console.log(actionset.length);
            for(let i= 0; i< actionset.length; i++){
              this.addSetConditionEdit(actionset[i]);
            }
            // for(let i = 0; i < actionset.length; i++){
            //   this.addSetConditionEdit(actionset[i], i);
            // }
            //this.membershipForm.controls['actionset'].push();
            //console.log(this.membershipForm.controls['membershipIcon'].value);
            //this.router.navigate(['/membership']);
        }
      });
    }

    deletemembership(id){
      this.membershipservice.deleteMembership(id).then(data => {
        if(data['text'] == "true"){
          this.snotifyService.success('Deleted Successfully', '', this.getConfig());
          this.membershipservice.getMembershipTime(new Date().getTime()).then(data => {
            if(data['membership']){
              this.members = data['membership'];
              this.collectionSize = data['membership'].length;
              this.totalList = data['membership'].length;
            }
          },
          error => {
          });
        }
        else if(data['text'] == 'cannot delete'){
          this.snotifyService.success('Connot delete membership as it is used in loyalty program', '', this.getConfig());
        }
      });
    }

    uploadmembershipIcon(files: File[]){
      this.membershipIconLabel.nativeElement.innerHTML = files[0].name;
      this.memLogo = files[0].name;
      //console.log(this.membershipIconLabel.nativeElement);
      //alert(files[0].name);
      this.uploadAndProgress(files);
    }

    uploadAndProgress(files: File[]){
      console.log(files)
      var formData = new FormData();
      Array.from(files).forEach(f => formData.append('file',f));

      this.http.post('https:/click365.com.au/usermanagement/uploadLogo.php?fld=MembershipLogo', formData, {reportProgress: true, observe: 'events'})
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            //this.percentDone = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            //this.uploadSuccess = true;
          }
      });
    }

    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }

}

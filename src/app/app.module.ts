
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from "./shared/shared.module";
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { StoreModule } from '@ngrx/store';
import { AngularHighchartsChartModule } from 'angular-highcharts-chart';

import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';

import { AppComponent } from './app.component';
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";
import { FullLayoutComponent } from "./layouts/full/full-layout.component";

import { DragulaService } from 'ng2-dragula';
import { AuthService } from './shared/auth/auth.service';
import { AuthGuard } from './shared/auth/auth-guard.service';
import { UserService } from './shared/data/user.service';
import { ImportLogService } from './shared/data/importlog.service';
import { ListsService } from './shared/data/lists.service';
import { MembershipService } from './shared/data/membership.service';
import { RenewalService } from './shared/data/renewal.service';
import { newsLetterService } from './shared/data/newsletter.service';
import { StatisticsService } from '././shared/data/statistics.service';
import { EmailTemplateService } from '././shared/data/email-template.service';
import { loyaltyService } from '././shared/data/loyalty.service';
import { loyaltypointService } from '././shared/data/loyaltypoint.service';
import { AuthenticationService } from '././shared/data/authentication.service';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { NgxPaginationModule } from 'ngx-pagination';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { SettingsService } from './shared/data/settings.service';
import { ChartCongigService } from './shared/data/chart-congig.service';
import { SubscriptionService } from './shared/data/subscription.service';
import { OrganizationService } from './shared/data/organization.service';
import * as $ from 'jquery';
import { AddFieldsService } from './shared/data/addFields.service';
import { TransactionsService } from './shared/data/transactions.service';
import { FeedbackService } from './shared/data/feedback.service';
import { redeemloyaltyService } from '././shared/data/redeemloyalty.service';
import { click365webpushService } from './shared/data/click365webpush.service';
import { crewsService } from '././shared/data/crews.service';
import { WebDashboardService } from '././shared/data/web-dashboard.service';
import { WebEngagementService } from '././shared/data/web-engagement.service';
import { WebTemplateService } from '././shared/data/web-template.service';


import { TemplateMappingService } from '././shared/data/template-mapping.service';
import { EventMappingService } from '././shared/data/event-mapping.service';
import { ABTestingStatisticsService } from '././shared/data/abtestingStats.service';
import { MembershipSegmentsService } from '././shared/data/membershipSegments.service';
import { VerifyNewOrganization } from '././shared/data/verifyNewOrganization.service';
import { EventsService } from './shared/data/events.service';
import { GroupsService } from './shared/data/groups.service';
import {NgChatModule} from 'ng-chat';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  }

@NgModule({
    declarations: [
        AppComponent,
        FullLayoutComponent,
        ContentLayoutComponent
    ],
    imports: [
        BrowserAnimationsModule,
        StoreModule.forRoot({}),
        AngularHighchartsChartModule,
        AppRoutingModule,
        SharedModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        NgxPaginationModule,
        GooglePlaceModule,
        SnotifyModule.forRoot(),
        HttpClientModule,
        NgbModule.forRoot(),
        NgChatModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
              }
        }),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBr5_picK8YJK7fFR2CPzTVMj6GG1TtRGo'
        })
    ],
    providers: [
        { provide: 'SnotifyToastConfig', useValue: ToastDefaults },
        GoogleMapsAPIWrapper,
        SnotifyService,
        AuthService,
        AuthGuard,
        UserService,
        ListsService,
        MembershipService,
        RenewalService,
        newsLetterService,
        StatisticsService,
        EmailTemplateService,
        loyaltyService,
        loyaltypointService,
        AuthenticationService,
        DragulaService,
        SettingsService,
        ChartCongigService,
        SubscriptionService,
        OrganizationService,
        AddFieldsService,
        TransactionsService,
        FeedbackService,
        redeemloyaltyService,
        click365webpushService,
        crewsService,
        AddFieldsService,
        ImportLogService,
        WebDashboardService,
        WebEngagementService,
        WebTemplateService,
        TemplateMappingService,
        EventMappingService,
        ABTestingStatisticsService,
        MembershipSegmentsService,
        VerifyNewOrganization,
        EventsService,
        GroupsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

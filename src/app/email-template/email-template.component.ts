import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EmailTemplateService } from '../shared/data/email-template.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.scss']
})
export class EmailTemplateComponent implements OnInit {
	emailTemplateForm: FormGroup;
	defaultTemplate;
	emailTemplates;
	p: number = 1;
	pageSize: number = 5;
	collectionSize;
	showTable = true;
	showEdit = false;
	new = true;
  typeIdExsits = false;
	emailTemDetails: any;
	public options: Object = {
	    charCounterCount: true,
	    // Set the image upload parameter.
	    imageUploadParam: 'image_param',

	    // Set the image upload URL.
	    imageUploadURL: 'https://click365.com.au/usermanagement/images',

	    // Additional upload params.
	    imageUploadParams: {id: 'my_editor'},

	    // Set request type.
	    imageUploadMethod: 'POST',

	    // Set max image size to 5MB.
	    imageMaxSize: 5 * 1024 * 1024,

	    // Allow to upload PNG and JPG.
	    imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif'],

      //Edit toolbar
      toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'indent', 'outdent', '|', 'fontFamily', 'fontSize', 'color', 'paragraphStyle', '|', 'paragraphFormat',
      , 'insertImage', 'insertLink', 'viewcode', 'insertHR', 'selectAll', 'clearFormatting', 'html', 'undo', 'redo'],

      events:  {
			'froalaEditor.initialized':  function () {
				console.log('initialized');
			},
  			'froalaEditor.image.beforeUpload':  function  (e,  editor,  images) {
			    //Your code
			    if  (images.length) {
					// Create a File Reader.
					const  reader  =  new  FileReader();
					// Set the reader to insert images when they are loaded.
					reader.onload  =  (ev)  =>  {
					const  result  =  ev.target['result'];
					editor.image.insert(result,  null,  null,  editor.image.get());
					console.log(ev,  editor.image,  ev.target['result'])
					};
					// Read image as base64.
					reader.readAsDataURL(images[0]);
		    	}
		    	// Stop default upload chain.
		    	return  false;
		  	}
		}
	};
	success = false;
	error = false;
	editEmailTemDetails = {
		'id': ''
	};

	timeout = 3000;
	position: SnotifyPosition = SnotifyPosition.centerTop;
	progressBar = true;
	closeClick = true;
	newTop = true;
	backdrop = -1;
	dockMax = 8;
	blockMax = 6;
	pauseHover = true;
	titleMaxLength = 15;
	bodyMaxLength = 80;

	constructor(private snotifyService: SnotifyService, private formBuilder: FormBuilder, public emailtemplateservice: EmailTemplateService, public router: Router, private _FB: FormBuilder) {
		this.defaultTemplate = this.emailtemplateservice.getDefaultTemplate();
	}

	ngOnInit() {
		this.emailTemplateForm = this.formBuilder.group({
    	'temname': ['', Validators.required],
			'temsub': ['', Validators.required],
			'tembody': this.defaultTemplate,
      'template':false,
      'temtypeid': ['', null],
      'mapusers' : this._FB.array([
      ]),
    }, {updateOn: 'blur'});
      	this.emailtemplateservice.getEmailTemp().then(data => {
	        console.log(data['emailTemplate']);
	        if(data['status'] == 'success'){
	        	this.emailTemplates = data['emailTemplate'];
	        	this.collectionSize = data['emailTemplate'].length;
	          }
            else{
  	        	this.emailTemplates = [];
  	        	this.collectionSize = 0;
            }
	    },
	    error => {
	    });
	}

	get f() { return this.emailTemplateForm.controls; }

	addet(){
		this.showTable = false;
		this.new = true;
	}

  initMapFields() : FormGroup{
    return this._FB.group({
      nametype   : [],
      uservalue   : []
    });
  }

  addSetCondition(){
   const control = <FormArray>this.emailTemplateForm.controls.mapusers;
   control.push(this.initMapFields());
  }

  removeSetCondition(i : number) : void{
    const control = <FormArray>this.emailTemplateForm.controls.mapusers;
    control.removeAt(i);
  }

  addSetConditionEdit(myobj){
    const control = <FormArray>this.emailTemplateForm.controls.mapusers;
    control.push(this.initMapFieldsEdit(myobj));
   }

   initMapFieldsEdit(myobj) : FormGroup{
     return this._FB.group({
       nametype   : [myobj['nametype']],
       uservalue   : [myobj['uservalue']]
     });
   }

	cancel(){
	    this.showTable = true;
	    this.new = true;
	    this.showEdit = false;
      this.emailTemplateForm.reset();
	    this.emailtemplateservice.getEmailTempTimeStamp(new Date().getTime()).then(data => {
	      console.log(data['emailTemplate']);
	      if(data['status'] == 'success'){
          this.emailTemplateForm = this.formBuilder.group({
          	'temname': ['', Validators.required],
      			'temsub': ['', Validators.required],
      			'tembody': this.defaultTemplate,
            'template':false,
            'temtypeid': ['', null],
            'mapusers' : this._FB.array([
            ]),
          });
	        this.emailTemplates = data['emailTemplate'];
	        this.collectionSize = data['emailTemplate'].length;
	      }
        else{
          this.emailTemplates = [];
          this.collectionSize = 0;
        }
	    },
	    error => {
	    });
	}

	editet(id){
	    this.emailtemplateservice.getEmailTempById(id).then(data => {
  			console.log(data['emailTemplate']);
        if(data['status'] == 'success'){
          this.emailTemplateForm = this.formBuilder.group({
          	'temname': ['', Validators.required],
      			'temsub': ['', Validators.required],
      			'tembody': this.defaultTemplate,
            'template':false,
            'temtypeid': ['', null],
            'mapusers' : this._FB.array([
            ]),
          });
    			this.emailTemplateForm.controls['temname'].setValue(data['emailTemplate'][0].name);
    	    this.emailTemplateForm.controls['temsub'].setValue(data['emailTemplate'][0].subject);
    	    this.emailTemplateForm.controls['tembody'].setValue(data['emailTemplate'][0].body);
    			this.editEmailTemDetails.id = data['emailTemplate'][0].id;
    			// this.editEmailTemDetails.name = data['emailTemplate'][0].name;
    			// this.editEmailTemDetails.subject = data['emailTemplate'][0].subject;
    			// this.editEmailTemDetails.body = data['emailTemplate'][0].body;
    			if(data['emailTemplate'][0].type == "news"){
    				this.emailTemplateForm.controls['template'].setValue(true);
    			}
    			else{
    				this.emailTemplateForm.controls['template'].setValue(false);
    			}
          this.emailTemplateForm.controls['temtypeid'].setValue(data['emailTemplate'][0].typeId);
          if(data['emailTemplate'][0].mapUsers != '' && data['emailTemplate'][0].mapUsers != null){
            let mapUsers = data['emailTemplate'][0].mapUsers;
            //console.log(mapUsers.length);
            for(let i= 0; i< mapUsers.length; i++){
              this.addSetConditionEdit(mapUsers[i]);
            }
          }
    			this.new = true;
    			this.showTable = false;
    			this.showEdit = true;
    			this.success = false;
    			this.error = false;
          this.typeIdExsits = false;
        }
	    },
	    error => {
	    });
	}

	deleteet(id){
        this.emailtemplateservice.deleteEmailTempById(id).then(data => {
	        if(data['status'] == "success"){
	        	this.showTable = true;
		        this.new = true;
	        	this.error = false;
            this.typeIdExsits = false;
	            this.success = false;
	            this.snotifyService.success('Deleted Successfully', '', this.getConfig());
		        this.emailtemplateservice.getEmailTempTimeStamp(new Date().getTime()).then(data => {
			        console.log(data['emailTemplate']);
			        if(data['status'] == 'success'){
			        	this.emailTemplates = data['emailTemplate'];
			        	this.collectionSize = data['emailTemplate'].length;
			         }
               else{
     	        	this.emailTemplates = [];
     	        	this.collectionSize = 0;
               }
		  	    },
		  	    error => {
		  	    });
	        }
	    },
	    error => {
	    });
	}

	addEmailTemplate(){
		this.emailTemDetails = {
			name: this.f.temname.value,
			subject: this.f.temsub.value,
			body: this.f.tembody.value,
			type: this.f.template.value.toString(),
      typeId: this.f.temtypeid.value,
      mapUsers: this.f.mapusers.value
		}
		this.emailtemplateservice.addEmailTemp(this.emailTemDetails).then(data => {
			if(data['status']){
		        this.showTable = true;
		        this.new = true;
	        	this.error = false;
            this.typeIdExsits = false;
            this.success = true;
            this.snotifyService.success('Saved Successfully', '', this.getConfig());
            this.emailTemplateForm.reset();
			this.emailTemplateForm = this.formBuilder.group({
            	'temname': ['', Validators.required],
        			'temsub': ['', Validators.required],
        			'tembody': this.defaultTemplate,
              'template':false,
              'temtypeid': ['', null],
              'mapusers' : this._FB.array([
              ]),
            }, {updateOn: 'blur'});
		        this.emailtemplateservice.getEmailTempTimeStamp(new Date().getTime()).then(data => {
			        console.log(data['emailTemplate']);
			        if(data['status'] == 'success'){
			        	this.emailTemplates = data['emailTemplate'];
			        	this.collectionSize = data['emailTemplate'].length;
			         }
               else{
     	        	this.emailTemplates = [];
     	        	this.collectionSize = 0;
               }
		  	    },
		  	    error => {
		  	    });
			}
			else{
		        this.showTable = false;
		        this.new = true;
	        	this.error = true;
	            this.success = false;
			}
		});
	}

	updateEmailTemplate(){
		this.emailTemDetails = {
			id: this.editEmailTemDetails.id,
			name: this.f.temname.value,
			subject: this.f.temsub.value,
			body: this.f.tembody.value,
			type: this.f.template.value.toString(),
      typeId: this.f.temtypeid.value,
      mapUsers: this.f.mapusers.value
		}
		this.emailtemplateservice.updateEmailTemp(this.emailTemDetails).then(data => {
			if(data['status']){
				this.showTable = true;
				this.showEdit = false;
				this.new = true;
      	this.error = false;
        this.typeIdExsits = false;
        this.success = false;
        this.snotifyService.success('Updated Successfully', '', this.getConfig());
        this.emailTemplateForm.reset();
		this.emailTemplateForm = this.formBuilder.group({
          'temname': ['', Validators.required],
          'temsub': ['', Validators.required],
          'tembody': this.defaultTemplate,
          'template':false,
          'temtypeid': ['', null],
          'mapusers' : this._FB.array([
          ]),
        });
        this.emailtemplateservice.getEmailTempTimeStamp(new Date().getTime()).then(data => {
	        console.log(data['emailTemplate']);
	        if(data['status'] == 'success'){
	        	this.emailTemplates = data['emailTemplate'];
	        	this.collectionSize = data['emailTemplate'].length;
	         }
           else{
 	        	this.emailTemplates = [];
 	        	this.collectionSize = 0;
           }
  	    });
			}
			else{
        this.showTable = false;
        this.new = true;
      	this.error = true;
        this.success = false;
				this.showEdit = true;
			}
		});
	}

	close(){
		this.success = false;
		this.error = false;
    this.typeIdExsits = false;
	}

  checkUniqueId(value){
    this.emailtemplateservice.getUniqueId(value).then(data => {
      if(data['status'] == 'success' && data['emailTemplate']){
        this.typeIdExsits = true;
      }
      else{
        this.typeIdExsits = false;
      }
    });
  }

	getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
  	}

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { VerifyNewOrganization } from '../../../shared/data/verifyNewOrganization.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {

  userId;
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  options = {
    types: ['geocode'],
    componentRestrictions: { country: 'AU' }
  };
  newAddress;
  organizationDetails = {
    'orgName': '',
    'orgAddress': '',
    'orgEmail': '',
    'orgPhone': ''
  };
  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  constructor(public verifyNewOrganization: VerifyNewOrganization, private snotifyService: SnotifyService, private router: Router, private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {this.userId = window.atob(params['id']);});
  }

  ngOnInit() {

  }

  public handleAddressChange(address){
    this.organizationDetails.orgAddress = address.formatted_address;
  }

  isNumberKey(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
    }
    return true;
  }

  onSubmit(){
    console.log(this.organizationDetails);
    this.verifyNewOrganization.saveOrganization(this.organizationDetails).then(data => {
      if(data['status'] == 'success'){
        let orgId = data['id'];
        this.verifyNewOrganization.updateUserOrganization(orgId, this.userId).then(data => {
          if(data['status'] == 'success'){
            this.snotifyService.success('Organization created successfully.', '', this.getConfig());
          }
        });
      }
    });
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
   }
}

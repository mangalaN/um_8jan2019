import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FeedbackService } from '../../../shared/data/feedback.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

@Component({
	selector: 'app-feedbacksubmit',
	templateUrl: './feedbacksubmit.component.html',
	styleUrls: ['./feedbacksubmit.component.scss']
})
export class FeedbacksubmitComponent implements OnInit {
	public form: Object;
	campaignData = [];
	formTitle ='';
	mydata = {
			id:0
		};
	showForm = true;


		timeout = 3000;
		position: SnotifyPosition = SnotifyPosition.centerTop;
		progressBar = true;
		closeClick = true;
		newTop = true;
		backdrop = -1;
		dockMax = 8;
		blockMax = 6;
		pauseHover = true;
		titleMaxLength = 15;
		bodyMaxLength = 80;

		multiSave = false;
		oi;
	constructor(
		public feedbackservice: FeedbackService,
		private snotifyService: SnotifyService,
		private router: Router,
		private route: ActivatedRoute) {
		this.route.queryParams.subscribe(params => {

			let decrpypt = atob(params['id']);
			let formId = parseInt(decrpypt)
			this.mydata.id = formId/53620000;

			this.oi = params['oi'];
		});
		let id =btoa(this.mydata.id.toString());

		//this.oi = "MV91JDBy";

		//let id =params['id'];

		this.feedbackservice.getCampaignById(id,this.oi).then(data => {
			//this.form = [];
			this.form = JSON.parse(data['campaign'][0].form_data);//)
			console.log(JSON.stringify(this.form));
			this.formTitle= data['campaign'][0].name;
			if(data['campaign'][0].tblmap){
				this.multiSave = true;
			}
			//this.campaignData =  JSON.parse(data['campaign'].form_data);
			//this.form = this.campaignData[0].form_data;
			//this.formTitle= this.campaignData[0].name;
		})


	}

	ngOnInit() {
	}

	onSubmit(submission: any) {
		let addressKey =[];
		let components = this.form['components'];
		for(let i =0; i< components.length; i++){
			console.log(JSON.stringify(submission));
			if(components[i]['type']=='address'){
				addressKey.push(components[i]['key']);
			}
		}

		if(addressKey.length >0){
			for(let i =0; i< addressKey.length; i++){
				submission['data'][addressKey[i]] = submission['data'][addressKey[i]]['formatted_address'];
			}
		}
		
		let mydata = {
			id: btoa(this.mydata.id.toString()),
			form_data: JSON.stringify(submission),
			multiSave: this.multiSave
		};



		this.feedbackservice.addSubmission(mydata,this.oi).then(data => {
        if(data['status'] == "true"){
        	this.showForm = false;
        	// this.snotifyService.success('Form Saved Successfullly', '', this.getConfig());
          // this.showSaveForm = false;
          // this.f.name.setValue('');
          // this.f.sdate.setValue('');
          // this.f.edate.setValue('');
          // this.f.description.setValue('');
          // this.form1 ={components: []};
         

          // this.newsLetter.name = '';
          // this.newsLetter.subject = '';
          // this.newsLetter.listid = '';
          // this.newsLetter.to = '';
          // this.newsLetter.cc = '';
          // this.newsLetter.bcc = '';
          // this.newsLetter.body = this.defaultTemplate;
          // this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
          //   if(data['status']){
          //     this.newsletterDetails = data['newsletter'];
          //     this.collectionSize = data['newsletter'].length;
          //     this.newsLetter.body = this.defaultTemplate;
          //   }
          // });
          //window.location.reload();
        }
      });
		//console.log(JSON.stringify(this.form));
    console.log(JSON.stringify(submission)); // This will print out the full submission from Form.io API.
  }

    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }

}

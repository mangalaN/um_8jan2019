import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { AuthenticationService } from '../../../shared/data/authentication.service';
import { SubscriptionService } from '../../../shared/data/subscription.service';
import { UserService } from '../../../shared/data/user.service';
import { OrganizationService } from '../../../shared/data/organization.service';

@Component({
  selector: 'app-org-register',
  templateUrl: './org-register.component.html',
  styleUrls: ['./org-register.component.scss']
})
export class OrgRegisterComponent implements OnInit {
	userdata;
	registerForm: FormGroup;
	registerForm1:FormGroup;
  subscriptionForm:FormGroup;
  public usernameerror: boolean = false;
  public emailerror: boolean = false;
  subscriptionList;
  planList;
  selectedSubcription;
  subscribed: boolean = false;
  newaddress;
  options = {
	    types: ['geocode'],
      componentRestrictions: { country: 'AU' }
  }
  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;
  submitted = false;

	constructor(private snotifyService: SnotifyService, private router: Router, public userservice: UserService, public organizationservice: OrganizationService, private formBuilder: FormBuilder, private route: ActivatedRoute, public authenticationService: AuthenticationService, public subscriptionService: SubscriptionService) {
		this.subscriptionService.getSubscription(new Date().getTime()).then(data => {
            this.subscriptionList = data['subscription'];
            console.log('Get Subscription list - ' + JSON.stringify(this.subscriptionList));
        });
        this.subscriptionService.getPlans(new Date().getTime()).then(data => {
        	this.planList = data['plan'];
            console.log('Get Subscription list - ' + JSON.stringify(data));
        });
	}

	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			uname: ['', Validators.required],
			inputPass: ['', Validators.required],
			fname: ['', Validators.required],
			lname: ['', Validators.required],
			email: ['', Validators.compose([Validators.email, Validators.required])],
			phone: ['', Validators.required]
		});
		this.registerForm1 = this.formBuilder.group({
			address1: ['', Validators.required]
		});
	}

	get f() { return this.registerForm.controls; }

	getId(id,type){
        this.subscribed = true;
        this.selectedSubcription = type;
    }

    handleAddressChange(address) {
       // Do some stuff
       this.newaddress = address.formatted_address;
    }

	onSubmit() {
  	this.userdata = {
			username: this.f.uname.value,
			password: this.f.inputPass.value,
			firstName: this.f.fname.value,
			lastName: this.f.lname.value,
			email: this.f.email.value,
			phone: this.f.phone.value,
			dob: '',
			genderRadios: '',
			address: this.registerForm1.controls.address1.value,
    	profileImage: '',
			sublist: '',
			role: '', //users groupid
      type: this.selectedSubcription,
      organization: ''
    }
    this.submitted = true;
  	this.authenticationService.addsimpleregister(this.userdata).then(data => {
      if(data['status'] == "success"){
        this.snotifyService.success('Registration was Successfull. Please verify your email to continue further with your membership.', '', this.getConfig());
        this.submitted = false;
        this.router.navigate(['/pages/login']);
      }
      else if(data['status'] == "Failed"){
        this.snotifyService.error('Try Registering after some time.', '', this.getConfig());
        this.submitted = false;
        //alert("Try Registering after some time");
      }
      else if(data['status'] == "Register Exits"){
        this.snotifyService.error('Registration Failed.', '', this.getConfig());
        this.submitted = false;
        //alert("Registration Failed")
        if(data['value'] == "Username Already Exits."){
          this.usernameerror = true;
          this.emailerror = false;
        }
        else{
          this.emailerror = true;
          this.usernameerror = false;
        }
      }
    },
    error => {
    });
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
  }
}

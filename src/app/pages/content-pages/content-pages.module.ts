import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArchwizardModule } from 'angular-archwizard';

import { ContentPagesRoutingModule } from "./content-pages-routing.module";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { ComingSoonPageComponent } from "./coming-soon/coming-soon-page.component";
import { ErrorPageComponent } from "./error/error-page.component";
import { ForgotPasswordPageComponent } from "./forgot-password/forgot-password-page.component";
import { LockScreenPageComponent } from "./lock-screen/lock-screen-page.component";
import { LoginPageComponent } from "./login/login-page.component";
import { MaintenancePageComponent } from "./maintenance/maintenance-page.component";
import { OrgRegisterComponent } from "./org-register/org-register.component";
import { RegisterPageComponent } from "./register/register-page.component";
import { FeedbacksubmitComponent } from './feedbacksubmit/feedbacksubmit.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { FormioModule } from 'angular-formio';
import { VerifyEmailComponent } from './verify-email/verify-email.component';


@NgModule({
    imports: [
        CommonModule,
        ContentPagesRoutingModule,
        GooglePlaceModule,
        FormsModule,
        ArchwizardModule,
        ReactiveFormsModule,
        Ng4LoadingSpinnerModule.forRoot(),
        FormioModule
    ],
    declarations: [
        ComingSoonPageComponent,
        ErrorPageComponent,
        ForgotPasswordPageComponent,
        LockScreenPageComponent,
        LoginPageComponent,
        MaintenancePageComponent,
        OrgRegisterComponent,
        RegisterPageComponent,
        FeedbacksubmitComponent,
        VerifyEmailComponent
    ]
})
export class ContentPagesModule { }

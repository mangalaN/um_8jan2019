import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import { AuthenticationService } from '../../../shared/data/authentication.service';
import { SubscriptionService } from '../../../shared/data/subscription.service';
import { UserService } from '../../../shared/data/user.service';
import { OrganizationService } from '../../../shared/data/organization.service';

@Component({
    selector: 'app-register-page',
    templateUrl: './register-page.component.html',
    styleUrls: ['./register-page.component.scss']
})

export class RegisterPageComponent {
    //@ViewChild('f') registerForm: NgForm;
    userdata;
    ageDate;
    usernameerror = false;
    emailerror = false;
    submitted =  false;
    gender = 'M';
    options = {
		types: ['geocode'],
		componentRestrictions: { country: 'AU' }
    }
    registerForm: FormGroup;
    subscriptionForm:FormGroup;
    subscriptionList;
    newaddress;
    selectedSubcription;
    subscribed: boolean = false;
    date;
    orgName = '';roleName = '';invUserData = '';
    public roles;
    public organizations;
    public organization: any;
    public rolename: any;
    emailRegister: boolean = false;
    roleN;
    public email: any;
    firstParam: string;
    allowRegister;

    constructor(private spinnerService: Ng4LoadingSpinnerService, private router: Router, public userservice: UserService, public organizationservice: OrganizationService, private formBuilder: FormBuilder, private route: ActivatedRoute, public authenticationService: AuthenticationService, public subscriptionService: SubscriptionService) {
    	this.route.paramMap.subscribe(params => {
          this.firstParam = this.route.snapshot.queryParamMap.get('q');
        });
        if(this.firstParam != '' && this.firstParam != null){
        this.authenticationService.getInvitedUserData(this.firstParam).then(data => {
            this.userservice.getUserByEmailAndUniId(data['organization'][0]['email'],this.firstParam).then(user => {
              if(user['status'] == 'invalid'){
                this.allowRegister = true;
              }
              else if(user['status'] == 'success'){
                this.allowRegister = false;
              }
            });
        });
        }
        else{
          this.allowRegister = true;
        }
        this.emailRegister = false;
        this.subscriptionService.getSubscription(new Date().getTime()).then(data => {
            this.subscriptionList = data['subscription'];
        });
        this.organizationservice.getOrganization(new Date().getTime()).then(data => {
          this.organizations = data['organization'];
        });
        this.userservice.getGroups().then(data => {
          this.roles = data['groups'];
        });
        this.authenticationService.getInvitedUserData(this.firstParam).then(data => {
          if(data['status'] == 'success'){
              this.emailRegister = true;
              this.email = data['organization'][0]['email'];
              this.invUserData = data['organization'][0];
              this.roleName = data['organization'][0]['role_id'];
              this.roleN = data['organization'][0]['role'];
              this.orgName = data['organization'][0]['org_id'];
              this.authenticationService.getRoleAndOrg(data['organization'][0]['role_id'], data['organization'][0]['org_id']).then(orgdata => {});
          }
        });
        /*this.route.paramMap.subscribe(params => {
            const firstParam: string = this.route.snapshot.queryParamMap.get('q');
            this.authenticationService.getInvitedUserData(firstParam).then(data => {
              if(data['status'] == 'success'){
                  this.emailRegister = true;
                  this.invUserData = data['organization'][0];
                  this.roleName = data['organization'][0]['role_id'];
                  this.roleN = data['organization'][0]['role'];
                  this.orgName = data['organization'][0]['org_id'];
                  this.authenticationService.getRoleAndOrg(data['organization'][0]['role_id'], data['organization'][0]['org_id']).then(orgdata => {});
              }
            });
        });*/
    }

	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			uname: ['', Validators.required],
			inputPass: ['', Validators.required],
			fname: ['', Validators.required],
			lname: ['', Validators.required],
			inputEmail: ['', Validators.compose([Validators.email, Validators.required])],
			phone: ['', Validators.required],
			dob: ['', Validators.required],
			genderRadios: 'F',
			address: ['', Validators.required],
            role: ['', Validators.required],
            organization: ['', Validators.required]
		});
	}

    get f() { return this.registerForm.controls; }

    getId(id,type){
        this.subscribed = true;
        this.selectedSubcription = type;
    }

    handleAddressChange(address) {
       // Do some stuff
       this.newaddress = address.formatted_address;
    }

    //  On submit click, reset field value
    onSubmit() {
      this.spinnerService.show();
      if(this.orgName != ''){
        this.organization = this.invUserData['org_id'];
      }
      else if(this.orgName == ''){
        this.organization = this.f.organization.value;
      }
      if(this.roleName != ''){
        this.rolename = this.invUserData['role_id'];
      }
      else if(this.roleName == ''){
        this.rolename = this.f.role.value;
      }
    	this.userdata = {
  			username: this.f.uname.value,
  			password: this.f.inputPass.value,
  			firstName: this.f.fname.value,
  			lastName: this.f.lname.value,
  			email: this.f.inputEmail.value,
  			phone: this.f.phone.value,
  			dob: this.f.dob.value,
  			genderRadios: this.f.genderRadios.value,
  			address: this.f.address.value,
        profileImage: '',
  			sublist: '',
  			role: this.rolename, //users groupid
        type: this.selectedSubcription,
        organization: this.organization
      }
    	this.authenticationService.addregister(this.userdata).then(data => {
            console.log(data);
            if(data['status'] == "success"){
              //alert("Register Done Successfully");
              //this.router.navigate(['/dashboard/charts']);
              // this.router.navigate(['/dashboard/charts']).then(nav => {
              //   this.spinnerService.hide();
              // });
              this.router.navigate(['/pages/login']);
            }
            else if(data['status'] == "Failed"){
              alert("Try Registering after some time");
            }
            else if(data['status'] == "Register Exits"){
                alert("Registration Failed")
				if(data['value'] == "Username Already Exits."){
					this.usernameerror = true;
					this.emailerror = false;
				}
				else{
					this.emailerror = true;
					this.usernameerror = false;
				}
            }
        },
        error => {
        	//this.spinner.hide();
        });
        //this.registerForm.reset();
    }

    onLogin() {
        this.router.navigate(['login'], { relativeTo: this.route.parent });
    }

    checkAge(){
      //convert date again to type Date
      const bdate = new Date(this.registerForm.controls['dob'].value);
      const timeDiff = Math.abs(Date.now() - bdate.getTime() );
      this.ageDate = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    }
}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { FormManagerIndexComponent, FormManagerService, FormManagerConfig } from 'angular-formio/manager';
import { FeedbackService } from '../../shared/data/feedback.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
//import { newsLetterService } from '../../shared/data/newsletter.service';
//import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-createform',
  templateUrl: './createform.component.html',
  styleUrls: ['./createform.component.scss']
})

//export class CreateformComponent extends FormManagerIndexComponent {
	export class CreateformComponent implements OnInit {
	public form1: Object = {
    components: []
  };	
  showSaveForm = false;
  saveCampaign: FormGroup;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  constructor(
    public feedbackservice: FeedbackService,
    private snotifyService: SnotifyService
    // public service: FormManagerService,
    // public route: ActivatedRoute,
    // public router: Router,
    // public config: FormManagerConfig
    ) { 
     //super(service, route, router, config);
     this.form1 = {components: []};
  }

  ngOnInit() {

    this.saveCampaign = new FormGroup({
      'name': new FormControl(null, [Validators.required]),
      'description': new FormControl(null, [Validators.required]),
      'sdate': new FormControl(null, [Validators.required]),
      'edate': new FormControl(null, [Validators.required]),
      /*'status': new FormControl(null, [Validators.required]),
      'user_id': new FormControl(null, null)*/
    });
  }

  get f() { return this.saveCampaign.controls; }

  saveToggle(){
     this.showSaveForm = true;
  }
  cancel(){
      this.showSaveForm = false;
      //this.form1 ={components: []};
      this.f.name.setValue('');
      this.f.sdate.setValue('');
      this.f.edate.setValue('');
      this.f.description.setValue('');
    }

  addcampaign(){

    let userId= localStorage.getItem('currentUserId');

        let mydata = {
          name: this.f.name.value,
          description:this.f.description.value,
          from_date:this.f.sdate.value,
          end_date:this.f.edate.value,
          status:0,
          user_id:userId,
          form_data: JSON.stringify(this.form1)
        };

        this.feedbackservice.addCampaign(mydata).then(data => {
        if(data['status']){
          this.showSaveForm = false;
          this.f.name.setValue('');
          this.f.sdate.setValue('');
          this.f.edate.setValue('');
          this.f.description.setValue('');
          this.form1 ={components: []};
          this.snotifyService.success('Survey Saved Successfullly', '', this.getConfig());

          // this.newsLetter.name = '';
          // this.newsLetter.subject = '';
          // this.newsLetter.listid = '';
          // this.newsLetter.to = '';
          // this.newsLetter.cc = '';
          // this.newsLetter.bcc = '';
          // this.newsLetter.body = this.defaultTemplate;
          // this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
          //   if(data['status']){
          //     this.newsletterDetails = data['newsletter'];
          //     this.collectionSize = data['newsletter'].length;
          //     this.newsLetter.body = this.defaultTemplate;
          //   }
          // });
          //window.location.reload();
        }
      });


        //  this.feedbackservice.addCampaign(mydata).then(data => {
        //     console.log(data);
        //     if(data['status'] == "success"){
        //       // this.router.navigate(['/dashboard/charts']).then(nav => {
        //       //   this.spinnerService.hide();
        //       // });
        //     }
        //     else{
        //       alert("failed");
        //     }
        // },
        // error => {
        //   //this.spinner.hide();
        // });


       //  this.feedbackservice.addCampaign(mydata)
       // .subscribe(
       //  r => {
       //    alert("Fom saved.");
       //      // if(r['status'] == 'success'){
       //      //     //this.router.navigate(['/apps/dashboards/analytics']);
       //      // }
            
       //     // this._location.go('apps/dashboards/analytics');
       //      //alert("hi");
       //    // if (r.token) {
       //    //  // this.customer.setToken(r.token);
       //    //  // this.router.navigateByUrl('/dashboard');
       //    // }
       //  },
       //  r => {
       //    alert(r.error.error);
       //  });

    console.log(JSON.stringify(this.form1));
  }

    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateformComponent } from "./createform/createform.component";
import { RenderformComponent } from "./renderform/renderform.component";
import { ChartsreportComponent } from "./chartsreport/chartsreport.component";
import {ManageformComponent} from "./manageform/manageform.component";
import {ViewsubmissionsComponent} from "./viewsubmissions/viewsubmissions.component";
import {DbmapingComponent} from "./dbmaping/dbmaping.component";
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'createform',
        component: CreateformComponent,
        data: {
          title: 'Create Feedback Form'
        }
      },
      {
        path: 'renderform',
        component: RenderformComponent,
        data: {
          title: 'Render Form'
        }
      },
      {
        path: 'formmanager',
        component: ManageformComponent,
        data: {
          title: 'Form Manager'
        }
      },
      {
        path: 'viewsubmissions',
        component: ViewsubmissionsComponent,
        data: {
          title: 'Submissions Data'
        }
      },
      {
        path: 'dbmaping',
        component: DbmapingComponent,
        data: {
          title: 'Assign columns'
        }
      },
      {
        path: 'chartsreport',
        component: ChartsreportComponent,
        data: {
          title: 'Charts'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeedbackRoutingModule { }

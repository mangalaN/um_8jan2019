import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FeedbackService } from '../../shared/data/feedback.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
//import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as jspdf from 'jspdf';
import 'jspdf-autotable';

@Component({
	selector: 'app-viewsubmissions',
	templateUrl: './viewsubmissions.component.html',
	styleUrls: ['./viewsubmissions.component.scss']
})



export class ViewsubmissionsComponent implements OnInit {
	mydata = {
		id:0
	};
	public form: Object;

	campaignheaders = [];

	campaignData = [];

	respdata =[];
	emptyMsg = false;

	formTitle ='';

	//EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
	//EXCEL_EXTENSION = '.xlsx';

	// data: any[] = [
	// {
	// 	"name": "Ethel Price",
	// 	"gender": "female",
	// 	"company": "Johnson, Johnson and Partners, LLC CMP DDC",
	// 	"age": 22
	// },
	// {
	// 	"name": "Claudine Neal",
	// 	"gender": "female",
	// 	"company": "Sealoud",
	// 	"age": 55
	// },
	// {
	// 	"name": "Beryl Rice",
	// 	"gender": "female",
	// 	"company": "Velity",
	// 	"age": 67
	// }
	// ];
	rows = [];
	loadingIndicator: boolean = true;
	reorderable: boolean = true;

	columns = [];
	customHeaders = [];
	dataFromServer = [];
	pdfheaders =[];


	headerLabels =[];


	 // @ViewChild(DatatableComponent) table: DatatableComponent;
	 constructor(
	 	public feedbackservice: FeedbackService,
	 	private snotifyService: SnotifyService,
	 	private router: Router,
	 	private route: ActivatedRoute) { 
	 	this.route.queryParams.subscribe(params => {

	 		let decrpypt = atob(params['id']);
	 		let formId = parseInt(decrpypt)
	 		this.mydata.id = formId/53620000;
	 	});

	 	let id =btoa(this.mydata.id.toString());	
	 	let orgId = localStorage.getItem('oi'); 		


		// this.customHeaders = {
  //   thead: ['CUSTOM NAME 1', 'SOME COOL NAME', 'ANOTHER NAME'], // the Column Name in table head.
  //   displayed: ['someFeild1', 'someFeild2', 'someFeild3'] // the data it should populate in table.
  // };

  
  // this.dataFromServer =
  //   [{
  //     'id': 20,
  //     'someFeild1': 'asdfasdf',
  //     'someFeild2': 'asdf',
  //     'someFeild3': 'asdfasdfasfasdfa',
  //     },
  //    {
  //     'id': 81,
  //     'someFeild1': 'aasdfsdf',
  //     'someFeild2': 'asasdfdf',
  //     'someFeild3': 'dfasfasdfa',
  //     }, 
  //   ];


 // this.customHeaders = ['someFeild1', 'someFeild2'];


			// this.dataFromServer =
			// [{
			// 	'id': 20,
			// 	'someFeild1': 'asdfasdf',
			// 	'someFeild2': 'asdf',
			// 	'someFeild3': 'asdfasdfasfasdfa',
			// },
			// {
			// 	'id': 81,
			// 	'someFeild1': 'aasdfsdf',
			// 	'someFeild2': 'asasdfdf',
			// 	'someFeild3': 'dfasfasdfa',
			// }, 
			// ];
			
			this.feedbackservice.getCampaignById(id,orgId).then(data => {
				this.form = JSON.parse(data['campaign'][0].form_data);
				let components = this.form['components'];

				//second api call 
			this.feedbackservice.getSubmissionsById(id).then(data => {

				if(data['status'] == "success"){

					this.respdata = data['subs'];
					if(this.respdata){

						for(let i =0; i< this.respdata.length; i++){

							if(i==0){

								this.emptyMsg = false;
								this.dataFromServer.length = 0;
								let subs = JSON.parse(this.respdata[0]['sub_data']);
								this.customHeaders = Object.keys(subs.data);
								this.pdfheaders =Object.keys(subs.data);
								this.customHeaders.sort();

								this.formTitle= this.respdata[0]['name'];

								for(let y =0; y< this.customHeaders.length; y++){
									for(let z =0; z< components.length; z++){
										if(components[z]['key']== this.customHeaders[y]){
											this.headerLabels.push(components[z]['label']);
										}
									}
								}
							}

							let myObj = JSON.parse(this.respdata[i]['sub_data']);
							let myData = myObj.data;

							Object.keys(myData).forEach(key => {
								if(typeof myData[key] == 'object'){
									let subObj = myData[key];
									console.log(JSON.stringify(subObj));
									let mystr =[];
									Object.keys(subObj).forEach(key1 => {
										if(subObj[key1] == true){
											mystr.push(key1);
										}
									})
									myData[key] =mystr.toString();
								}
							});

							//this.dataFromServer.push(myObj.data);
							this.dataFromServer.push(myData);

							// this.customHeaders.forEach((item,index) =>{
							// 	alert(item);
							// 	alert(typeof myObj[item);
							// })
						//this.dataFromServer.push(Object..values(myObj.data))
						//console.log(JSON.stringify(this.customHeaders));
						//console.log(JSON.stringify(this.dataFromServer));
					}
				}
			}
			if(data['status'] =="invalid"){
				this.emptyMsg = true;
			}
		})
			//this.campaignData= respdata




			



			// console.log(this.columns);

			//  this.rows = this.data;
			//this.form = [];
			//this.form = JSON.parse(data['campaign'][0].form_data);//)
			//this.formTitle= data['campaign'][0].name;
			//this.campaignData =  JSON.parse(data['campaign'].form_data);
			//this.form = this.campaignData[0].form_data;
			//this.formTitle= this.campaignData[0].name;



		// this.feedbackservice.getCampaignById(id).then(data => {
		// 	this.form = JSON.parse(data['campaign'][0].form_data);
		// 	let components = this.form['components'];


		// 	for(let i =0; i< components.length; i++){
		// 		if(components[i]['key']=='address'){

		// 		}

		// 		console.log(JSON.stringify(submission));
		// 		if(components[i]['type']=='address'){
		// 			addressKey.push(components[i]['key']);
		// 		}
		// 	}
		//})
			//this.formTitle= data['campaign'][0].name;
			//this.campaignData =  JSON.parse(data['campaign'].form_data);
			//this.form = this.campaignData[0].form_data;
			//this.formTitle= this.campaignData[0].name;
		})

	}

	// isObject(val) { 
	// 	alert(typeof val);
	// 	let myVal = val;
	// 	if(typeof val === 'object'){
	// 		myVal = myVal.toString();

	// 	}

	// 	return myVal; 
	// }

	isObject(val) { 
		if(typeof val === 'object'){
			return true;
		}
		return false; 
	}

	ngOnInit() {

	}


  // exportAsExcelFile(json: any[], excelFileName: string): void {
  // 	const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
  // 	const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
  // 	const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
  // 	this.saveAsExcelFile(excelBuffer, excelFileName);
  // }
  // saveAsExcelFile(buffer: any, fileName: string): void {
  // 	const data: Blob = new Blob([buffer], {type: this.EXCEL_TYPE});
  // 	FileSaver.saveAs(data, fileName + '_export_' + new  Date().getTime() + this.EXCEL_EXTENSION);
  // }

  ExportTOExcel(){
  //	this.exportAsExcelFile(this.dataFromServer, 'submissions');

  const workBook = XLSX.utils.book_new();
  const workSheet = XLSX.utils.json_to_sheet(this.dataFromServer);

  XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
  XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.xlsx');

}

ExportTOCsv(){
	const workBook = XLSX.utils.book_new();
	const workSheet = XLSX.utils.json_to_sheet(this.dataFromServer);
	const csv = XLSX.utils.sheet_to_csv(workSheet);

	XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
	XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.csv');
}

ExportToPdf(){
	var doc = new jspdf();
	var col = this.pdfheaders;
	var rows = [];
	this.dataFromServer.forEach(element => {   

		let row =[]
		for(var key in element){
			var temp = [element[key]];
			row.push(temp);
		}
		rows.push(row);

			// var temp = [element.loyalty, element.points_redeemed, element.user_id, element.remaining_points, element.redeemed_on, element.expiry_date];
			// rows.push(temp);
		}); 
	doc.autoTable(col, rows, { startY: 10 });    
	doc.save('renewal_'+ new Date().getTime() +'.pdf');
}




deleteByIdS(ids){
	//console.log(id); // this function gives the ID of deleted rows.. as an array
}
updateChanges(row){
       // console.log(row); // This return the row which is updated with the id.
   }

}

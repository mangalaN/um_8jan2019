import { Component, OnInit, ViewChild, ElementRef , Input, ViewEncapsulation} from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import { FeedbackService } from '../../shared/data/feedback.service';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-manageform',
  templateUrl: './manageform.component.html',
  styleUrls: ['./manageform.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ManageformComponent implements OnInit {
  tblData = [];
  collectionSize;
  totalList;

  timeout = 1000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;


  columns = [
    { name: 'Form Name' },
    { name: 'Start Date' },
    { name: 'End Date' },
    { name: 'Action' }
  ];

  public form: Object;
  formTitle;
  exturl;
  iframecode;
  modalTitle;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(
  	public feedbackservice: FeedbackService,
    private snotifyService: SnotifyService,
    private modalService: NgbModal,
    private http: HttpClient, public router: Router
    ) { 

  }

  ngOnInit() {

  	this.feedbackservice.getCampaignByOrg().then(data => {
  		if(data['campaign']){
  			this.tblData = data['campaign'];
  			this.collectionSize = data['campaign'].length;
  			this.totalList = data['campaign'].length;
  		}
  	},
  	error => {
  	});

  }
  memToggle(){
  	this.router.navigate(['/feedback/createform']);
  }

  encrypt(id){
  	return  btoa((id*1000*10*5362).toString());
  }

      editcampaign(id){

      	this.encrypt(id);
      	
      // this.membershipservice.getMembershipById(id).then(data => {
      //   if(data['status'] == 'success'){
      //       this.membershipForm.controls['membershipName'].setValue(data['membership'][0]['plans']);
      //       this.membershipForm.controls['textArea'].setValue(data['membership'][0]['description']);
      //       this.membershipForm.controls['price'].setValue(data['membership'][0]['price']);
      //       this.membershipForm.controls['duration'].setValue(data['membership'][0]['terms']);
      //       this.membershipForm.controls['durationIn'].setValue(data['membership'][0]['duration']);
      //       this.memLogo = data['membership'][0]['membershipIcon'];
      //       //this.membershipForm.controls['membershipIcon'].setValue(data['membership'][0]['membershipIcon']);
      //       //this.membershipIconLabel.nativeElement.innerHTML = data['membership'][0]['membershipIcon'];
      //       this.selectval = data['membership'][0]['duration'];
      //       this.memId = data['membership'][0]['id'];
      //       this.showMembership = true;
      //       this.editMem = true;
      //       this.memShowLogo = true;
      //       //console.log(this.membershipForm.controls['membershipIcon'].value);
      //       //this.router.navigate(['/membership']);
      //   }
      // });
    }

    viewSubmissions(id){
      let encrypt = this.encrypt(id);
      this.router.navigate(['/feedback/viewsubmissions'],{queryParams:{id:encrypt}});
    }

    viewcampaign(id){
    	this.encrypt(id);
    }

    deletecampaign(id){
      this.feedbackservice.deleteCampaign(id).then(data => {
        if(data['status']){
        	this.snotifyService.error('Deleted Successfullly', '', this.getConfig());

        	//alert(index);

        	this.tblData = this.tblData.filter(h => h.id != id);
        	this.collectionSize = this.tblData.length;
        	this.totalList =  this.tblData.length;
        	// this.feedbackservice.getCampaignByOrg().then(data => {
        	// 	if(data['campaign']){
        	// 		this.tblData = data['campaign'];
        	// 		this.collectionSize = data['campaign'].length;
        	// 		this.totalList = data['campaign'].length;
        	// 	}
        	// },
        	// error => {
        	// });

       }
      });
    }

    copyToClipboard(str){
    	const el = document.createElement('textarea');
    	el.value = str;
    	el.setAttribute('readonly', '');
    	el.style.position = 'absolute';
    	el.style.left = '-9999px';
    	document.body.appendChild(el);
    	el.select();
    	document.execCommand('copy');
    	document.body.removeChild(el);
    }

    openurl(id){
    	let encrpypt = btoa((id*1000*10*5362).toString());
      let oi = localStorage.getItem('oi'); 
    	this.copyToClipboard("http://localhost:4200/#/pages/feedbacksubmit?id="+encrpypt+"&oi="+oi);

    	this.snotifyService.info('Copied Successfullly', '', this.getConfig());

    	//this.router.navigate(['/pages/feedbacksubmit'],{queryParams:{id:encrpypt}});

    	// const copyToClipboard = str => {
    	// 	const el = document.createElement('textarea');
    	// 	el.value = str;
    	// 	el.setAttribute('readonly', '');
    	// 	el.style.position = 'absolute';
    	// 	el.style.left = '-9999px';
    	// 	document.body.appendChild(el);
    	// 	el.select();
    	// 	document.execCommand('copy');
    	// 	document.body.removeChild(el);
    	// };
    }

    open(formdata,content,title) {
    	this.formTitle = title;
    	//this.form = JSON.parse(formdata);
      this.form =formdata;
        this.modalService.open(content).result.then((result) => {
           // this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }


    // openContent() {
    //     const modalRef = this.modalService.open(NgbdModalContent);
    //     modalRef.componentInstance.name = 'World';
    // }

    openurlModal(contenturl,id,name) {

      let encrpypt = btoa((id*1000*10*5362).toString());
      this.modalTitle= name;
      //this.copyToClipboard("http://localhost:4200/#/pages/feedbacksubmit?id="+encrpypt);


      //let domain = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port : '');
      //this.exturl ="http://localhost:4200/#/pages/feedbacksubmit?id="+encrpypt;
      //this.exturl =domain+"/#/pages/feedbacksubmit?id="+encrpypt;

      let fullUrl = location.href;
      let splitUrl = fullUrl.split("#");

      let oi = localStorage.getItem('oi'); 

      this.exturl = splitUrl[0]+"#/pages/feedbacksubmit?id="+encrpypt+"&oi="+oi;




      this.iframecode='<iframe src="'+this.exturl+'" scrolling="yes" id="zgfm-iframe-5" frameborder="0" style="border:none;width:100%;min-height:100px;height: 100%;position: absolute;" allowTransparency="true"></iframe>';
        this.modalService.open(contenturl).result.then((result) => {
            //this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
           // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }


    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }

}

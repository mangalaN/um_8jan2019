import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl,FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
//import { , FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FeedbackService } from '../../shared/data/feedback.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import * as chartsData from '../../shared/configs/ngx-charts.config';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
//import { barChartSingle, barChartmulti, pieChartSingle, pieChartmulti, lineChartSingle, lineChartMulti, areaChartSingle, areaChartMulti } from '../../shared/data/ngxChart';

@Component({
  selector: 'app-chartsreport',
  templateUrl: './chartsreport.component.html',
  styleUrls: ['./chartsreport.component.scss']
})
export class ChartsreportComponent implements OnInit {

	 regularForm: FormGroup;

    //Chart Data
    // lineChartMulti = lineChartMulti;
    // areaChartMulti = areaChartMulti;
    // pieChartSingle = pieChartSingle;
    // barChartSingle =barChartSingle;

    // barChartmulti = barChartmulti;
    barChartSingle =[];
    //Bar Charts
    barChartView: any[] = chartsData.barChartView;

    // options
    barChartShowYAxis = chartsData.barChartShowYAxis;
    barChartShowXAxis = chartsData.barChartShowXAxis;
    barChartGradient = chartsData.barChartGradient;
    barChartShowLegend = chartsData.barChartShowLegend;
    barChartShowXAxisLabel = chartsData.barChartShowXAxisLabel;
    //barChartXAxisLabel = chartsData.barChartXAxisLabel;
    barChartShowYAxisLabel = chartsData.barChartShowYAxisLabel;
    //barChartYAxisLabel = chartsData.barChartYAxisLabel;
    barChartColorScheme = chartsData.barChartColorScheme;

    barChartXAxisLabel = 'Options';
    barChartYAxisLabel = 'Count';

//   constructor() { 
//   	this.barChartSingle = [
//   {
//     "name": "Germany",
//     "value": 1000
//   },
//   {
//     "name": "USA",
//     "value": 200
//   },
//   {
//     "name": "France",
//     "value": 111
//   },
//   {
//     "name": "Australia",
//     "value": 650
//   }
// ];

//   }
	public form: FormGroup;
	formsData=[];
	formsDataSelected =[];

	timeout = 3000;
	position: SnotifyPosition = SnotifyPosition.centerTop;
	progressBar = true;
	closeClick = true;
	newTop = true;
	backdrop = -1;
	dockMax = 8;
	blockMax = 6;
	pauseHover = true;
	titleMaxLength = 15;
	bodyMaxLength = 80;

	submissions =[];

	formatedData =[];
	formsDataSelectedAllControls =[];

	showSaveForm =false;


	// 	data ={"lineArea6": {
	// 	"labels": [
	// 	'fgh',
	// 	'twfgo',
	// 	'gfh'
	// 	],
	// 	"series": [
	// 	[
	// 	10,
	// 	20,
	// 	30
	// 	]
	// 	]
	// }};

	formIdSelected;
	qSelected;

  chartLabels={"name":'',"xaxis":'',"yaxis":''};

  	constructor(public feedbackservice: FeedbackService,
		private snotifyService: SnotifyService,
    private modalService: NgbModal,
		private _FB: FormBuilder) { }


  ngOnInit() {
  	this.feedbackservice.getCampaignByOrg().then(data => {
  		if(data['campaign']){
  			this.formsData = data['campaign'];

  			console.log("ngOnInit"+JSON.stringify(this.formsData));
  		//to fill default values
  		//this.formDataSelectedFull =this.formsData[0];
  		//let inputTypes =["radio","select"];

  		//this.formsDataSelected =this.formsData[0]['form_data']['components'].filter(val => (val.key !='submit' && (val.type == 'radio' || val.type == 'select')));
  	}
  },
  error => {
  });


  	// this.regularForm = new FormGroup({
  	// 	'name': new FormControl(null, null),
  	// 	'chartType': new FormControl(null, null),
  	// 	//'data': new FormControl(null, null),
  	// 	'xaxsis': new FormControl(null, null),
  	// 	'yaxsis': new FormControl(null, null),
  	// 	//'columns': new FormControl(null, null),
  	// 	'daterange': new FormControl(null, null),
  	// 	//'newscolumns': new FormControl(null, null),
  	// 	//'xdata': new FormControl(null, null)
  	// });

  	this.regularForm = this._FB.group({
  		name:  ['', Validators.required],
  		chartType:  ['', Validators.required],
  		//'data':  ['', Validators.required],
  		xaxsis:  ['', Validators.required],
  		yaxsis:  ['', Validators.required],
  		//'columns':  ['', Validators.required],
  		daterange:  ['', Validators.required],
  		formNames: ['', Validators.required],
  		tblNames : ['', Validators.required],
  		tblmap : this._FB.array([
  			this.initMapFields()
  			])
  	});


  	this.form = this._FB.group({
  		formNames : ['', Validators.required],
  		fieldKey : ['', Validators.required],
  		//sdate : ['', Validators.required],
  		sdate :[],
  		edate : [],
  		//edate : ['', Validators.required],
  		tblmap : this._FB.array([
  			//this.initMapFields()
  			])
  	});

  }

  	onSelectFormName(index){

   	//this.formsDataSelected = this.formsData[index]['form_data']['components'].filter(val => val.key !='submit');
   	this.formsDataSelected = this.formsData[index]['form_data']['components'].filter(val => (val.key !='submit' && (val.type == 'radio' || val.type == 'select' || val.type == 'selectboxes')));
   	this.formsDataSelectedAllControls =this.formsData[index]['form_data']['components'].filter(val => (val.key !='submit'));

   	//this.formsDataSelected =this.formsData[index]['form_data']['components'].filter( this.inputTypes.indexOf((val => val.type).toString()) > -1);
   	this.formIdSelected = this.formsData[index]['id'];

   	//let id =btoa(this.formsData[index]['id'].toString());
   	//this.getSubmissions(id);
   }

   //onSelectQuestion(keyVal){
    submissionDatatoGraph(keyVal){
   	this.qSelected =keyVal;

   	let labelsObj =[];
   	let valuesAll =[];
   	let labels =[];
   	let valuesCount =[];

   	for(let i =0; i< this.formsDataSelected.length; i++){
   		if(this.formsDataSelected[i]['key'] == keyVal){
   			console.log("control type"+this.formsDataSelected[i]['type']);
   			if(this.formsDataSelected[i]['type'] == 'radio' || this.formsDataSelected[i]['type'] == 'selectboxes'){
   				labelsObj.push(this.formsDataSelected[i].values);
   				//labels.push(this.formsDataSelected[i].values[0][i]);	
   			}
   			if(this.formsDataSelected[i]['type'] == 'select'){
   				labelsObj.push(this.formsDataSelected[i]['data'].values);	
   			}
   		}
   		
   	}



   	//let subs = JSON.parse(this.submissions['sub_data']);
   	for(let i =0; i< this.submissions.length; i++){
   		let myObj = JSON.parse(this.submissions[i]['sub_data']);
   		let myData = myObj.data;
   		//if(this.formsDataSelected[i]['type'] == 'radio' || this.formsDataSelected[i]['type'] == 'select'){
         Object.keys(myData).forEach(key => {
           if(key == keyVal){
   				//for checkbox list multi options exist   				
   				if(typeof(myData[key]) =='object'){
   					console.log(JSON.stringify(myData[key]));
   					Object.keys(myData[key]).forEach(objkey => {
   						//alert(objkey);
   						if(myData[key][objkey] == true){
   							valuesAll.push(objkey);
   						}

   					})
   				}
   				else{
   					valuesAll.push(myData[key]);
   				}
   			}
   		})
   	//}
   	// else if( this.formsDataSelected[i]['type'] == 'selectboxes'){

   	// }
   }

   let counts = {};

   for (let i = 0; i < valuesAll.length; i++) {
     let num = valuesAll[i];
     counts[num] = counts[num] ? counts[num] + 1 : 1;
   }

   for(let i =0; i< labelsObj[0].length; i++){

     labels.push(labelsObj[0][i].label);

     let count =counts[labelsObj[0][i].value];
     if(!count){
       count = 0;
     }
   		// if(count){
   		// 	valuesCount.push(count);
   		// }
   		// else{
   		// 	valuesCount.push(0);
   		// }
   		if(i==0){
   			this.formatedData =[];
   		}

   		this.formatedData.push({
   			"name": labelsObj[0][i].label,
   			"value": count
   		})
   	}

   	//this.data.lineArea6.labels = labels;
   //	this.data.lineArea6.series[0] = valuesCount;


//console.log(JSON.stringify(this.barChartSingle));

this.barChartSingle =this.formatedData;

//   	this.barChartSingle = [
//   {
//     "name": "Germany",
//     "value": 1000
//   },
//   {
//     "name": "USA",
//     "value": 200
//   },
//   {
//     "name": "France",
//     "value": 111
//   },
//   {
//     "name": "Australia",
//     "value": 650
//   }
// ];
console.log(JSON.stringify(this.barChartSingle));

}

	// getSubmissions(id) {
	// 	this.feedbackservice.getSubmissionsById(id).then(data => {
	// 		//alert(JSON.stringify(data));
	// 		console.log("getSubmissions" + JSON.stringify(data));
	// 		if (data['status'] == "success") {
	// 			this.submissions = data['subs'];
	// 			//console.log("submissions"+JSON.stringify(this.submissions));
	// 		}
	// 	})
	// }

  addtoDashboard(dashboardmodal){
        this.modalService.open(dashboardmodal).result.then((result) => {
            //this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
           // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
  }
  saveDashboardSubmit(c){

//{"formNames":"11","fieldKey":"radio2","sdate":"2019-01-31","edate":"2019-02-15","tblmap":[{"formCol":"textField2","seloperator":"3","txtfilterval":"aaa"}]}
let userId= localStorage.getItem('currentUserId');
        let chartDetail = {
        uid: userId, //login userId
        name: this.chartLabels.name,
        chartType: 'bar',
        //data: this.regularForm.controls['data'].value,
        xaxsis: this.chartLabels.xaxis,
        yaxsis: this.chartLabels.yaxis,
        //columns: (this.userData) ? this.regularForm.controls['columns'].value : '',
        //daterange:  this.regularForm.controls['daterange'].value,
        fromDate:  this.form.value.sdate,
        toDate:   this.form.value.edate,
        filters: JSON.stringify(this.form.value.tblmap),
        formId:this.formIdSelected,
        qkey:this.form.value.fieldKey
        //newsletterColumns: this.newsletterSelectedColumn,
        //xdata: (this.userData) ? this.regularForm.controls['xdata'].value : '',
        //subTableName: this.regularForm.controls['newscolumns'].value,
        //mainRecordId: (!this.userData) ? this.regularForm.controls['columns'].value : ''
    }
        this.feedbackservice.saveDashboardChart(chartDetail).then(data => {
        if(data['status'] == 'true'){
          c('hjk');
          this.snotifyService.success('Chart Saved Successfullly.', '', this.getConfig());
          this.form.reset();
        }
      });
    
  }

	savetoDashboard(){
		this.showSaveForm =true;
	}
	cancel(){
		this.showSaveForm =false;
		this.form.reset();
    //this.form.tblmap.length =0;
	}

	/*onReactiveFormSubmit(){

		console.log(JSON.stringify(this.regularForm));

		let chartDetail = {
        uid: 1, //login userId
        name: this.regularForm.controls['name'].value,
        chartType: this.regularForm.controls['chartType'].value,
        //data: this.regularForm.controls['data'].value,
        xaxsis: this.regularForm.controls['xaxsis'].value,
        yaxsis: this.regularForm.controls['yaxsis'].value,
        //columns: (this.userData) ? this.regularForm.controls['columns'].value : '',
        daterange:  this.regularForm.controls['daterange'].value,
        formId:this.formIdSelected,
        qkey:this.qSelected
        //newsletterColumns: this.newsletterSelectedColumn,
        //xdata: (this.userData) ? this.regularForm.controls['xdata'].value : '',
        //subTableName: this.regularForm.controls['newscolumns'].value,
        //mainRecordId: (!this.userData) ? this.regularForm.controls['columns'].value : ''
    }
        this.feedbackservice.saveDashboardChart(chartDetail).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('Chart Saved Successfullly.', '', this.getConfig());
          this.regularForm.reset();
          this.showSaveForm =false;
          // this.chartService.getCharts(1, new Date().getTime()).then(data => {
          //   if(data['status'] == 'success'){
          //     this.charts = data['charts'];
          //     this.chartsCount = data['charts'].length;
          //     setTimeout(() => { this.loadingIndicator = false; }, 1500);
          //   }
          // });
        }
      });
	}*/



   // initMapFields() : FormGroup
   // {
   //    return this._FB.group({
   //       formCol 		: ['', Validators.required],
   //       seloperator 	: ['', Validators.required],
   //       txtfilterval 	: ['', Validators.required]        
   //    });
   // }
   initMapFields() : FormGroup
   {
      return this._FB.group({
         formCol 		: [],
         seloperator 	: [],
         txtfilterval 	: []        
      });
   }

//operatorloop=[];
//valuesArray =[[],[{"label":"one","value":"1","shortcut":""},{"label":"two","value":"2","shortcut":""},{"label":"three","value":"3","shortcut":""}]];
valuesArray =[];
operatorArray=[];
selectedformcol ='';
onSelectFields(val,i){

	let selindex =0;
	this.formsDataSelectedAllControls.forEach((ctrl,index) => {
		if(val == ctrl.key){
			selindex = index;
		}
	})


	//this.selectedformcol = this.formsDataSelectedAllControls[val];
	//console.log("red"+JSON.stringify(val));

	let selectedField = this.formsDataSelectedAllControls[selindex];
	//console.log("sasa"+i);

	if(selectedField['type'] =='textfield'){
		this.operatorArray[i]=[{"opvalue":"3","optext":"equal to"}];
		//this.valuesArray[i] =[];

		this.valuesArray[i]={};
		this.valuesArray[i].type="textfield";
		this.valuesArray[i].values =[];

		//this.valuesArray[i] = [{"type":"textfield"}];
	}
	if(selectedField['type'] =='number'){
    //doit later 
		this.operatorArray[i]=[
		//{"opvalue":"1", "optext":"less than or equal to"},
		//{"opvalue":"2",  "optext":"less than"},
		{"opvalue":"3",  "optext":"equal to"},
		//{"opvalue":"4",  "optext":"greater than"},
		//{"opvalue":"5", "optext":"greater than or equal to"}
		];


		this.valuesArray[i]={};
		this.valuesArray[i].type="number";
		this.valuesArray[i].values =[];

		//this.valuesArray[i] = [{"type":"textfield"}];
	}

	if(selectedField['type'] =='radio'){
		this.operatorArray[i]=[{"opvalue":"3","optext":"equal to"}];

		this.valuesArray[i]={};
		this.valuesArray[i].type="radio";
		this.valuesArray[i].values =  this.formsDataSelectedAllControls[selindex]['values'];
	}
	if(selectedField['type'] =='select'){
		this.operatorArray[i]=[{"opvalue":"3","optext":"equal to"}];

		this.valuesArray[i]={};
		this.valuesArray[i].type="select";
		this.valuesArray[i].values =  this.formsDataSelectedAllControls[selindex]['data']['values'];
	}	
	if(selectedField['type'] =='selectboxes'){
		this.operatorArray[i]=[{"opvalue":"3","optext":"equal to"}];
		this.valuesArray[i]={};
		this.valuesArray[i].type="selectboxes";
		this.valuesArray[i].values =  this.formsDataSelectedAllControls[selindex]['values'];
	}
	console.log(JSON.stringify(this.operatorArray));
	console.log(JSON.stringify(this.valuesArray));
}


   //for form

   manage(val : any) : void
   {
   	let id =btoa(this.formIdSelected.toString());
   	this.feedbackservice.getSubmissionsByIdFilter(id,val).then(data => {
   	console.log("getSubmissions" + JSON.stringify(data));
   	if (data['status'] == "success") {

   		this.submissions = data['subs'];

   		/*let allSubs = data['subs'];
   		//submissionloop this.submissions
   		this.submissions =[];
   		for(let i =0; i< allSubs.length; i++){
   			let myObj = JSON.parse(allSubs[i]['sub_data']);
   			let myData = myObj.data;

   			Object.keys(myData).forEach(key => {
   				for(let j=0; j<val['tblmap'].length;j++){
   					if(key == val['tblmap'][j]['formCol'] ){
   						if(myData[key] == val['tblmap'][j]['txtfilterval']){
   							this.submissions.push(allSubs[i]);
   						}
   					}
   				}
   			})
   		}*/
   	}
   		this.submissionDatatoGraph(val.fieldKey);
	})
     console.log("aaa"+JSON.stringify(val));
     console.log("bbb"+JSON.stringify(this.formsDataSelectedAllControls));
   }

	addNewInputFields() : void
	{
		const control = <FormArray>this.form.controls.tblmap;
		control.push(this.initMapFields());
		this.valuesArray.push([]);
		this.operatorArray.push([]);
	}
	removeInputFields(i : number) : void
	{
		const control = <FormArray>this.form.controls.tblmap;
		control.removeAt(i);
		this.valuesArray.splice(i, 1);
		this.operatorArray.splice(i, 1);
	}

	//for form
	// addNewInputField() : void
	// {
	// 	const control = <FormArray>this.regularForm.controls.tblmap;
	// 	control.push(this.initMapFields());
	// }
	// removeInputField(i : number) : void
	// {
	// 	const control = <FormArray>this.regularForm.controls.tblmap;
	// 	control.removeAt(i);
	// }

	getConfig(): SnotifyToastConfig {
		this.snotifyService.setDefaults({
			global: {
				newOnTop: this.newTop,
				maxAtPosition: this.blockMax,
				maxOnScreen: this.dockMax,
			}
		});
		return {
			bodyMaxLength: this.bodyMaxLength,
			titleMaxLength: this.titleMaxLength,
			backdrop: this.backdrop,
			position: this.position,
			timeout: this.timeout,
			showProgressBar: this.progressBar,
			closeOnClick: this.closeClick,
			pauseOnHover: this.pauseHover
		};
	}

}

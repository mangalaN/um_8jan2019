import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FeedbackService } from '../../shared/data/feedback.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist";
//const data: any = require('../../shared/data/chartist.json');

export interface Chart {
	type: ChartType;
	data: Chartist.IChartistData;
	options?: any;
	responsiveOptions?: any;
	events?: ChartEvent;
}

@Component({
	selector: 'app-renderform',
	templateUrl: './renderform.component.html',
	styleUrls: ['./renderform.component.scss']
})
export class RenderformComponent implements OnInit {

	public form: FormGroup;
	formsData=[];
	formsDataSelected =[];

	timeout = 3000;
	position: SnotifyPosition = SnotifyPosition.centerTop;
	progressBar = true;
	closeClick = true;
	newTop = true;
	backdrop = -1;
	dockMax = 8;
	blockMax = 6;
	pauseHover = true;
	titleMaxLength = 15;
	bodyMaxLength = 80;
	data ={"lineArea6": {
		"labels": [
		'fgh',
		'twfgo',
		'gfh'
		],
		"series": [
		[
		10,
		20,
		30
		]
		]
	}};



	submissions =[];

	constructor(public feedbackservice: FeedbackService,
		private snotifyService: SnotifyService) { }

	ngOnInit() {
		this.feedbackservice.getCampaignByOrg().then(data => {
			if(data['campaign']){
				this.formsData = data['campaign'];

				console.log("ngOnInit"+JSON.stringify(this.formsData));
  		//to fill default values
  		//this.formDataSelectedFull =this.formsData[0];
  		//let inputTypes =["radio","select"];

  		this.formsDataSelected =this.formsData[0]['form_data']['components'].filter(val => (val.key !='submit' && (val.type == 'radio' || val.type == 'select')));
  	}
  },
  error => {
  });
	}

	onSelectFormName(index){

   	//this.formsDataSelected = this.formsData[index]['form_data']['components'].filter(val => val.key !='submit');
   	this.formsDataSelected = this.formsData[index]['form_data']['components'].filter(val => (val.key !='submit' && (val.type == 'radio' || val.type == 'select')));
   	

   	//this.formsDataSelected =this.formsData[index]['form_data']['components'].filter( this.inputTypes.indexOf((val => val.type).toString()) > -1);

   	let id =btoa(this.formsData[index]['id'].toString());
   	this.getSubmissions(id);
   }

   onSelectQuestion(keyVal){
   	let labelsObj =[];
   	let valuesAll =[];
   	let labels =[];
   	let valuesCount =[];
   	

   	for(let i =0; i< this.formsDataSelected.length; i++){
   		if(this.formsDataSelected[i]['key'] == keyVal){
   			if(this.formsDataSelected[i]['type'] == 'radio'){
   				labelsObj.push(this.formsDataSelected[i].values);
   				//labels.push(this.formsDataSelected[i].values[0][i]);	
   			}
   			if(this.formsDataSelected[i]['type'] == 'select'){
   				labelsObj.push(this.formsDataSelected[i]['data'].values);	
   			}
   		}
   		
   	}



   	//let subs = JSON.parse(this.submissions['sub_data']);
   	for(let i =0; i< this.submissions.length; i++){
   		let myObj = JSON.parse(this.submissions[i]['sub_data']);
   		let myData = myObj.data;
   		Object.keys(myData).forEach(key => {
   			if(key == keyVal){
   				valuesAll.push(myData[key]);
   			}
   		})
   	}


   	let counts = {};

   	for (let i = 0; i < valuesAll.length; i++) {
   		let num = valuesAll[i];
   		counts[num] = counts[num] ? counts[num] + 1 : 1;
   	}

   	for(let i =0; i< labelsObj[0].length; i++){

   		labels.push(labelsObj[0][i].label);

   		let count =counts[labelsObj[0][i].value];
   		if(count){
   			valuesCount.push(count);
   		}
   		else{
   			valuesCount.push(0);
   		}
   	}

   	this.data.lineArea6.labels = labels;
   	this.data.lineArea6.series[0] = valuesCount;




   	
    // Line with Area Chart 6 Starts
    this.lineArea6 = {
    	type: 'Line',
    	data: this.data['lineArea6'],
    	options: {
    		low: 0,
    		fullWidth: true,
    		showArea: true,
    		onlyInteger: true,
    		axisY: {
    			low: 0,
    			scaleMinSpace: 50,
    		},
    		axisX: {
    			showGrid: false
    		},
        // lineSmooth: Chartist.Interpolation.simple({
        //     divisor: 4
        // }),
    },
    events: {
    	created(data: any): void {
    		var defs = data.svg.elem('defs');
    		defs.elem('linearGradient', {
    			id: 'linear6',
    			x1: 0,
    			y1: 1,
    			x2: 0,
    			y2: 0
    		}).elem('stop', {
    			offset: 0,
    			'stop-color': 'rgba(45,121,255,1)'
    		}).parent().elem('stop', {
    			offset: 1,
    			'stop-color': 'rgba(249,81,255, 1)'
    		});

    		defs.elem('linearGradient', {
    			id: 'gradient6',
    			x1: 0,
    			y1: 0,
    			x2: 1,
    			y2: 0
    		}).elem('stop', {
    			offset: 0.2,
    			'stop-color': 'rgba(200,220,255, 1)'
    		}).parent().elem('stop', {
    			offset: 1,
    			'stop-color': 'rgba(247,250,255, 1)'
    		});
    	},
    	draw(data: any): void {
    		
    		var circleRadius = 10;
    		if (data.type === 'point') {
    			var circle = new Chartist.Svg('circle', {
    				cx: data.x,
    				cy: data.y,
    				r: circleRadius,
    				class: data.value.y === 15 ? 'ct-point-circle' : 'ct-point-circle-transperent'
    			});
    			data.element.replace(circle);
    		}
    	}

    },
};
// Line with Area Chart 6 Ends


   	console.log("labelsObj"+JSON.stringify(labelsObj));
   	console.log("labels"+JSON.stringify(labels));
   	console.log("valuesAll"+JSON.stringify(valuesAll));
   	console.log("valuesCount"+JSON.stringify(valuesCount));

   }


   getSubmissions(id){
   	this.feedbackservice.getSubmissionsById(id).then(data => {
				//alert(JSON.stringify(data));
				console.log("getSubmissions"+JSON.stringify(data));

				if(data['status'] == "success"){
					this.submissions = data['subs'];
				 	//console.log("submissions"+JSON.stringify(this.submissions));
				 }

			// 		this.respdata = data['subs'];
			// 		if(this.respdata){

			// 			for(let i =0; i< this.respdata.length; i++){

			// 				if(i==0){

			// 					this.emptyMsg = false;

			// 					this.dataFromServer.length = 0;
			// 					let subs = JSON.parse(this.respdata[0]['sub_data']);
			// 					this.customHeaders = Object.keys(subs.data);
			// 					this.pdfheaders =Object.keys(subs.data);
			// 					this.customHeaders.sort();
			// 					this.formTitle= this.respdata[0]['name'];
			// 				}

			// 				let myObj = JSON.parse(this.respdata[i]['sub_data']);

			// 				let myData = myObj.data;

			// 				Object.keys(myData).forEach(key => {
			// 					if(typeof myData[key] == 'object'){
			// 						let subObj = myData[key];
			// 						console.log(JSON.stringify(subObj));
			// 					let mystr =[];
			// 						Object.keys(subObj).forEach(key1 => {
			// 							if(subObj[key1] == true){
			// 								mystr.push(key1);
			// 							}
			// 						})
			// 						myData[key] =mystr.toString();
			// 					}
			// 				});

			// 				this.dataFromServer.push(myData);
			// 		}
			// 	}
			// }
			// if(data['status'] =="invalid"){
			// 	this.emptyMsg = true;
			// }
		})


   }


    // Line with Area Chart 6 Starts
    lineArea6: Chart = {
    	type: 'Line',
    	data: this.data['lineArea6'],
    	options: {
    		low: 0,
    		fullWidth: true,
    		showArea: true,
    		onlyInteger: true,
    		axisY: {
    			low: 0,
    			scaleMinSpace: 50,
    		},
    		axisX: {
    			showGrid: false
    		},
        // lineSmooth: Chartist.Interpolation.simple({
        //     divisor: 4
        // }),
    },
    events: {
    	created(data: any): void {
    		var defs = data.svg.elem('defs');
    		defs.elem('linearGradient', {
    			id: 'linear6',
    			x1: 0,
    			y1: 1,
    			x2: 0,
    			y2: 0
    		}).elem('stop', {
    			offset: 0,
    			'stop-color': 'rgba(45,121,255,1)'
    		}).parent().elem('stop', {
    			offset: 1,
    			'stop-color': 'rgba(249,81,255, 1)'
    		});

    		defs.elem('linearGradient', {
    			id: 'gradient6',
    			x1: 0,
    			y1: 0,
    			x2: 1,
    			y2: 0
    		}).elem('stop', {
    			offset: 0.2,
    			'stop-color': 'rgba(200,220,255, 1)'
    		}).parent().elem('stop', {
    			offset: 1,
    			'stop-color': 'rgba(247,250,255, 1)'
    		});
    	},
    	draw(data: any): void {

    		var circleRadius = 10;
    		if (data.type === 'point') {
    			var circle = new Chartist.Svg('circle', {
    				cx: data.x,
    				cy: data.y,
    				r: circleRadius,
    				class: data.value.y === 15 ? 'ct-point-circle' : 'ct-point-circle-transperent'
    			});
    			data.element.replace(circle);
    		}
    	}

    },
};
// Line with Area Chart 6 Ends

}

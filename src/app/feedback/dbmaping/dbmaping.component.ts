import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FeedbackService } from '../../shared/data/feedback.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

@Component({
  selector: 'app-dbmaping',
  templateUrl: './dbmaping.component.html',
  styleUrls: ['./dbmaping.component.scss']
})
export class DbmapingComponent implements OnInit {

extraFields=[];
tableValues;
result;
//public dbmapingForm: FormGroup;
public form: FormGroup;
formsData=[];

//formsDataSelected ={"id":"","name":"","form_data":{"components":[]},"org_id":"","userid":""};
formsDataSelected =[];
formDataSelectedFull=[];
tblDataSelected =[];
formid;
tblName;


  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  constructor(  	
  	public feedbackservice: FeedbackService,
    private snotifyService: SnotifyService,
     public router: Router,
    private _FB: FormBuilder) { 

  this.feedbackservice.getTablewithColumns().then(data => {
      this.feedbackservice.getDynamicFields().then(data => {
        this.extraFields = data['table'];
      });
      if(data['status'] == "success"){
        for(var i = 0; i < this.extraFields.length; i++){
          data['table'].push(this.extraFields[i]);
        }
        this.tableValues = data['table'];
        var groups = new Set(this.tableValues.map(item => item.table_name));
        this.result = [];
        groups.forEach(g => {
            this.result.push({
              name: g, 
              values: this.tableValues.filter(i => i.table_name === g)
            })
        })

        //to fill default values
        console.log(this.result);
        console.log(typeof(this.result));
       // this.tblDataSelectedFull =this.result[0];
  		this.tblDataSelected =this.result[0]['values'];
      }
    });


  this.feedbackservice.getCampaignByOrg().then(data => {
  	if(data['campaign']){
  		this.formsData = data['campaign'];
  		//console.log(JSON.stringify(this.formsData));
  		//to fill default values
  		this.formDataSelectedFull =this.formsData[0];
  		this.formsDataSelected =this.formsData[0]['form_data']['components'].filter(val => val.key !='submit');
  	}
  },
  error => {
  });

}

  ngOnInit() {

  	// this.dbmapingForm = this._fb.group({
  	// 	columnmap: this._fb.array([
  	// 		this.initcolumnmap(),
  	// 		])
  	// });

  	 this.form = this._FB.group({
         formNames       	  : ['', Validators.required],
         tblNames       	  : ['', Validators.required],
         tblmap     : this._FB.array([
            this.initMapFields()
         ])
      });

  }

//   initcolumnmap() : FormGroup
//   {
//   	return this._fb.group({
//   		name : ['', Validators.required]
//   	});
//   }

// addNewInputField() : void
// {
//    const control = <FormArray>this.dbmapingForm.controls.tblcol;
//    control.push(this.initcolumnmap());
// }

// removeInputField(i : number) : void
// {
//    const control = <FormArray>this.dbmapingForm.controls.tblmap;
//    control.removeAt(i);
// }

 /**
    * Generates a FormGroup object with input field validation rules for
    * the tblmap form object
    *
    * @public
    * @method initMapFields
    * @return {FormGroup}
    */
   initMapFields() : FormGroup
   {
      return this._FB.group({
         formCol 		: ['', Validators.required],
          tblCol 		: ['', Validators.required],

      });
   }



   /**
    * Programmatically generates a new technology input field
    *
    * @public
    * @method addNewInputField
    * @return {none}
    */
   addNewInputField() : void
   {
      const control = <FormArray>this.form.controls.tblmap;
      control.push(this.initMapFields());
   }



   /**
    * Programmatically removes a recently generated technology input field
    *
    * @public
    * @method removeInputField
    * @param i    {number}      The position of the object in the array that needs to removed
    * @return {none}
    */
   removeInputField(i : number) : void
   {
      const control = <FormArray>this.form.controls.tblmap;
      control.removeAt(i);
   }



   /**
    * Receive the submitted form data
    *
    * @public
    * @method manage
    * @param val    {object}      The posted form data
    * @return {none}
    */
   manage(val : any) : void
   {
   	 //const optGroupLabel = ev.target.options[selectedIndex].parentNode.getAttribute('label');
      
      //console.log(typeof(val)); 
      //alert(val['tblmap'][0].formCol.split("::")[1]);

     // let formid = val['tblmap'][0].formCol.split("::")[1];

      console.log("aaa"+JSON.stringify(this.tblDataSelected));
       let formid = this.formDataSelectedFull['id'];
       //alert(formid);
      val.formid=formid;
      val.tblname = this.tblDataSelected[val.tblNames]['table_name'];
      let myval= JSON.stringify(val);

     //  console.log(JSON.stringify(val));
     this.feedbackservice.updateTblMap(formid,myval).then(data => {  
     	this.snotifyService.success('Saved Successfullly', '', this.getConfig());
       this.router.navigate(['/feedback//formmanager']);
     })
   }

   onSelectFormName(index){
   	this.formDataSelectedFull =this.formsData[index];
   	this.formsDataSelected = this.formsData[index]['form_data']['components'].filter(val => val.key !='submit');
   	//alert(val);
   	console.log(JSON.stringify(this.formsDataSelected));
   }
   onSelectTblName(index){

   	//Object.keys(this.result[index]).map(i => this.tblDataSelected[i]);
   	//this.tblDataSelected=[];
   	//this.tblDataSelected = this.result[index];
   	//this.tblDataSelectedFull = this.result[index];
   	this.tblDataSelected = this.result[index]['values'];
   	//console.log(JSON.stringify(this.tblDataSelected[0]['table_name']));
   }

   cancel(){
     this.form.reset();
   }


    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }

}

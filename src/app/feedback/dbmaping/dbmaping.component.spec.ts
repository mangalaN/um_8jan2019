import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DbmapingComponent } from './dbmaping.component';

describe('DbmapingComponent', () => {
  let component: DbmapingComponent;
  let fixture: ComponentFixture<DbmapingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DbmapingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DbmapingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FeedbackRoutingModule } from "./feedback-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';


// import { ReportsComponent } from "./reports/reports.component";
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

import { FormioModule } from 'angular-formio';
import { FormioGrid } from 'angular-formio/grid';
import { FormManagerModule, FormManagerRoutes, FormManagerService, FormManagerConfig } from 'angular-formio/manager';

import { CreateformComponent } from "./createform/createform.component";
import { RenderformComponent } from './renderform/renderform.component';
import { ManageformComponent } from './manageform/manageform.component';
import { ViewsubmissionsComponent } from './viewsubmissions/viewsubmissions.component';
//import { JSONTableModule } from 'angular-json-table';
import { DbmapingComponent } from './dbmaping/dbmaping.component';
import { ChartistModule } from 'ng-chartist';
import { ChartsreportComponent } from './chartsreport/chartsreport.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
    imports: [
        CommonModule,
        FeedbackRoutingModule,
        Ng2SmartTableModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NgxDatatableModule,
        NgxPaginationModule,
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        FormioModule,
        FormioGrid,
        FormManagerModule,
        //JSONTableModule,
        ChartistModule,
        NgxChartsModule
    ],
    declarations: [
        CreateformComponent,
        RenderformComponent,
        ManageformComponent,
        ViewsubmissionsComponent,
        DbmapingComponent,
        ChartsreportComponent
    ],
    providers: [
    // FormManagerService,
    // {provide: FormManagerConfig, useValue: {
    //   tag: 'common'
    // }}
  ]
})
export class FeedbackModule { }

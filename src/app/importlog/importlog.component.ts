import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { ImportLogService } from '../shared/data/importlog.service';
import { UserService } from '../shared/data/user.service';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Router, ActivatedRoute } from '@angular/router';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { NgbDateStruct, NgbDatepickerI18n, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

@Component({
  selector: 'app-importlog',
  templateUrl: './importlog.component.html',
  styleUrls: ['./importlog.component.scss']
})
export class ImportLogComponent implements OnInit {
    listData: any;
    listData1: any;
    schedule=[];
    public showTable = true;
    public showForm: boolean = false;
    public submitted: boolean = false;
    public save: any;
    public status;
    importlog = {
        'id': '',
        'name': '',
        'email': '',
        'phonenum': '',
        'address': '',
        'newsletter': ''
    };
    options = {
        types: ['geocode'],
        componentRestrictions: { country: 'AU' }
    }
    p: number = 1;
    totalList;
    totalList1;
    previousPage: any;
    pageSize: number = 10;
    collectionSize;
    collectionSize1;
    newaddress;
    listForm: FormGroup;
    users: any;
    rows = [];
    count =[];
    fromDt;
    toDt;
    // columns = [
    //   { prop: 'firstName' },
    //   { prop: 'lastName' },
    //   { prop: 'email' },
    //   { prop: 'address'},
    //   { prop: 'listName' },
    //   { prop: 'subscribedOn'},
    //   { prop: 'subscribedStatus' },
    //   { prop: 'Subscribed' }
    // ];
    style = 'material';
    title = 'Snotify title!';
    body = 'Lorem ipsum dolor sit amet!';
    timeout = 3000;
    position: SnotifyPosition = SnotifyPosition.centerTop;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;

    @ViewChild(DatatableComponent) mydatatable: DatatableComponent;
    @ViewChild("placesRef") placesRef : GooglePlaceDirective;

    constructor(private snotifyService: SnotifyService,public importlogservice: ImportLogService,public userservice: UserService, public router: Router, private route: ActivatedRoute) {
        //this.lists = new Array();
        if(localStorage.getItem("redirect") == "Y")
        {
          localStorage.setItem("redirect", "N");
          window.location.reload();
        }
    }

    ngOnInit() {
        this.listForm = new FormGroup({
            'fname': new FormControl(null, [Validators.required]),
            'address': new FormControl(null, [Validators.required]),
            'email': new FormControl(null, [Validators.required]),
            'phone': new FormControl(null, [Validators.required]),
            'newsletter': new FormControl(null,null)
        });
      this.getImportLogData();
    }
getImportLogData()
{
    this.importlogservice.getImportLog().then(data => {
        if(data['status'] == 'success'){
            this.listData = data['lists'];
            //alert(JSON.stringify(this.listData));
            for(let i = 0; i < this.listData.length; i++){
              let encodeJSON = JSON.parse(this.listData[i]['parameters']);
              //alert(encodeJSON);
              if(encodeJSON != null)
                this.listData[i]['parameters']= encodeJSON['link'];
            }
            this.collectionSize = data['lists'].length;
            this.totalList = data['lists'].length;
        }
    });
}
    public handleAddressChange(address) {
        // Do some stuff
        this.newaddress = address.formatted_address;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
          this.previousPage = page;
          //this.loadData();
        }
    }

    detail(task_id){
        this.showTable = false;
        this.showForm = true;
        this.save = true;

        this.importlogservice.getImportLogById(task_id).then(data1 => {
          //alert(JSON.stringify(data1));
          if(data1['status'] == 'success'){
              this.schedule = data1['lists'];
          }
        })

        this.importlogservice.getLogDetail(task_id).then(data => {
          if(data['status'] == 'success'){
              this.listData1 = data['lists'];
              //alert(JSON.stringify(this.listData));

              // for(let i = 0; i < this.listData1.length; i++){
              //   let encodeJSON = JSON.parse(this.listData1[i]['parameters']);
              //   this.listData1[i]['parameters']= encodeJSON['link'];
              // }
              this.collectionSize1 = data['lists'].length;
              this.totalList1 = data['lists'].length;
          }
        })
    }
    redirect(){
      //this.router.navigate(['/loyalty/transactions'],{queryParams:{id:window.btoa(id+"$reportEdit")}});
      this.router.navigate(['/configurations/import']);
    }
    edit(id){

        // this.importlogservice.getImportById(id).then(data => {
        //   this.rows = data['users'];
        // })
    }

    updateStatus(event, userid){
      let SubscriptionDetail = {
        id: userid
      }
      if(event){
        this.userservice.addSubscriptionUser(SubscriptionDetail).then(data => {
            this.snotifyService.success('Successfullly Subscribed', '', this.getConfig());
        });
      }
      else{
        this.userservice.unSubscriptionUser(SubscriptionDetail).then(data => {
            this.snotifyService.success('Successfullly Unsubscribed', '', this.getConfig());
        });
      }
    }

    cancel(){
        this.showTable = true;
        this.showForm = false;
        this.save = true;
        this.importlog.id = '';
        this.importlog.name = '';
        this.importlog.email = '';
        this.importlog.phonenum = '';
        this.importlog.address = '';
        this.importlog.newsletter = '';
	}

  reset()
  {
    this.getImportLogData();
    this.fromDt=null;
    this.toDt = null;
  }
  search()
  {
    if(this.fromDt == null && this.toDt == null)
    {
      //alert("Please Enter Start Date & End Date.");
      swal("Error!","Please Enter Start Date & End Date.","error");
    // this.snotifyService.success('Please Enter Start Date & End Date.', 'Error', this.getConfig());
      return;
    }

    let stDate=new Date().toISOString().slice(0,10);
    let endDate=new Date().toISOString().slice(0,10);
    let flag =false;
    if(this.fromDt != null)
    {
      stDate = this.fromDt['year']+'-'+this.fromDt['month']+'-'+this.fromDt['day'];
      flag = true;
    }
    else
    {
      stDate = "2019-1-1";
    }

    if(this.toDt != null)
    {
      endDate = this.toDt['year']+'-'+this.toDt['month']+'-'+this.toDt['day'];
      flag = true;
    }
    else
    {
      flag = false;
    }

    //this.popupModel.dp = stDate;
    if ((stDate > endDate) && flag == true)
    {
       //alert("End Date must be greater than Start Date.");
       swal("Error!","End Date must be greater than Start Date.","error");
        // this.snotifyService.success('End Date must be greater than Start Date.', 'Error', this.getConfig());
       return;
    }
     //let whereClause =  "active='Y' and (date(started_at)>='" + stDate + "' AND date(started_at)<='" + endDate + "')";
    let whereClause =  "(date(started_at)>='" + stDate + "' AND date(started_at)<='" + endDate + "')";

    //alert(whereClause);
    this.importlogservice.filter(whereClause).then(data => {
        if(data['status'] == 'success'){
            this.listData = data['lists'];
            //alert(JSON.stringify(this.listData));
            for(let i = 0; i < this.listData.length; i++){
              let encodeJSON = JSON.parse(this.listData[i]['parameters']);
              this.listData[i]['parameters']= encodeJSON['link'];
            }
            this.collectionSize = data['lists'].length;
            this.totalList = data['lists'].length;
        }
        else
        {
          this.listData=null;
          this.collectionSize = 0;
          this.totalList = 0;
        }
    });
  }
    delete(id){
        this.importlogservice.deleteById(id).then(data => {
          for (var i in this.listData) {
          if (this.listData[i].task_id === id) {
              this.listData.splice(i, 1);
            }
          }
          this.collectionSize = this.listData.length;
          this.totalList = this.listData.length;
          // alert('Deleted Successfully!');
          this.snotifyService.success('Deleted Successfully!', 'Success', this.getConfig());
        },
        error => {
        });
    }

    // list(){
    //     this.lists.address = this.newaddress;
    //     this.submitted = true;
    //     if(this.lists.id != ''){
    //         this.listsservice.addlist(this.lists, 'update').then(data => {
    //             if(data['status']){
    //                 this.snotifyService.success('Updated Successfullly', '', this.getConfig());
    //                 this.listData = [];
    //                 this.collectionSize = [];
    //                 this.totalList = [];
    //                 this.listsservice.getlist(new Date().getTime()).then(listdata => {
    //                     if (listdata['status'] == 'success') {
    //                         this.listData = listdata['lists'];
    //                         this.collectionSize = listdata['lists'].length;
    //                         this.totalList = listdata['lists'].length;
    //                         this.submitted = false;
    //                         this.showForm = false;
    //                         this.showTable = true;
    //                     }
    //                 });
    //             }
    //         });
    //     }
    //     else{
    //         this.listsservice.addlist(this.lists, 'add').then(data => {
    //             if(data['status']){
    //                 this.snotifyService.success('Saved Successfullly', '', this.getConfig());
    //                 this.listData = [];
    //                 this.collectionSize = [];
    //                 this.totalList = [];
    //                 this.listsservice.getlist(new Date().getTime()).then(listdata => {
    //                     if (listdata['status'] == 'success') {
    //                         this.listData = listdata['lists'];
    //                         this.collectionSize = listdata['lists'].length;
    //                         this.totalList = listdata['lists'].length;
    //                         this.submitted = false;
    //                         this.showForm = false;
    //                         this.showTable = true;
    //                     }
    //                 });
    //             }
    //         });
    //     }
    // }

    getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
    }
}

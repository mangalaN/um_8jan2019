import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImportLogComponent } from "./importlog.component";

const routes: Routes = [
    {
        // path: '',
        // children: [{
        //     path: 'cards',
        //     component: CardsComponent
        // }
        // ]
        path: '',
        component: ImportLogComponent,
       data: {
         title: 'ImportLog'
       },
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ImportLogRoutingModule { }

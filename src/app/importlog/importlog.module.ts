import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ImportLogRoutingModule } from "./importlog-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { ImportLogComponent } from "./importlog.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
    imports: [
        CommonModule,
        ImportLogRoutingModule,
        NgxChartsModule,
        NgbModule,
        MatchHeightModule,
        FormsModule,
        ReactiveFormsModule,
        NgxDatatableModule,
        NgxPaginationModule,
        GooglePlaceModule,
        UiSwitchModule
    ],
    declarations: [
        ImportLogComponent,
   ]
})
export class ImportLogModule { }

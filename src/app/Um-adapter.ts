import { ChatAdapter, IChatGroupAdapter, User, Group, Message, ChatParticipantStatus, ParticipantResponse, ParticipantMetadata, ChatParticipantType, IChatParticipant } from 'ng-chat';
import { Observable, of } from 'rxjs';
import { delay,map,catchError } from "rxjs/operators";
import { HttpClient, HttpHeaders } from '@angular/common/http';
//import {chatService} from './shared/data/chat.service';

export class UmAdapter extends ChatAdapter implements IChatGroupAdapter
//export class UmAdapter
{
    private apiUrl = 'https://click365.com.au/usermanagement/getChat.php?action=friends&userId=2&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime();
    //public  mockedParticipants: IChatParticipant[];
    // constructor(private http: HttpClient) {
    //         super();
    // };
   // public   mockedParticipants: IChatParticipant[] =[];
    public user = JSON.parse(localStorage.getItem('currentUser'));

    constructor(private http: HttpClient
        ) {
        super();

            // this.http.get('https://click365.com.au/usermanagement/getChat.php?action=friends&userId=2&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime())
            // .subscribe(data => {
            //     console.log(data);
            //     UmAdapter.mockedParticipants = data['data'];
            //     }, err => {
            //    console.log("vbn"+ JSON.stringify(err));
            // });

        //this.initializeConnection();
    }

    //alert(ChatParticipantStatus.Online);
    public static mockedParticipants: IChatParticipant[] = [
    {
        participantType: ChatParticipantType.User,
        id: 1,
        displayName: "Arya Stark",
        avatar: "https://66.media.tumblr.com/avatar_9dd9bb497b75_128.pnj",
        status: ChatParticipantStatus.Online
    },
    {
        participantType: ChatParticipantType.User,
        id: 2,
        displayName: "Cersei Lannister",
        avatar: null,
        status: ChatParticipantStatus.Online
    },
    {
        participantType: ChatParticipantType.User,
        id: 3,
        displayName: "Daenerys Targaryen",
        avatar: "https://68.media.tumblr.com/avatar_d28d7149f567_128.png",
        status: ChatParticipantStatus.Busy
    },
    {
        participantType: ChatParticipantType.User,
        id: 4,
        displayName: "Eddard Stark",
        avatar: "https://pbs.twimg.com/profile_images/600707945911844864/MNogF757_400x400.jpg",
        status: ChatParticipantStatus.Offline
    },
    {
        participantType: ChatParticipantType.User,
        id: 5,
        displayName: "Hodor",
        avatar: "https://pbs.twimg.com/profile_images/378800000449071678/27f2e27edd119a7133110f8635f2c130.jpeg",
        status: ChatParticipantStatus.Offline
    },
    {
        participantType: ChatParticipantType.User,
        id: 6,
        displayName: "Jaime Lannister",
        avatar: "https://pbs.twimg.com/profile_images/378800000243930208/4fa8efadb63777ead29046d822606a57.jpeg",
        status: ChatParticipantStatus.Busy
    },
    {
        participantType: ChatParticipantType.User,
        id: 7,
        displayName: "John Snow",
        avatar: "https://pbs.twimg.com/profile_images/3456602315/aad436e6fab77ef4098c7a5b86cac8e3.jpeg",
        status: ChatParticipantStatus.Busy
    },
    {
        participantType: ChatParticipantType.User,
        id: 8,
        displayName: "Lorde Petyr 'Littlefinger' Baelish",
        avatar: "http://68.media.tumblr.com/avatar_ba75cbb26da7_128.png",
        status: ChatParticipantStatus.Offline
    },
    {
        participantType: 0,
        id: 9,
        displayName: "Sansa Stark",
        avatar: "http://pm1.narvii.com/6201/dfe7ad75cd32130a5c844d58315cbca02fe5b804_128.jpg",
        status: ChatParticipantStatus.Online
    },
    {
        participantType: ChatParticipantType.User,
        id: 10,
        displayName: "Theon Greyjoy",
        avatar: "https://thumbnail.myheritageimages.com/502/323/78502323/000/000114_884889c3n33qfe004v5024_C_64x64C.jpg",
        status: ChatParticipantStatus.Away
    }];

    // listFriends(): Observable<ParticipantResponse[]> {
    //      return

    //         .pipe(
    //     map((res: any) => res.data),
    //     catchError((error: any) => Observable.throw(error.error || 'Server error'))
    //    )
    //     //});
    //   //   return this.http
    //   // .get(this.apiUrl)
    //   // .pipe(
    //   //   map((res: any) => res.data),
    //   //   catchError((error: any) => Observable.throw(error.error || 'Server error'))
    //   //  );
    // }



    listFriends(): Observable<ParticipantResponse[]> {
        console.log("asss"+localStorage.getItem('currentUser'));
        return this.http.get('https://click365.com.au/usermanagement/getChat.php?action=friends&userId='+this.user['id']+'&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime()).pipe(
            map((data: any[]) => data['data'].map(user => {
                {
                    let participantResponse = new ParticipantResponse();
                    participantResponse.participant = user;
                    participantResponse.metadata = {
                        totalUnreadMessages: Math.floor(Math.random() * 10)
                    }
                    return participantResponse;
                }
            })
        ),
    );

        // this.http.get('https://click365.com.au/usermanagement/getChat.php?action=friends&userId=2&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime())
        //     .subscribe(data => {
        //         console.log(data);
        //         UmAdapter.mockedParticipants = data['data'];

        //    return of(UmAdapter.mockedParticipants.map(user => {
        //     //let participantResponse = new ParticipantResponse();
        //     let participantResponse = new ParticipantResponse();
        //     participantResponse.participant = user;
        //     participantResponse.metadata = {
        //         totalUnreadMessages: Math.floor(Math.random() * 10)
        //     }
        //     console.log("azzza"+JSON.stringify(participantResponse));
        //     return participantResponse;
        //     }));
        //         }, err => {
        //        console.log("vbn"+ JSON.stringify(err));
        //     })

        // return of(UmAdapter.mockedParticipants.map(user => {
        //     let participantResponse = new ParticipantResponse();
        //     participantResponse.participant = user;
        //     participantResponse.metadata = {
        //         totalUnreadMessages: Math.floor(Math.random() * 10)
        //     }
        //     console.log("azzza"+JSON.stringify(participantResponse));

        //     return participantResponse;
        // }));
      // return of([])
    }

    getMessageHistory(destinataryId: any): Observable<Message[]> {
        let mockedHistory: Array<Message>;

        // mockedHistory = this.chatService.getMessages(2).then(data =>{

        // })

   //  let apiURL = 'https://click365.com.au/usermanagement/getChat.php?action=messages&userId='+2+'&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime();
   // //mockedHistory.push(
   //     return this.http.get(apiURL).pipe(
   //    map(res => {

   //        mockedHistory =res['data'];
   //      // return res['data'].map(item => {
   //      //     return item;
   //      //   // return new SearchItem(
   //      //   //   item.trackName,
   //      //   //   item.artistName,
   //      //   //   item.trackViewUrl,
   //      //   //   item.artworkUrl30,
   //      //   //   item.artistId
   //      //   // );
   //      // });
   //    })
   //  );
       //)
       //alert(destinataryId);

       const url = 'https://click365.com.au/usermanagement/getChat.php?action=messages&userId='+this.user['id']+'&friendId='+destinataryId+'&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime();
       return this.http.get(url).pipe(
           // Adapt each item in the raw data array
           map((data: any[]) => data['data'].map(item => {
               return item;
           })),
        );

        // mockedHistory = [
        //     {
        //         fromId: 1,
        //         toId: 999,
        //         message: "Hi there, just type any message bellow to test this Angular module.",
        //         dateSent: new Date()
        //     },
        //     {

        //         fromId: 999,
        //         toId: 1,
        //         message: "Hi ",
        //         dateSent: new Date()
        //     },
        //     {

        //         fromId: 1,
        //         toId: 999,
        //         message: "this",
        //         dateSent: new Date()
        //     }
        // ];

        return of(mockedHistory).pipe(delay(2000));
    }

    sendMessage(message: Message): void {

        console.log(message);


            this.http.post('https://click365.com.au/usermanagement/addChat.php?action=addmsg&userId='+this.user['id']+'&oi='+localStorage.getItem('oi')+'&q='+Date.now(), { data: message })
            .subscribe(data => {
                console.log(data);
                //resolve(data);
            }, err => {
                console.log("vbn"+err);
            });



        // setTimeout(() => {
        //     let replyMessage = new Message();

        //     //alert(message);

        //     replyMessage.message = "You have typed '" + message.message + "'";
        //     replyMessage.dateSent = new Date();

        //     if (isNaN(message.toId))
        //     {
        //         let group = UmAdapter.mockedParticipants.find(x => x.id == message.toId) as Group;

        //         // Message to a group. Pick up any participant for this
        //         let randomParticipantIndex = Math.floor(Math.random() * group.chattingTo.length);
        //         replyMessage.fromId = group.chattingTo[randomParticipantIndex].id;

        //         replyMessage.toId = message.toId;

        //         this.onMessageReceived(group, replyMessage);
        //     }
        //     else
        //     {
        //         replyMessage.fromId = message.toId;
        //         replyMessage.toId = message.fromId;

        //         let user = UmAdapter.mockedParticipants.find(x => x.id == replyMessage.fromId);

        //         this.onMessageReceived(user, replyMessage);
        //     }
        // }, 1000);
    }

    groupCreated(group: Group): void {
        UmAdapter.mockedParticipants.push(group);

        UmAdapter.mockedParticipants = UmAdapter.mockedParticipants.sort((first, second) =>
            second.displayName > first.displayName ? -1 : 1
        );

        // Trigger update of friends list
        this.listFriends().subscribe(response => {
            this.onFriendsListChanged(response);
        });
    }
}

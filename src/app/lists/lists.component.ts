import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Router, ActivatedRoute } from '@angular/router';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import swal from 'sweetalert2';


import { ListsService } from '../shared/data/lists.service';
import { UserService } from '../shared/data/user.service';
import { AddFieldsService } from '../shared/data/addFields.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit {
    listData: any;
    public showTable = true;
    public showForm: boolean = false;
    public submitted: boolean = false;
    public save: any;
    public status;
    lists = {
        'id': '',
        'name': '',
        'email': '',
        'phonenum': '',
        'address': '',
        'newsletter': '',
        'fields': '',
        // 'textValue': '',
        // 'dateValue': '',
        // 'numberValue': '',
        // 'floatValue': '',
        // 'toggleValue': ''
    };
    lists_extra = {};
    options = {
        types: ['geocode'],
        componentRestrictions: { country: 'AU' }
    }
    p: number = 1;
    totalList;
    previousPage: any;
    pageSize: number = 10;
    collectionSize; newaddress;
    //listForm: FormGroup;
    users: any;
    rows;
    count =[];
    
    style = 'material';
    title = 'Snotify title!';
    body = 'Lorem ipsum dolor sit amet!';
    timeout = 3000;
    position: SnotifyPosition = SnotifyPosition.centerTop;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;

    extraFields = false;
    FieldsValue = [];
    addFields = [];

    @ViewChild(DatatableComponent) mydatatable: DatatableComponent;
    @ViewChild("listForm") listForm: NgForm;
    @ViewChild("placesRef") placesRef : GooglePlaceDirective;
    
    columns = [
      { prop: 'firstName' },
      { prop: 'lastName' },
      { prop: 'email' },
      { prop: 'address'},
      { prop: 'listName' },
      { prop: 'subscribedOn'},
      { prop: 'subscribedStatus' },
      { prop: 'Subscribed' }
    ];
     columns1 = [
    { name: 'Name' },
    { name: 'Email' },
    { name: 'Phone'},
    { name: 'Address'}
    
    ];
  
  allColumns1 = [
    { name: 'Name' },
    { name: 'Email' },
    { name: 'Phone' },
    { name: 'Address'}
    
  ]; 
  
   isCollapsed = false;

    constructor(private snotifyService: SnotifyService, public addFieldsService: AddFieldsService, public listsservice: ListsService,public userservice: UserService, public router: Router, private route: ActivatedRoute) {
        //this.lists = new Array();
    }

    ngOnInit() {
        // this.listForm = new FormGroup({
        //     'fname': new FormControl(null, [Validators.required]),
        //     'address': new FormControl(null, [Validators.required]),
        //     'email': new FormControl(null, [Validators.required, Validators.email]),
        //     'phone': new FormControl(null, [Validators.required]),
        //     'newsletter': new FormControl(null,null)
        // });
        this.listsservice.getlist(new Date().getTime()).then(data => {
            if(data['status'] == 'success'){
                this.listData = data['lists'];
                this.collectionSize = data['lists'].length;
                this.totalList = data['lists'].length;
            }
        });
        this.addFieldsService.getFieldsByTable('list').then(data=>{
          if(data['status'] == 'success'){
            //addFields
            for(let i=0; i< data['fields'].length; i++){
              let fields = JSON.parse(data['fields'][i]['fields']);
                this.addFields.push({
                  'label': fields['label'],
            			'datatype':fields['datatype'],
            			'length':fields['length'],
            			'mandatory':fields['mandatory'],
            			'validation':fields['validation'],
            			'validationMsg':fields['validationMsg'],
            			'fieldType':fields['fieldType'],
                  'fieldName':fields['fieldName'],
                  'id':data['fields'][i]['id']
                });
            }
            this.extraFields = true;
          }
          else{
            this.addFields = [];
            this.extraFields = false;
          }
        });
    }

    public handleAddressChange(address) {
        // Do some stuff
        this.newaddress = address.formatted_address;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
          this.previousPage = page;
          //this.loadData();
        }
    }

    addlist(){
        this.showTable = false;
        this.showForm = true;
        this.save = true;
    }
    
     toggle(col) {
    const isChecked = this.isChecked(col);
    console.log('isChecked value - ' + isChecked);
    if(isChecked) {
      this.columns1 = this.columns1.filter(c => {
        console.log('Name - ' + c.name);
        return c.name !== col.name; 
      });
    } else {
      this.columns1 = [...this.columns1, col];
      console.log('columns1 - ' + this.columns1);
    }
  }

  isChecked(col) {
    return this.columns1.find(c => {
      return c.name === col.name;
    });
  }

    editlist(id){
      this.listsservice.getlistById(id).then(data => {
        this.save = false;
        this.showTable = false;
        this.showForm = false;
        this.lists.id = data['lists'][0].id;
        this.lists.name = data['lists'][0].name;
        this.lists.email = data['lists'][0].email;
        this.lists.phonenum = data['lists'][0].phone;
        this.lists.address = data['lists'][0].address; // this.newaddress; data['lists'][0].address;
        this.newaddress = data['lists'][0].address;
        this.lists.newsletter = data['lists'][0].newsletter;
        this.lists.fields = data['lists'][0].extraFieldsValue;
        if(data['lists'][0].extraFieldsValue != ''){
          this.FieldsValue = JSON.parse(data['lists'][0].extraFieldsValue);
          for(let j = 0; j < this.addFields.length; j++){
            for(let i = 0; i < this.FieldsValue.length; i++){
              if(this.addFields[j].id == this.FieldsValue[i].id){
                let modelName =  this.FieldsValue[i].fieldName+'_extra';
                this.addFields[j].valuesBind = this.FieldsValue[i].value;
                //if(this.addFields[j].fieldType == 'text'){
                  this.lists_extra[modelName] = this.FieldsValue[i].value;
                //}
                // if(this.addFields[j].fieldType == 'date'){
                //   this.lists_extra.this.FieldsValue[i].fieldName+'_extra' = this.FieldsValue[i].value;
                // }
                // if(this.addFields[j].fieldType == 'number'){
                //   this.lists_extra.this.FieldsValue[i].fieldName+'_extra' = this.FieldsValue[i].value;
                // }
                // if(this.addFields[j].fieldType == 'float'){
                //   this.lists_extra.this.FieldsValue[i].fieldName+'_extra' = this.FieldsValue[i].value;
                // }
                // if(this.addFields[j].fieldType == 'toggle'){
                // this.lists_extra.this.FieldsValue[i].fieldName+'_extra' = this.FieldsValue[i].value;
                // }
              }
            }
          }
        }
      },
      error => {
      });
      this.listsservice.getUsersByListId(id).then(data => {
        if(data['status'] == 'success'){
          this.rows = data['users'];
        }
        else{
          this.rows = '';
        }
      });
    }

    updateStatus(event, userid){
      let SubscriptionDetail = {
        id: userid,
        listId: this.lists.id
      }
      if(event){
        this.userservice.addSubscriptionUser(SubscriptionDetail).then(data => {
            this.snotifyService.success('Successfully Subscribed', '', this.getConfig());
        });
      }
      else{
        this.userservice.unSubscriptionUser(SubscriptionDetail).then(data => {
            this.snotifyService.success('Successfully Unsubscribed', '', this.getConfig());
        });
      }
    }

    cancel(){
      this.showTable = true;
      this.showForm = false;
      this.save = true;
      this.listForm.reset();
      this.lists.id = '';
      this.lists.name = '';
      this.lists.email = '';
      this.lists.phonenum = '';
      this.lists.address = '';
      this.lists.newsletter = '';
    }

    fieldValueChange(id, value, fieldName){
      if(this.FieldsValue.length > 0){
        let checkValue = false;
        for(let i = 0; i < this.FieldsValue.length; i++){
          if(this.FieldsValue[i].id == id){
            this.FieldsValue[i].value = value;
            this.FieldsValue[i].fieldName = fieldName;
            checkValue = true;
          }
        }
        if(!checkValue){
          this.FieldsValue.push({"id": id, "value": value, "fieldName": fieldName});
        }
      }
      else{
        this.FieldsValue.push({"id": id, "value": value, "fieldName": fieldName});
      }
      console.log('val -' +JSON.stringify(this.FieldsValue));
    }

    deletelist(id){
      let self = this;
      swal({
            title: '',
            html:
            ''+
            'Users Subscribed to the list will be deleted. Do you want to continue?',
            showCloseButton: true,
            confirmButtonText: 'Ok',
            allowOutsideClick: false,
            allowEscapeKey: false,
            showCancelButton: true,
        }).then(function (dismiss) {
            //console.log(JSON.stringify(dismiss));
            if (dismiss.value && dismiss !== 'cancel') {
              self.listsservice.deletelistById(id).then(data => {
                  self.snotifyService.success('Deleted Successfully', '', self.getConfig());
                  self.listData = [];
                  self.collectionSize = [];
                  self.totalList = [];
                  self.listsservice.getlist(new Date().getTime()).then(data => {
                      if(data['status'] == 'success'){
                          self.listData = data['lists'];
                          self.collectionSize = data['lists'].length;
                          self.totalList = data['lists'].length;
                      }
                  });
              },
              error => {
              });
            }
        });
    }

    list(){
      if(this.newaddress != ''){
        this.lists.address = this.newaddress;
      }
      console.log(this.lists);
      this.submitted = true;
      if(this.extraFields){
        this.lists.fields = JSON.stringify(this.FieldsValue);
      }
      else{
        this.lists.fields = '';
      }
      if(this.lists.id != ''){
        this.listsservice.addlist(this.lists, 'update').then(data => {
          if(data['status']){
            this.listForm.reset();
            // this.lists.id = '';
            // this.lists.name = '';
            // this.lists.email = '';
            // this.lists.phonenum = '';
            // this.lists.address = '';
            // this.lists.newsletter = '';
            this.snotifyService.success('Updated Successfully', '', this.getConfig());
            this.listData = [];
            this.collectionSize = [];
            this.totalList = [];
            this.listsservice.getlist(new Date().getTime()).then(listdata => {
              if (listdata['status'] == 'success') {
                  this.listData = listdata['lists'];
                  this.collectionSize = listdata['lists'].length;
                  this.totalList = listdata['lists'].length;
                  this.submitted = false;
                  this.showForm = false;
                  this.showTable = true;
              }
            });
          }
        });
      }
      else{
        this.listsservice.addlist(this.lists, 'add').then(data => {
          if(data['status']){
            this.listForm.reset();
            // this.lists.id = '';
            // this.lists.name = '';
            // this.lists.email = '';
            // this.lists.phonenum = '';
            // this.lists.address = '';
            // this.lists.newsletter = '';
            this.snotifyService.success('Saved Successfully', '', this.getConfig());
            this.listData = [];
            this.collectionSize = [];
            this.totalList = [];
            this.listsservice.getlist(new Date().getTime()).then(listdata => {
              if (listdata['status'] == 'success') {
                this.listData = listdata['lists'];
                this.collectionSize = listdata['lists'].length;
                this.totalList = listdata['lists'].length;
                this.submitted = false;
                this.showForm = false;
                this.showTable = true;
              }
            });
          }
        });
      }
    }

    isNumberKey(evt) {
      let charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
          return false;
      }
      return true;
    }

    getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
    }
}

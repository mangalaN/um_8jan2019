import { Component, OnInit, ViewChild } from '@angular/core';
import { OrganizationService } from '../shared/data/organization.service';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { Router, ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})

export class ReportComponent implements OnInit {
	organization;
	rows = [];
  count =[];
	columns = [
        { prop: 'organization' },
        { prop: 'noOfLists' },
        { prop: 'noOfMembership' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    list: any;

    constructor(public OrganizationService: OrganizationService, private router: Router, private route: ActivatedRoute) { }

  	ngOnInit() {
      this.OrganizationService.getListCount().then(data => {
        console.log('Test Data - ' + JSON.stringify(data));
        this.list = data['count'];
        for (let i = 0; i < data['count'].length; i++) {
          let array = {
            organization : data['count'][i]['id'],
            noOfLists : data['count'][i]['listcount'],
            noOfMembership : data['count'][i]['memcount']
          }
          this.count.push(array);
        }
      this.rows = this.count;
      });
  }

}

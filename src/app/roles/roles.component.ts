import { Component, OnInit } from '@angular/core';
import {UserService} from '../shared/data/user.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
groups;

  constructor(public userData: UserService) { }

  ngOnInit() {
  	this.userData.getGroups().then(data => {
        console.log(data['groups']);
        this.groups = data['groups'];
    },
    error => {
    });
  }

}

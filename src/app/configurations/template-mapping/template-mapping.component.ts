import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

import { TemplateMappingService } from '../../shared/data/template-mapping.service';

@Component({
  selector: 'app-template-mapping',
  templateUrl: './template-mapping.component.html',
  styleUrls: ['./template-mapping.component.scss']
})
export class TemplateMappingComponent implements OnInit {
  @ViewChild('templateMapping') templateMappingForm: NgForm;
  templates;
  templateMappingModel = {
    'id': '',
    'template': '',
    'emailType': '',
    'url': ''
  };
  isCollapsed = true;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;
  datachange = false;
  selectedTemplateUrl = '';
  htmlContent = '';
  temMappings = [];
  canvasdata = false;

  constructor(private snotifyService: SnotifyService, public templateMappingService: TemplateMappingService) {}

  ngOnInit() {
    this.templateMappingService.getNewsTemp().then(data => {
      if(data['status'] == 'success'){
        this.templates = data['emailTemplate'];
      }
    });
    this.templateMappingService.gettemplateMapping().then(data => {
      if(data['status'] == 'success'){
        this.temMappings = data['templateMapping'];
      }
    });
  }
  
  onChange(value){
    for(let i=0; i<this.temMappings.length; i++){
      if(value == this.temMappings[i]['emailType']){
        this.snotifyService.success('Template Mapping is done already, if you need to change the template edit the mapping from below table.', '', this.getConfig());
        this.templateMappingForm.reset();
      }
    }
  }

  onReactiveFormSubmit(){
    if(this.selectedTemplateUrl){
      this.templateMappingModel.url = this.selectedTemplateUrl;
    }
    this.templateMappingService.saveMapping(this.templateMappingModel).then(data => {
      if(data['status'] == 'success'){
        this.snotifyService.success('Template Mapping was Successfully.', '', this.getConfig());
        this.templateMappingForm.reset();
        this.templateMappingModel.id = '';
        this.isCollapsed = true;
        this.selectedTemplateUrl ='';
        document.getElementById('imgMessage').innerHTML = '';
        this.templateMappingService.gettemplateMapping().then(data => {
          if(data['status'] == 'success'){
            this.temMappings = data['templateMapping'];
          }
        });
      }
    });
  }

  cancel(){
    this.templateMappingForm.reset();
    this.templateMappingModel.id = '';
    this.isCollapsed = true;
    this.selectedTemplateUrl ='';
    document.getElementById('imgMessage').innerHTML = '';
    this.canvasdata = false;
  }

  deleteTemMapping(id){
    this.templateMappingService.deleteMapping(id).then(data => {
      if(data['status'] == 'success'){
        this.snotifyService.success('Template Mapping deleted Successfully.', '', this.getConfig());
        this.templateMappingService.gettemplateMapping().then(data => {
          if(data['status'] == 'success'){
            this.temMappings = data['templateMapping'];
          }
        });
      }
    });
  }

  editTemMapping(id){
    this.templateMappingService.editMapping(id).then(data => {
      if(data['status'] == 'success'){
        this.templateMappingModel.id = id;
        this.templateMappingModel.template = data['templateMapping'][0]['template'];
        this.templateMappingModel.emailType = data['templateMapping'][0]['emailType'];
        //this.templateMappingModel.url = data['templateMapping'][0]['url'];
        //this.selectedTemplateUrl = data['templateMapping'][0]['url'];
        this.isCollapsed = false;
      }
    });
  }

  updateTemMapping(){
    if(this.selectedTemplateUrl){
      this.templateMappingModel.url = this.selectedTemplateUrl;
    }
    this.templateMappingService.updateMapping(this.templateMappingModel).then(data => {
      if(data['status'] == 'success'){
        this.snotifyService.success('Template Mapping was updated Successfully.', '', this.getConfig());
        this.templateMappingForm.reset();
        this.templateMappingModel.id = '';
        this.isCollapsed = true;
        this.selectedTemplateUrl ='';
        this.templateMappingService.gettemplateMapping().then(data => {
          if(data['status'] == 'success'){
            this.temMappings = data['templateMapping'];
          }
        });
      }
    });
  }

  // getTemplateURL(url, id){
  //   return document.getElementById(id).apidpendChild(JSON.parse(url));
  // }

  selectTemplate(template){
    document.getElementById('imgMessage').innerHTML = '';
    this.canvasdata = true;
    this.templateMappingService.getTemplateHTML(template).then(dataReturn => {
        this.htmlContent = '';
        if(dataReturn['status'] == 'success'){
          this.htmlContent = dataReturn['emailTemplate'][0]['message'];
          document.getElementById('contentToConvert').innerHTML = this.htmlContent;
          var data = document.getElementById('contentToConvert');

          // html2canvas(data).then(canvas =>{
          //     const contentDataURL = canvas.toDataURL('image/png');
          //     this.selectedTemplateUrl = contentDataURL;
          // });

          html2canvas(document.querySelector('#contentToConvert'), {allowTaint: true, useCORS: true}).then((canvas) =>{
              document.getElementById('imgMessage').appendChild(canvas);
              // this.selectedTemplateUrl = (canvas.toString());
              // console.log(this.selectedTemplateUrl);
              this.htmlContent = '';
              this.canvasdata = false;
              // let pageData = canvas.toDataURL('image/jpeg', 1.0);
              // this.selectedTemplateUrl = pageData;
          });


          //html2canvas(document.getElementById('contentToConvert'), { letterRendering: 1, allowTaint : true, onrendered : function (canvas) { } });
          //html2canvas(data, {allowTaint: true, logging: true}).then(canvas => {
          // html2canvas(document.querySelector('#contentToConvert'), {useCORS: true}).then(canvas => {
          //   // Few necessary setting options crossorigin
          //   const contentDataURL = canvas.toDataURL('image/jpeg', 1.0);
          //   this.selectedTemplateUrl = contentDataURL;
          //   if(this.selectedTemplateUrl != ''){
          //     this.htmlContent = '';
          //   }
          //   // let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
          //   // var position = 0;
          //   // pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
          //   // pdf.save('MYPdf.pdf'); // Generated PDF
          // });
        }
        else{
          this.canvasdata = false;
        }
    },
    error => {
    });

  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
   }
}

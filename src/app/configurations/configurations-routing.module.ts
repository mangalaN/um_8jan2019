import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildChartComponent } from "./build-chart/build-chart.component";
import { TilesConfigComponent } from "./tiles-config/tiles-config.component";
import { SettingsComponent } from "./settings/settings.component";
import { AddExtraFieldsComponent } from "./add-extra-fields/add-extra-fields.component";
import { ImportComponent } from "./import/import.component";
import { TemplateMappingComponent } from "./template-mapping/template-mapping.component";
import { EventMappingComponent } from "./event-mapping/event-mapping.component";
import {click365webpushComponent } from "./click365webpush/click365webpush.component";


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'build-chart',
        component: BuildChartComponent,
        data: {
          title: 'build chart'
        }
      },
      {
        path: 'tiles-config',
        component: TilesConfigComponent,
        data: {
          title: 'tiles config'
        }
      },
      {
        path: 'settings',
        component: SettingsComponent,
        data: {
          title: 'Settings'
        }
      },
      {
        path: 'add-fields',
        component: AddExtraFieldsComponent,
        data: {
          title: 'Add Fields'
        }
      },
      {
        path: 'import',
        component: ImportComponent,
        data: {
          title: 'Import'
        }
      },
      {
        path: 'template-mapping',
        component: TemplateMappingComponent,
        data:{
          title: 'Template Mapping'
        }
      },
      {
        path: 'event-mapping',
        component: EventMappingComponent,
        data:{
          title: 'Event Mapping'
        }
      },
      {
            path: 'click365webpush',
            component: click365webpushComponent,
            data:{
              title: ' Click365 WebPush'
            }
          }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigurationsRoutingModule { }

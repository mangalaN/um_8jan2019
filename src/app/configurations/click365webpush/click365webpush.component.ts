import { Component, OnInit,ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { FormControl, FormGroup, FormBuilder,Validators, NgForm } from '@angular/forms';
import { click365webpushService } from '../../shared/data/click365webpush.service';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';


@Component({
  selector: 'app-click365webpush',
  templateUrl: './click365webpush.component.html',
  styleUrls: ['./click365webpush.component.scss']
})
export class click365webpushComponent implements OnInit {
  // rows = [];
  // public imagePath;
 image_url: any;
 // public message: string;
 click365webpushForm: FormGroup;
 url;
   loader:boolean = false;
   form = {
     'image': '',
   };


 // preview(files) {
 //   if (files.length === 0)
 //     return;
 //
 //   var mimeType = files[0].type;
 //   if (mimeType.match(/image\/*/) == null) {
 //     this.message = "Only images are supported.";
 //     return;
 //   }
 //
 //   var reader = new FileReader();
 //   this.imagePath = files;
 //   reader.readAsDataURL(files[0]);
 //   reader.onload = (_event) => {
 //     this.image_url = reader.result;
 //   }
 // }

 timeout = 3000;
 position: SnotifyPosition = SnotifyPosition.centerTop;
 progressBar = true;
 closeClick = true;
 newTop = true;
 backdrop = -1;
 dockMax = 8;
 blockMax = 6;
 pauseHover = true;
 titleMaxLength = 15;
 bodyMaxLength = 80;
 @ViewChild(DatatableComponent) table: DatatableComponent;


  constructor(private http: HttpClient,private snotifyService: SnotifyService, private formBuilder: FormBuilder,public Click365webpushService: click365webpushService) { }

  ngOnInit() {
    this.click365webpushForm = new FormGroup({
      // 'titles': new FormControl(null,[Validators.required]),
      'title': new FormControl(null,[Validators.required]),
      'description': new FormControl(null,[Validators.required]),
      'disallow_text': new FormControl(null,[Validators.required]),
      'allow_text': new FormControl(null,[Validators.required]),
      // 'mobile_description': new FormControl(null,[Validators.required]),
      // 'mobile_opt': new FormControl(null,[Validators.required]),
      'image_url': new FormControl(null, null)
    })
  }

// saveclick365webpush(){
//   let push={
//     titles:this.click365webpushForm.controls['titles'].value,
//     title:this.click365webpushForm.controls['title'].value,
//     description:this.click365webpushForm.controls['description'].value,
//     disallow_text:this.click365webpushForm.controls['disallow_text'].value,
//     allow_text:this.click365webpushForm.controls['allow_text'].value,
//     mobile_description:this.click365webpushForm.controls['mobile_description'].value,
//     // mobile_opt:this.click365webpushForm.controls['mobile_opt'].value,
//     image_url:this.click365webpushForm.controls['image_url'].value
//   }
//   this.snotifyService.success('Saved Successfullly', '', this.getConfig());
//   this.click365webpushForm.reset();
//   this.image_url='';
//
//   this.Click365webpushService.saveclick365webpush(push).then(data => {
//     console.log(data);
//     if(data['status'] == 'success'){
//
//       //this.rows = data['click365webpush'];
//   }
//   else{
//     // this. rows=[];
//   }
// });
//
// }


updateclick365webpush(){
  console.log('IM URL - ' + this.click365webpushForm.controls['image_url'].value);
  let push={
    // titles:this.click365webpushForm.controls['titles'].value,
    title:this.click365webpushForm.controls['title'].value,
    description:this.click365webpushForm.controls['description'].value,
    disallow_text:this.click365webpushForm.controls['disallow_text'].value,
    allow_text:this.click365webpushForm.controls['allow_text'].value,
    // mobile_description:this.click365webpushForm.controls['mobile_description'].value,
    // mobile_opt:this.click365webpushForm.controls['mobile_opt'].value,
    image_url:this.url//this.click365webpushForm.controls['image_url'].value//this.url
  }
  this.snotifyService.success('Saved Successfully', '', this.getConfig());
  this.click365webpushForm.reset();
  this.image_url='';

  this.Click365webpushService.updateclick365webpush(push).then(data => {
    console.log(data);
    if(data['status'] == 'success'){

    }
  })
}

uploadmembershipIcon(files: File[]){
  this.loader = true;
      this.url = 'https://click365.com.au/usermanagement/images/UploadImages/'+files[0].name;
      console.log('dxcvdx - ' + this.url);
      this.uploadAndProgress(files);
}

uploadAndProgress(files: File[]){
  var formData = new FormData();
  Array.from(files).forEach(f => formData.append('file',f));

  this.http.post('https:/click365.com.au/usermanagement/uploadImages.php?fld=UploadImages', formData)
    .subscribe(event => {
      this.loader = false;
      if(event && event['status'] == "Sorry, your file is too large."){
        alert('Sorry, your file is too large. Recommended Size: 192x192.');
        this.url = '';
      }
  });
}

getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
  }
}

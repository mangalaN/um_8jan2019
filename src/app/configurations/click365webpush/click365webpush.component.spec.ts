import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { click365webpushComponent } from './click365webpush.component';

describe('PushcrewComponent', () => {
  let component: click365webpushComponent;
  let fixture: ComponentFixture<click365webpushComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ click365webpushComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(click365webpushComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
//import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

import { SettingsService } from '../../shared/data/settings.service';
import { SubscriptionService } from '../../shared/data/subscription.service';
import { UserService } from '../../shared/data/user.service';
import { AuthenticationService } from '../../shared/data/authentication.service';
import * as moment from 'moment-timezone'

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  @ViewChild('companylogoLabel') companylogoLabel : ElementRef;
  @ViewChild('defaultlogoLabel') defaultlogoLabel : ElementRef;
  regularForm: FormGroup;
  inviteForm: FormGroup;
  options = {
    types: ['geocode'],
    componentRestrictions: { country: 'AU' }
  };
  storeName;
  currentDate;
  userId;
  error:any={isError:false,errorMessage:''};
  subscription;
  expiry_date;
  pendingDays;
  showSubscription: boolean = false;
  subscriptionList;
  subscribed: boolean = false;
  selectedSubcription;
  subtype;subscriptionId;
  newaddress;
  update = false;
  settingId = 0;
  logo = '';
   weblogo = '';
  roles;
  dataSource = [];
  p: number = 1;
  pageSize: number = 10;
  collectionSize = [];
  style = 'material';
  title = 'Snotify title!';
  body = 'Lorem ipsum dolor sit amet!';
  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  public invitedUsers;
  bodyMaxLength = 80;
  public isAdmin: boolean = false;
  public showInvite: boolean = false;
  public showSettings: boolean = true;
  public submitted: boolean = false;
  public timezone;

  module = {
    'emailManagement': false,
    'loyalty': false,
    'survey': false,
    'membership': false,
    'webPush': false,
    'events': false,
    'emailManagementSub': [],
    'loyaltySub': [],
    'membersSub': [],
    'surveySub': [],
    'webPushSub': [],
    'eventsSub': []
  };
  emailManagement = {
    'newsletter': false,
    'reports': false,
    'emailTemplate': false,
    'emailAutomation': false,
    'keys': false
  };
  loyalty = {
    'memPlans': false,
    'renewals': false,
    'transactions': false,
    'loyaltypoints': false,
    'redeemloyalty': false,
    'loyaltyProgram': false,
    'rewards': false
  };
  members = {
    'members': false,
    'subscribers': false,
    'management': false,
    'list': false,
    'segments' : false
  };
  survey = {
    'createForm': false,
    'formmanager': false,
    'tablemapping': false,
    'chartsreport': false
  };
  webPushDashboard = {
    'webPushDashboard': false,
    'webEngagement': false,
    'segments': false,
    'tokens': false,
    'webtemplate': false,
    'webnotification': false,
    'safaripushconfig': false
  }
  events = {
    'events': false
  };
  modulesettings = false;

  constructor(private snotifyService: SnotifyService, public authenticationService: AuthenticationService, private http: HttpClient, public userservice: UserService, public subscriptionService: SubscriptionService, private router: Router, private route: ActivatedRoute, public settingsservice: SettingsService) {
    this.userId = JSON.parse(localStorage.getItem('currentUser'));
    this.timezone = moment.tz.guess();
    if(this.userId){
      if(this.userId['role_id'] == '1'){
        this.isAdmin = true;
      }
    }
    this.subscriptionService.getSubscription(new Date().getTime()).then(data => {
      this.subscriptionList = data['subscription'];
      console.log('Get Subscription list - ' + JSON.stringify(this.subscriptionList));
    });

    this.authenticationService.getinvitedusersstatus(new Date().getTime()).then(data => {
      console.log('getinvitedusersstatus - ' + JSON.stringify(data));
      if(data['organization']){
        this.invitedUsers = data['organization'];
        this.collectionSize = data['organization'].length;
      }
    });
    this.subscriptionService.getOrgSubscriptionById(this.userId['id']).then(data => {
      if(data['status'] == 'success'){
        this.expiry_date = data['organization'][0].expiry_date;
        var x = new Date(this.expiry_date)
        var myVar = x.toDateString();
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date();
        var secondDate = new Date(myVar);
        this.pendingDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
      }
    });
    this.userservice.getGroups().then(data => {
      this.roles = data['groups'];
    });
    /*this.userservice.getUsersByStatus('y', new Date().getTime()).then(data => {
      if(data['status'] == "success"){
        this.dataSource = data['user'];
        this.collectionSize = data['user'].length;
      }
    });*/
  }

  ngOnInit() {
    this.inviteForm = new FormGroup({
        'inviteEmail': new FormControl(null, [Validators.required, Validators.email]),
        'role': new FormControl(null, [Validators.required])
    });
    this.settingsservice.ModuleSettings(this.module, 'getModule').then(data => {
      if(data['status'] == 'success'){
        this.modulesettings = true;
        this.module.emailManagement = data['module'][0]['emailManagement'];
        this.module.loyalty = data['module'][0]['loyalty'];
        this.module.survey = data['module'][0]['survey'];
        this.module.membership = data['module'][0]['membership'];
        this.module.webPush = data['module'][0]['webPush'];
        this.module.events = data['module'][0]['events'];
        this.emailManagement.newsletter = data['module'][0]['emailManagementSub'][0]['newsletter'];
        this.emailManagement.reports = data['module'][0]['emailManagementSub'][0]['reports'];
        this.emailManagement.emailTemplate = data['module'][0]['emailManagementSub'][0]['emailTemplate'];
        this.emailManagement.emailAutomation = data['module'][0]['emailManagementSub'][0]['emailAutomation'];
        this.emailManagement.keys = data['module'][0]['emailManagementSub'][0]['keys'];
        this.loyalty.memPlans = data['module'][0]['loyaltySub'][0]['memPlans'];
        this.loyalty.renewals = data['module'][0]['loyaltySub'][0]['renewals'];
        this.loyalty.transactions = data['module'][0]['loyaltySub'][0]['transactions'];
        this.loyalty.loyaltypoints = data['module'][0]['loyaltySub'][0]['loyaltypoints'];
        this.loyalty.redeemloyalty = data['module'][0]['loyaltySub'][0]['redeemloyalty'];
        this.loyalty.loyaltyProgram = data['module'][0]['loyaltySub'][0]['loyaltyProgram'];
        this.loyalty.rewards = data['module'][0]['loyaltySub'][0]['rewards'];
        this.members.members = data['module'][0]['membersSub'][0]['members'];
        this.members.subscribers = data['module'][0]['membersSub'][0]['subscribers'];
        this.members.management = data['module'][0]['membersSub'][0]['management'];
        this.members.list = data['module'][0]['membersSub'][0]['list'];
        this.members.segments = data['module'][0]['membersSub'][0]['segments'];
        this.survey.createForm = data['module'][0]['surveySub'][0]['createForm'];
        this.survey.formmanager = data['module'][0]['surveySub'][0]['formmanager'];
        this.survey.tablemapping = data['module'][0]['surveySub'][0]['tablemapping'];
        this.survey.chartsreport = data['module'][0]['surveySub'][0]['chartsreport'];
        this.webPushDashboard.webPushDashboard = data['module'][0]['webPushSub'][0]['webPushDashboard'];
        this.webPushDashboard.webEngagement = data['module'][0]['webPushSub'][0]['webEngagement'];
        this.webPushDashboard.segments = data['module'][0]['webPushSub'][0]['segments'];
        this.webPushDashboard.tokens = data['module'][0]['webPushSub'][0]['tokens'];
        this.webPushDashboard.webtemplate = data['module'][0]['webPushSub'][0]['webtemplate'];
        this.webPushDashboard.webnotification = data['module'][0]['webPushSub'][0]['webnotification'];
        this.webPushDashboard.safaripushconfig = data['module'][0]['webPushSub'][0]['safaripushconfig'];
        this.events.events = data['module'][0]['eventsSub'][0]['events'];
        this.module.emailManagementSub = [];
        this.module.loyaltySub = [];
        this.module.membersSub = [];
        this.module.surveySub = [];
        this.module.webPushSub = [];
        this.module.eventsSub = [];
      }
      else{
        this.modulesettings = false;
      }
    });
    this.regularForm = new FormGroup({
      'webname': new FormControl(null, [Validators.required]),
      'sitedir': new FormControl(null, [Validators.required]),
      'companylogo': new FormControl(null, [Validators.required]),
     'defaultlogo': new FormControl(null, [Validators.required]),
      'webemail': new FormControl(null, [Validators.required]),
      'defaultcurrency': new FormControl(null, [Validators.required]),
      'payemail': new FormControl(null, [Validators.required]),
      'webphone': new FormControl(null, [Validators.required]),
      'address': new FormControl(null, [Validators.required]),
      'registrationnotification': new FormControl(null, null),
      'allowregistration': new FormControl(null, null),
      //'points': new FormControl(null, null),
      'regverification': new FormControl(null, null),
      'listadmin': new FormControl(null, null),
      'subscriptionType': new FormControl(null, [Validators.required]),
      'pendingPeriod': new FormControl(null, [Validators.required]),
      'timezone': new FormControl(null, [Validators.required])
    }, {updateOn: 'blur'});
      this.currentDate = Date.now();
    	this.settingsservice.getSettings().then(data => {
          this.settingId = data['settings'][0]['id'];
        console.log('getSubscriptionByType - ' + JSON.stringify(data));
        this.subscriptionService.getSubscriptionByType(localStorage.getItem('subscriptionType')).then(subdata => {
           if(data['status'] == 'success'){

      				this.regularForm.controls['webname'].setValue(data['settings'][0].webname);
      				this.regularForm.controls['sitedir'].setValue(data['settings'][0].sitedir);
      				//this.regularForm.controls['companylogo'].setValue(data['settings'][0].companylogo);
      				this.regularForm.controls['webemail'].setValue(data['settings'][0].webemail);
      				this.regularForm.controls['payemail'].setValue(data['settings'][0].payemail);
      				//this.regularForm.controls['points'].setValue(data['settings'][0].loyaltyCheck);
      				this.regularForm.controls['address'].setValue(data['settings'][0].address);
      				this.regularForm.controls['allowregistration'].setValue(data['settings'][0].allowregistration);
      				this.regularForm.controls['regverification'].setValue(data['settings'][0].regverification);
      				this.regularForm.controls['registrationnotification'].setValue(data['settings'][0].registrationnotification);
      				this.regularForm.controls['defaultcurrency'].setValue(data['settings'][0].defaultcurrency);
      				this.regularForm.controls['webphone'].setValue(data['settings'][0].webphone);
      				this.regularForm.controls['listadmin'].setValue(data['settings'][0].listadmin);
              this.regularForm.controls['subscriptionType'].setValue(subdata['type'][0]['type']);
              this.regularForm.controls['pendingPeriod'].setValue(this.pendingDays + ' Days');
              this.regularForm.controls['timezone'].setValue(this.timezone);
              if(data['settings'][0].webname !== '' || data['settings'][0].webname !== undefined){
          			this.update = true;
          		}
              this.regularForm.controls['companylogo'].setValue(data['settings'][0]['companylogo']);
              this.logo = data['settings'][0]['companylogo'];
              this.regularForm.controls['defaultlogo'].setValue(data['settings'][0]['defaultlogo']);
             this.weblogo = data['settings'][0]['defaultlogo'];
			      }
            this.subscription = subdata['type'][0]['type'];
            console.log('this.subscription - ' + this.subscription);
            this.subscriptionId = subdata['type'][0]['id'];
            this.settingId = data['settings'][0]['id'];
          });
      });
  }

  handleAddressChange(address) {
    // Do some stuff
    this.newaddress = address.formatted_address;
  }

  uploadCompanyLogo(files: File[]){
	    //pick from one of the 4 styles of file uploads below
	    //this.uploadAndProgress(files);
	    //this.settingDetail.companylogo = files[0].name;
      this.companylogoLabel.nativeElement.innerHTML = files[0].name;
      this.logo = files[0].name;
      console.log(this.companylogoLabel.nativeElement);
      //alert(files[0].name);
      this.uploadAndProgress(files);
	}

  uploadAndProgress(files: File[]){
    console.log(files)
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f))

    this.http.post('https:/click365.com.au/usermanagement/uploadLogo.php?fld=CompanyLogo', formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          //this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          //this.uploadSuccess = true;
        }
    });
  }
  uploaddefaultlogo(files: File[]){
       this.defaultlogoLabel.nativeElement.innerHTML = files[0].name;
       this.weblogo = 'https://click365.com.au/usermanagement/images/CompanyLogo/'+files[0].name;
       console.log(this.defaultlogoLabel.nativeElement);
       //alert(files[0].name);
       this.uploadwebProgress(files);
 	}
   uploadwebProgress(files: File[]){
     this.error = '';
     console.log(files)
     var formData = new FormData();
     Array.from(files).forEach(f => formData.append('file',f))
     this.http.post('https:/click365.com.au/usermanagement/uploadImages.php?fld=CompanyLogo', formData)
       .subscribe(event => {
         if(event && event['status'] == "Sorry, your file is too large."){
           this.error={isError:true,errorMessage:'Sorry, your file is too large. Recommended Size: 192x192.'}
           //alert('Sorry, your file is too large. Recommended Size: 192x192.');
           this.defaultlogoLabel.nativeElement.innerHTML = '';
         }
     });
   }
  change() {
    this.showSubscription = true;
  }

  saveModuleSettings(){
    let action = '';
    if(this.modulesettings){
      action = 'updateModule';
    }
    else{
      action = 'saveModule';
    }
    this.module.emailManagementSub.push(this.emailManagement);
    this.module.loyaltySub.push(this.loyalty);
    this.module.membersSub.push(this.members);
    this.module.surveySub.push(this.survey);
    this.module.webPushSub.push(this.webPushDashboard);
    this.module.eventsSub.push(this.events);
    this.settingsservice.ModuleSettings(this.module, action).then(data => {
      this.modulesettings = true;
      this.settingsservice.ModuleSettings(this.module, 'getModule').then(data => {
        if(data['status'] == 'success'){
          this.module.emailManagement = data['module'][0]['emailManagement'];
          this.module.loyalty = data['module'][0]['loyalty'];
          this.module.survey = data['module'][0]['survey'];
          this.module.membership = data['module'][0]['membership'];
          this.module.webPush = data['module'][0]['webPush'];
          this.module.events = data['module'][0]['events'];
          this.emailManagement.newsletter = data['module'][0]['emailManagementSub'][0]['newsletter'];
          this.emailManagement.reports = data['module'][0]['emailManagementSub'][0]['reports'];
          this.emailManagement.emailTemplate = data['module'][0]['emailManagementSub'][0]['emailTemplate'];
          this.emailManagement.emailAutomation = data['module'][0]['emailManagementSub'][0]['emailAutomation'];
          this.emailManagement.keys = data['module'][0]['emailManagementSub'][0]['keys'];
          this.loyalty.memPlans = data['module'][0]['loyaltySub'][0]['memPlans'];
          this.loyalty.renewals = data['module'][0]['loyaltySub'][0]['renewals'];
          this.loyalty.transactions = data['module'][0]['loyaltySub'][0]['transactions'];
          this.loyalty.loyaltypoints = data['module'][0]['loyaltySub'][0]['loyaltypoints'];
          this.loyalty.redeemloyalty = data['module'][0]['loyaltySub'][0]['redeemloyalty'];
          this.loyalty.loyaltyProgram = data['module'][0]['loyaltySub'][0]['loyaltyProgram'];
          this.loyalty.rewards = data['module'][0]['loyaltySub'][0]['rewards'];
          this.members.members = data['module'][0]['membersSub'][0]['members'];
          this.members.subscribers = data['module'][0]['membersSub'][0]['subscribers'];
          this.members.management = data['module'][0]['membersSub'][0]['management'];
          this.members.list = data['module'][0]['membersSub'][0]['list'];
          this.members.segments = data['module'][0]['membersSub'][0]['segments'];
          this.survey.createForm = data['module'][0]['surveySub'][0]['createForm'];
          this.survey.formmanager = data['module'][0]['surveySub'][0]['formmanager'];
          this.survey.tablemapping = data['module'][0]['surveySub'][0]['tablemapping'];
          this.survey.chartsreport = data['module'][0]['surveySub'][0]['chartsreport'];
          this.webPushDashboard.webPushDashboard = data['module'][0]['webPushSub'][0]['webPushDashboard'];
          this.webPushDashboard.webEngagement = data['module'][0]['webPushSub'][0]['webEngagement'];
          this.webPushDashboard.segments = data['module'][0]['webPushSub'][0]['segments'];
          this.webPushDashboard.tokens = data['module'][0]['webPushSub'][0]['tokens'];
          this.webPushDashboard.webtemplate = data['module'][0]['webPushSub'][0]['webtemplate'];
          this.webPushDashboard.webnotification = data['module'][0]['webPushSub'][0]['webnotification'];
          this.webPushDashboard.safaripushconfig = data['module'][0]['webPushSub'][0]['safaripushconfig'];
          this.events.events = data['module'][0]['eventsSub'][0]['events'];
          this.module.emailManagementSub = [];
          this.module.loyaltySub = [];
          this.module.membersSub = [];
          this.module.surveySub = [];
          this.module.webPushSub = [];
          this.module.eventsSub = [];

          this.menuhidefunction(action);

          if(action == 'saveModule'){
            this.snotifyService.success('Saved Successfully', '', this.getConfig());
          }
          else{
            this.snotifyService.success('Updated Successfully', '', this.getConfig());
          }
        }
      });
    });
  }

  menuhidefunction(action){
    //user Menu
    let userMenu = [
      {
        'path': '/users/profile', 'title': 'User Profile', 'icon': 'fas fa-user', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': []
      }
    ];
    //Main menu
    let mainMenu;
    if(this.userId['role_id'] == '1'){
      mainMenu = [{
        'path': '/dashboard/charts', 'title': 'Home', 'icon': 'icon-home', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': []
      },
      {
        'path': '', 'title': 'Configurations', 'icon': 'ft-settings', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [
          { 'path': '/configurations/settings', 'title': 'Settings', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },
          { 'path': '/configurations/build-chart', 'title': 'Configure Chart', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },
          { 'path': '/configurations/tiles-config', 'title': 'Configure Tiles', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },
          { 'path': '/configurations/add-fields', 'title': 'Add Fields', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },
          { 'path': '/configurations/template-mapping', 'title': 'Template Mapping', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },
          { 'path': '/configurations/event-mapping', 'title': 'Event Mapping', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },
          { 'path': '/importlog', 'title': 'Import', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },
          { 'path': '', 'title': 'WebPush', 'icon': '', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [
              { 'path': '/configurations/click365webpush', 'title': 'Notification', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },
              { 'path': '/crews', 'title': 'Code', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },
            ]
          },
        ]
      }
    ];
  }
  else{
    mainMenu = [{
      'path': '/dashboard/charts', 'title': 'Home', 'icon': 'icon-home', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': []
    }];
  }

    if(this.module.membership){
      let membermenu = {'path': '', 'title': 'Membership', 'icon': 'ft-users', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': []};

      if(this.members.members){
        membermenu.submenu.push({ 'path': '/users/users', 'title': 'Members', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.members.subscribers){
        membermenu.submenu.push({ 'path': '/users/subscribers', 'title': 'Subscriber', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.members.management){
        membermenu.submenu.push({ 'path': '/users/move-users', 'title': 'Management', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.members.list){
        membermenu.submenu.push({ 'path': '/users/lists', 'title': 'Lists', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.members.segments){
        membermenu.submenu.push({ 'path': '/users/segments', 'title': 'Segments', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      mainMenu.push(membermenu);
    }

    if(this.module.emailManagement){
      let campaignmenu = {'path': '', 'title': 'Campaign', 'icon': 'icon-grid', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false,'submenu': []}

      if(this.emailManagement.newsletter){
        campaignmenu.submenu.push({ 'path': '/campaign/newsletter', 'title': 'Newsletter', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.emailManagement.reports){
        campaignmenu.submenu.push({ 'path': '/campaign/reports', 'title': 'Reports', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.emailManagement.emailTemplate){
        campaignmenu.submenu.push({ 'path': '/campaign/emailtemplate', 'title': 'Email Template', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.emailManagement.emailAutomation){
        campaignmenu.submenu.push({ 'path': '/campaign/email-automation', 'title': 'Email Automation', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.emailManagement.keys){
        campaignmenu.submenu.push({ 'path': '/campaign/keys', 'title': 'Keys', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      mainMenu.push(campaignmenu);
    }

    if(this.module.loyalty){
      let loyaltymenu = {'path': '', 'title': 'Loyalty', 'icon': 'icon-present', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false,'submenu': []};

      if(this.loyalty.memPlans){
        loyaltymenu.submenu.push({ 'path': '/loyalty/membership', 'title': 'Membership', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.loyalty.renewals){
        loyaltymenu.submenu.push({ 'path': '/loyalty/renewals', 'title': 'Renewals', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.loyalty.transactions){
        loyaltymenu.submenu.push({ 'path': '/loyalty/transactions', 'title': 'Transactions', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        userMenu.push({ 'path': '/loyalty/transactions', 'title': 'Transactions', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.loyalty.loyaltypoints){
        loyaltymenu.submenu.push({ 'path': '/loyalty/loyalty-points', 'title': 'Loyalty Points', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.loyalty.redeemloyalty){
        loyaltymenu.submenu.push({ 'path': '/loyalty/redeem-loyalty-display', 'title': 'Redeem Loyalty ', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        userMenu.push({ 'path': '/loyalty/redeem-loyalty-display', 'title': 'Redeem Loyalty ', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.loyalty.loyaltyProgram){
        loyaltymenu.submenu.push({ 'path': '/loyalty/loyalty-display', 'title': 'Loyalty Program', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        userMenu.push({ 'path': '/loyalty/loyalty-display', 'title': 'Loyalty Program', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      if(this.loyalty.rewards){
        loyaltymenu.submenu.push({ 'path': '/loyalty/redeem-reward', 'title': 'Rewards', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
        userMenu.push({ 'path': '/loyalty/redeem', 'title': 'Rewards', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] });
      }
      mainMenu.push(loyaltymenu);
    }

    if(this.module.survey){
      let surveymenu = {'path': '', 'title': 'Feedback', 'icon': 'icon-envelope', 'class': 'has-sub', 'badge': '', 'badgeClass': '', 'isExternalLink': false,'submenu': []};

      if(this.survey.createForm){
        surveymenu.submenu.push({ 'path': '/feedback/createform', 'title': 'Create Form', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },);
      }
      if(this.survey.formmanager){
        surveymenu.submenu.push({ 'path': '/feedback/formmanager', 'title': 'Form Manager', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },);
      }
      if(this.survey.tablemapping){
        surveymenu.submenu.push({ 'path': '/feedback/dbmaping', 'title': 'Table Maping', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },);
      }
      if(this.survey.chartsreport){
        surveymenu.submenu.push({ 'path': '/feedback/chartsreport', 'title': 'Charts Report', 'icon': '', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': [] },);
      }
      mainMenu.push(surveymenu);
    }

    if(this.module.webPush){
      let webPushmenu = {"path": "", "title": "Web Push", "icon": "", "class": "has-sub", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []};

      if(this.webPushDashboard.webPushDashboard){
        webPushmenu.submenu.push({"path": "/web-dashboard/dashboard", "title": "Dashboard", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
      }
      if(this.webPushDashboard.webEngagement){
        webPushmenu.submenu.push({"path": "/web-dashboard/web-engagement", "title": "Campaign", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
      }
      if(this.webPushDashboard.segments){
        webPushmenu.submenu.push({"path": "/web-dashboard/segments", "title": "Segments", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
      }
      if(this.webPushDashboard.tokens){
        webPushmenu.submenu.push({"path": "/web-dashboard/tokens", "title": "Tokens", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
      }
      if(this.webPushDashboard.webtemplate){
        webPushmenu.submenu.push({"path": "/web-dashboard/web-template", "title": "Web-Template", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
      }
      if(this.webPushDashboard.webnotification){
        webPushmenu.submenu.push({"path": "/web-dashboard/web-notification", "title": "Web Notifications", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
      }
      if(this.webPushDashboard.safaripushconfig){
        webPushmenu.submenu.push({"path": "/web-dashboard/safaripushconfig", "title": "Safari Push Config", "icon": "", "class": "", "badge": "", "badgeClass": "", "isExternalLink": false, "submenu": []});
      }
      mainMenu.push(webPushmenu);
    }

    if(this.module.events){
      let eventsmenu = {'path': '/events', 'title': 'Events', 'icon': 'icon-list', 'class': '', 'badge': '', 'badgeClass': '', 'isExternalLink': false, 'submenu': []};

      mainMenu.push(eventsmenu);
    }
    this.settingsservice.addMenu(mainMenu, userMenu, action).then(data => {
      console.log('mainMenu - '+JSON.stringify(mainMenu));
    });
  }

  getId(id,type){
    this.subscribed = true;
    this.selectedSubcription = type;
  }

  cancel(){
    this.showSubscription = false;
  }

  inviteUser(){
    this.showInvite = true;
    this.showSettings = false;
  }

  onSave(){
    this.subscriptionService.updateSubscriptionType(this.selectedSubcription,this.userId['id']).then(data => {
      if(data['status'] == 'true'){
        localStorage.setItem('subscriptionType', this.selectedSubcription);
        this.subscriptionService.getSubscriptionByType(localStorage.getItem('subscriptionType')).then(subdata => {
          this.regularForm.controls['subscriptionType'].setValue(subdata['type'][0]['type']);
          this.regularForm.controls['pendingPeriod'].setValue(this.pendingDays + ' Days');
          this.subscription = this.selectedSubcription;
        });

        this.regularForm.controls['subscriptionType'].setValue(this.selectedSubcription);
        this.showSubscription = false;
      }
    });
  }

  onReactiveFormSubmit(){
    let currentaddress = '';
    if(this.newaddress != ''){
      currentaddress = this.newaddress;
    }
    else{
      currentaddress = this.regularForm.controls['address'].value
    }
    if(!this.update){
      let settingDetail = {
        'webname': this.regularForm.controls['webname'].value,
    		'sitedir': this.regularForm.controls['sitedir'].value,
    		'webemail': this.regularForm.controls['webemail'].value,
    		'payemail': this.regularForm.controls['payemail'].value,
    		'defaultcurrency': this.regularForm.controls['defaultcurrency'].value,
    		'webphone': this.regularForm.controls['webphone'].value,
    		'address': currentaddress,
    		'companylogo': this.logo,
          'defaultlogo': this.weblogo,
    		'registrationnotification': this.regularForm.controls['registrationnotification'].value,
    		'allowregistration': this.regularForm.controls['allowregistration'].value,
    		//'loyaltyCheck': this.regularForm.controls['points'].value,
    		'regverification': this.regularForm.controls['regverification'].value,
        'listadmin': this.regularForm.controls['listadmin'].value
      };
      this.settingsservice.saveSettings(settingDetail).then(data => {
  			if(data['status']){
          this.settingsservice.getSettings().then(data => {
            this.subscriptionService.getSubscriptionByType(localStorage.getItem('subscriptionType')).then(subdata => {
    			     if(data['status'] == 'success'){
                   console.log('getSubscriptionByType - ' + JSON.stringify(subdata));
          				this.regularForm.controls['webname'].setValue(data['settings'][0].webname);
          				this.regularForm.controls['sitedir'].setValue(data['settings'][0].sitedir);
          				//this.regularForm.controls['companylogo'].setValue(data['settings'][0].companylogo);
          				this.regularForm.controls['webemail'].setValue(data['settings'][0].webemail);
          				this.regularForm.controls['payemail'].setValue(data['settings'][0].payemail);
          				//this.regularForm.controls['points'].setValue(data['settings'][0].loyaltyCheck);
          				this.regularForm.controls['address'].setValue(data['settings'][0].address);
          				this.regularForm.controls['allowregistration'].setValue(data['settings'][0].allowregistration);
          				this.regularForm.controls['regverification'].setValue(data['settings'][0].regverification);
          				this.regularForm.controls['registrationnotification'].setValue(data['settings'][0].registrationnotification);
          				this.regularForm.controls['defaultcurrency'].setValue(data['settings'][0].defaultcurrency);
          				this.regularForm.controls['webphone'].setValue(data['settings'][0].webphone);
          				this.regularForm.controls['listadmin'].setValue(data['settings'][0].listadmin);
                  this.regularForm.controls['subscriptionType'].setValue(subdata['type'][0]['type']);
                  this.regularForm.controls['pendingPeriod'].setValue(this.pendingDays + ' Days');
                  if(data['settings'][0].webname !== '' || data['settings'][0].webname !== undefined){
              			this.update = true;
              		}
                  this.regularForm.controls['companylogo'].setValue(data['settings'][0]['companylogo']);
                  this.logo = data['settings'][0]['companylogo'];
                  this.regularForm.controls['defaultlogo'].setValue(data['settings'][0]['defaultlogo']);
               this.weblogo = data['settings'][0]['defaultlogo'];
    			      }
                this.subscription = subdata['type'][0]['type'];
                this.subscriptionId = subdata['type'][0]['id'];
              });
          });
  			}
  		});
    }
    else{
      let settingDetail = {
        'webname': this.regularForm.controls['webname'].value,
    		'sitedir': this.regularForm.controls['sitedir'].value,
    		'webemail': this.regularForm.controls['webemail'].value,
    		'payemail': this.regularForm.controls['payemail'].value,
    		'defaultcurrency': this.regularForm.controls['defaultcurrency'].value,
    		'webphone': this.regularForm.controls['webphone'].value,
    		'address': currentaddress,
    		'companylogo': this.logo,
         'defaultlogo': this.weblogo,
    		'registrationnotification': this.regularForm.controls['registrationnotification'].value,
    		'allowregistration': this.regularForm.controls['allowregistration'].value,
    		//'loyaltyCheck': this.regularForm.controls['points'].value,
    		'regverification': this.regularForm.controls['regverification'].value,
        'listadmin': this.regularForm.controls['listadmin'].value,
        'id': this.settingId
      };
      this.settingsservice.updateSettings(settingDetail).then(data => {
  			if(data['status']){
          this.snotifyService.success('Updated Successfully', '', this.getConfig());
          this.settingsservice.getSettings().then(data => {
            this.subscriptionService.getSubscriptionByType(this.userId['subscription_type']).then(subdata => {
    			     if(data['status'] == 'success'){

          				this.regularForm.controls['webname'].setValue(data['settings'][0].webname);
          				this.regularForm.controls['sitedir'].setValue(data['settings'][0].sitedir);
          				//this.regularForm.controls['companylogo'].setValue(data['settings'][0].companylogo);
          				this.regularForm.controls['webemail'].setValue(data['settings'][0].webemail);
          				this.regularForm.controls['payemail'].setValue(data['settings'][0].payemail);
          				//this.regularForm.controls['points'].setValue(data['settings'][0].loyaltyCheck);
          				this.regularForm.controls['address'].setValue(data['settings'][0].address);
          				this.regularForm.controls['allowregistration'].setValue(data['settings'][0].allowregistration);
          				this.regularForm.controls['regverification'].setValue(data['settings'][0].regverification);
          				this.regularForm.controls['registrationnotification'].setValue(data['settings'][0].registrationnotification);
          				this.regularForm.controls['defaultcurrency'].setValue(data['settings'][0].defaultcurrency);
          				this.regularForm.controls['webphone'].setValue(data['settings'][0].webphone);
          				this.regularForm.controls['listadmin'].setValue(data['settings'][0].listadmin);
                  this.regularForm.controls['subscriptionType'].setValue(subdata['type'][0]['type']);
                  this.regularForm.controls['pendingPeriod'].setValue(this.pendingDays + ' Days');
                  if(data['settings'][0].webname !== '' || data['settings'][0].webname !== undefined){
              			this.update = true;
              		}
                  this.regularForm.controls['companylogo'].setValue(data['settings'][0]['companylogo']);
                  this.logo = data['settings'][0]['companylogo'];
                  this.regularForm.controls['defaultlogo'].setValue(data['settings'][0]['defaultlogo']);
                this.weblogo = data['settings'][0]['defaultlogo'];
    			      }
                this.subscription = subdata['type'][0]['type'];
                this.subscriptionId = subdata['type'][0]['id'];
              });
          });
  			}
  		});
    }
	}

  cancelEdit(){
    this.showSettings = true;
    this.regularForm.reset();
    this.inviteForm.reset();
  }

  sendInvite(id,email){
    this.submitted = true;
    this.authenticationService.getInvitedUsersByEmail(email,new Date().getTime()).then(data => {
      if(data['status'] == "invalid"){
        this.userservice.sendEmail(id,email,this.userId['organization_id']).then(data => {
          if(data['status'] == 'Failed'){
            this.snotifyService.error('User Already Exists', '', this.getConfig());
            this.submitted = false;
          }
          else if(data['status'] == 'success'){
            this.invitedUsers = [];
            this.collectionSize = [];
            this.authenticationService.getinvitedusersstatus(new Date().getTime()).then(data => {
              if(data['organization']){
                this.invitedUsers = data['organization'];
                this.collectionSize = data['organization'].length;
              }
              console.log('getinvitedusersstatus - ' + JSON.stringify(data));
            });
            this.showSubscription = false;
            this.showSettings = true;
            this.showInvite = false;
            this.submitted = false;
            this.inviteForm.reset();
            const { timeout, closeOnClick, ...config } = this.getConfig();
            this.snotifyService.confirm('Email Sent successfullly!', 'Success', {
                  ...config,
                  buttons: [
                      { text: 'Ok', action: (toast) => {
                        this.snotifyService.remove(toast.id);
                      } }
                  ]
            });
          }
        });
      }
      else if(data['status'] == "success"){
        this.resendInvite(data['organization'][0]['id'],data['organization'][0]['email'],data['organization'][0]['unique_id'])
      }
    })
  }

  resendInvite(id,email,uniqueId){
    this.submitted = true;
    this.userservice.resendEmail(id,email,uniqueId,this.userId['organization_id']).then(data => {
      if(data['status'] == 'Failed'){
        this.snotifyService.error('User Already Exists', '', this.getConfig());
        this.submitted = false;
      }
      else if(data['status'] == 'success'){
        this.invitedUsers = [];
        this.collectionSize = [];
        this.authenticationService.getinvitedusersstatus(new Date().getTime()).then(data => {
          if(data['organization']){
            this.invitedUsers = data['organization'];
            this.collectionSize = data['organization'].length;
          }
          console.log('getinvitedusersstatus - ' + JSON.stringify(data));
        });
        this.showSubscription = false;
        this.showSettings = true;
        this.showInvite = false;
        this.submitted = false;
        this.inviteForm.reset();
        const { timeout, closeOnClick, ...config } = this.getConfig();
        this.snotifyService.confirm('Email Sent successfullly!', 'Success', {
              ...config,
              buttons: [
                  { text: 'Ok', action: (toast) => {
                    this.snotifyService.remove(toast.id);
                  } }
              ]
        });
      }
    });
  }

  deactivate(val, id){
    this.userservice.deactivateUsers(val, id).then(data => {
      this.dataSource = [];
      this.collectionSize = [];
      let store;
      if(this.storeName.startsWith("Auto One")){
        store = this.storeName;
      }
      else{
        store = '';
      }
      this.userservice.getUsersByStatus('y', store, new Date().getTime()).then(data => {
        this.dataSource = [];
        this.collectionSize = [];
        if(data['status'] == "success"){
          this.dataSource = data['user'];
          this.collectionSize = data['user'].length;
        }
      });
      if(val == 'n'){
        this.snotifyService.success('Deactivated Successfullly', '', this.getConfig());
      }
    });
  }

  getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
  }

  // selectUser(value){
  //   this.SelectedTab = value.nextId;
  //   this.userservice.getUsersByStatus(value.nextId, new Date().getTime()).then(data => {
  //     this.dataSource = [];
  //     this.collectionSize = [];
  //     if(data['status'] == "success"){
  //       this.dataSource = data['user'];
  //       this.collectionSize = data['user'].length;
  //     }
  //   });
  // }
}

import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ArchwizardModule } from 'angular-archwizard';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ConfigurationsRoutingModule } from "./configurations-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgxPaginationModule } from 'ngx-pagination';

import { BuildChartComponent } from "./build-chart/build-chart.component";
import { TilesConfigComponent } from "./tiles-config/tiles-config.component";
import { SettingsComponent } from "./settings/settings.component";
import { ConfigurationsComponent } from "./configurations.component"

import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AddExtraFieldsComponent } from './add-extra-fields/add-extra-fields.component';
import { ImportComponent } from "./import/import.component";
import { click365webpushComponent } from "./click365webpush/click365webpush.component";
import { TemplateMappingComponent } from "./template-mapping/template-mapping.component";
import { EventMappingComponent } from "./event-mapping/event-mapping.component";


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CustomFormsModule,
        ConfigurationsRoutingModule,
        Ng2SmartTableModule,
        NgxDatatableModule,
        NgbModule,
        ArchwizardModule,
        GooglePlaceModule,
        UiSwitchModule,
        NgxPaginationModule
    ],
    declarations: [
        BuildChartComponent,
        TilesConfigComponent,
        SettingsComponent,
        ConfigurationsComponent,
        AddExtraFieldsComponent,
        ImportComponent,
        click365webpushComponent,
        TemplateMappingComponent,
        EventMappingComponent
    ]
})
export class  ConfigurationsModule { }

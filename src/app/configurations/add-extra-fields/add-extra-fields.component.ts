import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, NgForm } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { AddFieldsService } from '../../shared/data/addFields.service';

@Component({
  selector: 'app-add-extra-fields',
  templateUrl: './add-extra-fields.component.html',
  styleUrls: ['./add-extra-fields.component.scss']
})
export class AddExtraFieldsComponent implements OnInit {

	columns = [
		{ name: 'Label' },
		{ name: 'DataType' },
		{ name: 'Mandatory' },
		{ name: 'Validation' },
		{ name: 'Action' },
    { name: 'Table Name' }
	];

	columns1 = [
		{ name: 'Label' },
		{ name: 'Datatype' },
		{ name: 'Mandatory' },
		{ name: 'Validation' },
	    { name: 'Datatable' }
	];

	allColumns1 = [
		{ name: 'Label' },
		{ name: 'Datatype' },
		{ name: 'Mandatory' },
		{ name: 'Validation' },
	    { name: 'Datatable' }
	];

	timeout = 3000;
	position: SnotifyPosition = SnotifyPosition.centerTop;
	progressBar = true;
	closeClick = true;
	newTop = true;
	backdrop = -1;
	dockMax = 8;
	blockMax = 6;
	pauseHover = true;
	titleMaxLength = 15;
	bodyMaxLength = 80;

	addFieldsDetails = [];
	showForm = false;
	showValidationMsg = false;
	fieldId = 0;
	edit = false;
	showCol = false;
	//fieldForm: FormGroup;

  @ViewChild('selectForm') selectLabelForm: NgForm;
  @ViewChild('addForm') addLabelForm: NgForm;

  fields = {
    'datatable':'',
		'label':'',
		'datatype':'',
		'length':'',
		'mandatory':false,
		'validation':false,
		'validationMsg':'',
		'id':''
	}


	constructor(private snotifyService: SnotifyService, public addFieldsService: AddFieldsService) { }

	ngOnInit() {
		// this.fieldForm = new FormGroup({
    //     'label': new FormControl(null, [Validators.required]),
    //     'datatype': new FormControl(null, [Validators.required]),
    //     'length': new FormControl(null, null),
    //     'mandatory': new FormControl(false, null),
    //     'validation': new FormControl(false, null),
    //     'validationMsg': new FormControl(null, null)
    // });
		// pass organistaion id and admin id
		this.addFieldsService.getFields(new Date().getTime()).then(data => {
			if(data['status'] = 'success'){
				//console.log(data);
				let datas = [];
				for(let i = 0; i < data['fields'].length; i++){
					let encodeJSON = JSON.parse(data['fields'][i]['fields']);
					let fields = {
						'label': encodeJSON['label'],
						'datatype': encodeJSON['datatype'],
						'length': encodeJSON['length'],
						'mandatory': encodeJSON['mandatory'],
						'validation': encodeJSON['validation'],
						'validationMsg': encodeJSON['validationMsg'],
						'id': data['fields'][i]['id'],
						'datatable': data['fields'][i]['table_name']
					}
					datas.push(fields);
				}
				this.addFieldsDetails = datas;
				console.log(this.addFieldsDetails);
			}
		});
	}

	// showMsg(val){
	// 	//alert(val);
  //   this.showValidationMsg = val;
  //   if(!val){
  //     this.fields.validationMsg = '';
  //   }
	// }

	memToggle(){
		this.showForm = true;
	}

	cancel(){
		this.showForm = false;
		this.selectLabelForm.reset();
    	this.addLabelForm.reset();
		this.edit = false;
		this.fields.id = '';
	}

	editFields(id){
		this.fields.id = id;
		this.edit = true;
		this.addFieldsService.getFieldsById(this.fields.id, new Date().getTime()).then(data => {
			if(data['status'] = 'success'){
				this.showForm = true;
        let encodeJSON = JSON.parse(data['fields'][0]['fields']);
				//console.log(data['fields']);
				this.fields.label = encodeJSON['label'];
				this.fields.datatype = encodeJSON['datatype'];
				this.fields.length = encodeJSON['length'];
				this.fields.mandatory = encodeJSON['mandatory'];
				this.fields.validation = encodeJSON['validation'];
				this.fields.datatable = data['fields'][0]['table_name'];
        if(encodeJSON['validation']){
          this.showValidationMsg = true;
        }
        else{
          this.showValidationMsg = false;
        }
				this.fields.validationMsg = encodeJSON['validationMsg'];
			}
		});
	}

	updateFields(){
		this.addFieldsService.editFields(this.fields, new Date().getTime()).then(data => {
			if(data['status'] = 'success'){
        this.selectLabelForm.reset();
        this.addLabelForm.reset();
				//this.addFieldsDetails = data['fields'];
				this.edit = false;
				this.fields.id = '';
				this.snotifyService.success('Field Details Were Updated Successfully', '', this.getConfig());
				this.addFieldsService.getFields(new Date().getTime()).then(data => {
					if(data['status'] = 'success'){
						//console.log(data['fields']);
						this.showForm = false;
						let datas = [];
						for(let i = 0; i < data['fields'].length; i++){
							let encodeJSON = JSON.parse(data['fields'][i]['fields']);
							let fields = {
								'label': encodeJSON['label'],
								'datatype': encodeJSON['datatype'],
								'length': encodeJSON['length'],
								'mandatory': encodeJSON['mandatory'],
								'validation': encodeJSON['validation'],
								'validationMsg': encodeJSON['validationMsg'],
								'id': data['fields'][i]['id'],
                'datatable': data['fields'][i]['table_name']
							}
							datas.push(fields);
						}
						this.addFieldsDetails = datas;
					}
				});
			}
		});
	}

	deleteFields(id){
		this.addFieldsService.deleteFields(id, new Date().getTime()).then(data => {
			if(data['status'] = 'success'){
				this.snotifyService.success('Deleted Successfully', '', this.getConfig());
				this.addFieldsService.getFields(new Date().getTime()).then(data => {
					if(data['status'] = 'success'){
						//console.log(data['fields']);
						this.showForm = false;
						let datas = [];
						for(let i = 0; i < data['fields'].length; i++){
							let encodeJSON = JSON.parse(data['fields'][i]['fields']);
							let fields = {
								'label': encodeJSON['label'],
								'datatype': encodeJSON['datatype'],
								'length': encodeJSON['length'],
								'mandatory': encodeJSON['mandatory'],
								'validation': encodeJSON['validation'],
								'validationMsg': encodeJSON['validationMsg'],
								'id': data['fields'][i]['id'],
                'datatable': data['fields'][i]['table_name']
							}
							datas.push(fields);
						}
						this.addFieldsDetails = datas;
					}
				});
			}
		});
	}

	addFields(){
		this.addFieldsService.addFields(this.fields, new Date().getTime()).then(data => {
			if(data['status'] = 'success'){
        this.selectLabelForm.reset();
        this.addLabelForm.reset();
				this.snotifyService.success('Field Details Were Added Successfully', '', this.getConfig());
				this.addFieldsService.getFields(new Date().getTime()).then(data => {
					if(data['status'] = 'success'){
						//console.log(data['fields']);
						let datas = [];
						this.showForm = false;
						for(let i = 0; i < data['fields'].length; i++){
							let encodeJSON = JSON.parse(data['fields'][i]['fields']);
							let fields = {
								'label': encodeJSON['label'],
								'datatype': encodeJSON['datatype'],
								'length': encodeJSON['length'],
								'mandatory': encodeJSON['mandatory'],
								'validation': encodeJSON['validation'],
								'validationMsg': encodeJSON['validationMsg'],
								'id': data['fields'][i]['id'],
    						'datatable': data['fields'][i]['table_name']
							}
							datas.push(fields);
						}
						this.addFieldsDetails = datas;
					}
				});
			}
		});
	}

  getColumns(tableName){
    this.addFieldsService.getColumns(tableName).then(data => {
      console.log(data);
    });
  }

  toggle(col) {
    const isChecked = this.isChecked(col);
    console.log('isChecked value - ' + isChecked);
    if(isChecked) {
      this.columns1 = this.columns1.filter(c => {
        console.log('Name - ' + c.name);
        return c.name !== col.name; 
      });
    } else {
      this.columns1 = [...this.columns1, col];
      console.log('columns1 - ' + this.columns1);
    }
  }

  isChecked(col) {
    return this.columns1.find(c => {
      return c.name === col.name;
    });
  }

  getColumns1() {
    this.showCol = !this.showCol;

    // CHANGE THE NAME OF THE BUTTON.
    if(this.showCol)  
      this.showCol = true;
    else
      this.showCol = false;
  }

	getConfig(): SnotifyToastConfig {
		this.snotifyService.setDefaults({
		  global: {
		      newOnTop: this.newTop,
		      maxAtPosition: this.blockMax,
		      maxOnScreen: this.dockMax,
		  }
		});
		return {
		  bodyMaxLength: this.bodyMaxLength,
		  titleMaxLength: this.titleMaxLength,
		  backdrop: this.backdrop,
		  position: this.position,
		  timeout: this.timeout,
		  showProgressBar: this.progressBar,
		  closeOnClick: this.closeClick,
		  pauseOnHover: this.pauseHover
		};
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { WebEngagementService } from '../../shared/data/web-engagement.service';
import { EventMappingService } from '../../shared/data/event-mapping.service';
import { EmailTemplateService } from '../../shared/data/email-template.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

@Component({
  selector: 'app-event-mapping',
  templateUrl: './event-mapping.component.html',
  styleUrls: ['./event-mapping.component.scss']
})
export class EventMappingComponent implements OnInit {
  	@ViewChild('eventMapping') eventMappingForm: NgForm;

	eventMappingModel = {
		'id': '',
	    'template': '',
	    'event': ''
	};
	emailTemplate;
	isCollapsed = true;
	eveMappings;

	timeout = 3000;
	position: SnotifyPosition = SnotifyPosition.centerTop;
	progressBar = true;
	closeClick = true;
	newTop = true;
	backdrop = -1;
	dockMax = 8;
	blockMax = 6;
	pauseHover = true;
	titleMaxLength = 15;
	bodyMaxLength = 80;
	datachange = false;
	selectedTemplateUrl = '';
	htmlContent = '';
	temMappings = [];
	canvasdata = false;
  events;

  constructor(private emailtemplateservice: EmailTemplateService , private snotifyService: SnotifyService,public webengagementservice: WebEngagementService, public eventmappingservice:EventMappingService) { }

	ngOnInit() {
    this.emailtemplateservice.emailEvents().then(data => {
       if(data['status'] == 'success'){
         this.events = data['emailTemplate'];
       }
       else{
         this.events = []
       }
    });
    this.emailtemplateservice.getEmailTemp().then(data => {
      if(data['status'] == 'success'){
        this.emailTemplate = data['emailTemplate'];
      }
      else{
        this.emailTemplate = []
      }
    });
		this.eventmappingservice.getEventMapping().then(data => {
			if(data['status'] == 'success'){
				this.eveMappings = data['eventMapping'];
			}
		});
	}

	onChange(value){
		for(let i=0; i<this.eveMappings.length; i++){
			if(value == this.eveMappings[i]['event']){
				this.snotifyService.success('Event Mapping is already done, if you need to change the event edit the mapping in the table below.', '', this.getConfig());
				this.eventMappingForm.reset();
			}
		}
	}

	onSubmit(){
		this.eventmappingservice.saveMapping(this.eventMappingModel).then(data => {
		  if(data['status'] == 'success'){
		    this.snotifyService.success('Event Mapping was Successfull.', '', this.getConfig());
		    this.eventMappingForm.reset();
		    this.eventMappingModel.id = '';
		    this.isCollapsed = true;
		    this.eventmappingservice.getEventMapping().then(data => {
		      if(data['status'] == 'success'){
		        this.eveMappings = data['eventMapping'];
		      }
		    });
		  }
		});
	}

	editEventMapping(id){
		this.eventmappingservice.editMapping(id).then(data => {
			if(data['status'] == 'success'){
				this.eventMappingModel.id = id;
				this.eventMappingModel.event = data['eventMapping'][0]['event'];
				this.eventMappingModel.template = data['eventMapping'][0]['template'];
				this.isCollapsed = false;
			}
		});
	}

	updateTemMapping(){
		this.eventmappingservice.updateMapping(this.eventMappingModel).then(data => {
			if(data['status'] == 'success'){
				this.snotifyService.success('Event Mapping was updated Successfully.', '', this.getConfig());
				this.eventMappingForm.reset();
				this.eventMappingModel.id = '';
				this.isCollapsed = true;
				this.eventmappingservice.getEventMapping().then(data => {
					if(data['status'] == 'success'){
						this.eveMappings = data['eventMapping'];
					}
				});
			}
		});
	}

	cancel(){
	    this.eventMappingForm.reset();
	    this.eventMappingModel.id = '';
	    this.isCollapsed = true;
	    this.selectedTemplateUrl ='';
	    document.getElementById('imgMessage').innerHTML = '';
	    this.canvasdata = false;
	}

	deleteEventMapping(id){
		this.eventmappingservice.deleteMapping(id).then(data => {
	      if(data['status'] == 'success'){
	        this.snotifyService.success('Event Mapping deleted Successfully.', '', this.getConfig());
	        this.eventmappingservice.getEventMapping().then(data => {
	          if(data['status'] == 'success'){
	            this.eveMappings = data['eventMapping'];
	          }
	        });
	      }
	    });
	}

	getConfig(): SnotifyToastConfig {
	    this.snotifyService.setDefaults({
	        global: {
	            newOnTop: this.newTop,
	            maxAtPosition: this.blockMax,
	            maxOnScreen: this.dockMax,
	        }
	    });
	    return {
	        bodyMaxLength: this.bodyMaxLength,
	        titleMaxLength: this.titleMaxLength,
	        backdrop: this.backdrop,
	        position: this.position,
	        timeout: this.timeout,
	        showProgressBar: this.progressBar,
	        closeOnClick: this.closeClick,
	        pauseOnHover: this.pauseHover
	    };
    }

}

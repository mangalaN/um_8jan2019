import { Component, OnInit,ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { FormControl, FormGroup, FormBuilder,Validators, NgForm } from '@angular/forms';
import { pushcrewService } from '../../shared/data/pushcrew.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';


@Component({
  selector: 'app-pushcrew',
  templateUrl: './pushcrew.component.html',
  styleUrls: ['./pushcrew.component.scss']
})
export class PushcrewComponent implements OnInit {
  // rows = [];
  public imagePath;
 image_url: any;
 public message: string;
 pushcrewForm: FormGroup;


 preview(files) {
   if (files.length === 0)
     return;

   var mimeType = files[0].type;
   if (mimeType.match(/image\/*/) == null) {
     this.message = "Only images are supported.";
     return;
   }

   var reader = new FileReader();
   this.imagePath = files;
   reader.readAsDataURL(files[0]);
   reader.onload = (_event) => {
     this.image_url = reader.result;
   }
 }

 timeout = 3000;
 position: SnotifyPosition = SnotifyPosition.centerTop;
 progressBar = true;
 closeClick = true;
 newTop = true;
 backdrop = -1;
 dockMax = 8;
 blockMax = 6;
 pauseHover = true;
 titleMaxLength = 15;
 bodyMaxLength = 80;
 @ViewChild(DatatableComponent) table: DatatableComponent;


  constructor(private snotifyService: SnotifyService, private formBuilder: FormBuilder,public PushcrewService: pushcrewService) { }

  ngOnInit() {
    this.pushcrewForm = new FormGroup({
      'titles': new FormControl(null,[Validators.required]),
      'title': new FormControl(null,[Validators.required]),
      'description': new FormControl(null,[Validators.required]),
      'disallow_text': new FormControl(null,[Validators.required]),
      'allow_text': new FormControl(null,[Validators.required]),
      'mobile_description': new FormControl(null,[Validators.required]),
      // 'mobile_opt': new FormControl(null,[Validators.required]),
      'image_url': new FormControl(null, null)
    })
  }

savePushcrew(){
  let push={
    titles:this.pushcrewForm.controls['titles'].value,
    title:this.pushcrewForm.controls['title'].value,
    description:this.pushcrewForm.controls['description'].value,
    disallow_text:this.pushcrewForm.controls['disallow_text'].value,
    allow_text:this.pushcrewForm.controls['allow_text'].value,
    mobile_description:this.pushcrewForm.controls['mobile_description'].value,
    // mobile_opt:this.pushcrewForm.controls['mobile_opt'].value,
    image_url:this.pushcrewForm.controls['image_url'].value
  }
  this.snotifyService.success('Saved Successfullly', '', this.getConfig());
  this.pushcrewForm.reset();
    this.image_url='';
  
  this.PushcrewService.savePushcrew(push).then(data => {
    console.log(data);
    if(data['status'] == 'success'){
      //this.rows = data['pushcrew'];
  }
  else{
    // this. rows=[];
  }
});

}

getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
  }
}

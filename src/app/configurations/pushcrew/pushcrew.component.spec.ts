import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PushcrewComponent } from './pushcrew.component';

describe('PushcrewComponent', () => {
  let component: PushcrewComponent;
  let fixture: ComponentFixture<PushcrewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PushcrewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PushcrewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import { TransactionsService } from '../../shared/data/transactions.service';
import swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

//import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
//import { NgxSpinnerService } from 'ngx-spinner';

declare var require: any;
//const data: any = require('../shared/data/company.json');

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {
  p: number = 1;
  totalList;
  previousPage: any;
  pageSize: number = 10;
  collectionSize;

    str = [];
    rows=[];
    IsArray = true;
  columns;
  option;
  title;
  saveTxt = 'Save';
  IsSchedule = false;
  every = 1;
  weekday = 0;
  month = 1;
  day = 1;
  hour = 0;
  minute = 0;
  //dayValues = [0, 1, 2, 3, 4, 5, 6];
  days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
  dayOfMonthValues = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
  //months = ["January", "February","March","April","May","June","July","August","September","October","November","December"];
  months = [
      { name: 'January', value: 1},
      { name: 'February', value: 2},
      { name: 'March', value: 3},
      { name: 'April', value: 4},
      { name: 'May', value: 5},
      { name: 'June', value: 6},
      { name: 'July', value: 7},
      { name: 'August', value: 8},
      { name: 'September', value: 9},
      { name: 'October', value: 10},
      { name: 'November', value: 11},
      { name: 'December', value: 12}
  ];

  minuteValues = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55];
  hourValues = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

  style = 'material';
  // title = 'Snotify title!';
  body = 'Lorem ipsum dolor sit amet!';
  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

// p: number = 1;
  // pageSize: number = 2;
  // collectionSize=0;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('uploadLabel') uploadLabel : ElementRef;

  fileName = '';
  selectedValue = 'local';
  link = null;
  serverVal = null;
  usernameVal = null;
  passwordVal = null;
  headernameVal = null;
  headervalueVal = null;
  files = [];
  //flag=false;
  json;
  selectedTable = 'transaction';

  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  Form: FormGroup;

  constructor(private snotifyService: SnotifyService,
    //private spinnerService: NgxSpinnerService,
    private http: HttpClient, public router: Router,public transactionsservice: TransactionsService) {
    // this.temp = [...data];
    // this.rows = data;
  }

convert(input)
{
  switch (input) {
          case 1:
              return "1st";
          case 2:
              return "2nd";
          case 3:
              return "3rd";
          case 21:
              return "21st";
          case 22:
              return "22nd";
          case 23:
              return "23rd";
          case 31:
              return "31st";
          case null:
              return null;
          default:
              return input + "th";
      }
}
getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
}
SaveTask(task,start_dt=null,end_dt=null)
{
  if(this.title.trim() == '' || this.title == null)
  {
    swal("Error!","Please Enter Title","error");
    return;
  }
  var user_id = localStorage.getItem('currentUserId');
  var parameters;
  if(this.selectedValue == 'local' && task == 'schedule')
  {
      swal("Error!","Schedule cannot be created for Local File.","error");
      return;
  }
  else if(this.selectedValue == 'link')
  {
    let link = this.link;
    if(link == null || link.trim() == "")
    {
      swal("Error!","Please Insert Link","error");
      return;
    }
    parameters = {'link':link};
  }
  else if(this.selectedValue == 'local')
  {
    let link = "Local";
    parameters = {'link':link};
  }
  else if(this.selectedValue == 'ftp')
  {
    if(this.link == null || this.serverVal== null || this.usernameVal==null || this.passwordVal==null || this.link.trim() == "" || this.serverVal.trim() == "" || this.usernameVal.trim() == "" || this.passwordVal.trim() == "")
    {
      swal("Error!","Any FTP Detail should not be emply.","error");
      return;
    }
    parameters = {'link': this.link,'server': this.serverVal,'username': this.usernameVal,'password': this.passwordVal};
  }
  else if(this.selectedValue == 'api')
  {
    if(this.link == null || this.link.trim() == "")
    {
      swal("Error!","Please Insert API URL","error");
      return;
    }
    parameters = {'link':this.link, 'headername': this.headernameVal, 'headervalue': this.headervalueVal};
  }
  var minute1="";
  var hour1="";
  var day1="";
  var month1="";
  var weekday1="";
  if(task == 'schedule')
  {
    if(this.every == 1)
    {
      minute1="*";
      hour1="*";
      day1="*";
      month1="*";
      weekday1="*";
    }
    else if(this.every == 2)
    {
      minute1 =this.minute + "";
      hour1="*";
      day1="*";
      month1="*";
      weekday1="*";
    }
    else if(this.every == 3)
    {
      minute1=this.minute + "";
      hour1=this.hour + "";
      day1="*";
      month1="*";
      weekday1="*";
    }
    else if(this.every == 4)
    {
      minute1=this.minute + "";
      hour1=this.hour + "";
      day1="*";
      month1="*";
      weekday1=this.weekday + "";
    }
    else if(this.every == 5)
    {
      minute1=this.minute + "";
      hour1=this.hour + "";
      day1=this.day + "";
      month1="*";
      weekday1="*";
    }
    else if(this.every == 6)
    {
      minute1=this.minute + "";
      hour1=this.hour + "";
      day1=this.day + "";
      month1=this.month + "";
      weekday1="*";
    }
  }
  let src_cols = [];
  let dest_cols = [];
  for (var j = 0; j < this.columns.length; j++) {
    if((<HTMLInputElement>document.getElementById("c" + j)).value != 'Nothing(skip)')
    {
      src_cols.push(this.columns[j]);
      dest_cols.push((<HTMLInputElement>document.getElementById("c" + j)).value);
    }
  }
  var user_id = localStorage.getItem('currentUserId');

  this.transactionsservice.saveSchedule(user_id,src_cols,dest_cols,this.selectedTable,this.selectedValue,parameters,minute1,hour1,day1,month1,weekday1,this.title,task)
  .then(data => {
    if(task == 'schedule')
      // alert("Schedule Submitted Successfully!");
      this.snotifyService.success('Schedule Submitted Successfully!', '', this.getConfig());
    else
    {
      //alert(start_dt);
      this.transactionsservice.saveImportLog(data['id'],start_dt,end_dt,'Succeeded')
      .then(data => {
        // swal("Success!","Saved Successfully!","success");
        this.snotifyService.success('Saved Successfully!', '', this.getConfig());
      });
    }
    localStorage.setItem("redirect", "Y");
    this.router.navigate(['/importlog']);
  });
}

ngOnInit() {
    //this.users = JSON.parse(localStorage.getItem('currentUser'));
      //console.log(this.users);
      // this.Form = new FormGroup({
      //     'link': new FormControl(null, [Validators.required]),
      //     'server': new FormControl(null, [Validators.required]),
      //     'username': new FormControl(null, [Validators.required]),
      //     'password': new FormControl(null, [Validators.required]),
      //     'headername': new FormControl(null, [Validators.required]),
      //     'headervalue': new FormControl(null, [Validators.required])
      // }, {updateOn: 'blur'});
  }

  select()
  {
    this.saveTxt="Save";
    this.link = null;
    this.serverVal = null;
    this.usernameVal = null;
    this.passwordVal = null;
    this.headernameVal = null;
    this.headervalueVal = null;
    this.json = null;
    // this.link = null;
    // this.serverVal = null;
    // this.usernameVal = null;
    // this.passwordVal = null;
    // this.headernameVal = null;
    // this.headervalueVal = null;
  }
  getCol()
  {
    this.saveTxt = "Save";
    if(this.selectedValue == 'api')
      this.CreateTableFromAPI(this.json);
    else
      this.CreateTableFromJSON(this.json);
  }

  upload(files: File[]){
      //pick from one of the 4 styles of file uploads below
      //this.uploadAndProgress(files);
      //this.settingDetail.companylogo = files[0].name;
      this.uploadLabel.nativeElement.innerHTML = files[0].name;
      this.fileName = files[0].name;
      console.log(this.uploadLabel.nativeElement);
      //alert(files[0].name);
      this.files = files;
      //this.uploadAndProgress(files);
  }

  // uploadAndProgress(files: File[]){
  //   console.log(files)
  //   var formData = new FormData();
  //   Array.from(files).forEach(f => formData.append('file',f))
  //
  //   this.http.post('https:/click365.com.au/usermanagement/uploadCsv.php?fld=csv', formData, {reportProgress: true, observe: 'events'})
  //     .subscribe(event => {
  //       if (event.type === HttpEventType.UploadProgress) {
  //         //this.percentDone = Math.round(100 * event.loaded / event.total);
  //       } else if (event instanceof HttpResponse) {
  //         //this.uploadSuccess = true;
  //       }
  //   });
  // }
  uploadAndProgress(files: File[]){
    console.log(files)
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f))
    var dest_file = new Date().getTime() + ".csv";
    this.http.post('https:/click365.com.au/usermanagement/uploadCsv.php?fld=csv&file=' + dest_file, formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          //this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          //this.uploadSuccess = true;
        }
        if(event.type == 3)
        {
          var path = "uploads/csv/" + dest_file;
          //alert(path);
          this.transactionsservice.getByCsv(path,'local').then(data => {
              this.json = data["data"];
              this.CreateTableFromJSON(data["data"]);
          });
        }
    });
    // this.fileName = file;
    // alert(this.fileName);
  }

  // get f() { return this.Form.controls; }

  import()
  {
    this.IsArray = true;
    this.json = null;
    this.saveTxt = "Save";
      //this.spinnerService.show();
    //this.flag = true;
    if(this.title.trim() == '' || this.title == null)
    {
      swal("Error!","Please Enter Title","error");
      return;
    }
    let link = "";
    if(this.selectedValue == 'local')
    {
      if(this.fileName == '')
      {
        swal("Error!","Please select File","error");
        return;
      }
      this.uploadAndProgress(this.files);

      // link = "uploads/csv/" + this.fileName;
      // if(this.fileName == '')
      // {
      //   alert("Please select File");
      //   return;
      // }
    }
    else if(this.selectedValue == 'link')
    {
      link = this.link;
      if(link == null || link.trim() == "")
      {
        swal("Error!","Please Insert Link","error");
        return;
      }
    }
    //if(this.selectedValue == 'local' || this.selectedValue == 'link')
    if(this.selectedValue == 'link')
    {
      this.transactionsservice.getByCsv(link).then(data => {
        this.json = data["data"];
        this.CreateTableFromJSON(data["data"]);
      });
    }
    else if(this.selectedValue == 'ftp')
    {
      if(this.link == null || this.serverVal== null || this.usernameVal==null || this.passwordVal==null || this.link.trim() == "" || this.serverVal.trim() == "" || this.usernameVal.trim() == "" || this.passwordVal.trim() == "")
      {
        swal("Error!","Any FTP Detail should not be emply.","error");
        return;
      }

        this.transactionsservice.getByFTP(this.link,this.serverVal,this.usernameVal,this.passwordVal).then(data => {
          this.json = data["data"];
          this.CreateTableFromJSON(data["data"]);
      });
    }
    else if(this.selectedValue == 'api')
    {
      if(this.link == null || this.link.trim() == "")
      {
        swal("Error!","Please Insert API URL","error");
        return;
      }
      this.transactionsservice.getByAPI(this.link,this.headernameVal,this.headervalueVal).then(data => {
        this.json = data;
        this.CreateTableFromAPI(data);
        //alert(JSON.stringify(data));
          //this.CreateTableFromJSON(data);
      });
    }
    //this.spinnerService.hide();
}

CreateTableFromAPI1(json) {
    this.json= json;
    let key2, key1;
    var col = [];
    var flag = 0;
    for (key1 in json)
    {
      // flag = 0;
      // for (key2 in key1)
      // {
      //   flag =1;
      //   //key2 = key1;
      //   //alert(key2);
      //   col.push(key1[key2]);
      //   //break;
      // }
      if(flag == 0)
      {
        //alert(key1);
        col.push(key1);
      }
      //break;
    }
    // EXTRACT VALUE FOR HTML HEADER.
        // for (var i = 0; i < json[key2].length; i++) {
        //     for (var key in json[key2][i]) {
        //         if (col.indexOf(key) === -1) {
        //             col.push(key);
        //         }
        //     }
        // }

        // for (var i = 0; i < json.length; i++) {
        //     for (var key in json[i]) {
        //         if (col.indexOf(key) === -1) {
        //             col.push(key);
        //         }
        //     }
        // }

        this.columns = col;
  //alert(JSON.stringify(this.columns));

      // CREATE DYNAMIC TABLE.
      var table = document.createElement("table");

      // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

      var tr = table.insertRow(-1);                   // TABLE ROW.

      for (var i = 0; i < col.length; i++) {
          var th = document.createElement("th");      // TABLE HEADER.
          th.innerHTML = col[i] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
          tr.appendChild(th);
      }
      this.transactionsservice.getColNames(this.selectedTable).then(data => {
        //alert(JSON.stringify(data));
        tr = table.insertRow(-1);

        let start = '';
        this.option = "<option value='Nothing(skip)'>Nothing(skip)</option>";
        let end = '';
        this.str = [];
        for (var j = 0; j < data["cols"].length; j++) {
            this.option = this.option + "<option value='" + data["cols"][j] + "'>" + data["cols"][j] + "</option>";
            this.str.push(data["cols"][j]);
        }
        //alert(JSON.stringify(this.str));

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");
            //th.innerHTML = "<input type='text' value='" + col[i] + "' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            start = "<select id='c" + i + "'>";
            end = "</select>";//"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            th.innerHTML = start + this.option + end;
            // let s = start + option + end;
            // this.str.push(s);// = start + option + end;
            tr.appendChild(th);
            // var divContainer = document.getElementById("s");
            // divContainer.innerHTML = "";
            // divContainer.appendChild(table);
        }
        // ADD JSON DATA TO THE TABLE AS ROWS.
        //var rows=[];
        // for (var i = 0; i < json[key2].length; i++) {
        //
        //     tr = table.insertRow(-1);
        //
        //     for (var j = 0; j < col.length; j++) {
        //         var tabCell = tr.insertCell(-1);
        //         tabCell.innerHTML = json[key2][i][col[j]] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        //         rows.push(json[key2][i][col[j]]);
        //     }
        // }
        // for (var i = 0; i < json.length; i++) {
        //
        //     tr = table.insertRow(-1);
        //
        //     for (var j = 0; j < col.length; j++) {
        //         var tabCell = tr.insertCell(-1);
        //         tabCell.innerHTML = json[i][col[j]] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        //         rows.push(json[i][col[j]]);
        //     }
        // }

        //this.addAttributeInput(json);
        // let key;
        // for (key in json)
        // {
        var rows=[];

        //for (var i = 0; i < json.length; i++) {
      //  var key;
      //  for (key in json){
          tr = table.insertRow(-1);
          var value = "";

          for (var j = 0; j < col.length; j++) {
            // alert("length=" + json[col[j]].length);
            // alert(json[col[j]]);

              if (typeof json[col[j]] == "object") {
                  //this.addAttributeInput(obj[o]);
                  value = "object";
              } else {
                  value = json[col[j]];
              }

              var tabCell = tr.insertCell(-1);
              tabCell.innerHTML = value;
              rows.push(value);
          }
      //  }
// alert(JSON.stringify(rows));


       // }

      //  alert(JSON.stringify(rows));

        // var divContainer = document.getElementById("showData");
        // divContainer.innerHTML = "";
        // divContainer.appendChild(table);
    });


    //     for (var i = 0; i < json.length; i++) {
    //
    //         tr = table.insertRow(-1);
    //
    //         for (var j = 0; j < col.length; j++) {
    //             var tabCell = tr.insertCell(-1);
    //             tabCell.innerHTML = json[i][col[j]] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    //             //this.row.push(json[i][col[j]]);
    //         }
    //     }
    //     // var divContainer = document.getElementById("showData");
    //     // divContainer.innerHTML = "";
    //     // divContainer.appendChild(table);
    // });

              //alert(JSON.stringify(this.row));
      // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
      // var divContainer = document.getElementById("showData");
      // divContainer.innerHTML = "";

//   this.json= json;
//   let key2, key1;
//   var col = [];
//
//   for (key1 in json)
//   {
//     key2 = key1;
//     break;
//   }
//   // EXTRACT VALUE FOR HTML HEADER.
//     {
//       for (var i = 0; i < json[key2].length; i++) {
//           for (var key in json[key2][i]) {
//               if (col.indexOf(key) === -1) {
//                   col.push(key);
//               }
//           }
//       }
//       this.columns = col;
// alert(JSON.stringify(this.columns));
//       // CREATE DYNAMIC TABLE.
//       var table = document.createElement("table");
//
//       // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
//
//       var tr = table.insertRow(-1);                   // TABLE ROW.
//
//       for (var i = 0; i < col.length; i++) {
//           var th = document.createElement("th");      // TABLE HEADER.
//           th.innerHTML = col[i] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//           tr.appendChild(th);
//       }
//       this.transactionsservice.getColNames(this.selectedTable).then(data => {
//         //alert(JSON.stringify(data));
//         tr = table.insertRow(-1);
//
//         let start = '';
//         let option = "<option value='Nothing(skip)'>Nothing(skip)</option>";
//         let end = '';
//         for (var j = 0; j < data["cols"].length; j++) {
//             option = option + "<option value='" + data["cols"][j] + "'>" + data["cols"][j] + "</option>";
//         }
//
//         for (var i = 0; i < col.length; i++) {
//             var th = document.createElement("th");
//             //th.innerHTML = "<input type='text' value='" + col[i] + "' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//             start = "<select id='c" + i + "'>";
//             end = "</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//             th.innerHTML = start + option + end;
//             tr.appendChild(th);
//         }
//
//         // ADD JSON DATA TO THE TABLE AS ROWS.
//         for (var i = 0; i < json[key2].length; i++) {
//
//             tr = table.insertRow(-1);
//
//             for (var j = 0; j < col.length; j++) {
//                 var tabCell = tr.insertCell(-1);
//                 tabCell.innerHTML = json[key2][i][col[j]] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//                 //this.row.push(json[0][i][col[j]]);
//             }
//         }
//         var divContainer = document.getElementById("showData");
//         divContainer.innerHTML = "";
//         divContainer.appendChild(table);
//     });
//
//               //alert(JSON.stringify(this.row));
//       // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
//       // var divContainer = document.getElementById("showData");
//       // divContainer.innerHTML = "";
//       // divContainer.appendChild(table);
  }

addAttributeInput(obj) {
  for (var o in obj) {
    if (typeof obj[o] == "object") {
        this.addAttributeInput(obj[o]);
    } else {
        // alert(obj[o]);
    }
  }
}

  CreateTableFromJSON(json) {
    this.totalList = json.length;
    this.collectionSize = json.length;

    this.json= json;
    //alert(JSON.stringify(json));
    //this.collectionSize = json.length;

   //alert(JSON.stringify(this.json));

        // EXTRACT VALUE FOR HTML HEADER.
        var col = [];
        for (var i = 0; i < json.length; i++) {
            for (var key in json[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }
        this.columns = col;
//alert(JSON.stringify(this.columns));
        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");

        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

        var tr = table.insertRow(-1);                   // TABLE ROW.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            tr.appendChild(th);
        }
        this.transactionsservice.getColNames(this.selectedTable).then(data => {
          //alert(JSON.stringify(data));
          tr = table.insertRow(-1);

          let start = '';
          this.option = "<option value='Nothing(skip)'>Nothing(skip)</option>";
          let end = '';
          this.str = [];
          for (var j = 0; j < data["cols"].length; j++) {
              this.option = this.option + "<option value='" + data["cols"][j] + "'>" + data["cols"][j] + "</option>";
              this.str.push(data["cols"][j]);
          }
          //alert(JSON.stringify(this.str));

          for (var i = 0; i < col.length; i++) {
              var th = document.createElement("th");
              //th.innerHTML = "<input type='text' value='" + col[i] + "' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
              start = "<select id='c" + i + "'>";
              end = "</select>";//"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
              th.innerHTML = start + this.option + end;
              // let s = start + option + end;
              // this.str.push(s);// = start + option + end;
              tr.appendChild(th);
              // var divContainer = document.getElementById("s");
              // divContainer.innerHTML = "";
              // divContainer.appendChild(table);
          }
          // ADD JSON DATA TO THE TABLE AS ROWS.
          for (var i = 0; i < json.length; i++) {

              tr = table.insertRow(-1);

              for (var j = 0; j < col.length; j++) {
                  var tabCell = tr.insertCell(-1);
                  tabCell.innerHTML = json[i][col[j]] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                  //this.row.push(json[i][col[j]]);
              }
          }
          // var divContainer = document.getElementById("showData");
          // divContainer.innerHTML = "";
          // divContainer.appendChild(table);
      });

                //alert(JSON.stringify(this.row));
        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        // var divContainer = document.getElementById("showData");
        // divContainer.innerHTML = "";
        // divContainer.appendChild(table);
    }

    CreateTableFromAPI(json) {
      if (json instanceof Array) {
          // get JSON array
          this.IsArray = true;
      } else {
        this.IsArray = false;
        //Convert json into array
        // var result = [];
        // var keys = Object.keys(json);
        // keys.forEach(function(key){
        //   result.push(json[key]);
        // });
        // json = result;
      }
      this.json= json;
      var col = [];
      if(this.IsArray == false)
      {
        for (key1 in json)
        {
            col.push(key1);
        }
        this.totalList = 1;
        this.collectionSize = 1;
      }
      else
      {
        var rowCnt = 0;
        for (var key1 in json) {
          for (var key2 in json[key1]) {
              col.push(key2);
          }
          break;
        }
      }
      this.columns = col;

    //alert(JSON.stringify(this.columns));
          // CREATE DYNAMIC TABLE.
          var table = document.createElement("table");

          // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

          var tr = table.insertRow(-1);                   // TABLE ROW.

          for (var i = 0; i < col.length; i++) {
              var th = document.createElement("th");      // TABLE HEADER.
              th.innerHTML = col[i] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
              tr.appendChild(th);
          }
          this.transactionsservice.getColNames(this.selectedTable).then(data => {
            //alert(JSON.stringify(data));
            tr = table.insertRow(-1);

            let start = '';
            this.option = "<option value='Nothing(skip)'>Nothing(skip)</option>";
            let end = '';
            this.str = [];
            for (var j = 0; j < data["cols"].length; j++) {
                this.option = this.option + "<option value='" + data["cols"][j] + "'>" + data["cols"][j] + "</option>";
                this.str.push(data["cols"][j]);
            }
            //alert(JSON.stringify(this.str));

            for (var i = 0; i < col.length; i++) {
                var th = document.createElement("th");
                //th.innerHTML = "<input type='text' value='" + col[i] + "' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                start = "<select id='c" + i + "'>";
                end = "</select>";//"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                th.innerHTML = start + this.option + end;
                // let s = start + option + end;
                // this.str.push(s);// = start + option + end;
                tr.appendChild(th);
                // var divContainer = document.getElementById("s");
                // divContainer.innerHTML = "";
                // divContainer.appendChild(table);
            }
            // ADD JSON DATA TO THE TABLE AS ROWS.
          //  var key;
          // for (var i = 0; i < json.length; i++) {
        //  for (key in json)
          //{
                tr = table.insertRow(-1);
                //var value = "";
                //for (var j = 0; j < col.length; j++) {
                if(this.IsArray == true)
                {
                  for (var key1 in json) {
                    rowCnt = rowCnt + 1;
                    for (var key2 in json[key1]) {
                    //  alert(JSON.stringify(json[key1][key2]));
                      if (typeof json[key1][key2] == "object") {
                          //this.addAttributeInput(obj[o]);
                          json[key1][key2] = JSON.stringify(json[key1][key2]);// "object";
                          //value = "object";
                        } else {
                            //value = json[col[j]];
                        }
                     }
                  }
                  this.totalList = rowCnt;
                  this.collectionSize = rowCnt;
                }
                else
                {
                  for (var key1 in json) {
                      if (typeof json[key1] == "object") {
                          //this.addAttributeInput(obj[o]);
                          json[key1] = JSON.stringify(json[key1]);// "object";
                          //value = "object";
                        } else {
                            //value = json[col[j]];
                        }
                     }
                }  // if (typeof json[col[j]] == "object") {
                  //     //this.addAttributeInput(obj[o]);
                  //     json[col[j]] = JSON.stringify(json[col[j]]);// "object";
                  //     //value = "object";
                  // } else {
                  //     //value = json[col[j]];
                  // }
          //        }
        //   }
              //alert(JSON.stringify(this.rows));
        });
      }
//importData;
    save()
    {
      var records = [];
      var start_dt = new Date().toISOString();
      this.saveTxt = "Processing...";
      var row = '';
      var col = '';
      col = this.validate();
      if(col == '')
      {
        this.saveTxt = "Save";
        return;
      }
      // let cnt = 0;
      if(this.selectedValue != "api")
      {
        for (var i = 0; i < this.json.length; i++) {
          row = '';
          for (var j = 0; j < this.columns.length; j++) {
            if((<HTMLInputElement>document.getElementById("c" + j)).value != 'Nothing(skip)')
            //    row = row + ",'" + this.columns[j] + "': '" + this.json[i][this.columns[j]] + "'";
                row = row + ",'" + this.json[i][this.columns[j]] + "'";
                //this.importData = this.json[i];
          }

          row = row.substring(1, row.length);
          records.push(row);
        //alert(JSON.stringify(row));

        //   this.transactionsservice.saveTable(this.selectedTable,col,row).then(data => {
        //     cnt = cnt + 1;
        //   if(cnt == this.json.length)
        //   {
        //     var end_dt = new Date().toISOString();
        //     this.saveTxt = "Saved";
        //     //alert("Saved Successfully!");
        //     this.SaveTask('manual',start_dt,end_dt);
        //   }

        // });
      }
 //alert(JSON.stringify(records));
         
      this.transactionsservice.saveTable(this.selectedTable,col,records).then(data => {
          //   cnt = cnt + 1;
          if(data['status'] = "success")
          {
            var end_dt = new Date().toISOString();
            this.saveTxt = "Saved";
            //alert(data['last_id']);
            this.SaveTask('manual',start_dt,end_dt);
          }
          else
          {
            swal("Error","Failed!","error");
            this.saveTxt = "Failed";
          }

        });
    }
    else
    {
      if(this.IsArray == false)
      {
          var j = 0;
          for (var key1 in this.json)
          {
              if((<HTMLInputElement>document.getElementById("c" + j)).value != 'Nothing(skip)')
              {
                row = row + ",'" + this.json[key1] + "'";
              }
              j = j + 1;
          }

          row = row.substring(1, row.length);
          //alert(JSON.stringify(row));

          this.transactionsservice.saveTable(this.selectedTable,col,row).then(data => {

          if(data['status'] = "success")
          {
            var end_dt = new Date().toISOString();
            this.saveTxt = "Saved";
            //alert("Saved Successfully!");
            this.SaveTask('manual',start_dt,end_dt);
          }
          else
          {
            swal("Error","Failed!","error");
            this.saveTxt = "Failed";
          }
        });
      }
      else
      {
        var j=0;
        //var key1,key2;
        row = '';
        var length = 0;
        for (var key1 in this.json) {
          length = length + 1;
          j=0;
          row='';
          for (var key2 in this.json[key1]) {
            //alert("j=" + j);
            //alert("col=" + (<HTMLInputElement>document.getElementById("c" + j)).value);
            if((<HTMLInputElement>document.getElementById("c" + j)).value != 'Nothing(skip)')
            {
              ///alert("value=" + this.json[key1][key2]);
              row = row + ",'" + this.json[key1][key2] + "'";
            }
            j=j+1;
          }

          row = row.substring(1, row.length);
          //alert(JSON.stringify(row));
          records.push(row);
        
            //   this.transactionsservice.saveTable(this.selectedTable,col,row).then(data => {
            //     cnt = cnt + 1;
            //   if(cnt == length)
            //   {
            //     var end_dt = new Date().toISOString();
            //     this.saveTxt = "Saved";
            //     //alert("Saved Successfully!");
            //     this.SaveTask('manual',start_dt,end_dt);
            //   }

            // });
          }
          //alert(JSON.stringify(records));
          this.transactionsservice.saveTable(this.selectedTable,col,records).then(data => {
              if(data['status'] = "success")
              {
                var end_dt = new Date().toISOString();
                this.saveTxt = "Saved";
                //alert("Saved Successfully!");
                this.SaveTask('manual',start_dt,end_dt);
              }
              else
              {
                swal("Error","Failed!","error");
                this.saveTxt = "Failed";
              }
            });
      }
    }
  }
  
  validate()
  {
      var col = '';

      for (var i1 = 0; i1 < this.columns.length; i1++) {
        if((<HTMLInputElement>document.getElementById("c" + i1)).value != 'Nothing(skip)')
        {
            col = col + ",";// + (<HTMLInputElement>document.getElementById("c" + i1)).value;
            // alert(col);
            // alert("," + (<HTMLInputElement>document.getElementById("c" + i1)).value + ",");
            if(col.includes("," + (<HTMLInputElement>document.getElementById("c" + i1)).value + ","))
            {
              swal("Error!","Same column cannot be selected multiple times.","error");
              col = '';
              return col;
            }
          //col = col + "," + document.getElementById("c" + i).value;
            col = col + (<HTMLInputElement>document.getElementById("c" + i1)).value;
        }
      }
      col = col.substring(1, col.length);
      // alert(col);
      if(col == '')
      {
        swal("Error!","Please select atleast one column","error");
        return col;
      }
      return col;
    }
    schedule()
    {
      let col = '';
      col = this.validate();
      if(col != '')
      {
        this.IsSchedule = true;
      }
  }
}
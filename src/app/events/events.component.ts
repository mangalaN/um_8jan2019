import { Component, Input, ViewChild, NgZone, OnInit, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm, AbstractControl } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Router, ActivatedRoute } from '@angular/router';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import swal from 'sweetalert2';
import { EventsService } from '../shared/data/events.service';

import { MapsAPILoader, AgmMap } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services';

import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import {NgbTabset} from "@ng-bootstrap/ng-bootstrap";
import { NgbDateStruct, NgbDatepickerI18n, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';

declare var google: any;
declare var require: any;

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

interface Location {
  lat: number;
  lng: number;
  viewport?: Object;
  zoom: number;
  address_level_1?:string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  marker?: Marker;
  address: string;
  event_id:string;
}

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  autocomplete;
  circleRadius:number = 5000; // km
  geocoder:any;
  public location:Location = {
    lat: 0,//31.9505,
    lng: 0,//115.8605,
    marker: {
      lat: 0,//31.9505,
      lng: 0,//115.8605,
      draggable: true
    },
    zoom: 5,
    address:'',
    event_id:''
  };

  @ViewChild(AgmMap) map: AgmMap;
    flag=false;
    SelectedTab = '';
    images: any;
    eventData: any;
    public showTable = true;
    public showForm: boolean = false;
    public submitted: boolean = false;
    public save: any;
    public data: any;
    public status;
    events = {
        'id': '',
        'title': '',
        'description': '',
        'member': false,
        'faq': '',
        'url': '',
        'email': '',
        'st_dt' : null,
        'end_dt': null,
        'st_time' : null,
        'end_time': null
    };
    tickets = {
        'id': '',
        'type': '',
        'description': '',
        'price': '',
        'active': false,
        'no_available': '',
        'event_id' : '',
        'total_tickets' : ''
    };
    ticketData: any;
    ticketcollectionSize;
    totalticket;
    contacts = {
        'id': '',
        'name': '',
        'phone': '',
        'event_id' : ''
    };
    contactData: any;
    contactcollectionSize;
    totalcontact;

    sponsors = {
        'id': '',
        'img': '',
        'website': '',
        'event_id' : ''
    };
    sponsorData: any;
    sponsorcollectionSize;
    totalsponsor;

    p: number = 1;
    totalevent;
    eventTable = true;
    previousPage: any;
    pageSize: number = 10;
    collectionSize; newaddress;
    //eventForm: FormGroup;
    users: any;
    rows;
    count =[];
    columns = [
      { prop: 'name' },
      { prop: 'description' },
      { prop: 'price' },
      { prop: 'no_available'},
      { prop: 'active' }
    ];
    style = 'material';
    title = 'Snotify title!';
    body = 'Lorem ipsum dolor sit amet!';
    timeout = 1000;
    position: SnotifyPosition = SnotifyPosition.centerTop;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;
    dt;
    @ViewChild(DatatableComponent) mydatatable: DatatableComponent;
    @ViewChild("eventForm") eventForm: NgForm;
    @ViewChild("placesRef") placesRef : GooglePlaceDirective;
    @ViewChild("ticketForm") ticketForm: NgForm;
    @ViewChild("locationForm") locationForm: NgForm;
    @ViewChild("contactForm") contactForm: NgForm;
    @ViewChild("sponsorForm") sponsorForm: NgForm;
    @ViewChild('uploadLabel') uploadLabel : ElementRef;
    @ViewChild('uploadLogoLabel') uploadLogoLabel : ElementRef;
    @ViewChild('tabs')
    private tabs:NgbTabset;

    fileName = '';
    files = [];
    isDisabled = true;
    isLogoDisabled = true;
    @ViewChild('f') floatingLabelForm: NgForm;
    @ViewChild('vform') validationForm: FormGroup;
    Form: FormGroup;

    constructor(private http: HttpClient,public mapsApiLoader: MapsAPILoader,
                private zone: NgZone,
                private wrapper: GoogleMapsAPIWrapper
                ,private snotifyService: SnotifyService, public eventsservice: EventsService, public router: Router, private route: ActivatedRoute) {
        //this.events = new Array();
        this.mapsApiLoader = mapsApiLoader;
        this.zone = zone;
        this.wrapper = wrapper;
        this.mapsApiLoader.load().then(() => {
          this.geocoder = new google.maps.Geocoder();
          this.initAutocomplete();
          // this.findLocation("Australia");
          // this.location.address = '';
        });
    }

    isEmail()
    {
        if (this.events.email == null) {
          return false;
        }
        // Assigning the email format regular expression
        let emailPattern = "^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})";
        // alert(this.events.email.match(emailPattern));
        return this.events.email.match(emailPattern);
      }
  isURL(url)
  {
    if(url == null) {
      return false;
    }
    // Assigning the url format regular expression
      let urlPattern = "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
    return url.match(urlPattern);
  }
    upload(files: File[]){
           this.files = files;
           
        var img = new Image();
        var width = 0;
        var height = 0;
        var i=0;
          for(i=0;i<files.length;i++)
                {
          
        img.src = window.URL.createObjectURL( files[i] );

        img.onload = () => {
            width = img.naturalWidth;
            height = img.naturalHeight;
            if(((width/height) != 3/2) || height < 200)
            {
                alert("Image height must be >= 200 px and ratio must be 3:2 (width:height)");
                
                this.uploadLabel.nativeElement.innerHTML = "Choose Logo";
                this.files = null;
                return;
            }

            else
            {
              this.isDisabled = false;
                // this.uploadLabel.nativeElement.innerHTML = files[0].name;
                // this.fileName = files[0].name;
                // var i=0;
                // this.uploadLabel.nativeElement.innerHTML = "";
                // for(i=0;i<files.length;i++)
                // {
                  // alert(files[i].name);
                 // this.uploadLabel.nativeElement.innerHTML = this.uploadLabel.nativeElement.innerHTML + " " + (i+1) + ") " + files[i].name;
                // }
               
            }     
         }
          this.uploadLabel.nativeElement.innerHTML="";
           for(i=0;i<files.length;i++)
                {
                  this.uploadLabel.nativeElement.innerHTML = this.uploadLabel.nativeElement.innerHTML + " " + (i+1) + ") " + files[i].name;
                }
              
      }
      // this.files = files;
      // this.isDisabled = false;
      //   // this.uploadLabel.nativeElement.innerHTML = files[0].name;
      //   // this.fileName = files[0].name;
      //   var i=0;
      //   this.uploadLabel.nativeElement.innerHTML = "";
      //   for(i=0;i<files.length;i++)
      //   {
      //     this.uploadLabel.nativeElement.innerHTML = this.uploadLabel.nativeElement.innerHTML + " " + (i+1) + ") " + files[i].name;
      //   }
      //   console.log(this.uploadLabel.nativeElement);
      //  //this.uploadAndProgress(files);
    }

// validate MM/DD/YYYY
  // static date(c: AbstractControl): { [key: string]: boolean } {
  //   let value = c.value;
  //   if (value && typeof value === "string") {
  //     let match = value.match(/^([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4})$/);
  //     if (!match) {
  //       return {'dateInvalid': true};
  //     }
  //     let date = new Date(`${match[3]}-${match[1]}-${match[2]}`);
  //     if (isNaN(date.getTime())) {
  //       return {'dateInvalid': true};
  //     }
  //   }
  //   return null;
  // }

    uploadImage(str)
    {
      this.uploadAndProgress(this.files,str);
    }
    uploadAndProgress(files: File[],str){
      this.submitted = true;

      console.log(files);
      var i=0;
      for(i=0;i<files.length;i++)
      {
        var formData = new FormData();
        //Array.from(files[i]).forEach(f => formData.append('file',f));
        formData.append('file',files[i]);
        this.fileName = files[i].name;

        var ext=this.fileName.substring(this.fileName.lastIndexOf('.')+1, this.fileName.length);
        var dest_file = new Date().getTime() + "." + ext;
        //alert(dest_file);
        var id=this.events.id;
        this.http.post('https:/click365.com.au/usermanagement/uploadEventImages.php?event_id=' + id + '&action=add&fld=event_images&file=' + dest_file, formData, {reportProgress: true, observe: 'events'})
        .subscribe(event => {
          // alert(JSON.stringify(event));
          if(event && event['status'] == "Sorry, your file is too large."){
            this.snotifyService.success('Sorry, your file dimension is not proper. Recommended Size in 6:18 ratio.', '', this.getConfig());
          }

          // if (event.type === HttpEventType.UploadProgress) {
          //   //this.percentDone = Math.round(100 * event.loaded / event.total);
          // } else if (event instanceof HttpResponse) {
          //   //this.uploadSuccess = true;
          // }

          if(i == files.length - 1)
          {
            this.submitted = false;
            // this.snotifyService.success('Uploaded Successfully!', '', this.getConfig());
            // swal("Success!",'Uploaded Successfully!', 'success');
            swal({
                  type:'success',
                  title: '',
                  html:
                  ''+
                  'Uploaded Successfully!',
                  showCloseButton: false,
                  confirmButtonText: 'Ok',
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  showCancelButton: false,
              }).then(function (dismiss) {
                // this.viewImages();
                  //console.log(JSON.stringify(dismiss));
                  if (dismiss.value && dismiss !== 'cancel') {
                    if(str=='next')
                    {
                      let element: HTMLElement = document.getElementById("sponsors") as HTMLElement;
                      element.click();
                      // this.tabs.select('tickets');
                    }
                    else
                    {
                      let element: HTMLElement = document.getElementById("viewImages") as HTMLElement;
                      // element.addEventListener("click", this.viewImages());
                      element.click();
                    }
                   }
              });
              this.files = null;
          }
      });
    }
}

sponsorTab()
{
  // alert('test');
  this.showTable = false;
  this.showForm = true;
  this.save = true;
  this.sponsors.event_id = this.events.id;
  this.tabs.select('sponsors');
}

viewImages()
{
    this.eventsservice.getimages(this.events.id).then(data1 => {
      // alert(JSON.stringify(data1));

      if(data1['status'] == 'success'){
          this.images = data1['images'];
      }
      else
      {
        this.images = [];
      }
  });
  this.files = null;
  this.isDisabled = true;
  this.uploadLabel.nativeElement.innerHTML = "Choose Image files";
}
    get f() { return this.Form.controls; }

//Below is location code
    initAutocomplete() {
    this.autocomplete = new google.maps.places.Autocomplete(
      document.getElementById('address'), {types: ['geocode'],componentRestrictions: { country: 'AU' }});
    // address fields in the form.
    //this.autocomplete.addListener('place_changed', this.findLocation((<HTMLInputElement>document.getElementById('autocomplete')).value));
    //this.findLocation((<HTMLInputElement>document.getElementById('autocomplete')).value);
    }
    findLocation(address='') {
      if(address=='')
        address = (<HTMLInputElement>document.getElementById('address')).value;

      this.location.address = address;
      if (!this.geocoder) this.geocoder = new google.maps.Geocoder()
      this.geocoder.geocode({
        'address': address
      }, (results, status) => {
        console.log(results);
        if (status == google.maps.GeocoderStatus.OK) {
          for (var i = 0; i < results[0].address_components.length; i++) {
            let types = results[0].address_components[i].types
            console.log(types);
            if (types.indexOf('locality') != -1) {
              this.location.address_level_2 = results[0].address_components[i].long_name
            }
            if (types.indexOf('country') != -1) {
              this.location.address_country = results[0].address_components[i].long_name
            }
            if (types.indexOf('postal_code') != -1) {
              this.location.address_zip = results[0].address_components[i].long_name
            }
            if (types.indexOf('administrative_area_level_1') != -1) {
              this.location.address_state = results[0].address_components[i].long_name
            }
          }
          if (results[0].geometry.location) {
            this.location.lat = results[0].geometry.location.lat();
            this.location.lng = results[0].geometry.location.lng();
            this.location.marker.lat = results[0].geometry.location.lat();
            this.location.marker.lng = results[0].geometry.location.lng();
            this.location.marker.draggable = true;
            this.location.viewport = results[0].geometry.viewport;
          }

          this.map.triggerResize()
        } else {
          //alert("Please enter proper address.");
        }
      })
    }

    findAddressByCoordinates() {
      this.geocoder.geocode({
        'location': {
          lat: this.location.marker.lat,
          lng: this.location.marker.lng
        }
      }, (results, status) => {
        this.decomposeAddressComponents(results);
      })
    }

    decomposeAddressComponents(addressArray) {
      if (addressArray.length == 0) return false;
      let address = addressArray[0].address_components;
      console.log(address);
      for(let element of address) {
        console.log(element);
        if (element.length == 0 && !element['types']) continue
        if (element['types'].indexOf('street_number') > -1) {
          this.location.address_level_1 = element['long_name'];
          continue;
        }
        if (element['types'].indexOf('route') > -1) {
          this.location.address_level_1 += ', ' + element['long_name'];
          continue;
        }
        if (element['types'].indexOf('locality') > -1) {
          this.location.address_level_2 = element['long_name'];
          continue;
        }
        if (element['types'].indexOf('administrative_area_level_1') > -1) {
          this.location.address_state = element['long_name'];
          continue;
        }
        if (element['types'].indexOf('country') > -1) {
          this.location.address_country = element['long_name'];
          continue;
        }
        if (element['types'].indexOf('postal_code') > -1) {
          this.location.address_zip = element['long_name'];
          continue;
        }
      }
    }

    updateOnMap() {
      console.log('updating')
      let full_address:string = this.location.address_level_1 || ""
      if (this.location.address_level_2) full_address = full_address + " " + this.location.address_level_2
      if (this.location.address_state) full_address = full_address + " " + this.location.address_state
      if (this.location.address_country) full_address = full_address + " " + this.location.address_country
      this.findLocation(full_address);
      console.log(full_address);
    }

    circleRadiusInKm() {
      return this.circleRadius / 1000;
    }

    milesToRadius(value) {
       this.circleRadius = value / 0.00062137;
    }

    circleRadiusInMiles() {
      return this.circleRadius * 0.00062137;
    }

    markerDragEnd(m: any, $event: any) {
     this.location.marker.lat = m.coords.lat;
     this.location.marker.lng = m.coords.lng;
     this.findAddressByCoordinates();
    }
//Above is location code

    ngOnInit() {

      this.Form = new FormGroup({ }, {updateOn: 'blur'});

      this.location.marker.draggable = true;

        this.SelectedTab = 'details';
        this.eventTable = true;
        this.eventsservice.getevent(new Date().getTime()).then(data => {
          // alert(JSON.stringify(data));
            if(data['status'] == 'success'){
                this.eventData = data['events'];
                this.collectionSize = data['events'].length;
                this.totalevent = data['events'].length;
            }
        });
    }

    selectData(value){
      this.SelectedTab = value.nextId;

      if(value.nextId == 'location'){
        this.save = true;
        this.location.event_id = this.events.id;
        this.location.address = '';

        this.eventsservice.getlocation(this.events.id).then(data => {
            if(data['status'] == 'success'){
                this.location.address = data['location'][0].address;
                this.save = false;
                this.findLocation(this.location.address);
            }
            else
            {
              this.findLocation("Australia");
              this.location.address = '';
            }
        });
      }
      else if(value.nextId == 'tickets'){
        this.tickets.event_id = this.events.id;
        this.showTable = true;
        this.showForm = false;
      
        this.eventsservice.getticket(this.tickets.event_id).then(data => {
            if(data['status'] == 'success'){
              // alert(JSON.stringify(data));
                this.ticketData = data['tickets'];
                this.ticketcollectionSize = data['tickets'].length;
                this.totalticket = data['tickets'].length;
            }
        });
      }
      else if(value.nextId == 'contacts'){
        // this.contactData = [];
        // this.contactcollectionSize = [];
        // this.totalcontact = [];

        this.contacts.event_id = this.events.id;
        this.showTable = true;
        this.showForm = false;
        this.eventsservice.getcontact(this.contacts.event_id).then(data => {
          //alert(JSON.stringify(data));
            if(data['status'] == 'success'){
                this.contactData = data['contacts'];
                this.contactcollectionSize = data['contacts'].length;
                this.totalcontact = data['contacts'].length;
            }
            else
            {
              // this.contactData = [];
              // this.contactcollectionSize = [];
              // this.totalcontact = [];
            }
        });
      }
      else if(value.nextId == 'images'){
        this.viewImages();
        // this.eventsservice.getimages(this.events.id).then(data => {
        //   //alert(JSON.stringify(data));
        //
        //     if(data['status'] == 'success'){
        //         this.images = data['images'];
        //     }
        // });
      }
      else if(value.nextId == 'sponsors'){
        // this.contactData = [];
        // this.contactcollectionSize = [];
        // this.totalcontact = [];

        this.sponsors.event_id = this.events.id;
        this.showTable = true;
        this.showForm = false;
        this.eventsservice.getsponsor(this.sponsors.event_id).then(data => {
          //alert(JSON.stringify(data));
            if(data['status'] == 'success'){
                this.sponsorData = data['sponsors'];
                this.sponsorcollectionSize = data['sponsors'].length;
                this.totalsponsor = data['sponsors'].length;
            }
            else
            {
              // this.contactData = [];
              // this.contactcollectionSize = [];
              // this.totalcontact = [];
            }
        });
      }
    }

    deleteImg(id)
    {
      let self = this;
      swal({
            title: '',
            html:
            ''+
            'Image will be deleted. Do you want to continue?',
            showCloseButton: true,
            confirmButtonText: 'Ok',
            allowOutsideClick: false,
            allowEscapeKey: false,
            showCancelButton: true,
        }).then(function (dismiss) {
            //console.log(JSON.stringify(dismiss));
            if (dismiss.value && dismiss !== 'cancel') {
              self.eventsservice.deleteImgById(id).then(data => {
                  self.snotifyService.success('Deleted Successfully', '', self.getConfig());
                  for (var i in self.images) {
                  if (self.images[i].id === id) {
                      self.images.splice(i, 1);
                    }
                  }

                  // self.eventsservice.getimages(self.images.event_id).then(data => {
                  //     if(data['status'] == 'success'){
                  //       this.images = data['images'];
                  //     }
                  // });
              },
              error => {
              });
            }
        });
    }

    listEvent()
    {
       this.eventTable = true;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
          this.previousPage = page;
          //this.loadData();
        }
    }

    createEvent(){
      this.resetall();
      this.events.id = '';
      this.events.title = '';
      this.events.email = '';
      this.events.description = '';
      this.events.url = '';
      this.events.faq = '';
      this.events.st_dt  = '';
      this.events.end_time  = '';
      this.events.st_time  = '';
      this.events.end_dt  = '';
      this.flag = true;
      this.eventTable = false;
      this.save = true;
    }

    resetall()
    {
      // this.eventForm.reset();
      // this.contactForm.reset();
      // this.locationForm.reset();
      // this.ticketForm.reset();

      this.location.address = '';
      this.images=[];
      // this.eventData=[];
      this.ticketData=[];
      this.ticketcollectionSize==[];
      this.totalticket=[];
      this.contactData=[];
      this.contactcollectionSize=[];
      this.totalcontact=[];
      // this.collectionSize=[];
      this.sponsorData=[];
      this.sponsorcollectionSize=[];
      this.totalsponsor=[];
      
    }

    editevent(id){
      this.resetall();
      this.flag = false;
      this.eventsservice.geteventById(id).then(data => {
        // alert(JSON.stringify(data));
        this.save = false;
        this.eventTable = false;
        this.showForm = false;
        this.showTable = true;//200319
        this.events.id = data['events'][0].id;
        this.events.title = data['events'][0].title;
        this.events.email = data['events'][0].email;
        this.events.description = data['events'][0].description;
        this.events.url = data['events'][0].url;
        this.events.faq = data['events'][0].faq;
        this.events.st_dt  = data['events'][0].st_dt.slice(0, 10);
        this.events.end_dt  = data['events'][0].end_dt.slice(0, 10);
        this.events.st_time  = data['events'][0].st_time;
        this.events.end_time  = data['events'][0].end_time;
      
        if(data['events'][0].member == 1)
          this.events.member = true;
        else
          this.events.member = false;
    },
      error => {
      });
    }

    cancel(){
      this.showTable = true;
      this.showForm = false;
      this.save = true;
      this.eventForm.reset();
      this.events.id = '';
      this.events.title = '';
      this.events.email = '';
      this.events.url = '';
      this.events.description = '';
      this.events.faq = '';
      this.events.end_dt = '';
      this.events.st_dt = '';
      this.events.st_time  = '';
      this.events.end_time  = '';
      this.events.member = false;
    }

    deleteevent(id){
      let self = this;
      swal({
            title: '',
            html:
            ''+
            'Event will be deleted. Do you want to continue?',
            showCloseButton: true,
            confirmButtonText: 'Ok',
            allowOutsideClick: false,
            allowEscapeKey: false,
            showCancelButton: true,
        }).then(function (dismiss) {
            //console.log(JSON.stringify(dismiss));
            if (dismiss.value && dismiss !== 'cancel') {
              self.eventsservice.deleteeventById(id).then(data => {
                  self.snotifyService.success('Deleted Successfully', '', self.getConfig());
                  // window.location.reload();
                  for (var i in self.eventData) {
                  if (self.eventData[i].id === id) {
                      self.eventData.splice(i, 1);
                    }
                  }
                  self.collectionSize = self.eventData.length;
                  self.totalevent = self.eventData.length;

                  // self.eventData = [];
                  // self.collectionSize = [];
                  // self.totalevent = [];
                  // this.eventTable = true;
                  //
                  // self.eventsservice.getevent(new Date().getTime()).then(data => {
                  //     if(data['status'] == 'success'){
                  //         self.eventData = data['events'];
                  //         self.collectionSize = data['events'].length;
                  //         self.totalevent = data['events'].length;
                  //     }
                  // });
              },
              error => {
              });
            }
        });
    }

    insertevent(str){

    let stDate=new Date().toISOString().slice(0,10);
    let endDate=new Date().toISOString().slice(0,10);
    let flag =false;
    
    if(this.events.st_dt != null)
    {
      stDate = this.events.st_dt;
      // endDate = this.toDt['year']+'-'+this.toDt['month']+'-'+this.toDt['day'];
      flag = true;
    }
    else
    {
      flag = false;
    }
    endDate = this.events.end_dt;
    //this.popupModel.dp = stDate;
    if ((stDate > endDate) && flag == true)
    {
       //alert("End Date must be greater than Start Date.");
       swal("Error!","End Date must be greater than Start Date.","error");
        // this.snotifyService.success('End Date must be greater than Start Date.', 'Error', this.getConfig());
       return;
    }
  
   if(this.events.st_time != null)
    {
      flag = true;
    }
    else
    {
      flag = false;
    }
    if ((this.events.st_time > this.events.end_time) && flag == true)
    {
       //alert("End Date must be greater than Start Date.");
       swal("Error!","End Time must be greater than Start Time.","error");
        // this.snotifyService.success('End Date must be greater than Start Date.', 'Error', this.getConfig());
       return;
    }
  
    this.events.title = this.events.title.replace("'","\\'"); 
    this.events.description = this.events.description.replace("'","\\'"); 
    
    //  if(this.events.id != ''){
    this.submitted = true;
        this.eventsservice.addevent(this.events, 'add').then(data => {
          // alert(JSON.stringify(data));

          this.flag = false;
          if(data['status']){
            // this.eventForm.reset();
            this.snotifyService.success('Saved Successfully', '', this.getConfig());
            this.eventData = [];
            this.collectionSize = [];
            this.totalevent = [];
            // alert(JSON.stringify(data));
// alert(data['events']);
            this.events.id = data['events'];

            this.eventsservice.getevent(new Date().getTime()).then(eventdata => {
              // alert(JSON.stringify(eventdata));

              if (eventdata['status'] == 'success') {
                this.eventData = eventdata['events'];
                this.collectionSize = eventdata['events'].length;
                this.totalevent = eventdata['events'].length;
                this.submitted = false;
                if(str!='next')
                {
                  this.showForm = false;
                  this.eventTable = true;
                }
                else
                {
                  this.location.event_id = this.events.id;
                  this.tabs.select('location');
                }
              }
            });
          }
        });
    }

updateevent()
{
    let stDate=new Date().toISOString().slice(0,10);
    let endDate=new Date().toISOString().slice(0,10);
    let flag =false;
    
    if(this.events.st_dt != null)
    {
      stDate = this.events.st_dt;
      // endDate = this.toDt['year']+'-'+this.toDt['month']+'-'+this.toDt['day'];
      flag = true;
    }
    else
    {
      flag = false;
    }
    endDate = this.events.end_dt;
    //this.popupModel.dp = stDate;
    if ((stDate > endDate) && flag == true)
    {
       //alert("End Date must be greater than Start Date.");
       swal("Error!","End Date must be greater than Start Date.","error");
        // this.snotifyService.success('End Date must be greater than Start Date.', 'Error', this.getConfig());
       return;
    }

    if(this.events.st_time != null)
    {
      flag = true;
    }
    else
    {
      flag = false;
    }
    if ((this.events.st_time > this.events.end_time) && flag == true)
    {
       //alert("End Date must be greater than Start Date.");
       swal("Error!","End Time must be greater than Start Time.","error");
        // this.snotifyService.success('End Date must be greater than Start Date.', 'Error', this.getConfig());
       return;
    }
  
    this.submitted = true;
// alert(JSON.stringify(this.events));

  this.eventsservice.addevent(this.events, 'update').then(data => {
    if(data['status']){
      this.submitted = false;

      this.eventForm.reset();
      this.snotifyService.success('Updated Successfully', '', this.getConfig());
      this.eventData = [];
      this.collectionSize = [];
      this.totalevent = [];
      this.eventsservice.getevent(new Date().getTime()).then(eventdata => {
        if (eventdata['status'] == 'success') {
            this.eventData = eventdata['events'];
            this.collectionSize = eventdata['events'].length;
            this.totalevent = eventdata['events'].length;
            this.submitted = false;
            this.showForm = false;
            this.eventTable = true;
        }
      });
    }
  });

}

cancellocation(){
  this.showTable = true;
  this.showForm = false;
  this.save = true;
  this.locationForm.reset();
  this.location.address='';
  this.eventTable = true;
}
insertlocation(str){
//alert(JSON.stringify(this.location));
this.submitted = true;

  this.location.address = (<HTMLInputElement>document.getElementById('address')).value;
    this.eventsservice.addlocation(this.location, 'add').then(data => {
      if(data['status']){
        this.submitted = false;

        // this.eventTable = true;
        // this.showForm = false;
        this.locationForm.reset();
        this.snotifyService.success('Saved Successfully', '', this.getConfig());
        if(str!='next')
        {
          this.showForm = false;
          this.eventTable = true;
        }
        else
        {
          this.showTable = false;
          this.showForm = true;
          this.save = true;
          this.contacts.event_id = this.events.id;
          this.tabs.select('contacts');
        }
      }
    });
}

updatelocation()
{
  this.submitted = true;

  this.location.address = (<HTMLInputElement>document.getElementById('address')).value;

this.eventsservice.addlocation(this.location, 'update').then(data => {
if(data['status']){
  this.submitted = false;

  // this.eventTable = true;
  // this.showForm = false;

    this.locationForm.reset();
    this.snotifyService.success('Updated Successfully', '', this.getConfig());
    // this.tabs.select("contacts");
  }
});

}
    isNumberKey(evt) {
      let charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
          return false;
      }
      return true;
    }

    addticket(){
            // this.ticketForm.reset();
            this.tickets.id = '';
            this.tickets.type = '';
            this.tickets.description = '';
            this.tickets.price = null;
            this.tickets.no_available = null;
            this.tickets.total_tickets = null;
            this.tickets.active = false;

            this.showTable = false;
            this.showForm = true;
            this.save = true;
        }

        editticket(id){
          this.eventsservice.getticketById(id).then(data => {
            //alert(JSON.stringify(data));
            this.save = false;
            this.showTable = false;
            this.showForm = true;
            this.tickets.id = data['tickets'][0].id;
            this.tickets.type = data['tickets'][0].type;
            this.tickets.price = data['tickets'][0].price;
            this.tickets.no_available = data['tickets'][0].no_available;
            this.tickets.total_tickets = data['tickets'][0].total_tickets;
            if(data['tickets'][0].active == 1)
              this.tickets.active = true;
            else
              this.tickets.active = false;
            this.tickets.description = data['tickets'][0].description;
            this.tickets.event_id = data['tickets'][0].event_id;

          },
          error => {
          });
        }


        cancelticket(){
          this.showTable = true;
          this.showForm = false;
          this.save = true;
          this.ticketForm.reset();
          this.tickets.id = '';
          this.tickets.type = '';
          this.tickets.price = '';
          this.tickets.no_available = '';
          this.tickets.total_tickets = '';
          this.tickets.active = false;
          this.tickets.description = '';
          this.tickets.event_id = '';

        }


        deleteticket(id){
          let self = this;
          swal({
                title: '',
                html:
                ''+
                'Event Ticket will be deleted. Do you want to continue?',
                showCloseButton: true,
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCancelButton: true,
            }).then(function (dismiss) {
                //console.log(JSON.stringify(dismiss));
                if (dismiss.value && dismiss !== 'cancel') {
                  self.eventsservice.deleteticketById(id).then(data => {
                      self.snotifyService.success('Deleted Successfully', '', self.getConfig());
                      // alert(JSON.stringify(self.ticketData));

                      for (var i in self.ticketData) {
                      if (self.ticketData[i].id === id) {
                          self.ticketData.splice(i, 1);
                        }
                      }
                      self.ticketcollectionSize = self.ticketData.length;
                      self.totalticket = self.ticketData.length;

                      // self.ticketData = [];
                      // self.ticketcollectionSize = [];
                      // self.totalticket = [];
                      // self.eventsservice.getticket(this.tickets.event_id).then(data => {
                      //     if(data['status'] == 'success'){
                      //         self.ticketData = data['tickets'];
                      //         self.ticketcollectionSize = data['tickets'].length;
                      //         self.totalticket = data['tickets'].length;
                      //     }
                      // });
                  },
                  error => {
                  });
                }
            });
        }

        updateticket(){
          if(parseInt(this.tickets.price) < 0)
          {
            swal("Error!","Must be valid Price","error");
            return;
          }

          if(parseInt(this.tickets.no_available) < 0)
          {
            swal("Error!","No. of Tickets Available must be Valid","error");
            return;
          }

          this.submitted = true;

            this.eventsservice.addticket(this.tickets, 'update').then(data => {
              this.submitted = false;

              if(data['status']){
                this.ticketForm.reset();
                this.snotifyService.success('Updated Successfully', '', this.getConfig());
                this.ticketData = [];
                this.ticketcollectionSize = [];
                this.totalticket = [];

                this.showTable = true;
                this.showForm = false;
                this.save = false;

                // this.eventsservice.getticket(this.tickets.event_id).then(data => {
                //     if(data['status'] == 'success'){
                        this.ticketData = data['tickets'];
                        this.ticketcollectionSize = data['tickets'].length;
                        this.totalticket = data['tickets'].length;
                        this.submitted = false;
                //     }
                // });

              }
            });
}
insertticket()
{
  if(parseInt(this.tickets.price) < 0)
  {
    swal("Error!","Must be valid Price","error");
    return;
  }

  if(parseInt(this.tickets.no_available) < 0)
  {
    swal("Error!","No. of Tickets Available must be Valid","error");
    return;
  }

  this.submitted = true;

  this.eventsservice.addticket(this.tickets, 'add').then(data => {
    this.showTable = true;
    this.showForm = false;
    this.save = false;

    this.submitted = false;
    // alert(JSON.stringify(data));

    if(data['status']){

      this.ticketForm.reset();
      this.snotifyService.success('Saved Successfully', '', this.getConfig());
      this.ticketData = [];
      this.ticketcollectionSize = [];
      this.totalticket = [];
      // this.eventsservice.getticket(this.tickets.event_id).then(data1 => {
// alert(JSON.stringify(data1));
          // if(data1['status'] == 'success'){
              this.ticketData = data['tickets'];
              this.ticketcollectionSize = data['tickets'].length;
              this.totalticket = data['tickets'].length;
              this.submitted = false;
          // }
      // });

    }
  });
}

active(val)
{
  //alert(val);
  if(val == true)
    this.tickets.active = false
  else
    this.tickets.active = true
}
member(val)
{
  //alert(val);
  if(val == true)
    this.events.member = false
  else
    this.events.member = true
}

addcontact(){
        // this.contactForm.reset();
        this.contacts.id = '';
        this.contacts.phone = null;
        this.contacts.name = '';

        this.showTable = false;
        this.showForm = true;
        this.save = true;
    }

    editcontact(id){
      this.eventsservice.getcontactById(id).then(data => {
        //alert(JSON.stringify(data));
        this.save = false;
        this.showTable = false;
        this.showForm = true;
        this.contacts.id = data['contacts'][0].id;
        this.contacts.name = data['contacts'][0].name;
        this.contacts.phone = data['contacts'][0].phone;
        this.contacts.event_id = data['contacts'][0].event_id;

      },
      error => {
      });
    }


    cancelcontact(){
      this.showTable = true;
      this.showForm = false;
      this.save = true;
      this.contactForm.reset();
      this.contacts.id = '';
      this.contacts.name = '';
      this.contacts.phone = '';
      this.contacts.event_id = '';

    }


    deletecontact(id){
      let self = this;
      swal({
            title: '',
            html:
            ''+
            'Contact will be deleted. Do you want to continue?',
            showCloseButton: true,
            confirmButtonText: 'Ok',
            allowOutsideClick: false,
            allowEscapeKey: false,
            showCancelButton: true,
        }).then(function (dismiss) {
            //console.log(JSON.stringify(dismiss));
            if (dismiss.value && dismiss !== 'cancel') {
              self.eventsservice.deletecontactById(id).then(data => {
                  self.snotifyService.success('Deleted Successfully', '', self.getConfig());
                  for (var i in self.contactData) {
                  if (self.contactData[i].id === id) {
                      self.contactData.splice(i, 1);
                    }
                  }
                  self.contactcollectionSize = self.contactData.length;
                  self.totalcontact = self.contactData.length;

                  // self.contactData = [];
                  // self.contactcollectionSize = [];
                  // self.totalcontact = [];
                  // self.eventsservice.getcontact(this.contacts.event_id).then(data => {
                  //     if(data['status'] == 'success'){
                  //         self.contactData = data['contacts'];
                  //         self.contactcollectionSize = data['contacts'].length;
                  //         self.totalcontact = data['contacts'].length;
                  //     }
                  // });
              },
              error => {
              });
            }
        });
    }

    updatecontact(){
      if(parseInt(this.contacts.phone) < 0)
      {
        swal("Error!","Enter Valid Phone Number","error");
        return;
      }

      this.submitted = true;

        this.eventsservice.addcontact(this.contacts, 'update').then(data => {
          this.submitted = false;
// alert(JSON.stringify(data));
          if(data['status']){
            this.contactForm.reset();
            this.snotifyService.success('Updated Successfully', '', this.getConfig());
            this.contactData = [];
            this.contactcollectionSize = [];
            this.totalcontact = [];

            this.showTable = true;
            this.showForm = false;
            this.save = false;

            // this.eventsservice.getcontact(this.contacts.event_id).then(data => {
            //     if(data['status'] == 'success'){
                    this.contactData = data['contacts'];
                    this.contactcollectionSize = data['contacts'].length;
                    this.totalcontact = data['contacts'].length;
                    this.submitted = false;
            //     }
            // });

          }
        });
}

insertcontact(str)
{
  if(parseInt(this.contacts.phone) < 0)
  {
    swal("Error!","Enter Valid Phone Number","error");
    return;
  }
  this.submitted = true;

//  alert(JSON.stringify(this.contacts));
  this.eventsservice.addcontact(this.contacts, 'add').then(data => {
  // this.showTable = true;
  // this.showForm = false;
  this.save = false;
  this.submitted = false;

  if(data['status']){
  // this.contactForm.reset();
  this.snotifyService.success('Saved Successfully', '', this.getConfig());
  this.contactData = [];
  this.contactcollectionSize = [];
  this.totalcontact = [];
  // this.eventsservice.getcontact(this.contacts.event_id).then(data => {
    // alert('test');
      // if(data['status'] == 'success'){
          this.contactData = data['contacts'];
          this.contactcollectionSize = data['contacts'].length;
          this.totalcontact = data['contacts'].length;
          this.submitted = false;
      // }
  // });
        if(str!='next')
        {
          this.showForm = false;
          this.eventTable = true;
        }
        else
        {
          this.images.event_id = this.events.id;
          this.tabs.select('images');
        }
}
});

}

addsponsor(){
        // this.contactForm.reset();
        this.sponsors.id = '';
        this.sponsors.img = null;
        this.sponsors.website = '';

        this.showTable = false;
        this.showForm = true;
        this.save = true;
    }

    editsponsor(id){
      this.eventsservice.getsponsorById(id).then(data => {
        this.isLogoDisabled = false;
        //alert(JSON.stringify(data));
        this.save = false;
        this.showTable = false;
        this.showForm = true;
        this.sponsors.id = data['sponsors'][0].id;
        this.sponsors.img = data['sponsors'][0].img;
        this.sponsors.website = data['sponsors'][0].website;
        this.sponsors.event_id = data['sponsors'][0].event_id;
      },
      error => {
      });
    }


    cancelsponsor(){
      this.showTable = true;
      this.showForm = false;
      this.save = true;
      this.sponsorForm.reset();
      this.sponsors.id = '';
      this.sponsors.img = '';
      this.sponsors.website = '';
      this.sponsors.event_id = '';

    }


    deletesponsor(id){
      let self = this;
      swal({
            title: '',
            html:
            ''+
            'Sponsor will be deleted. Do you want to continue?',
            showCloseButton: true,
            confirmButtonText: 'Ok',
            allowOutsideClick: false,
            allowEscapeKey: false,
            showCancelButton: true,
        }).then(function (dismiss) {
            //console.log(JSON.stringify(dismiss));
            if (dismiss.value && dismiss !== 'cancel') {
              self.eventsservice.deletesponsorById(id).then(data => {
                  self.snotifyService.success('Deleted Successfully', '', self.getConfig());
                  for (var i in self.sponsorData) {
                  if (self.sponsorData[i].id === id) {
                      self.sponsorData.splice(i, 1);
                    }
                  }
                  self.sponsorcollectionSize = self.sponsorData.length;
                  self.totalsponsor = self.sponsorData.length;

                  // self.sponsorData = [];
                  // self.sponsorcollectionSize = [];
                  // self.totalsponsor = [];
                  // self.eventsservice.getsponsor(this.sponsors.event_id).then(data => {
                  //     if(data['status'] == 'success'){
                  //         self.sponsorData = data['sponsors'];
                  //         self.sponsorcollectionSize = data['sponsors'].length;
                  //         self.totalsponsor = data['sponsors'].length;
                  //     }
                  // });
              },
              error => {
              });
            }
        });
    }

  updatesponsor(){
     
    this.submitted = true;
    if(this.files.length > 0)
    {
      
      var ext=this.fileName.substring(this.fileName.lastIndexOf('.')+1, this.fileName.length);
      
      var dest_file = "uploads/event_sponsors/" + new Date().getTime() + "." + ext;
      this.uploadAndProgressLogo(dest_file);
      this.sponsors["img"]=dest_file;
    }
    let website: HTMLInputElement = document.getElementById("website") as HTMLInputElement;
    this.sponsors["website"]=website.value;
        //           
        this.eventsservice.addsponsor(this.sponsors, 'update').then(data => {
          this.submitted = false;
// alert(JSON.stringify(data));
          if(data['status']){
            this.sponsorForm.reset();
            this.snotifyService.success('Updated Successfully', '', this.getConfig());
            this.sponsorData = [];
            this.sponsorcollectionSize = [];
            this.totalsponsor = [];

            this.showTable = true;
            this.showForm = false;
            this.save = false;

            // this.eventsservice.getsponsor(this.sponsors.event_id).then(data => {
            //     if(data['status'] == 'success'){
                    this.sponsorData = data['sponsors'];
                    this.sponsorcollectionSize = data['sponsors'].length;
                    this.totalsponsor = data['sponsors'].length;
                    this.submitted = false;
            //     }
            // });

          }
        });
}

insertsponsor(str)
{
  this.submitted = true;

// var formData = new FormData();
//       formData.append('file',this.files[0]);
//       this.fileName = this.files[0].name;
// alert("filename = " + this.fileName);
      var ext=this.fileName.substring(this.fileName.lastIndexOf('.')+1, this.fileName.length);
      var dest_file = "uploads/event_sponsors/" + new Date().getTime() + "." + ext;
  this.uploadAndProgressLogo(dest_file);

this.sponsors["img"]=dest_file;
let website: HTMLInputElement = document.getElementById("website") as HTMLInputElement;
    this.sponsors["website"]=website.value;

      //alert(dest_file);
//&fld=event_images&file=' + dest_file, formData, {reportProgress: true, observe: 'events'}
//  alert(JSON.stringify(this.sponsors));
  this.eventsservice.addsponsor(this.sponsors, 'add').then(data => {
  // this.showTable = true;
  // this.showForm = false;
  this.save = false;
  this.submitted = false;

  if(data['status']){
  // this.sponsorForm.reset();
  this.snotifyService.success('Saved Successfully', '', this.getConfig());
  this.sponsorData = [];
  this.sponsorcollectionSize = [];
  this.totalsponsor = [];
  // this.eventsservice.getsponsor(this.sponsors.event_id).then(data => {
    // alert('test');
      // if(data['status'] == 'success'){
          this.sponsorData = data['sponsors'];
          this.sponsorcollectionSize = data['sponsors'].length;
          this.totalsponsor = data['sponsors'].length;
          this.submitted = false;
      // }
  // });
        if(str!='next')
        {
          this.showForm = false;
          this.eventTable = true;
        }
        else
        {
          this.tickets.event_id = this.events.id;
          this.tabs.select('tickets');
        }
}
});

}

// getImageDimension (image): Observable<any> {
//     return new Observable(observer => {
//         const img = new Image();
//         img.onload = function (event) {
//             const loadedImage: any = event.currentTarget;
//             image.width = loadedImage.width;
//             image.height = loadedImage.height;
//             observer.next(image);
//             observer.complete();
//         }
//         img.src = image.url;
//     });
// }

uploadLogo(files: File[]){
    // console.log(files[0]);
        var img = new Image();
        var width = 0;
        var height = 0;
    
        img.src = window.URL.createObjectURL( files[0] );

        img.onload = () => {
            width = img.naturalWidth;
            height = img.naturalHeight;
            if(width != 250 || height != 150)
            {
                alert("Your file width is " +width +" & height is " + height+ ". It must be 250 (width) * 150 (height)");
                
                this.uploadLogoLabel.nativeElement.innerHTML = "Choose Logo";
                this.files = null;
            }
            else
            {
              this.fileName = files[0].name;
              this.files = files;
              this.isLogoDisabled = false;
              this.uploadLogoLabel.nativeElement.innerHTML = files[0].name;
              console.log(this.uploadLogoLabel.nativeElement);
            }     
         }
       }

    uploadAndProgressLogo(dest_file){
      this.submitted = true;

      console.log(this.files);
      if(this.files.length > 0)
      {
        var formData = new FormData();
        formData.append('file',this.files[0]);
        this.fileName = this.files[0].name;
  // alert("this.fileName= " + this.fileName);
        // var ext=this.fileName.substring(this.fileName.lastIndexOf('.')+1, this.fileName.length);
        //var dest_file = new Date().getTime() + "." + ext;
        //alert(dest_file);
        this.http.post('https:/click365.com.au/usermanagement/uploadEventSponsorLogo.php?fld=event_sponsors&file=' + dest_file, formData, {reportProgress: true, observe: 'events'})
          .subscribe(event => {
             //alert(JSON.stringify(event));
            // if(event && event['status'] == "Sorry, your file is too large."){
            //   this.snotifyService.success('Sorry, your file dimension is not proper. Recommended Size is 250 * 150.', '', this.getConfig());
            // }
            this.files = null;
        });
    }
}


    getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
    }
}

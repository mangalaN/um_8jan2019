import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EventsRoutingModule } from "./events-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { EventsComponent } from "./events.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { UiSwitchModule } from 'ngx-ui-switch';

import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';

@NgModule({
    imports: [
      AgmCoreModule,

        CommonModule,
        EventsRoutingModule,
        NgxChartsModule,
        NgbModule,
        MatchHeightModule,
        FormsModule,
        ReactiveFormsModule,
        NgxDatatableModule,
        NgxPaginationModule,
        GooglePlaceModule,
        UiSwitchModule
    ],
    declarations: [
        EventsComponent,
   ]
})
export class EventsModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsletterComponent } from "./newsletter/newsletter.component";
import { ReportsComponent } from "./reports/reports.component";
import { EmailTemplateComponent } from "../email-template/email-template.component";
import { AbtestingStatsComponent } from "./abtesting-stats/abtesting-stats.component";
import { FailedCampaignComponent } from "./failed-campaign/failed-campaign.component";
import { EmailAutomationComponent } from './email-automation/email-automation.component';
import { KeysComponent } from './keys/keys.component';
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'newsletter',
        component: NewsletterComponent,
        data: {
          title: 'Newsletter Table'
        }
      },
      {
        path: 'reports',
        component: ReportsComponent,
        data: {
          title: 'Reports Table'
        }
      },
      {
        path: 'emailtemplate',
        component: EmailTemplateComponent,
        data: {
          title: 'Email Template'
        }
      },
      {
        path: 'abtestingStatics',
        component: AbtestingStatsComponent,
        data: {
          title: 'AB Testing Statistics'
        }
      },
      {
        path: 'failedcampaign',
        component: FailedCampaignComponent,
        data: {
          title: 'Campaigns Failed'
        }
      },
      {
       path: 'email-automation',
       component: EmailAutomationComponent,
       data: {
         title: 'Email Automation'
       }
     },
     {
     path: 'keys',
     component: KeysComponent,
     data: {
       title: 'Keys'
     }
   }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampaignRoutingModule { }

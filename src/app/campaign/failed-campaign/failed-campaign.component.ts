import { Component, OnInit, ViewChild } from '@angular/core';


import { newsLetterService } from '../../shared/data/newsletter.service';

@Component({
  selector: 'app-failed-campaign',
  templateUrl: './failed-campaign.component.html',
  styleUrls: ['./failed-campaign.component.scss']
})
export class FailedCampaignComponent implements OnInit {

  campaignDetails;
  abcampaignDetails;

  columns = [
    { name: 'Name' },
    { name: 'Subject' },
    { name: 'Status' },
    { name: 'Date' }
  ];

  adtestingcolumns = [
    { name: 'Name' },
    { name: 'Subject' },
    { name: 'From' },
    { name: 'Campaign Winner Type' },
    { name: 'Campaign A/B Testing On' },
    { name: 'Testing Time' },
    { name: 'Status' }
  ]

  constructor(public newsletterservice: newsLetterService) { }

  ngOnInit() {
    this.newsletterservice.getFailedCampaigns().then(data => {
      if(data['status']){
        this.campaignDetails = data['newsletter']['normal'];
        this.abcampaignDetails = data['newsletter']['abtesting'];
      }
    });
  }
  getName(name){
    if (!name) return '';
    if (name.length <= 20) {
        return name;
    }
    return name.substr(0, 20) + '...';
  }

}

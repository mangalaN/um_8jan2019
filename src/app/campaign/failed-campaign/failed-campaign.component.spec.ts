import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FailedCampaignComponent } from './failed-campaign.component';

describe('FailedCampaignComponent', () => {
  let component: FailedCampaignComponent;
  let fixture: ComponentFixture<FailedCampaignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FailedCampaignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FailedCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

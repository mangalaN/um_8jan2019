import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { EmailTemplateService } from '../../shared/data/email-template.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { ListsService } from '../../shared/data/lists.service';
import { newsLetterService } from '../../shared/data/newsletter.service';
import { EventMappingService } from '../../shared/data/event-mapping.service';

@Component({
  selector: 'app-email-automation',
  templateUrl: './email-automation.component.html',
  styleUrls: ['./email-automation.component.scss']
})
export class EmailAutomationComponent implements OnInit {

 automationForm:FormGroup;
  emailTemplate;
  emailTempData;
  emailAutomationData;
  columns = [
    { name: 'Event' },
    { name: 'Template' },
    { name: 'List' },
    { name: 'Segment' },
    { name: 'Duration' },
    { name: 'Action' }
  ];
   events ;
   control;
//  ContentEditable: boolean = false;
    style = 'material';
    title = 'Success';
    body = 'Organization created successfully!';
    timeout = 3000;
    position: SnotifyPosition = SnotifyPosition.centerTop;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;
    showTable = true;
    lists = [];
    emailAutomation = {
      'duration' : '',
      'span' : '',
      'event' : '',
      'template' : '',
      'list' : '',
      'segment': '',
      'id' : '',
      'select': ''
    };
    segmentDetails = [];
    eveMappings;
    templateSelected = false;

  constructor(private newsletterservice: newsLetterService, public eventmappingservice: EventMappingService, private listsservice: ListsService, private formBuilder: FormBuilder, public emailtemplateservice:EmailTemplateService, private snotifyService: SnotifyService) {
      this.listsservice.getNewsletterList().then(data => {
         if(data['status'] == 'success'){
           this.lists = data['lists'];
         }
         else{
           this.lists = []
         }
      });
      this.emailtemplateservice.emailEvents().then(data => {
         if(data['status'] == 'success'){
           this.events = data['emailTemplate'];
         }
         else{
           this.events = []
         }
      });
      this.emailtemplateservice.getEmailTemp().then(data => {
        if(data['status'] == 'success'){
          this.emailTemplate = data['emailTemplate'];
        }
        else{
          this.emailTemplate = []
        }
      });
      this.emailtemplateservice.getEmailAutomation().then(data => {
        if(data['status'] == 'success'){
          this.emailAutomationData = data['automation'];
        }
        else{
          this.emailAutomationData = []
        }
      });
      this.newsletterservice.getMembersSegment().then(data => {
        if(data['status'] == 'success'){
          this.segmentDetails = data['newsletter'];
        }
        else{
          this.segmentDetails = []
        }
      });
      this.eventmappingservice.getEventMapping().then(data => {
  			if(data['status'] == 'success'){
  				this.eveMappings = data['eventMapping'];
  			}
  		});
   }

  ngOnInit() {
    this.automationForm = new FormGroup({
      'immediately': new FormControl(null, [Validators.required]),
      'segment': new FormControl(null, [Validators.required]),
      'list': new FormControl(null, [Validators.required]),
      'duration': new FormControl(null, [Validators.required]),
      'span': new FormControl(null, [Validators.required]),
      'event': new FormControl(null, [Validators.required]),
      'template': new FormControl(null, [Validators.required]),
      'select': new FormControl(null, [Validators.required])
    });
   }

  deleteEmailAutomation(id){
     this.emailtemplateservice.deleteEmailAutomation(id).then(data => {
       if(data['status'] == 'success'){
         this.emailAutomationData = this.emailAutomationData.filter(h => h.id != id);
         this.snotifyService.success('Automation Deleted Successfully', '', this.getConfig());
       }
     });
   }

  addemailAutomation(){
    this.showTable = false;
  }

  cancel(){
      this.automationForm.reset();
      this.templateSelected = false;
      this.emailAutomation.duration = '';
      this.emailAutomation.span = '';
      this.emailAutomation.event = '';
      this.emailAutomation.template = '';
      this.emailAutomation.list = '';
      this.emailAutomation.segment = '';
      this.emailAutomation.id = '';
      this.emailAutomation.select = '';
      this.showTable = true;
  }

  saveEmailAutomation(){
    if(this.emailAutomation.select == 'list'){
      this.emailAutomation.segment = '';
    }
    else{
      this.emailAutomation.list = '';
    }
    this.emailtemplateservice.addEmailAutomation(this.emailAutomation).then(data => {
      if(data['status'] == 'success'){
        this.emailtemplateservice.getEmailAutomation().then(data => {
          if(data['status'] == 'success'){
            this.emailAutomationData = data['automation'];
          }
          else{
            this.emailAutomationData = []
          }
          this.snotifyService.success('Automation Scheduled Successfully', '', this.getConfig());
          this.automationForm.reset();
          this.emailAutomation.duration = '';
          this.emailAutomation.span = '';
          this.emailAutomation.event = '';
          this.emailAutomation.template = '';
          this.emailAutomation.list = '';
          this.emailAutomation.segment = '';
          this.emailAutomation.id = '';
          this.emailAutomation.select = '';
          this.showTable = true;
        });
      }
    });
  }

  onChange(value){
    for(let i=0; i<this.eveMappings.length; i++){
      if(value == this.eveMappings[i]['event']){
        this.snotifyService.success('Event Mapping is already done, if you need to change the template go to event mapping and edit the mapping in the table given.', '', this.getConfig());
        this.emailAutomation.template = this.eveMappings[i]['template'];
        this.templateSelected = true;
        break;
      }
      else{
        this.emailAutomation.template = '';
        this.templateSelected = false;
      }
    }
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
  }
}

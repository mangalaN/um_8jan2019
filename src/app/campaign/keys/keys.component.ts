import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import { FormControl,FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { EmailTemplateService } from '../../shared/data/email-template.service';


@Component({
  selector: 'app-keys',
  templateUrl: './keys.component.html',
  styleUrls: ['./keys.component.scss']
})


export class KeysComponent implements OnInit {

 inputForm:FormGroup;
  water;
  formData;
  API='';
Secret='';
  constructor(private _FB: FormBuilder,public emailtemplateservice:EmailTemplateService, private http: HttpClient) {

  }

  ngOnInit() {
    this.inputForm = new FormGroup({
          'api': new FormControl(null, [Validators.required]),
          'secret': new FormControl(null, [Validators.required]),
          //'whitelist': new FormControl(null, [Validators.required]),
          'whitelist' : this._FB.array([
            this.initMapFields()
          ]),

      });

        this.emailtemplateservice.apiKeys().then(data =>{
        if(data['status'] == 'success'){
        this.inputForm.controls['secret'].setValue(data['data']);
        }
});
}

        initMapFields() : FormGroup{
        return this._FB.group({
        whitelist     : []
        });
        }


        addSetCondition(){
        const control = <FormArray>this.inputForm.controls.whitelist;
        control.push(this.initMapFields());
        }

        removeSetCondition(i : number) : void{
        const control = <FormArray>this.inputForm.controls.whitelist;
        control.removeAt(i);

        }

        save () {

        this.formData = {
        API: this.inputForm.controls['api'].value,
        Secret: this.inputForm.controls['secret'].value,
        Whitelisted:this.inputForm.controls['whitelist'].value
        }

        this.emailtemplateservice.secretkey(this.formData).then(data => {
        console.log('key - ' + JSON.stringify(data));
        //if(data['status'] == 'success'){

        });
        }



}

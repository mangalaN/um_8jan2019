import { Component, OnInit, ViewChild } from '@angular/core';
import { StatisticsService } from '../../shared/data/statistics.service';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
	public reportCount = 0;
	reportDetails = [];
	columns = [
      { name: 'Status' },
      { name: 'Newsletter Name' },
      { name: 'Newsletter Sent' },
      { name: 'Open Rate' },
      { name: 'Bounce Rate' },
      { name: 'Click Through Rate' },
      { name: 'Last Sent' },
  ];
  abtestingcolumns = [
    { name: 'Name' },
    { name: 'A/B Testing On' },
    { name: 'Type A Open' },
    { name: 'Type B Open' },
    { name: 'Total Sent' },
    { name: 'Test Time' }
  ]

  @ViewChild(DatatableComponent) table: DatatableComponent;
  SelectedTab = '';

  constructor(public statisticsservice: StatisticsService, private router: Router, private route: ActivatedRoute) {
      this.SelectedTab = 'normal';
    }

  	ngOnInit() {
  		this.statisticsservice.getreports().then(data => {
	        console.log(data['reports']);
	        if(data['status'] == 'success'){
		        this.reportCount = data['reports'].length;
		        /*for (let i = 0; i < data['reports'].length; i++) {
		        	let array = {
		        		Status : data['reports'][i]['status'],
						NewsletterName : data['reports'][i]['name'],
						NewsletterSent : data['reports'][i]['sent'],
						OpenRate : data['reports'][i]['openRate'],
						BounceRate : data['reports'][i]['bounceRate'],
						ClickThroughrate : data['reports'][i]['linkRate'],
						LastSent : data['reports'][i]['date']
		        	}
		        	this.reportDetails.push(array);
		        	//this.reportDetails = data['reports'];
		        }*/
				this.reportDetails = data['reports'];
		        //this.rows = this.reportDetails;
		    }
	    },
	    error => {
	    });
  }

  getName(name){
    if (!name) return '';
   if (name.length <= 20) {
        return name;
    }

    return name.substr(0, 20) + '...';
  }

  redirect(id){
	  this.router.navigate(['/statistics'],{queryParams:{id:window.btoa(id+"$reportEdit")}});
  }

  abtestingStats(id){
    let val = ((id * 1080) / 8000);
	  this.router.navigate(['/campaign/abtestingStatics'],{queryParams:{id:window.btoa(val.toString())}});
  }

  selectData(value){
    this.SelectedTab = value.nextId;
    if(value.nextId == 'normal'){
      this.statisticsservice.getreports().then(data => {
          console.log(data['reports']);
          if(data['status'] == 'success'){
            this.reportCount = data['reports'].length;
            this.reportDetails = data['reports'];
          }
      },
      error => {
      });
    }
    else{
      this.statisticsservice.getabtestingReports(new Date().getTime()).then(data => {
          console.log(data['reports']);
          if(data['status'] == 'success'){
            this.reportCount = data['reports'].length;
            this.reportDetails = data['reports'];
          }
      },
      error => {
      });
    }
  }
}

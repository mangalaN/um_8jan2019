import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleCampaignComponent } from './multiple-campaign.component';

describe('MultipleCampaignComponent', () => {
  let component: MultipleCampaignComponent;
  let fixture: ComponentFixture<MultipleCampaignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleCampaignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

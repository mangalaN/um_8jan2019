import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, NgForm } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct, NgbDateStruct, NgbDatepickerI18n, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Router, ActivatedRoute } from "@angular/router";

import { newsLetterService } from '../../shared/data/newsletter.service';
import { ListsService } from '../../shared/data/lists.service';
import { EmailTemplateService } from '../../shared/data/email-template.service';
import { UserService } from '../../shared/data/user.service';


@Component({
  selector: 'app-multiple-campaign',
  templateUrl: './multiple-campaign.component.html',
  styleUrls: ['./multiple-campaign.component.scss']
})
export class MultipleCampaignComponent implements OnInit {
  abTestForm: FormGroup;
  abtest = {
    'name': '',
    'multipleData': '',
    'froma': '',
    'fromb': '',
    'subjecta': '',
    'subjectb': '',
    'membersCol': '',
    'bodya': '',
    'bodyb': '',
    'templatea': '',
    'templateb': '',
    'listid': '',
    'selectedUserA': '',
    //'selectedUserB': '',
    'campaign_type': 'abtesting',
    'rateTrack': '',
    'sendingCampaign': '',
    'testhrs': '',
    'send': 'send',
    'scheduledate': '',
    'time': '',
    'confirm_email': '',
    'segment': '',
    'segmentVal': ''
  };
  showWizard = false;
  public optionsa: Object = {
      charCounterCount: true,
      // Set the image upload parameter.
      imageUploadParam: 'image_param',

      // Set the image upload URL.
      imageUploadURL: 'https://click365.com.au/usermanagement/images',

      // Additional upload params.
      imageUploadParams: {id: 'my_editora'},

      // Set request type.
      imageUploadMethod: 'POST',

      // Set max image size to 5MB.
      imageMaxSize: 5 * 1024 * 1024,

      // Allow to upload PNG and JPG.
      imageAllowedTypes: ['jpeg', 'jpg', 'png'],
      events:  {
      'froalaEditor.initialized':  function () {
        console.log('initialized');
      },
        'froalaEditor.image.beforeUpload':  function  (e,  editor,  images) {
          //Your code
          if  (images.length) {
          // Create a File Reader.
          const  reader  =  new  FileReader();
          // Set the reader to insert images when they are loaded.
          reader.onload  =  (ev)  =>  {
          const  result  =  ev.target['result'];
          editor.image.insert(result,  null,  null,  editor.image.get());
          console.log(ev,  editor.image,  ev.target['result'])
          };
          // Read image as base64.
          reader.readAsDataURL(images[0]);
          }
          // Stop default upload chain.
          return  false;
        }
    }
  };
  public optionsb: Object = {
      charCounterCount: true,
      // Set the image upload parameter.
      imageUploadParam: 'image_param',

      // Set the image upload URL.
      imageUploadURL: 'https://click365.com.au/usermanagement/images',

      // Additional upload params.
      imageUploadParams: {id: 'my_editorb'},

      // Set request type.
      imageUploadMethod: 'POST',

      // Set max image size to 5MB.
      imageMaxSize: 5 * 1024 * 1024,

      // Allow to upload PNG and JPG.
      imageAllowedTypes: ['jpeg', 'jpg', 'png'],
      events:  {
      'froalaEditor.initialized':  function () {
        console.log('initialized');
      },
        'froalaEditor.image.beforeUpload':  function  (e,  editor,  images) {
          //Your code
          if  (images.length) {
          // Create a File Reader.
          const  reader  =  new  FileReader();
          // Set the reader to insert images when they are loaded.
          reader.onload  =  (ev)  =>  {
          const  result  =  ev.target['result'];
          editor.image.insert(result,  null,  null,  editor.image.get());
          console.log(ev,  editor.image,  ev.target['result'])
          };
          // Read image as base64.
          reader.readAsDataURL(images[0]);
          }
          // Stop default upload chain.
          return  false;
        }
    }
  };
  emailTemplates;
  defaultTemplate;
  newsletterId;
  listDetails;
  users;
  memberscols;
  selectedUserA = [];
  selectedUserB = [];
  sendEmail = false;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  hours = ['30min',1,2,3,4,5,6,7,8,9,10,11,12];
  Sendcampaign = false;

  showTable = false;
  newsletterDetails;
  collectionSize;
  newsLetter = {
    'body':''
  };
  segmentDetails = [];
  segmentUserCount = 0;
  minDate;
  submitted = false;

  constructor(private snotifyService: SnotifyService, private router: Router, public newsletterservice: newsLetterService, public userservice: UserService, public listsservice: ListsService, public emailtemplateservice: EmailTemplateService) {
    const current = new Date();
    console.log(current);
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate()
    };
    this.defaultTemplate = this.emailtemplateservice.getDefaultTemplate();
    this.emailtemplateservice.getNewsTemp(new Date().getTime()).then(data => {
        if(data['status'] == 'success'){
          this.emailTemplates = data['emailTemplate'];
        }
    },
    error => {
    });
    this.listsservice.getNewsletterList().then(data => {
        if(data['status'] == 'success'){
          this.listDetails = data['lists'];
        }
        else{
          this.listDetails = '';
        }
    },
    error => {
    });
    this.userservice.getUserrColumns(new Date().getTime()).then(data => {
        if(data['status'] == 'success'){
          console.log(data['userColumn']);
          this.memberscols = data['userColumn'];
        }
        else{
          this.memberscols = '';
        }
    },
    error => {
    });
    this.newsletterservice.getMembersSegment().then(data => {
      if(data['status'] == 'success'){
        this.segmentDetails = data['newsletter'];
      }
      else{
        this.segmentDetails = []
      }
    });
  }
  
  disabletime(control){
    console.log(control);
    const current = new Date();
    if(this.abtest.scheduledate != ''){
      let dates = this.abtest.scheduledate['day']+'/'+this.abtest.scheduledate['month']+'/'+this.abtest.scheduledate['year'];
      let currentdateformat = current.getDate()+'/'+(current.getMonth() + 1)+'/'+current.getFullYear();
      if(currentdateformat == dates){
        if (control.hour < current.getHours()) {
          this.abtest.time = '';
          if (control.minute <= current.getMinutes()) {
            this.abtest.time = '';
          }
        }
      }
    }
  }

  disableTimeByDate(control,type){
    const current = new Date();
    let dates = control.day+'/'+control.month+'/'+control.year;
    let currentdateformat = current.getDate()+'/'+(current.getMonth() + 1)+'/'+current.getFullYear();
    if(this.abtest.time != ''){
      if(currentdateformat == dates){
        if (this.abtest.time['hour'] < current.getHours()) {
          this.abtest.time = '';
          if (this.abtest.time['minute'] <= current.getMinutes()) {
            this.abtest.time = '';
          }
        }
      }
    }
  }

  ngOnInit() {
      this.abTestForm = new FormGroup({
        'multipleData': new FormControl(null, null),
        'froma': new FormControl(null, [Validators.email]),
        'fromb': new FormControl(null, [Validators.email]),
        'subjecta': new FormControl(null, null),
        'subjectb': new FormControl(null, null),
      });
  }

  displayTemplate(value, type){
    for(let i = 0; i < this.emailTemplates.length; i++){
      if(this.emailTemplates[i]['id'] == value){
        this.emailtemplateservice.getEmailTempById(value).then(data => {
          console.log(data['emailTemplate']);
          if(data['status'] == 'success'){
            if(type == 'a'){
              this.abtest.bodya = data['emailTemplate'][0]['body'];
            }
            else{
              this.abtest.bodyb = data['emailTemplate'][0]['body'];
            }
          }
        },
        error => {
        });
      }
      else{
        if(type == 'a'){
          this.abtest.bodya = this.defaultTemplate;
        }
        else{
          this.abtest.bodyb = this.defaultTemplate;
        }
      }
    }
  }

  displayMembers(value, event){
    if(value != ''){
      this.userservice.getUsersSubscriptionList(value, new Date().getTime()).then(data => {
          console.log(data['users']);
          this.users = data['users'].length;
      });
    }
    else{
      this.users = [];
    }
  }

  displaySegmentMembers(value, event){
    if(value != "Select Segment"){
      let condition = this.segmentDetails.filter(h => h.id == value);
      this.newsletterservice.getUserMembersSegment(condition).then(data => {
        if(data['status'] == 'success'){
          if(data['newsletter'][0] == 0){
            this.snotifyService.error('No Users in selected Segment to send Newsletter', '', this.getConfig());
            this.abtest.segmentVal = '';
            this.users = [];
          }
          else{
            this.users = data['newsletter'][0];
          }
        }
        else{
          this.snotifyService.error('No Users in selected Segment to send Newsletter', '', this.getConfig());
          this.abtest.segmentVal = '';
          this.users = [];
        }
      });
    }
  }

  submitCampaign(type){
  this.submitted = true;
    this.newsletterservice.saveabtestingNewletter(this.abtest, new Date().getTime()).then(data => {
      if(data['status']){
          this.newsletterId = data['id'];
          if(type == 'schedule'){
            this.ScheduleNewsletter();
          }
          else{
            this.sendCampaign();
          }
      }
    });
  }

  ScheduleNewsletter(){
  this.submitted = true;
    let sendCampaign = {
        'id': this.newsletterId,
        'send': this.abtest.send,
        'scheduledate': this.abtest.scheduledate,
        'time': this.abtest.time,
        'confirm_email': this.abtest.confirm_email
    };
    this.newsletterservice.ScheduleNewsletter(sendCampaign).then(data => {
      if(data['status']){
        this.snotifyService.success('Campaign Scheduled Successfully', '', this.getConfig());
        this.submitted = false;
        this.router.navigate(['/campaign/reports']);
      }
    });
  }

  sendCampaign(){
  this.submitted = true;
    this.newsletterservice.sendABTestingNewletter(this.newsletterId).then(data => {
        if(data['status']){
          console.log(data['status']);
          let val = ((this.newsletterId * 1080) / 8000);
          let self1 = this;
          let newId = window.btoa(val.toString())
          this.snotifyService.success('Campaign Sent Successfully', '', this.getConfig());
          swal({
                title: '<i class="fab fa-telegram"></i> Queue For immediate delivery',
                html:
                ''+
                '<ul class="list-group">'+
                  '<li class="list-group-item">Campaign Name: '+self1.abtest.name+'</li>'+
                  '<li class="list-group-item">Recepients: '+data['totalsent']+'</li>'+
                  '<li class="list-group-item">Not Received: '+data['notsent']+'</li>'+
                  '<li class="list-group-item">To Be Delivered: Immediately</li>'+
                '</ul>',
                showCloseButton: false,
                confirmButtonText: '<a href="#/campaign/abtestingStatics?id='+newId+'">See Campaign Report</a>',
                allowOutsideClick: true,
                allowEscapeKey: true
            }).then(function (dismiss) {
                // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                if (dismiss.dismiss.toString() == 'overlay' || dismiss.dismiss.toString() == 'esc' || dismiss.dismiss.toString() == 'cancel' || dismiss.dismiss.toString() == 'close' || dismiss.dismiss.toString() == 'timer') {
                  self1.router.navigate(['/campaign/reports']);
                  self1.submitted = false;
                }
                // if (dismiss.dismiss.toString() == 'overlay' || dismiss.dismiss.toString() == 'esc' || dismiss.dismiss.toString() == 'cancel' || dismiss.dismiss.toString() == 'close' || dismiss.dismiss.toString() == 'timer') {
                //   self1.router.navigate(['/campaign/reports']);
                // }
            });
        }
    });
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
  }

  cancel(){
    //alert('cancel');
  }

}

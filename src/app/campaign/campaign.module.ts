import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CampaignRoutingModule } from "./campaign-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { NewsletterComponent } from "./newsletter/newsletter.component";
import { ReportsComponent } from "./reports/reports.component";
import { EmailTemplateComponent } from "../email-template/email-template.component";
import { MultipleCampaignComponent } from "./multiple-campaign/multiple-campaign.component";
import { AbtestingStatsComponent } from "./abtesting-stats/abtesting-stats.component";
import { FailedCampaignComponent } from "./failed-campaign/failed-campaign.component";
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { ChartistModule } from 'ng-chartist';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { UiSwitchModule } from 'ngx-ui-switch';
import { ArchwizardModule } from 'angular-archwizard';
import { AngularHighchartsChartModule } from 'angular-highcharts-chart';
import { EmailAutomationComponent } from './email-automation/email-automation.component';
import { KeysComponent } from './keys/keys.component';
@NgModule({
    imports: [
        CommonModule,
        CampaignRoutingModule,
        Ng2SmartTableModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NgxDatatableModule,
        NgxPaginationModule,
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        UiSwitchModule,
        ArchwizardModule,
        AngularHighchartsChartModule,
        ChartistModule,
        NgxChartsModule,
        MatchHeightModule,
        Ng4LoadingSpinnerModule.forRoot()
    ],
    declarations: [
        NewsletterComponent,
        ReportsComponent,
        EmailTemplateComponent,
        MultipleCampaignComponent,
        AbtestingStatsComponent,
        FailedCampaignComponent,
        EmailAutomationComponent,
        KeysComponent
    ]
})
export class CampaignModule { }

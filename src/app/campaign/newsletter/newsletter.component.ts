import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, NgForm } from '@angular/forms';
import { newsLetterService } from '../../shared/data/newsletter.service';
import { ListsService } from '../../shared/data/lists.service';
import { EmailTemplateService } from '../../shared/data/email-template.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct, NgbDateStruct, NgbDatepickerI18n, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';


@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {

  preview = false;
  width;

  @ViewChild('newsletterForm1') scheduleForm: NgForm;

  columns = [
    { name: 'Name' },
    { name: 'Subject' },
    { name: 'Status' },
    { name: 'Date' }
  ];

 allColumns = [
   { name: 'Name' },
   { name: 'Subject' },
   { name: 'Status' },
   { name: 'Date' }
 ];

 newDisplay = [];

  adtestingcolumns = [
    { name: 'Name' },
    { name: 'Subject A'},
    { name: 'Subject B' },
    { name: 'From A' },
    { name: 'From B' },
    { name: 'Campaign Winner Type' },
    { name: 'Campaign A/B Testing On' },
    { name: 'Testing Time' },
    { name: 'Status' }
  ];

  adtestingallcolumns = [
    { name: 'Name' },
    { name: 'Subject A'},
    { name: 'Subject B' },
    { name: 'From A' },
    { name: 'From B' },
    { name: 'Campaign Winner Type' },
    { name: 'Campaign A/B Testing On' },
    { name: 'Testing Time' },
    { name: 'Status' }
  ];

  newsletterForm: FormGroup;
  // newsletter1Form: FormGroup;
  newsletterDetails = [];
  condition = true;
  showTable = true;
  newsLetter= {
    'name': '',
    'subject': '',
    'listid':'Select List',
    'to': '',
    'cc': '',
    'bcc': '',
    'body': '',
    'template': '',
    'id': '',
    'body1': '',
    'segment': false,
    'segmentVal': ''
  };
  sendCampaign = {
    'id': 0,
    'scheduledate': '',
    'send': 'send',
    'time': '',
    'confirm_email': ''
  }
  collectionSize: number;
  p: number = 1;
  pageSize: number = 5;

  public options: Object = {
      charCounterCount: true,
      // Set the image upload parameter.
      imageUploadParam: 'image_param',

      // Set the image upload URL.
      imageUploadURL: 'https://click365.com.au/usermanagement/images',

      // Additional upload params.
      imageUploadParams: {id: 'my_editor'},

      // Set request type.
      imageUploadMethod: 'POST',

      // Set max image size to 5MB.
      imageMaxSize: 5 * 1024 * 1024,

      // Allow to upload PNG and JPG.
      imageAllowedTypes: ['jpeg', 'jpg', 'png'],
      events:  {
      'froalaEditor.initialized':  function () {
        console.log('initialized');
      },
        'froalaEditor.image.beforeUpload':  function  (e,  editor,  images) {
          //Your code
          if  (images.length) {
          // Create a File Reader.
          const  reader  =  new  FileReader();
          // Set the reader to insert images when they are loaded.
          reader.onload  =  (ev)  =>  {
          const  result  =  ev.target['result'];
          editor.image.insert(result,  null,  null,  editor.image.get());
          console.log(ev,  editor.image,  ev.target['result'])
          };
          // Read image as base64.
          reader.readAsDataURL(images[0]);
          }
          // Stop default upload chain.
          return  false;
        }
    }
  };
  public listDetails: any;
  defaultTemplate;
  emailTemplates;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerCenter;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  selectList = '';
  edit = false;
  newsLetterId = 0;
  SelectedTab = '';
  Sendcampaign = false;
  savedNewsletterId = 0;
  segmentDetails = [];

  isCollapsed = false;
  submitted = false;
  copySubimitted = false;
  minDate;


    constructor(private modalService: NgbModal, private snotifyService: SnotifyService, public newsletterservice: newsLetterService, public listsservice: ListsService, public emailtemplateservice: EmailTemplateService) {
      const current = new Date();
      console.log(current);
      this.minDate = {
        year: current.getFullYear(),
        month: current.getMonth() + 1,
        day: current.getDate()
      };
      this.newsLetter.body = this.emailtemplateservice.getDefaultTemplate();
      this.defaultTemplate = this.emailtemplateservice.getDefaultTemplate();
      this.SelectedTab = 'normal';
      this.newsletterservice.getNewletter().then(data => {
        if(data['status']){
          console.log(data['newsletter']);
          this.newsletterDetails = data['newsletter'];
          this.collectionSize = data['newsletter'].length;
        }
      });
      this.listsservice.getNewsletterList().then(data => {
          if(data['status'] == 'success'){
            this.listDetails = data['lists'];
            this.selectList = data['lists'][0]['id'];
            this.newsLetter.listid = data['lists'][0]['id'];
            if(this.newsLetter.listid == 'Select List'){
              this.condition = false;
            }
            else{
              this.condition = true;
            }
          }
          else{
            this.condition = false;
            this.newsLetter.listid = 'Select List'
          }
      },
      error => {
      });
      this.newsletterservice.getMembersSegment().then(data => {
        if(data['status'] == 'success'){
          this.segmentDetails = data['newsletter'];
        }
        else{
          this.segmentDetails = []
        }
      });
      this.emailtemplateservice.getNewsTemp(new Date().getTime()).then(data => {
          if(data['status'] == 'success'){
            this.emailTemplates = data['emailTemplate'];
            //console.log(this.emailTemplates);
          }
      },
      error => {
      });
    }
    
    disabletime(control){
      console.log(control);
      const current = new Date();
        if(this.sendCampaign.scheduledate != ''){
          let dates = this.sendCampaign.scheduledate['day']+'/'+this.sendCampaign.scheduledate['month']+'/'+this.sendCampaign.scheduledate['year'];
          let currentdateformat = current.getDate()+'/'+(current.getMonth() + 1)+'/'+current.getFullYear();
          if(currentdateformat == dates){
            if (control.hour < current.getHours()) {
              this.sendCampaign.time = '';
              if (control.minute <= current.getMinutes()) {
                this.sendCampaign.time = '';
              }
            }
          }
        }
    }
    disableTimeByDate(control){
      const current = new Date();
      let dates = control.day+'/'+control.month+'/'+control.year;
      let currentdateformat = current.getDate()+'/'+(current.getMonth() + 1)+'/'+current.getFullYear();
        if(this.sendCampaign.time != ''){
          if(currentdateformat == dates){
            if (this.sendCampaign.time['hour'] < current.getHours()) {
              this.sendCampaign.time = '';
              if (this.sendCampaign.time['minute'] <= current.getMinutes()) {
                this.sendCampaign.time = '';
              }
            }
          }
        }
    }

    toggle(col) {
      const isChecked = this.isChecked(col);
      if(isChecked) {
        this.columns = this.columns.filter(c => {
          return c.name !== col.name;
        });
      } else {
        const newColumns = [...this.columns, col];
        this.columns = [];
        this.allColumns.forEach((f) => {
          newColumns.forEach((s) => {
            if ( s.name === f.name){
              this.columns.push(f);
            }
          });
       });
      }
    }

    isChecked(col) {
      return this.columns.find(c => {
        return c.name === col.name;
      });
    }

    toggleabtesting(col) {
      const isCheckedabtesting = this.isCheckedabtesting(col);
      if(isCheckedabtesting) {
        this.adtestingcolumns = this.adtestingcolumns.filter(c => {
          return c.name !== col.name;
        });
      } else {
        const newColumns = [...this.adtestingcolumns, col];
        this.adtestingcolumns = [];
        this.adtestingallcolumns.forEach((f) => {
          newColumns.forEach((s) => {
            if ( s.name === f.name){
              this.adtestingcolumns.push(f);
            }
          });
       });
      }
    }

    isCheckedabtesting(col) {
      return this.adtestingcolumns.find(c => {
        return c.name === col.name;
      });
    }

    getName(name){
      if (!name) return '';
     if (name.length <= 20) {
          return name;
      }
      return name.substr(0, 20) + '...';
    }

    ngOnInit() {
      this.newsletterForm = new FormGroup({
        'name' : new FormControl(null, [Validators.required]),
        'subject' : new FormControl(null, [Validators.required]),
        'listid' : new FormControl(null, null),
        'to' : new FormControl(null, [this.commaSepEmail]),
        'cc' : new FormControl(null, [this.commaSepEmail]),
        'bcc' : new FormControl(null, [this.commaSepEmail]),
        'template' : new FormControl(null, null),
        'body' : new FormControl(null, null),
        'segment' : new FormControl(null, null),
        'segmentVal' : new FormControl(null, null)
      });
    }

    commaSepEmail = (control: AbstractControl): { [key: string]: any } | null => {
      if(control.value != '' && control.value != null){
        const emails = control.value.split(',');
        const forbidden = emails.some(email => Validators.email(new FormControl(email)));
        console.log(forbidden);
        return forbidden ? { 'to': { value: control.value } } : null;
      }
    };

    saveNewsletter(type){
      this.submitted = true;
      this.savedNewsletterId = 0;
      this.newsletterservice.saveNewletter(this.newsLetter).then(data => {
        if(data['status']){
          this.savedNewsletterId = data['id'];
          if(type == 'schedule'){
            this.ScheduleNewsletter();
          }
          else{
            this.sendnewsletter(this.savedNewsletterId);
          }
          }
      });
    }

    ScheduleNewsletter(){
      if(this.savedNewsletterId > 0){
        this.sendCampaign.id = this.savedNewsletterId;
      }
      else{
        this.sendCampaign.id = this.newsLetterId;
      }
      this.newsletterservice.ScheduleNewsletter(this.sendCampaign).then(data => {
        if(data['status']){
          this.newsLetter.name = '';
          this.newsLetter.subject = '';
          this.newsLetter.listid = '';
          this.newsLetter.to = '';
          this.newsLetter.cc = '';
          this.newsLetter.bcc = '';
          this.newsLetter.template = '';
          this.newsLetter.segment = false;
          this.newsLetter.segmentVal = '';
          this.newsLetter.body = this.defaultTemplate;
          this.scheduleForm.reset();
          this.savedNewsletterId = 0;
          if(!this.edit){
            this.snotifyService.success('Campaign Saved & Scheduled Successfully', '', this.getConfig());
          }
          else{
            this.snotifyService.success('Campaign Updated & Scheduled Successfully', '', this.getConfig());
          }
          this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
            if(data['status']){
              this.newsletterDetails = data['newsletter'];
              this.collectionSize = data['newsletter'].length;
              this.newsLetter.body = this.defaultTemplate;
              this.showTable = true;
              this.submitted = false;
            }
          });
        }
      });
    }

    sendnewsletter(id){
      this.newsletterservice.sendNewletter(id).then(data => {
        if(data['status']){
          this.savedNewsletterId = 0;
          if(!this.edit){
            this.snotifyService.success('Campaign Saved & Sent Successfully', '', this.getConfig());
          }
          else{
            this.snotifyService.success('Campaign Updated & Sent Successfully', '', this.getConfig());
          }
          let self = this;
          let newid = window.btoa(id+"$reportEdit");
          swal({
                title: '<i class="fab fa-telegram"></i> Queue For immediate delivery',
                html:
                ''+
                '<ul class="list-group">'+
                  '<li class="list-group-item">Campaign Name: '+this.newsLetter.name+'</li>'+
                  '<li class="list-group-item">Recepients: '+data['totalsent']+'</li>'+
                  '<li class="list-group-item">Not Received: '+data['notsent']+'</li>'+
                  '<li class="list-group-item">To Be Delivered: Immediately</li>'+
                '</ul>',
                showCloseButton: false,
                confirmButtonText: '<a href="#/statistics?id='+newid+'">See Campaign Report</a>',
                allowOutsideClick: true,
                allowEscapeKey: true
            }).then(function (dismiss) {
                // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                if (dismiss.dismiss.toString() == 'overlay' || dismiss.dismiss.toString() == 'esc' || dismiss.dismiss.toString() == 'cancel' || dismiss.dismiss.toString() == 'close' || dismiss.dismiss.toString() == 'timer') {
                  self.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
                    if(data['status']){
                      self.newsLetter.name = '';
                      self.newsLetter.subject = '';
                      self.newsLetter.listid = '';
                      self.newsLetter.to = '';
                      self.newsLetter.cc = '';
                      self.newsLetter.bcc = '';
                      self.newsLetter.template = '';
                      self.newsLetter.body = self.defaultTemplate;
                      self.newsLetter.segment = false;
                      self.newsLetter.segmentVal = '';
                      self.showTable = true;
                      self.newsletterDetails = data['newsletter'];
                      self.collectionSize = data['newsletter'].length;
                      self.newsLetter.body = self.defaultTemplate;
                      self.submitted = false;
                    }
                  });
                }
            });
        }
      });
    }

    checkvalue(val){
      if(val != "Select List"){
        this.newsLetter.to = '';
        this.condition = true;
      }
      else{
        this.condition = false;
      }
    }

    checkMemberExsits(val){
      if(val != "Select Segment"){
        // this.selectList = 'Select List';
        this.newsLetter.to = '';
        this.condition = false;
        let condition = this.segmentDetails.filter(h => h.id == val);
        this.newsletterservice.getUserMembersSegment(condition).then(data => {
          if(data['status'] == 'success'){
            if(data['newsletter'] == 0){
              this.snotifyService.error('No Users in selected Segment to send Newsletter', '', this.getConfig());
              this.newsLetter.segmentVal = '';
            }
          }
          else{
            this.snotifyService.error('No Users in selected Segment to send Newsletter', '', this.getConfig());
            this.newsLetter.segmentVal = '';
          }
        });
      }
    }

    addnews(){
      this.showTable = false;
    }

    sendtestmail(){
      this.newsletterservice.sendTestEmail(this.newsLetter).then(data => {
        if(data['status']){
            this.snotifyService.success('Test Email Sent Successfully', '', this.getConfig());
        }
      });
    }

    cancel(){
      this.showTable = true;
      this.edit = false;
      this.newsLetter.name = '';
      this.newsLetter.subject = '';
      this.newsLetter.to = '';
      this.newsLetter.cc = '';
      this.newsLetter.bcc = '';
      this.newsLetter.template = '';
      this.newsLetter.body = this.defaultTemplate;
      this.sendCampaign.scheduledate = '';
      this.sendCampaign.send = '';
      this.sendCampaign.time = '';
      this.sendCampaign.confirm_email = '';
      this.savedNewsletterId = 0;
      this.newsLetter.segment = false;
      this.newsLetter.segmentVal = '';
      this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
        if(data['status']){
          this.newsletterDetails = data['newsletter'];
          this.collectionSize = data['newsletter'].length;
          this.newsLetter.body = this.defaultTemplate;
        }
      });
    }

    displayTemplate(value){
      for(let i = 0; i < this.emailTemplates.length; i++){
        if(this.emailTemplates[i]['id'] == value){
          this.emailtemplateservice.getEmailTempById(value).then(data => {
              //console.log(data['emailTemplate']);
              if(data['status'] == 'success'){
                this.newsLetter.body = data['emailTemplate'][0]['body'];
                //console.log(this.emailTemplates);
              }
          },
          error => {
          });
        }
        else{
          this.newsLetter.body =  this.defaultTemplate;
        }
      }
    }

    EditNewsletter(id){
      let newsletterEditDetails = this.newsletterDetails.filter(h => h.id == id);
      this.newsLetter.name = newsletterEditDetails[0]['name'];
      this.newsLetter.subject = newsletterEditDetails[0]['subject'];
      this.newsLetter.listid = newsletterEditDetails[0]['listid'];
      this.newsLetter.segment = newsletterEditDetails[0]['segment'];
      if(newsletterEditDetails[0]['segment']){
        this.newsLetter.segmentVal = newsletterEditDetails[0]['listid'];
      }
      else{
        if(newsletterEditDetails[0]['listid'] == "0"){
          this.selectList = 'Select List';
          this.condition = false;
        }
        else{
          this.condition = true;
          this.selectList = newsletterEditDetails[0]['listid'];
        }
      }
      this.newsLetter.to = newsletterEditDetails[0]['to'];
      this.newsLetter.cc = newsletterEditDetails[0]['cc'];
      this.newsLetter.bcc = newsletterEditDetails[0]['bcc'];
      if(newsletterEditDetails[0]['template'] == "0"){
        this.newsLetter.template  = 'Choose Template';
      }
      else{
        this.newsLetter.template  = newsletterEditDetails[0]['template'];
      }
      this.sendCampaign.scheduledate = newsletterEditDetails[0]['scheduledate'];
      this.sendCampaign.send = newsletterEditDetails[0]['send'];
      this.sendCampaign.time = newsletterEditDetails[0]['scheduletime'];
      this.sendCampaign.confirm_email = newsletterEditDetails[0]['confirm_email'];

      //this.newsLetter.template = newsletterEditDetails[0]['template'];
      this.newsLetter.body = newsletterEditDetails[0]['message'];
      this.newsLetterId = id;
      this.showTable = false;
      this.edit = true;
      this.savedNewsletterId = 0;
    }

    updateNewsletter(type){
      this.submitted = true;
      let updateNewsLetter = {
        'name': this.newsLetter.name,
        'subject': this.newsLetter.subject,
        'listid':this.newsLetter.listid,
        'to': this.newsLetter.to,
        'cc': this.newsLetter.cc,
        'bcc': this.newsLetter.bcc,
        'body': this.newsLetter.body,
        'template': this.newsLetter.template,
        'id': this.newsLetterId,
        'send': this.sendCampaign.send,
        'scheduledate': this.sendCampaign.scheduledate,
        'scheduletime': this.sendCampaign.time,
        'confirm_email': this.sendCampaign.confirm_email
      };
      this.newsletterservice.editNewsletter(updateNewsLetter).then(data => {
        if(data['status']){
          if(type == 'schedule'){
            this.ScheduleNewsletter();
          }
          else{
            this.sendnewsletter(this.newsLetterId);
          }
          // this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
          //   if(data['status']){
          //     this.showTable = true;
          //     this.newsletterDetails = data['newsletter'];
          //     this.collectionSize = data['newsletter'].length;
          //     this.newsLetter.body = this.defaultTemplate;
          //   }
          // });
        }
      });
    }

    CopyNewsletter(id){
      this.newsletterservice.copyNewsletter(id).then(data => {
        if(data['status']){
          this.snotifyService.success('Newsletter Copied Successfully', '', this.getConfig());
          this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
            if(data['status']){
              this.showTable = true;
              this.newsletterDetails = data['newsletter'];
              this.collectionSize = data['newsletter'].length;
              this.newsLetter.body = this.defaultTemplate;
            }
          });
        }
      });
    }

    selectData(value){
      this.SelectedTab = value.nextId;
      if(value.nextId == 'normal'){
        this.newsletterservice.getNewletter().then(data => {
          if(data['status']){
            this.newsletterDetails = [];
            this.newsletterDetails = data['newsletter'];
          }
        });
      }
      else{
        this.newsletterservice.getABTestingNewletter(new Date().getTime()).then(data => {
          if(data['status']){
            this.newsletterDetails = [];
            console.log(data['newsletter']);
            this.newsletterDetails = data['newsletter'];
          }
        });
      }
    }

    responsive(classname){
      // this.width = w;
      document.getElementById('templateResponsive').innerHTML = this.newsLetter.body;
      if(document.getElementById('templateResponsive').firstElementChild){
        let elem = document.getElementById("templateResponsive").firstElementChild;
        elem.className = classname;
      }
    }

    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }
}

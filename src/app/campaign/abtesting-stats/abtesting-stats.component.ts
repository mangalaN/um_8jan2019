import { Component, OnInit, ViewChild } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist";
import { Router, ActivatedRoute } from '@angular/router';
//import { interval } from 'rxjs/observable/interval';
import { interval, Subscription } from 'rxjs';
import * as Highcharts from 'highcharts';

import * as chartsData from '../../shared/configs/ngx-charts.config';
import * as shape from 'd3-shape';

import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import { StatisticsService } from '../../shared/data/statistics.service';
import { newsLetterService } from '../../shared/data/newsletter.service';
import { ABTestingStatisticsService } from '../../shared/data/abtestingStats.service';

@Component({
  selector: 'app-abtesting-stats',
  templateUrl: './abtesting-stats.component.html',
  styleUrls: ['./abtesting-stats.component.scss']
})
export class AbtestingStatsComponent implements OnInit {
  newsletterId;
  abtestingReport;
  openRates;
  clickRates;
  unsubscribers;
  name;
  subscription: Subscription;
  currentTime = new Date();
  Highcharts = Highcharts;

  loadChart = false;
  chartOptions;
  stopTime;

  lineChartXAxisLabel;
  lineChartYAxisLabel;
  lineChartName;
  ChartColorScheme = {
      domain: ['#666EE8', '#FF9149', '#FF4961', '#AAAAAA']
  };
  lineChartMulti;
  winnerRate;
  sendABtestManual = false;
  showmanualSent = false;
  winnerType = '';
  successSent = false;
  stoplooping = false;
  campaignOverView;

  newsletterDate;
  newsletterDetail;
  subject;
  list;
  totalSent = 0;
  notSent = 0;
  sent;
  totalSentPerc;
  notSentPerc;
  clickCount;
  openCount;
  links;
  domains;
  openSubscribers;
  clickSubscribers;
  campaignUnsubscribers;
  clickCountTable: boolean;
  openCountTable: boolean;
  unsubscriberTable: boolean;
  SelectedTab = '';
  noOfUsers = 0;

  subjecta='';
  subjectb='';

  constructor(private spinner: Ng4LoadingSpinnerService, public abtestingStatisticsService: ABTestingStatisticsService, public statisticsService: StatisticsService, private route: ActivatedRoute, public router: Router, public newsLetterService: newsLetterService) {
    this.route.queryParams.subscribe(params => {
      this.newsletterId = window.atob(params['id']);
    });
    this.newsletterId = ((this.newsletterId * 8000 ) / 1080);
    this.newsLetterService.getabtestingResult(this.newsletterId).then(data => {
      if(data['status'] == 'success'){
        this.campaignOverView = data['newsletter'][0];
        console.log(this.campaignOverView);
      }
      else{
        this.campaignOverView = '';
      }
    });
    this.statisticsService.getabtestingReportById(this.newsletterId, new Date().getTime()).then(data => {
      //console.log(data['reports']);
      //console.log('1-'+JSON.stringify(this.chartOptions));
      if(data['status'] == 'success'){
        this.abtestingReport = data['reports'];
        if(this.abtestingReport[0].type == 'manual'){
          this.sendABtestManual = true;
        }
        this.subjecta = this.abtestingReport[0].subjecta;
        this.subjectb = this.abtestingReport[0].subjectb;
        this.winnerRate = this.abtestingReport[0].rateTrack;
        this.name = this.abtestingReport[0].name;
        let senddate = this.abtestingReport[0].sendDate.split('-');
        //console.log(this.abtestingReport[0].endTime);
        let enddate = this.abtestingReport[0].endTime.split(' ')[0].split('-');
        this.stopTime = new Date(this.abtestingReport[0].endTime.split(' ')[0]+' '+this.abtestingReport[0].endTime.split(' ')[1]);
        let timeval = this.abtestingReport[0].sendTime.split(':');
        let month = new Date(this.abtestingReport[0].sendDate).getUTCMonth();
        //console.log('this.stopTime cons - '+ this.stopTime);
        this.statisticsService.getabTestStats(this.newsletterId, new Date().getTime()).then(data => {
          if(data['status'] == 'success'){
            let chartData = [];

            if(this.abtestingReport[0].rateTrack == 'open'){
              this.openRates = data['reports']['open'];
              if(this.openRates.length > 0){
                let lineChartData = [
                  {
                    "name": this.abtestingReport[0].rateTrack + ' Rate',
                    "series": []
                  }
                ];
                let lineyAxisTicks = [];
                let dataA = [];
                let dataB = [];
                for(let i=0; i<this.openRates.length;i++){
                  if(this.openRates[i].hasOwnProperty('OpenCounta')){
                    lineChartData[0].series.push({"name": this.openRates[i].OpenTimea, "value": parseInt(this.openRates[i].OpenCounta)});
                    lineyAxisTicks.push(parseInt(this.openRates[i].OpenCounta));
                  }
                  if(this.openRates[i].hasOwnProperty('OpenCountb')){
                    lineChartData[0].series.push({"name": this.openRates[i].OpenTimeb, "value": parseInt(this.openRates[i].OpenCountb)});
                    lineyAxisTicks.push(parseInt(this.openRates[i].OpenCountb));
                  }
                  if(this.openRates[i].hasOwnProperty('OpenCounta')){
                    dataA.push([this.openRates[i].OpenTimea, parseInt(this.openRates[i].OpenCounta)]);
                  }
                  else{
                    dataA.push(["00:00",0]);
                  }
                  if(this.openRates[i].hasOwnProperty('OpenCountb')){
                    dataB.push([this.openRates[i].OpenTimeb, parseInt(this.openRates[i].OpenCountb)]);
                  }
                  else{
                    dataB.push(["00:00",0]);
                  }
                }
                chartData.push({"layout": [...lineChartData], "yAxisTicks": lineyAxisTicks, "name": this.abtestingReport[0].rateTrack + " Rate For A/B Testing", "chartType": "'line'", "xaxis": this.abtestingReport[0].rateTrack + " Rate", "yaxis": "Time"});
                // console.log(this.chartOptions.series[0].data);
                // this.chartOptions.series[0].data = dataA;
                // console.log(this.chartOptions.series[0].data);
                // console.log(this.chartOptions.series[1].data);
                // this.chartOptions.series[1].data = dataB;
                // console.log(this.chartOptions.series[1].data);
                // this.chartOptions.xAxis.tickInterval = (10000*60);
                // let senddate = this.abtestingReport[0].sendDate.split('-');
                // let enddate = this.abtestingReport[0].endTime.split('-');
                // this.chartOptions.xAxis.min = Date.UTC(senddate[0], senddate[1], senddate[2], 0, 0, 0);
                // this.chartOptions.xAxis.max = Date.UTC(enddate[0], enddate[1], enddate[2], 0, 0, 0);
                // this.chartOptions.plotOptions['series'].pointInterval = (10000*60);
                // this.chartOptions.plotOptions['series'].pointStart = Date.UTC(senddate[0], senddate[1], senddate[2], 0, 0, 0);

                //console.log(senddate[0]+','+ parseInt(senddate[1] - 1)+','+ senddate[2]);
                Highcharts.setOptions({
                    time: {
                        timezoneOffset: parseInt(timeval[0]) * 60
                    }
                });
                this.chartOptions = {
                  title: {
                      text: this.abtestingReport[0].rateTrack + ' Rate'
                  },
                  chart: {
                    type: 'spline'
                  },
                  xAxis: {
                      type: 'datetime'
                  },
                  series: [{
                      data: dataA,
                      pointStart: Date.UTC((senddate[0]), month, (senddate[2])),
                      pointInterval: 10000 * 60,
                      name:'version A'
                  },
                  {
                      data: dataB,
                      pointStart: Date.UTC((senddate[0]), month, (senddate[2])),
                      pointInterval: 10000 * 60,
                      name:'version B'
                  }]
                };
              this.loadChart = true;
                //this.chartOptions = {
                  // chart: {
                  //   type: 'spline'
                  // },
                  // xAxis: {
                  //     type: 'datetime'
                  // },
                  // plotOptions: {
                  //   series: {
                  //       pointInterval : timeval[0] * 60
                  //   }
                  // },
                  // series: [{
                  //     data: [dataA],
                  //     //pointStart: Date.UTC(senddate[0], parseInt(senddate[1] - 1), senddate[2]),
                  //     //pointInterval: 60 * 10000,
                  //     name:'version A'
                  // },
                  // {
                  //     data: [dataB],
                  //     //pointStart: Date.UTC(senddate[0], (senddate[1] - 1), senddate[2]),
                  //     //pointInterval: 60 * 10000,
                  //     name:'version B'
                  // }]
                  //   xAxis: {
                  //   type :'datetime',
                  //   tickInterval : 10000 * 60,
                  //   min : Date.UTC(senddate[0], senddate[1], senddate[2]),
                  //   max : Date.UTC(enddate[0], enddate[1], enddate[2]),
                  //   label : {
                  //     align : 'left'
                  //   }
                  // },
                  // yAxis: {
                  //   title: {
                  //       text: this.abtestingReport[0].rateTrack +' Rate'
                  //   },
                  //   lineWidth: 2,
                  //   plotLines: [{
                  //       value: 0,
                  //       width: 1,
                  //       color: '#808080'
                  //   }]
                  // },
                  // plotOptions: {
                  //   series: {
                  //       pointStart: Date.UTC(senddate[0], senddate[1], senddate[2]),
                  //       pointInterval : 10000 * 60
                  //   }
                  // },
                  // series: [{
                  //     data:dataA,
                  //     name:'version A'
                  //     },{
                  //     data:dataB,
                  //     name:'version B'
                  //   }]
              	//}
              }
              //this.chartOptions = chartData;
              console.log(this.chartOptions);
            }
            if(this.abtestingReport[0].rateTrack == 'click'){
              this.clickRates = data['reports']['click'];
              if(this.clickRates.length > 0){
              let dataA = [];
              let dataB = [];
              for(let i=0; i<this.clickRates.length;i++){
                if(this.clickRates[i].hasOwnProperty('linkCounta')){
                  dataA.push([this.clickRates[i].ClickTimea, parseInt(this.clickRates[i].linkCounta)]);
                }
                else{
                  dataA.push(["00:00",0]);
                }
                if(this.clickRates[i].hasOwnProperty('linkCountb')){
                  dataB.push([this.clickRates[i].ClickTimeb, parseInt(this.clickRates[i].linkCountb)]);
                }
                else{
                  dataB.push(["00:00",0]);
                }
              }
              this.chartOptions = {
                title: {
                    text: this.abtestingReport[0].rateTrack + ' Rate'
                },
                chart: {
                  type: 'spline'
                },
                xAxis: {
                    type: 'datetime'
                },
                series: [{
                    data: dataA,
                    pointStart: Date.UTC(parseInt(senddate[0]), month, parseInt(senddate[2])),
                    pointInterval: 10000 * 60,
                    name:'version A'
                },
                {
                    data: dataB,
                    pointStart: Date.UTC(parseInt(senddate[0]), month, parseInt(senddate[2])),
                    pointInterval: 10000 * 60,
                    name:'version B'
                }]
              };
              console.log(JSON.stringify(this.chartOptions));
              this.loadChart = true;
              // Highcharts.setOptions({
              //     time: {
              //         timezoneOffset: 5 * 60
              //     }
              // });
              //
              // Highcharts.chart('container', {
              //     chart:{
              //       type: 'line'
              //     },
              //     title: {
              //         text: 'Timezone offset is 5 hours (=EST). The data series starts at midnight UTC, which is 7 PM EST.'
              //     },
              //     xAxis: {
              //         type: 'datetime'
              //     },
              //     series: [{
              //         data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
              //         pointStart: Date.UTC(2013, 0, 1),
              //         pointInterval: 36e5 // one hour
              //     }]
              // });

              // this.chartOptions = {
              //   title: {
              //       text: 'Timezone offset is 5 hours (=EST). The data series starts at midnight UTC, which is 7 PM EST.'
              //   },
              //
              //   xAxis: {
              //       type: 'datetime'
              //   },
              //
              //   series: [{
              //       data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
              //       pointStart: Date.UTC(2013, 0, 1),
              //       pointInterval: 36e5 // one hour
              //   }]
                // chart: {
                //   type: 'spline'
                // },
                // xAxis: {
                //     type: 'datetime'
                // },
                // plotOptions: {
                //   series: {
                //       pointInterval : timeval[0] * 60
                //   }
                // },
                // series: [{
                //     data: [dataA],
                //     //pointStart: Date.UTC(senddate[0], (senddate[1] - 1), senddate[2]),
                //     //pointInterval: 10000 * 60,
                //     name:'version A'
                // },
                // {
                //     data: [dataB],
                //     //pointStart: Date.UTC(senddate[0], (senddate[1] - 1), senddate[2]),
                //     //pointInterval: 10000 * 60,
                //     name:'version B'
                // }]
              //};
            }
            }
            if(this.abtestingReport[0].rateTrack == 'unsubscriber'){
              this.unsubscribers = data['reports']['unsubscribe'];
              if(this.unsubscribers.length > 0){
                  let dataA = [];
                  let dataB = [];
                  for(let i=0; i<this.unsubscribers.length;i++){
                    if(this.unsubscribers[i].hasOwnProperty('unsubscribeCountA')){
                      dataA.push([this.unsubscribers[i].Timea, parseInt(this.unsubscribers[i].unsubscribeCountA)]);
                    }
                    else{
                      dataA.push(["00:00",0]);
                    }
                    if(this.unsubscribers[i].hasOwnProperty('unsubscribeCountB')){
                      dataB.push([this.unsubscribers[i].Timeb, parseInt(this.unsubscribers[i].unsubscribeCountB)]);
                    }
                    else{
                      dataB.push(["00:00",0]);
                    }
                  }
                  this.chartOptions = {
                    title: {
                        text: this.abtestingReport[0].rateTrack + ' Rate'
                    },
                    chart: {
                      type: 'spline'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    series: [{
                        data: dataA,
                        pointStart: Date.UTC(parseInt(senddate[0]), month, parseInt(senddate[2])),
                        pointInterval: 10000 * 60,
                        name:'version A'
                    },
                    {
                        data: dataB,
                        pointStart: Date.UTC(parseInt(senddate[0]), month, parseInt(senddate[2])),
                        pointInterval: 10000 * 60,
                        name:'version B'
                    }]
                  };
                  console.log(JSON.stringify(this.chartOptions));
                  this.loadChart = true;
              }
            }
          }
          console.log('2-'+JSON.stringify(this.chartOptions));

        });

        if(this.stopTime.getTime() <= new Date().getTime()){
          this.stoplooping = true;
          if(this.sendABtestManual){
            this.newsLetterService.getABTestingWinner(this.newsletterId, this.winnerRate).then(data => {
              if(data['status'] == 'success'){
                this.showmanualSent = true;
                this.successSent = false;
                this.winnerType = data['newsletter'];
                this.subscription.unsubscribe();
              }
              else{
                this.showmanualSent = false;
                this.successSent = true;
                this.subscription.unsubscribe();
              }
            });
          }
          else{
            this.newsLetterService.sendABTestingNewletterwinner(this.newsletterId, this.winnerRate).then(data => {
              if(data['status']){
                this.successSent = true;
                this.subscription.unsubscribe();
              }
            });
          }
        }
      }
    },
    error => {
    });
  }

  ngOnInit() {
    if(!this.stoplooping){
      this.subscription = interval(2000 * 60).subscribe(x => {
        // console.log('this.stopTime - '+this.stopTime);
        // console.log('this.stopTime getTime - '+this.stopTime.getTime());
        // console.log('new date - '+new Date().getTime());
        if(this.stopTime.getTime() <= new Date().getTime()){
          this.stoplooping = true;
          if(this.sendABtestManual){
            this.newsLetterService.getABTestingWinner(this.newsletterId, this.winnerRate).then(data => {
              if(data['status'] == 'success'){
                this.showmanualSent = true;
                this.successSent = false;
                this.winnerType = data['newsletter'];
                this.subscription.unsubscribe();
              }
              else{
                this.showmanualSent = false;
                this.successSent = true;
                this.subscription.unsubscribe();
              }
            });
          }
          else{
            this.newsLetterService.sendABTestingNewletterwinner(this.newsletterId, this.winnerRate).then(data => {
              if(data['status']){
                this.successSent = true;
                this.subscription.unsubscribe();
              }
            });
          }
        }
        else{
          if(this.stopTime.getTime() > new Date().getTime()){
            this.statisticsService.getabtestingReportById(this.newsletterId, new Date().getTime()).then(data => {
              if(data['status'] == 'success'){
                this.abtestingReport = data['reports'];

                this.winnerRate = this.abtestingReport[0].rateTrack;
                this.name = this.abtestingReport[0].name;
                let senddate = this.abtestingReport[0].sendDate.split('-');
                //console.log(this.abtestingReport[0].endTime);
                let enddate = this.abtestingReport[0].endTime.split(' ')[0].split('-');
                this.stopTime = new Date(this.abtestingReport[0].endTime.split(' ')[0]+' '+this.abtestingReport[0].endTime.split(' ')[1]);
                let timeval = this.abtestingReport[0].sendTime.split(':');
                let month = new Date(this.abtestingReport[0].sendDate).getUTCMonth();
                console.log('date greater - '+ month);
                this.statisticsService.getabTestStats(this.newsletterId, new Date().getTime()).then(data => {
                  if(data['status'] == 'success'){
                    let chartData = [];
                    if(this.abtestingReport[0].rateTrack == 'open'){
                      this.openRates = data['reports']['open'];
                      if(this.openRates.length > 0){
                        let lineChartData = [
                          {
                            "name": this.abtestingReport[0].rateTrack + ' Rate',
                            "series": []
                          }
                        ];
                        let lineyAxisTicks = [];
                        let dataA = [];
                        let dataB = [];
                        for(let i=0; i<this.openRates.length;i++){
                          if(this.openRates[i].hasOwnProperty('OpenCounta')){
                            lineChartData[0].series.push({"name": this.openRates[i].OpenTimea, "value": parseInt(this.openRates[i].OpenCounta)});
                            lineyAxisTicks.push(parseInt(this.openRates[i].OpenCounta));
                          }
                          if(this.openRates[i].hasOwnProperty('OpenCountb')){
                            lineChartData[0].series.push({"name": this.openRates[i].OpenTimeb, "value": parseInt(this.openRates[i].OpenCountb)});
                            lineyAxisTicks.push(parseInt(this.openRates[i].OpenCountb));
                          }
                          if(this.openRates[i].hasOwnProperty('OpenCounta')){
                            dataA.push([this.openRates[i].OpenTimea, parseInt(this.openRates[i].OpenCounta)]);
                          }
                          else{
                            dataA.push(["00:00",0]);
                          }
                          if(this.openRates[i].hasOwnProperty('OpenCountb')){
                            dataB.push([this.openRates[i].OpenTimeb, parseInt(this.openRates[i].OpenCountb)]);
                          }
                          else{
                            dataB.push(["00:00",0]);
                          }
                        }
                        chartData.push({"layout": [...lineChartData], "yAxisTicks": lineyAxisTicks, "name": this.abtestingReport[0].rateTrack + " Rate For A/B Testing", "chartType": "'line'", "xaxis": this.abtestingReport[0].rateTrack + " Rate", "yaxis": "Time"});
                        // console.log(this.chartOptions.series[0].data);
                        // this.chartOptions.series[0].data = dataA;
                        // console.log(this.chartOptions.series[0].data);
                        // console.log(this.chartOptions.series[1].data);
                        // this.chartOptions.series[1].data = dataB;
                        // console.log(this.chartOptions.series[1].data);
                        // this.chartOptions.xAxis.tickInterval = (10000*60);
                        // let senddate = this.abtestingReport[0].sendDate.split('-');
                        // let enddate = this.abtestingReport[0].endTime.split('-');
                        // this.chartOptions.xAxis.min = Date.UTC(senddate[0], senddate[1], senddate[2], 0, 0, 0);
                        // this.chartOptions.xAxis.max = Date.UTC(enddate[0], enddate[1], enddate[2], 0, 0, 0);
                        // this.chartOptions.plotOptions['series'].pointInterval = (10000*60);
                        // this.chartOptions.plotOptions['series'].pointStart = Date.UTC(senddate[0], senddate[1], senddate[2], 0, 0, 0);

                        //console.log(senddate[0]+','+ parseInt(senddate[1] - 1)+','+ senddate[2]);
                        Highcharts.setOptions({
                            time: {
                                timezoneOffset: parseInt(timeval[0]) * 60
                            }
                        });
                        this.chartOptions = {
                          title: {
                              text: this.abtestingReport[0].rateTrack + ' Rate'
                          },
                          chart: {
                            type: 'spline'
                          },
                          xAxis: {
                              type: 'datetime'
                          },
                          series: [{
                              data: dataA,
                              pointStart: Date.UTC((senddate[0]), month, (senddate[2])),
                              pointInterval: 10000 * 60,
                              name:'version A'
                          },
                          {
                              data: dataB,
                              pointStart: Date.UTC((senddate[0]), month, (senddate[2])),
                              pointInterval: 10000 * 60,
                              name:'version B'
                          }]
                        };
                      this.loadChart = true;
                      }
                      console.log(this.chartOptions);
                    }
                    if(this.abtestingReport[0].rateTrack == 'click'){
                      this.clickRates = data['reports']['click'];
                      if(this.clickRates.length > 0){
                      let dataA = [];
                      let dataB = [];
                      for(let i=0; i<this.clickRates.length;i++){
                        if(this.clickRates[i].hasOwnProperty('linkCounta')){
                          dataA.push([this.clickRates[i].ClickTimea, parseInt(this.clickRates[i].linkCounta)]);
                        }
                        else{
                          dataA.push(["00:00",0]);
                        }
                        if(this.clickRates[i].hasOwnProperty('linkCountb')){
                          dataB.push([this.clickRates[i].ClickTimeb, parseInt(this.clickRates[i].linkCountb)]);
                        }
                        else{
                          dataB.push(["00:00",0]);
                        }
                      }
                      this.chartOptions = {
                        title: {
                            text: this.abtestingReport[0].rateTrack + ' Rate'
                        },
                        chart: {
                          type: 'spline'
                        },
                        xAxis: {
                            type: 'datetime'
                        },
                        series: [{
                            data: dataA,
                            pointStart: Date.UTC(parseInt(senddate[0]), month, parseInt(senddate[2])),
                            pointInterval: 10000 * 60,
                            name:'version A'
                        },
                        {
                            data: dataB,
                            pointStart: Date.UTC(parseInt(senddate[0]), month, parseInt(senddate[2])),
                            pointInterval: 10000 * 60,
                            name:'version B'
                        }]
                      };

                      this.loadChart = true;
                    }
                    }
                    if(this.abtestingReport[0].rateTrack == 'unsubscriber'){
                      this.unsubscribers = data['reports']['unsubscribe'];
                      if(this.unsubscribers.length > 0){
                          let dataA = [];
                          let dataB = [];
                          for(let i=0; i<this.unsubscribers.length;i++){
                            if(this.unsubscribers[i].hasOwnProperty('unsubscribeCountA')){
                              dataA.push([this.unsubscribers[i].Timea, parseInt(this.unsubscribers[i].unsubscribeCountA)]);
                            }
                            else{
                              dataA.push(["00:00",0]);
                            }
                            if(this.unsubscribers[i].hasOwnProperty('unsubscribeCountB')){
                              dataB.push([this.unsubscribers[i].Timeb, parseInt(this.unsubscribers[i].unsubscribeCountB)]);
                            }
                            else{
                              dataB.push(["00:00",0]);
                            }
                          }
                          this.chartOptions = {
                            title: {
                                text: this.abtestingReport[0].rateTrack + ' Rate'
                            },
                            chart: {
                              type: 'spline'
                            },
                            xAxis: {
                                type: 'datetime'
                            },
                            series: [{
                                data: dataA,
                                pointStart: Date.UTC(parseInt(senddate[0]), month, parseInt(senddate[2])),
                                pointInterval: 10000 * 60,
                                name:'version A'
                            },
                            {
                                data: dataB,
                                pointStart: Date.UTC(parseInt(senddate[0]), month, parseInt(senddate[2])),
                                pointInterval: 10000 * 60,
                                name:'version B'
                            }]
                          };
                          console.log(JSON.stringify(this.chartOptions));
                          this.loadChart = true;
                      }
                    }
                  }
                  console.log('2-'+JSON.stringify(this.chartOptions));
                });
              }
            });
          }
        }
      });
      this.newsLetterService.getabtestingResult(this.newsletterId).then(data => {
        this.campaignOverView = '';
        if(data['status'] == 'success'){
          this.campaignOverView = data['newsletter'][0];
          console.log(this.campaignOverView);
        }
        else{
          this.campaignOverView = '';
        }
      });
    }
  }

  selectData(value){
    this.SelectedTab = value.nextId;
    ////this.spinner.show();
    if(value.nextId == 'Overview'){
      this.abtestingStatisticsService.getReportById(this.newsletterId).then(data => {
        if(data['status']){
          //document.getElementById('messageTemplate').innerHTML = JSON.parse(data['reports'][0]['message']);
          this.newsletterDetail = JSON.parse(data['reports'][0]['message']);
          this.newsletterDate = data['reports'][0]['date'];
          this.subject = data['reports'][0]['subject'];
          this.list = data['reports'][0]['list'];
          this.totalSent = data['reports'][0]['totalSent'];
          this.notSent = data['reports'][0]['notSent'];
          this.noOfUsers = data['reports'][0]['numberUsers'];
          this.sent = data['reports'][0]['noOfUsers'] - data['reports'][0]['totalSent'];
          this.totalSentPerc = ((data['reports'][0]['totalSent'] / data['reports'][0]['numberUsers']) * 100);//(((data['reports'][0]['totalSent'] - data['reports'][0]['notSent']) / data['reports'][0]['totalSent']) * 100);
          this.notSentPerc = ((data['reports'][0]['notSent'] / data['reports'][0]['numberUsers']) * 100);//((data['reports'][0]['notSent'] / data['reports'][0]['totalSent']) * 100);
          this.clickCount = data['reports'][0]['clickCount'];
        }
      },
      error => {
      });
      this.abtestingStatisticsService.getclickCounts(this.newsletterId).then(data => {
        if(data['status'] = 'success'){
          this.links = data['reports'];
          this.openCount = data['reports'][0]['openCount'];
        }
        else{
          this.links = [];
          this.openCount = [];
        }
      },
      error => {
      });
      this.abtestingStatisticsService.getabtestingEmailDomain(this.newsletterId).then(data => {
        if(data['status'] = 'success'){
          this.domains = data['reports'];
        }
        else{
          this.domains = [];
        }
      },
      error => {
      });
      this.abtestingStatisticsService.getOpenSubscriberList(this.newsletterId).then(data => {
        if(data['status'] = 'success'){
          this.openSubscribers = data['reports'];
        }
      },
      error => {
      });

      this.abtestingStatisticsService.getClickSubscriberList(this.newsletterId).then(data => {
          if(data['status'] = 'success'){
                this.clickSubscribers = data['reports'];
            }
      },
      error => {
      });

      this.abtestingStatisticsService.getUnSubscriberList(this.newsletterId).then(data => {
          if(data['status'] = 'success'){
                this.campaignUnsubscribers = data['reports'];
            }
      },
      error => {
      });
    }
    else if(value.nextId == 'Link'){
      this.abtestingStatisticsService.getReportById(this.newsletterId).then(data => {
        if(data['status']){
          //document.getElementById('messageTemplate').innerHTML = JSON.parse(data['reports'][0]['message']);
          this.newsletterDetail = JSON.parse(data['reports'][0]['message']);
          this.newsletterDate = data['reports'][0]['date'];
          this.subject = data['reports'][0]['subject'];
          this.list = data['reports'][0]['list'];
          this.totalSent = data['reports'][0]['totalSent'];
          this.notSent = data['reports'][0]['notSent'];
          this.noOfUsers = data['reports'][0]['numberUsers'];
          this.sent = data['reports'][0]['totalSent'] - data['reports'][0]['notSent'];
          this.totalSentPerc = (((data['reports'][0]['totalSent'] - data['reports'][0]['notSent']) / data['reports'][0]['totalSent']) * 100);
          this.notSentPerc = ((data['reports'][0]['notSent'] / data['reports'][0]['totalSent']) * 100);
          this.clickCount = data['reports'][0]['clickCount'];
          //console.log(data['reports']);
        }
      },
      error => {
      });
      this.abtestingStatisticsService.getclickCounts(this.newsletterId).then(data => {
          if(data['status'] = 'success'){
              this.links = data['reports'];
              this.openCount = data['reports'][0]['openCount'];
          }
          else{
            this.links = [];
            this.openCount = [];
          }
          //this.spinner.hide();
      },
      error => {
      });
    }
    else if(value.nextId == 'Domain'){
      this.abtestingStatisticsService.getReportById(this.newsletterId).then(data => {
        if(data['status']){
          //document.getElementById('messageTemplate').innerHTML = JSON.parse(data['reports'][0]['message']);
          this.newsletterDetail = JSON.parse(data['reports'][0]['message']);
          this.newsletterDate = data['reports'][0]['date'];
          this.subject = data['reports'][0]['subject'];
          this.list = data['reports'][0]['list'];
          this.totalSent = data['reports'][0]['totalSent'];
          this.notSent = data['reports'][0]['notSent'];
          this.noOfUsers = data['reports'][0]['numberUsers'];
          this.sent = data['reports'][0]['totalSent'] - data['reports'][0]['notSent'];
          this.totalSentPerc = (((data['reports'][0]['totalSent'] - data['reports'][0]['notSent']) / data['reports'][0]['totalSent']) * 100);
          this.notSentPerc = ((data['reports'][0]['notSent'] / data['reports'][0]['totalSent']) * 100);
          this.clickCount = data['reports'][0]['clickCount'];
          //console.log(data['reports']);
        }
      },
      error => {
      });
      this.abtestingStatisticsService.getabtestingEmailDomain(this.newsletterId).then(data => {
        if(data['status'] = 'success'){
          this.domains = data['reports'];
        }
        else{
          this.domains = [];
        }
        //this.spinner.hide();
      },
      error => {
      });
    }
    else if(value.nextId == 'Activity'){
      this.abtestingStatisticsService.getOpenSubscriberList(this.newsletterId).then(data => {
          if(data['status'] = 'success'){
            this.openSubscribers = data['reports'];
          }
          else{
            this.openSubscribers = [];
          }
      },
      error => {
      });

      this.abtestingStatisticsService.getClickSubscriberList(this.newsletterId).then(data => {
          if(data['status'] = 'success'){
            this.clickSubscribers = data['reports'];
          }
          else{
            this.clickSubscribers = [];
          }
      },
      error => {
      });

      this.abtestingStatisticsService.getUnSubscriberList(this.newsletterId).then(data => {
          if(data['status'] = 'success'){
            this.campaignUnsubscribers = data['reports'];
          }
          else{
            this.campaignUnsubscribers = [];
          }
      },
      error => {
      });
      this.clickCountTable=false;
      this.unsubscriberTable=false;
      this.openCountTable=true;
    }
  }

  openSubscriberList () {
    this.clickCountTable=false;
    this.unsubscriberTable=false;
    this.openCountTable=true;
  }

  clickSubscriberList () {
    this.clickCountTable=true;
    this.openCountTable=false;
    this.unsubscriberTable=false;
  }

  unSubscriberList () {
    this.unsubscriberTable=true;
    this.clickCountTable=false;
    this.openCountTable=false;
  }

  sendCampaign(){
    this.newsLetterService.sendABTestingNewletterwinner(this.newsletterId, this.winnerRate).then(data => {
      if(data['status']){
        this.successSent = true;
        this.showmanualSent = false;
        this.subscription.unsubscribe();
      }
    });
  }

}

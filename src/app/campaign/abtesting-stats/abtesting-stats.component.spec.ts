import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbtestingStatsComponent } from './abtesting-stats.component';

describe('AbtestingStatsComponent', () => {
  let component: AbtestingStatsComponent;
  let fixture: ComponentFixture<AbtestingStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbtestingStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbtestingStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl,FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { MembershipSegmentsService } from '../../shared/data/membershipSegments.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

@Component({
  selector: 'app-segments',
  templateUrl: './segments.component.html',
  styleUrls: ['./segments.component.scss']
})
export class SegmentsComponent implements OnInit {

  segmentForm: FormGroup;
  segments;
  totalList;
  showSegment;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  propertyList: Array<any> = [
   { name: 'Address',operator:['Is equal to','Is not equal to'], values: [] },
   { name: 'City', operator:['Is equal to','Is not equal to'], values: [] },
   { name: 'State', operator:['Is equal to','Is not equal to'], values: [] },
   { name: 'Postcode', operator:['Is equal to','Is not equal to'], values: [] }
 ];

  constructor(public membershipSegmentsService: MembershipSegmentsService,private _FB: FormBuilder,private snotifyService: SnotifyService) { }

  ngOnInit() {
    this.segmentForm = new FormGroup({
        'name': new FormControl(null, [Validators.required]),
        'actionset' : this._FB.array([
        ]),
        'propertyset' : this._FB.array([
        ])
    });

    this.membershipSegmentsService.getSegmentByOrg().then(data => {
      if(data['status'] == 'success'){
        this.segments = data['segments'];
        //this.collectionSize = data['segments'].length;
        this.totalList = data['segments'].length;
      }
    },
    error => {
    });
  }
  get f() { return this.segmentForm.controls; }

   memToggle(){
     this.showSegment = true;
   }
    cancel(){
      //window.location.reload();
      this.showSegment = false;
      this.segmentForm.reset();this.segmentForm = new FormGroup({
          'name': new FormControl(null, [Validators.required]),
          'actionset' : this._FB.array([
          ]),
          'propertyset' : this._FB.array([
          ])
      });

    }

    deletesegment(id){
      this.membershipSegmentsService.deleteSegment(id).then(data => {
        if(data['status']){
          this.snotifyService.success('Deleted Successfully', '', this.getConfig());
          this.segments = this.segments.filter(h => h.id != id);
        }
      });
    }
    /*action dynamic code*/
    initMapFields1() : FormGroup{
      return this._FB.group({
        act1   : [],
        act2   : [],
        act3   : [],
        act4   : []
      });
    }

   // valuesArray1 =[];

   //  ddl1change1(val,index){
   //    this.valuesArray[index] = val;
   //  }

    addSetCondition1(){
     const control = <FormArray>this.segmentForm.controls.actionset;
     control.push(this.initMapFields1());
    }

    removeSetCondition1(i : number) : void{
      const control = <FormArray>this.segmentForm.controls.actionset;
      control.removeAt(i);
      //this.valuesArray.splice(i, 1);
    //this.operatorArray.splice(i, 1);
    }
    /*action dynamic code end*/
    /*property dynamic*/
    initMapFields() : FormGroup{
      return this._FB.group({
         ddl1     : [],
         seloperator   : [],
         txtfilterval   : []
      });
   }

   valuesArray =[];


    ddl1change(val,index){
      //alert(val);


      let nameary=["Address","City","State","Postcode"];
      let indexsel = nameary.indexOf(val);

      //alert("indexsel"+indexsel);
      //this.valuesArray.push(indexsel);

      this.valuesArray[index] = indexsel;
    }

    addSetCondition(){
      this.valuesArray.push(0);
     const control = <FormArray>this.segmentForm.controls.propertyset;
     control.push(this.initMapFields());
    }

    removeSetCondition(i : number) : void{
      const control = <FormArray>this.segmentForm.controls.propertyset;
      control.removeAt(i);
      this.valuesArray.splice(i, 1);
    //this.operatorArray.splice(i, 1);
    }
    /*property dynamic end*/

    onReactiveFormSubmit(val : any) : void{
      //console.log(JSON.stringify(val));
      let obj= {'name':'','condition':{'propertyset':'','actionset':''}};
      //let obj= {'name':'','condition':'','subscribers':0};
      obj['name'] = val.name;
      obj['condition']['propertyset'] = val.propertyset;
      obj['condition']['actionset'] = val.actionset;

      //console.log('segments - '+JSON.stringify(obj));

      //let mydata = JSON.stringify(obj);

      this.membershipSegmentsService.addSegment(obj).then(data => {
        if(data['status'] == 'true'){
           this.snotifyService.success('Added Successfully', '', this.getConfig());
           this.membershipSegmentsService.getSegmentByOrg().then(data => {
             if(data['status'] == 'success'){
               this.segments = data['segments'];
               this.totalList = data['segments'].length;
               this.showSegment = false;
               this.segmentForm.reset();
             }
           },
           error => {
           });
        }
      })
    }

    editsegment(id){
      let editSegment = this.segments.filter(h => h.id == id);
      this.segmentForm.controls['name'].setValue(editSegment[0]['name']);
      let condition = JSON.parse(editSegment[0]['condition']);
      for(let i= 0; i< condition['propertyset'].length; i++){
        this.addSetConditionEdit(condition['propertyset'][i]);
      }
      for(let i= 0; i< condition['actionset'].length; i++){
        this.addSetCondition1Edit(condition['actionset'][i]);
      }
      this.showSegment = true;
    }

    addSetConditionEdit(myobj){
      this.valuesArray.push(0);
      const control = <FormArray>this.segmentForm.controls.propertyset;
      control.push(this.initMapFieldsEdit(myobj));
    }

    initMapFieldsEdit(myobj) : FormGroup{
     return this._FB.group({
        ddl1     : [myobj['ddl1']],
        seloperator   : [myobj['seloperator']],
        txtfilterval   : [myobj['txtfilterval']]
     });
    }

    addSetCondition1Edit(myobj){
    const control = <FormArray>this.segmentForm.controls.actionset;
    control.push(this.initMapFields1Edit(myobj));
    }

    initMapFields1Edit(myobj) : FormGroup{
     return this._FB.group({
       act1   : [myobj['act1']],
       act2   : [myobj['act2']],
       act3   : [myobj['act3']],
       act4   : [myobj['act4']]
     });
    }

    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }
}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { NgbDateStruct, NgbDatepickerI18n, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';

import { UserService } from '../../shared/data/user.service';
import { ListsService } from '../../shared/data/lists.service';
import { OrganizationService } from '../../shared/data/organization.service';
import { AddFieldsService } from '../../shared/data/addFields.service';
import { MembershipService } from '../../shared/data/membership.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  @ViewChild('adduser') adduserForm: NgForm;

  dataSource = [];
  p: number = 1;
  pageSize: number = 10;
  style = 'material';
  title = 'Snotify title!';
  body = 'Lorem ipsum dolor sit amet!';
  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;
  collectionSize;
  userId;
  listDetails;
  roles;
  organizations;
  showlistadmin;
  usernameerror = false;
  emailerror = false;
  newaddress;listadmin;
  public showTable: boolean = true;
  public isSuperAdmin: boolean = false;
  public isAdmin: boolean = false;
  public showInvite: boolean = false;
  public submitted: boolean = false;
  public selectedTab: any;
  options = {
    types: ['geocode'],
    componentRestrictions: { country: 'AU' }
  }

  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  //regularForm: FormGroup;
  inviteForm: FormGroup;
  extraFields = false;
  addFields = [];
  toggleValue = false;
  textValue;
  dateValue;
  numberValue;
  floatValue;
  FieldsValue = [];
  dateerror = false;

  users = {
    'username': '',
    'password': '',
    'firstName': '',
    'lastName': '',
    'email': '',
    'dob': '',
    'genderRadios': '',
    'address': '',
    'phone': '',
    'profileImage': '',
    'sublist':'',
    'type': '',
    'role': '',
    'organization': '',
    'fields': '',
    'id':'',
    'address1':'',
    'city':'',
    'state':'',
    'country':'',
    'membership': '',
    'status': 'y',
    'postcode': ''
  }
  usersModel_extra = {};
  members;
  columns = [
    { name: 'First Name' },
    { name: 'Last Name' },
    { name: 'Email'},
    { name: 'Address'},
    { name: 'Phone'},
    { name: 'City'},
    { name: 'State'}
    ];

  allColumns = [
    { name: 'First Name' },
    { name: 'Last Name' },
    { name: 'Email'},
    { name: 'Address'},
    { name: 'Phone'},
    { name: 'City'},
    { name: 'State'}
  ];

   isCollapsed = false;
   public storeName;

  constructor(public addFieldsService: AddFieldsService, public membershipService: MembershipService, private snotifyService: SnotifyService, public organizationservice: OrganizationService, public listsservice: ListsService, public userservice: UserService, private router: Router, private route: ActivatedRoute) {
    let users = JSON.parse(localStorage.getItem('currentUser'));
    if(users){
      if(users['role_id'] == '2'){
        this.isSuperAdmin = true;
      }
      if(users['role_id'] == '1'){
        this.isAdmin = true;
      }
      this.storeName = users['name'];
    }
    this.userId = users;
    this.SelectedTab = 'all';
    this.membershipService.getMembership().then(data => {
      if(data['membership']){
        this.members = data['membership'];
      }
    },
    error => {
    });
    console.log('Selected - ' + this.selectedTab);
  }

  ngOnInit() {
      this.inviteForm = new FormGroup({
          'inviteEmail': new FormControl(null, [Validators.required, Validators.email]),
          'role': new FormControl(null, [Validators.required])
      });
      let store;
      if(this.storeName.startsWith("Auto One")){
        store = this.storeName;
      }
      else{
        store = '';
      }
      this.userservice.getUsers(store,new Date().getTime()).then(data => {
      console.log(data['user']);
      if(data['status'] == 'success'){
        this.dataSource = data['user'];
        this.collectionSize = data['user'].length;
      }
    },
    error => {
    });
    //if(!this.isSuperAdmin){
      // this.regularForm = new FormGroup({
      //   'username': new FormControl(null, [Validators.required]),
      //   'firstName': new FormControl(null, [Validators.required]),
      //   'email': new FormControl(null, [Validators.required, Validators.email]),
      //   'dob': new FormControl(null, [Validators.required]),
      //   'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
      //   'lastName': new FormControl(null, [Validators.required]),
      //   'genderRadios': new FormControl('Female'),
      //   'address': new FormControl(null, [Validators.required]),
      //   'phone': new FormControl(null, [Validators.required, Validators.maxLength(10)]),
      //   'sublist': new FormControl(null, [Validators.required]),
      //    'textValue': new FormControl(null, null),
      //   'dateValue': new FormControl(null, null),
      //   'numberValue': new FormControl(null, null),
      //   'floatValue': new FormControl(null, null),
      //   'toggleValue': new FormControl(null, null),
      // }, {updateOn: 'blur'});
      this.addFieldsService.getFieldsByTable('users').then(data=>{
        if(data['status'] == 'success'){
          //addFields
          for(let i=0; i< data['fields'].length; i++){
            let fields = JSON.parse(data['fields'][i]['fields']);
              this.addFields.push({
                'label': fields['label'],
          			'datatype':fields['datatype'],
          			'length':fields['length'],
          			'mandatory':fields['mandatory'],
          			'validation':fields['validation'],
          			'validationMsg':fields['validationMsg'],
          			'fieldType':fields['fieldType'],
                'id':data['fields'][i]['id'],
                'fieldName':fields['fieldName']
              });
          }
          this.extraFields = true;
        }
        else{
          this.extraFields = false;
        }
      });
    // }
    // else{
    //   this.regularForm = new FormGroup({
    //     'username': new FormControl(null, [Validators.required]),
    //     'firstName': new FormControl(null, [Validators.required]),
    //     'email': new FormControl(null, [Validators.required, Validators.email]),
    //     'dob': new FormControl(null, [Validators.required]),
    //     'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
    //     'lastName': new FormControl(null, [Validators.required]),
    //     'genderRadios': new FormControl('Female'),
    //     'address': new FormControl(null, [Validators.required]),
    //     'phone': new FormControl(null, [Validators.required]),
    //     'sublist': new FormControl(null, [Validators.required]),
    //     'role': new FormControl(null, [Validators.required]),
    //     'organization': new FormControl(null, [Validators.required])
    //       // 'inputEmail': new FormControl(null, [Validators.required, Validators.email]),
    //       // 'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
    //       // 'textArea': new FormControl(null, [Validators.required]),
    //       // 'radioOption': new FormControl('Option one is this')
    //   }, {updateOn: 'blur'});
    // }
    this.listsservice.getlist(new Date().getTime()).then(data => {
      this.listDetails = data['lists'];
    });
    this.userservice.getGroups().then(data => {
      this.roles = data['groups'];
    });
    this.organizationservice.getOrganization(new Date().getTime()).then(data => {
      this.organizations = data['organization'];
    });
    if(localStorage.getItem('listadmin') == '1'){
      this.showlistadmin = true;
    }
  }

  fieldValueChange(id, value, fieldName){
    if(this.FieldsValue.length > 0){
      let checkValue = false;
      for(let i = 0; i < this.FieldsValue.length; i++){
        if(this.FieldsValue[i].id == id){
          this.FieldsValue[i].value = value;
          checkValue = true;
        }
      }
      if(!checkValue){
        this.FieldsValue.push({"id": id, "value": value, "fieldName": fieldName});
      }
    }
    else{
      this.FieldsValue.push({"id": id, "value": value, "fieldName": fieldName});
    }
    console.log('val -' +this.FieldsValue);
  }

  public handleAddressChange(address) {
    this.newaddress = address.formatted_address;
    console.log(this.getComponentByType(address,"street_number"));
    console.log(this.getComponentByType(address,"route"));
    console.log(this.getComponentByType(address,"locality"));
    console.log(this.getComponentByType(address,"administrative_area_level_1"));
    console.log(this.getComponentByType(address,"postal_code"));
    console.log(this.getComponentByType(address,"country"));
    if(this.getComponentByType(address,"street_number") != ''){
      this.users.address1 = this.getComponentByType(address,"street_number") +', ';
    }
    if(this.getComponentByType(address,"route") != ''){
      this.users.address1 += this.getComponentByType(address,"route");
    }
    this.users.city = this.getComponentByType(address,"locality");
    this.users.state = this.getComponentByType(address,"administrative_area_level_1");
    this.users.postcode = this.getComponentByType(address,"postal_code");
    this.users.country = this.getComponentByType(address,"country");
    console.log('this.users - '+JSON.stringify(this.users));
  }

  public getComponentByType(address, type) {
    if(!type)
        return null;

    if (!address || !address.address_components || address.address_components.length == 0)
        return null;

    type = type.toLowerCase();

    for (let comp of address.address_components) {
        if(!comp.types || comp.types.length == 0)
            continue;

        if(comp.types.findIndex(x => x.toLowerCase() == type) > -1)
            return comp['long_name'];
    }

    return '';
  }

  edituser(id){
    let val = ((id*5362)/12345);
    this.router.navigate(['/users/edit-user'],{queryParams:{id:window.btoa(val.toString())}});
  }

  removeblacklisteduser(id){
    this.userservice.removeblacklisteduser(id).then(data => {
      if(data['status'] == 'success'){
        let store;
        if(this.storeName.startsWith("Auto One")){
          store = this.storeName;
        }
        else{
          store = '';
        }
        this.userservice.getUsersByStatus("b", store, new Date().getTime()).then(data => {
          this.dataSource = [];
          this.collectionSize = [];
          if(data['status'] == "success"){
            this.dataSource = data['user'];
            this.collectionSize = data['user'].length;
            this.snotifyService.success('User Removed From Blacklisted.', '', this.getConfig());
          }
        });
      }
    });
  }

  deleteuser(id){
    this.userservice.deleteUser(id).then(data => {
      let store;
      if(this.storeName.startsWith("Auto One")){
        store = this.storeName;
      }
      else{
        store = '';
      }
      this.userservice.getUsertime(store,new Date().getTime()).then(data => {
        if(data['status'] == 'success'){
          this.dataSource = data['user'];
          this.collectionSize = data['user'].length;
        }
      });
      this.snotifyService.error('Deleted Successfully', '', this.getConfig());
    },
    error => {
    });
  }

  toggle(col) {
    const isChecked = this.isChecked(col);
    console.log('isChecked value - ' + isChecked);
    if(isChecked) {
      this.columns = this.columns.filter(c => {
        console.log('Name - ' + c.name);
        return c.name !== col.name;
      });
    } else {
      this.columns = [...this.columns, col];
      console.log('columns - ' + this.columns);
    }
  }

  isChecked(col) {
    return this.columns.find(c => {
      return c.name === col.name;
    });
  }

  getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
  }

  addUser(){
    this.showTable = false;
    this.showInvite = false;
  }

  inviteUser(){
    this.showTable = false;
    this.showInvite = true;
  }

  sendInvite(id,email){
    this.submitted = true;
    this.userservice.sendEmail(id,email,this.userId['organization_id']).then(data => {
      if(data['status'] == 'Failed'){
        this.snotifyService.error('User Already Exists', '', this.getConfig());
        this.submitted = false;
      }
      else if(data['status'] == 'success'){
        this.getActiveUsers();
        this.showInvite = false;
        this.showTable = true;
        this.submitted = false;
        this.inviteForm.reset();
        const { timeout, closeOnClick, ...config } = this.getConfig();
        this.snotifyService.confirm('Email Sent successfullly!', 'Success', {
	            ...config,
	            buttons: [
	                { text: 'Ok', action: (toast) => {
	                	this.snotifyService.remove(toast.id);
	                } }
	            ]
        });
      }
    });
  }
  public SelectedTab;
  selectUser(value){
    console.log('Selected - ' + this.selectedTab);
    this.SelectedTab = value.nextId;
    console.log('Selected id - ' + this.SelectedTab);
    let store;
    if(this.storeName.startsWith("Auto One")){
      store = this.storeName;
    }
    else{
      store = '';
    }
    this.userservice.getUsersByStatus(value.nextId, store, new Date().getTime()).then(data => {
      this.dataSource = [];
      this.collectionSize = [];
      if(data['status'] == "success"){
        this.dataSource = data['user'];
        this.collectionSize = data['user'].length;
      }
    });
  }

  getActiveUsers(){
    let store;
    if(this.storeName.startsWith("Auto One") || this.storeName.startsWith("National Office")){
      store = this.storeName;
    }
    else{
      store = '';
    }
    this.userservice.getUsersByStatus('all', store, new Date().getTime()).then(data => {
      this.dataSource = [];
      this.collectionSize = [];
      if(data['status'] == "success"){
        this.dataSource = data['user'];
        this.collectionSize = data['user'].length;
      }
    });
  }

  cancelEdit(){
    this.adduserForm.reset();
    this.submitted = false;
    this.showTable = true;
    this.getActiveUsers();
    //this.SelectedTab = 'y';
    //this.selectUser(this.SelectedTab);
  }

  deactivate(val, id, email, fname, lname){
    console.log('deactivate Ev - ' + this.SelectedTab);
    console.log('deactivate val - ' + val);
    this.userservice.deactivateUsers(val, id).then(data => {
      if(val == 'y'){
        this.userservice.registerSuccessEmail(id,email,(fname+' '+lname)).then(data => {
          //console.log('Email Sent - ' + JSON.stringify(data));
        });
      }
      this.dataSource = [];
      this.collectionSize = [];
      let store;
      if(this.storeName.startsWith("Auto One") || this.storeName.startsWith("National Office")){
        store = this.storeName;
      }
      else{
        store = '';
      }
      this.userservice.getUsersByStatus(this.SelectedTab, store, new Date().getTime()).then(data => {
        this.dataSource = [];
        this.collectionSize = [];
        if(data['status'] == "success"){
          this.dataSource = data['user'];
          this.collectionSize = data['user'].length;
        }
      });
      if(val == 'n'){
        this.snotifyService.success('Deactivated Successfully', '', this.getConfig());
      }
      else if(val == 'y'){
        this.snotifyService.success('Activated Successfully', '', this.getConfig());
      }
    });
    // this.userservice.deactivateUsers(id).then(data => {
    //   this.dataSource = [];this.collectionSize = [];
    //   this.userservice.getUsers(new Date().getTime()).then(data => {
    //     if(data['status'] == 'success'){
    //       this.dataSource = data['user'];
    //       this.collectionSize = data['user'].length;
    //     }
    //   });
    //   this.snotifyService.error('Deactivated Successfully', '', this.getConfig());
    // });
  }

  onReactiveFormSubmit(){
    if(this.ageCheck()){
      this.dateerror = false;
      this.submitted = true;
      let role = '';
      let organization = '';
  		if (this.adduserForm.invalid) {
        return;
      }
  		if(!this.showlistadmin){
  			this.listadmin = 3; // users groupid
  		}
      if(!this.isSuperAdmin){
        this.users.role = '3';
        this.users.organization = localStorage.getItem('oi');
      }
      let dynamicfields = '';
      if(this.extraFields){
        this.users.fields = JSON.stringify(this.FieldsValue);
      }
      else{
        this.users.fields = '';
      }
      this.users.address = this.newaddress;
      // let userDetail = {
      //   username: this.regularForm.controls['username'].value,
      //   password: this.regularForm.controls['password'].value,
      //   firstName: this.regularForm.controls['firstName'].value,
      //   lastName: this.regularForm.controls['lastName'].value,
      //   email: this.regularForm.controls['email'].value,
      //   dob: this.regularForm.controls['dob'].value,
      //   genderRadios: this.regularForm.controls['genderRadios'].value,
      //   address: this.newaddress,
      //   phone: this.regularForm.controls['phone'].value,
      //   profileImage: '',
      //   sublist: this.regularForm.controls['sublist'].value,
      //   type: '',
      //   role: role, //this.listadmin,
      //   organization: organization,
      //   fields: dynamicfields
      // }
		this.userservice.addUser(this.users).then(data => {
			if(data['status'] == "success"){
            this.snotifyService.success('User Added Successfully', '', this.getConfig());
        this.showTable = true;
        this.showInvite = false;
        this.getActiveUsers();
        this.adduserForm.reset();
        this.submitted = false;
				//this.router.navigate(['/users/users']);
			}
			else if(data['status'] == "Failed"){
        this.submitted = false;
			}
			else if(data['status'] == "User Exits"){
				if(data['value'] == "Username Already Exits."){
					this.usernameerror = true;
					this.emailerror = false;
          this.submitted = false;
				}
				else{
					this.emailerror = true;
					this.usernameerror = false;
          this.submitted = false;
				}
			}
		});
      }
      else{
        this.submitted = false;
        this.dateerror = true;
        this.users.dob = '';
      }
	}
    ageCheck(){
      let today = new Date();
      let birthDate = new Date(this.users.dob);
      let age = today.getFullYear() - birthDate.getFullYear();
      let m = today.getMonth() - birthDate.getMonth();
      if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
      }
      if(age < 18){
        return false;
      }
      else{
        return true;
      }
    }

    isNumberKey(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
    }
    return true;
  }

  blacklistUser(){
    let self = this;
    swal({
      title: "<h5>Email</h5>",
      input: "text",
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText: 'Save'
    }).then(function(save){
      console.log(JSON.stringify(save));
      if(save.value){
        self.saveBlacklistUser(save.value);
      }
    })
  }

  saveBlacklistUser(value){
    this.userservice.getSubscribedListsFromEmail(value).then(data => {
      if(data['status'] == 'success'){
        this.snotifyService.success('Blacklisted User Successfully', '', this.getConfig());
      }
      else if(data['status'] == "user blacklisted"){
        this.snotifyService.success('User is already blacklisted', '', this.getConfig());
      }
    })
  }

}

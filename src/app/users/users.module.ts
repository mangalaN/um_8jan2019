import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { UsersRoutingModule } from "./users-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AddUsersComponent } from "./add-users/add-users.component";
import { SubscribersComponent } from "./subscribers/subscribers.component";
import { UsersListComponent } from "./users-list/users-list.component";
import { ListsComponent } from "../lists/lists.component";
import { EditUserComponent } from "./edit-user/edit-user.component";
import { MoveUsersComponent } from "./move-users/move-users.component";
import { ProfileComponent } from "./profile/profile.component";
import { NgxPaginationModule } from 'ngx-pagination';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { SegmentsComponent } from './segments/segments.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CustomFormsModule,
        UsersRoutingModule,
        Ng2SmartTableModule,
        NgbModule,
        NgxPaginationModule,
        GooglePlaceModule,
        UiSwitchModule,
        NgxDatatableModule,
        SelectDropDownModule
    ],
    declarations: [
        AddUsersComponent,
        SubscribersComponent,
        UsersListComponent,
        ListsComponent,
        EditUserComponent,
        MoveUsersComponent,
        ProfileComponent,
        SegmentsComponent
    ]
})
export class UsersModule { }

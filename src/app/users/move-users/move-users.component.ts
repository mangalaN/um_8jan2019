import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { UserService } from '../../shared/data/user.service';
import { ListsService } from '../../shared/data/lists.service';

@Component({
  selector: 'app-move-users',
  templateUrl: './move-users.component.html',
  styleUrls: ['./move-users.component.scss']
})
export class MoveUsersComponent implements OnInit {

  constructor(public userservice: UserService, private snotifyService: SnotifyService, public listsservice: ListsService, private router: Router, private route: ActivatedRoute) { }

  listDetails;
  moveFrom;
  moveTo;
  selectedToList = '';
  selectedFromList = '';
  users;
  fromUsers;
  moveSelectedUser = [];
  submitted = false;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;
  val = '';
  val1 = '';

  ngOnInit() {
    this.listsservice.getlist(new Date().getTime()).then(data => {
        console.log(data['lists']);
        this.listDetails = data['lists'];
    },
    error => {
    });
    // this.userservice.getUsersSubscribtion().then(data => {
    //     console.log(data['users']);
    //     this.users = data['users'];
    // },
    // error => {
    // });
  }

  removeToList(value, event){
    this.selectedFromList = value;
    console.log(this.selectedFromList);
    this.moveFrom = event.target.options[event.target.options.selectedIndex].text;
    if(value != ''){
      this.userservice.getUsersSubscriptionList(value, new Date().getTime()).then(data => {
          console.log(data['users']);
          this.users = data['users'];
      });
    }
    else{
      this.users = [];
      // this.userservice.getUsersSubscribtion().then(data => {
      //     console.log(data['users']);
      //     this.users = data['users'];
      // },
      // error => {
      // });
    }
  }

  removeFromList(value, event){
    this.selectedToList = value;
    this.moveTo = event.target.options[event.target.options.selectedIndex].text;
    if(value != ''){
      this.userservice.getUsersSubscriptionList(value, new Date().getTime()).then(data => {
          console.log(data['users']);
          this.fromUsers = data['users'];
      });
    }
    else{
      this.fromUsers = [];
      // this.userservice.getUsersSubscribtion().then(data => {
      //     console.log(data['users']);
      //     this.fromUsers = data['users'];
      // },
      // error => {
      // });
    }
  }

  moveUser(){
    this.submitted = true;
    if(this.moveSelectedUser != []){
      this.userservice.moveSelectedUser(this.selectedToList, this.selectedFromList, this.moveSelectedUser).then(data => {
          if(data['status'] == 'success'){
              this.snotifyService.success('Users moved to selected list Successfully.', '', this.getConfig());
              this.users = [];
              this.moveSelectedUser = [];
              this.fromUsers = [];
              this.val = '';
              this.val1 = '';
              this.selectedToList = '';
              this.selectedFromList = '';
              this.moveFrom = '';
              this.moveTo = '';
              this.submitted = false;
          }
      },
      error => {
      });
    }
    else{
      this.userservice.moveUser(this.selectedToList, this.selectedFromList).then(data => {
          if(data['status'] == 'success'){
              this.snotifyService.success('Users moved to selected list Successfully.', '', this.getConfig());
              this.users = [];
              this.moveSelectedUser = [];
              this.fromUsers = [];
              this.submitted = false;
          }
      },
      error => {
      });
    }
  }

  chooseUser(uid,checked,val){
    //console.log(event);
    if(!checked){
      let arr = this.moveSelectedUser;
      for( var i = 0; i < this.moveSelectedUser.length; i++){
         if ( this.moveSelectedUser[i] == uid) {
           arr.splice(i, 1);
         }
      }
    }
    else{
      let userExsits = false;
      for(var k=0; k<this.fromUsers.length; k++){
        if(uid == this.fromUsers[k].id){
          val.checked = false;
          this.snotifyService.success('Users already exsits in list, try other user.', '', this.getConfig());
          userExsits = true;
          break;
        }
      }
      if(!userExsits){
        this.moveSelectedUser.push(uid);
      }
    }
    //console.log(arr);
    console.log(this.moveSelectedUser);
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
  }

  cancelReset(){
    this.users = [];
    this.moveSelectedUser = [];
    this.fromUsers = [];
    this.val = '';
    this.val1 = '';
    this.selectedToList = '';
    this.selectedFromList = '';
    this.moveFrom = '';
    this.moveTo = '';
    this.submitted = false;
  }
}

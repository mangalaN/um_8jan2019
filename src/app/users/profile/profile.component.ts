import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/data/user.service';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userId = '';
  user;
  details;
  timeLines;
  hobbies;

  constructor(public userService: UserService, private router: Router, private route: ActivatedRoute){
    //this.route.queryParams.subscribe(params => {this.userId = params['id'];});
    if(this.userId == ''){ this.userId = localStorage.getItem('currentUserId')};
    this.userService.getUser(this.userId, new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        this.user = data['user'][0];
        console.log(this.user);
      }
    });

    this.userService.getUserDetails(this.userId).then(data => {
      if(data['status'] == 'success'){
        this.details = data['details'];
        console.log(this.details);
      }
    });

    this.userService.getTimeLine(this.userId).then(data => {
      if(data['status'] == 'success'){
        this.timeLines = data['timeLine'];
        console.log(this.timeLines);
      }
    });
  }

  ngOnInit() {
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { loyaltypointService } from '../../shared/data/loyaltypoint.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { UserService } from '../../shared/data/user.service';
import { AddFieldsService } from '../../shared/data/addFields.service';
import { StatisticsService } from '../../shared/data/statistics.service';
import { ListsService } from '../../shared/data/lists.service';
import { MembershipService } from '../../shared/data/membership.service';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  @ViewChild('edituser') edituserForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  regularForm: FormGroup;
  userId;
  gender;
  options = {
    types: ['geocode'],
    componentRestrictions: { country: 'AU' }
  };
  newaddress = '';
  submitted = false;
  user;
  listSubscription = false;
  userImage;
  listSubCount = 0;

  loyaltyPointsColumns = [
    { name: 'Name' },
    { name: 'Redeemed Points' },
    { name: 'Remaining Points' },
    { name: 'Redeemed On' }
  ];
  loyaltyPointDisplay;

  transactionColumns = [
    { name: 'Transaction ID' },
    { name: 'User ID' },
    { name: 'Membership ID' },
    { name: 'Amount' },
    { name: 'Renewal Date' },
    { name: 'Expiry Date' }
  ];
  listSub = '';
  loyaltyPointEarned;
  loyaltyPointsEarnedColumns = [
    { name: 'Loyalty' },
    { name: 'Points Earned' },
    { name: 'Transaction ID' }
  ]

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;
  extraFields = false;
  addFields = [];
  toggleValue = false;
  textValue;
  dateValue;
  numberValue;
  floatValue;
  FieldsValue = [];
  dynamicFieldValue = [];
  details;
  timeLines;
  newsLetterDetails = [];
  listSubid = {
    'sublistid': [],
    'userId': ''
  };
  listDetails = [];
  config = {
    displayKey:"description",
    search:true,
    height: 'auto',
    placeholder:'Select',
    customComparator: ()=>{},
    limitTo: this.listDetails.length,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder:'Search',
    searchOnKey: 'name'
  }

  dateerror = false;

  userModel = {
    'username': '',
    'firstName': '',
    'email': '',
    'dob': '',
    'password': '',
    'lastName': '',
    'genderRadios': '',
    'address': '',
    'phone': '',
    'textValue': '',
    'dateValue': '',
    'numberValue': '',
    'floatValue': '',
    'toggleValue': '',
    'fields': '',
    'id': '',
    'address1':'',
    'city':'',
    'state':'',
    'postcode':'',
    'country':'',
    'membership': ''
  };
  usersModel_extra = {};
  members;

  constructor(public statisticsService: StatisticsService, public membershipService: MembershipService, public addFieldsService: AddFieldsService, private snotifyService: SnotifyService, public loyaltyPointService: loyaltypointService, public userservice: UserService, private router: Router, private route: ActivatedRoute, public listsservice: ListsService) {
    // this.regularForm = new FormGroup({
    //   'username': new FormControl(null, [Validators.required]),
    //   'firstName': new FormControl(null, [Validators.required]),
    //   'email': new FormControl(null, [Validators.required, Validators.email]),
    //   'dob': new FormControl(null, [Validators.required]),
    //   'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
    //   'lastName': new FormControl(null, [Validators.required]),
    //   'genderRadios': new FormControl(null, [Validators.required]),
    //   'address': new FormControl(null, [Validators.required]),
    //   'phone': new FormControl(null, [Validators.required]),
    //   'textValue': new FormControl(null, null),
    //   'dateValue': new FormControl(null, null),
    //   'numberValue': new FormControl(null, null),
    //   'floatValue': new FormControl(null, null),
    //   'toggleValue': new FormControl(null, null),
    //   //'sublist': new FormControl(null, null),
    //     // 'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
    //     // 'textArea': new FormControl(null, [Validators.required]),
    //     // 'radioOption': new FormControl('Option one is this')
    // }, {updateOn: 'blur'});
    this.route.queryParams.subscribe(params => {this.userId = window.atob(params['id']);});
    this.userId = ((this.userId * 12345) / 5362);
    //encodeURIComponent(window.btoa(id+"$userEdit"))
    this.addFieldsService.getFieldsByTable('users').then(data=>{
      if(data['status'] == 'success'){
        //addFields
        for(let i=0; i< data['fields'].length; i++){
          let fields = JSON.parse(data['fields'][i]['fields']);
            this.addFields.push({
              'label': fields['label'],
              'datatype':fields['datatype'],
              'length':fields['length'],
              'mandatory':fields['mandatory'],
              'validation':fields['validation'],
              'validationMsg':fields['validationMsg'],
              'fieldType':fields['fieldType'],
              'fieldName':fields['fieldName'],
              'id':data['fields'][i]['id']
            });
        }
        this.extraFields = true;
      }
      else{
        this.extraFields = false;
      }
    });

    this.membershipService.getMembership().then(data => {
      if(data['membership']){
        this.members = data['membership'];
      }
    },
    error => {
    });

    this.userservice.getUserDetails(this.userId).then(data => {
      if(data['status'] == 'success'){
        this.details = data['details'];
        this.listSubid.sublistid = data['details'][0]['listId'];
        //console.log(this.details);
      }
      else{
        this.details = '';
        this.listSubid.sublistid = [];
      }
    });

    this.listsservice.getlist(new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        //this.listDetails = data['lists'];
        //console.log('List Data - ' + JSON.stringify(data['lists']));
        for(let i = 0; i < data['lists'].length; i++){
          //this.listDetails.push(data['lists'][i].name);
          this.listDetails = [...this.listDetails, {id: parseInt(data['lists'][i].id), description: data['lists'][i].name}];
        }
        //console.log('this.listDetails - ' + JSON.stringify(this.listDetails));
      }
    });

    this.userservice.getTimeLine(this.userId).then(data => {
      if(data['status'] == 'success'){
        this.timeLines = data['timeLine'];
        //console.log(this.timeLines);
      }
      else{
        this.timeLines = '';
      }
    });
  }

  selected(event){
    //console.log('event - '+ JSON.stringify(event));
    this.listSubid.sublistid = event;
    //this.selectedMemType = event;
  }

  subscribeUsers(){
    this.listSubid.userId = this.userId;
    this.userservice.subscribeUserThroughEdit(this.listSubid).then(data => {
      if(data['status'] == 'success'){
        this.snotifyService.success('User Subscription Was Successfully', '', this.getConfig());
        this.userservice.getUserSubscription(this.userId, new Date().getTime()).then(data=>{
          if(data['status'] == 'success'){
            this.listSub = data['rows'];
          }
        });
      }
    });
  }

  fieldValueChange(id, value, fieldName){
    if(this.FieldsValue.length > 0){
      let checkValue = false;
      for(let i = 0; i < this.FieldsValue.length; i++){
        if(this.FieldsValue[i].id == id){
          this.FieldsValue[i].value = value;
          this.FieldsValue[i].fieldName = fieldName;
          checkValue = true;
        }
      }
      if(!checkValue){
        this.FieldsValue.push({"id": id, "value": value, "fieldName": fieldName});
      }
    }
    else{
      this.FieldsValue.push({"id": id, "value": value, "fieldName": fieldName});
    }
    console.log('val -' +this.FieldsValue);
  }

  ngOnInit(){
    this.userservice.getUser(this.userId, new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        this.user = data['user'][0];
        this.userModel.username = data['user'][0].username;
        this.userModel.firstName = data['user'][0].firstName;
        this.userModel.email = data['user'][0].email;
        this.userModel.dob = data['user'][0].dob;
        this.userModel.password = data['user'][0].password;
        this.userModel.lastName = data['user'][0].lastName;
        this.userModel.address = data['user'][0].address;
        if(data['user'][0].gender == 'Female'){
          this.userModel.genderRadios = 'Female';
        }
        else{
          this.userModel.genderRadios = 'Male';
        }

        this.userModel.phone = data['user'][0].phone;
        this.gender = data['user'][0].gender;
        if(this.user.listName != ''){
          this.listSubscription = true;
        }
        this.userImage = data['user'][0].profileImage;
        this.userModel.membership = data['user'][0].membership_id;
        this.FieldsValue = JSON.parse(data['user'][0].fields);
        for(let j = 0; j < this.addFields.length; j++){
          for(let i = 0; i < this.FieldsValue.length; i++){
            if(this.addFields[j].id == this.FieldsValue[i].id){
              this.addFields[j].valuesBind = this.FieldsValue[i].value;
              let modelname = this.FieldsValue[i].fieldName_extra;
              this.usersModel_extra[modelname] = this.FieldsValue[i].value;
              // if(this.addFields[j].fieldType == 'text'){
              //   this.userModel.textValue = this.FieldsValue[i].value;
              // }
              // if(this.addFields[j].fieldType == 'date'){
              //   this.regularForm.controls['dateValue'].setValue(this.FieldsValue[i].value);
              // }
              // if(this.addFields[j].fieldType == 'number'){
              //   this.regularForm.controls['numberValue'].setValue(this.FieldsValue[i].value);
              // }
              // if(this.addFields[j].fieldType == 'float'){
              //   this.regularForm.controls['floatValue'].setValue(this.FieldsValue[i].value);
              // }
              // if(this.addFields[j].fieldType == 'toggle'){
              //   this.regularForm.controls['toggleValue'].setValue(this.FieldsValue[i].value);
              // }
            }
          }
          //document.getElementById("extraField"+this.FieldsValue[i].id).innerHTMl = this.FieldsValue[i].value;
        }
        console.log(JSON.stringify(this.addFields));
      }
    });
    this.userservice.getUserSubscription(this.userId, new Date().getTime()).then(data=>{
      if(data['status'] == 'success'){
        console.log(data);
        let allLists = [];
        this.listSub = data['rows'];
        this.listSubCount = data['rows'].length;
        for(let i=0; i<this.listSubCount; i++){
          allLists.push(this.listSub[i]['list']);
        }
        this.listSubid.sublistid = allLists;
      }
      else{
        this.listSub = '';
        this.listSubCount = 0;
        this.listSubid.sublistid = [];
      }
    });
    this.loyaltyPointService.getUserLoyaltyPoints(this.userId).then(data=>{
      if(data['status'] == 'success'){
        this.loyaltyPointDisplay = data['loyalPoints'];
      }
    });
    this.statisticsService.getStatsByUser(this.userId, new Date().getTime()).then(data=>{
      if(data['status'] == 'success'){
        this.newsLetterDetails = data['reports'];
      }
    });
    this.loyaltyPointEarned = [];
  }

  onReactiveFormSubmit(){
    if(this.ageCheck()){
      this.dateerror = false;
      this.submitted = true;
      let editAddress = '';
      if(this.newaddress !== '' ){
        this.userModel.address = this.newaddress;
      }
      this.userModel.fields = JSON.stringify(this.FieldsValue);
      this.userModel.id = this.userId;
      //console.log(this.regularForm.controls['genderRadios'].value);
      // let userDetail = {
      //   username: this.regularForm.controls['username'].value,
      //   password: this.regularForm.controls['password'].value,
      //   firstName: this.regularForm.controls['firstName'].value,
      //   lastName: this.regularForm.controls['lastName'].value,
      //   email: this.regularForm.controls['email'].value,
      //   dob: this.regularForm.controls['dob'].value,
      //   genderRadios: this.regularForm.controls['genderRadios'].value,
      //   address: editAddress,
      //   phone: this.regularForm.controls['phone'].value,
      //   profileImage: this.user['profileImage'].split('/').pop().split('#')[0].split('?')[0],
      //   id: this.userId,
      //   fields: this.FieldsValue
      // }
      this.userservice.editUser(this.userModel).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('User Details Updated Successfully', '', this.getConfig());
          this.submitted = false;
          this.userservice.getUser(this.userId, new Date().getTime()).then(data => {
            if(data['status'] == 'success'){
              this.user = data['user'][0];
              // this.regularForm.controls['username'].setValue(data['user'][0].username);
              // this.regularForm.controls['firstName'].setValue(data['user'][0].firstName);
              // this.regularForm.controls['email'].setValue(data['user'][0].email);
              // this.regularForm.controls['dob'].setValue(data['user'][0].dob);
              // this.regularForm.controls['password'].setValue(data['user'][0].password);
              // this.regularForm.controls['lastName'].setValue(data['user'][0].lastName);
              // this.regularForm.controls['address'].setValue(data['user'][0].address);
              // this.regularForm.controls['genderRadios'].setValue(data['user'][0].gender);
              // this.regularForm.controls['phone'].setValue(data['user'][0].phone);
              // this.gender = data['user'][0].gender;
              if(this.user.listName != ''){
                this.listSubscription = true;
              }

            }
          });
        }
        else{
          this.snotifyService.success('Error While Updating User Details. Please Try again.', '', this.getConfig());
          this.submitted = false;
        }
      });
    }
    else{
      this.submitted = false;
      this.dateerror = true;
      this.userModel.dob = '';
    }
  }

  cancelEdit(){
    this.edituserForm.reset();
    this.router.navigate(['/users/users']);
  }

  public handleAddressChange(address){
    // Do some stuff
    this.newaddress = address.formatted_address;
    if(this.getComponentByType(address,"street_number") != ''){
      this.userModel.address1 = this.getComponentByType(address,"street_number") +', ';
    }
    if(this.getComponentByType(address,"route") != ''){
      this.userModel.address1 += this.getComponentByType(address,"route");
    }
    this.userModel.city = this.getComponentByType(address,"locality");
    this.userModel.state = this.getComponentByType(address,"administrative_area_level_1");
    this.userModel.postcode = this.getComponentByType(address,"postal_code");
    this.userModel.country = this.getComponentByType(address,"country");
  }
  public getComponentByType(address, type) {
    if(!type)
        return null;

    if (!address || !address.address_components || address.address_components.length == 0)
        return null;

    type = type.toLowerCase();

    for (let comp of address.address_components) {
        if(!comp.types || comp.types.length == 0)
            continue;

        if(comp.types.findIndex(x => x.toLowerCase() == type) > -1)
            return comp['long_name'];
    }

    return '';
  }

  unsubscribe(event, id, subscriptionId){
    console.log(event);
    let SubscriptionDetail = {
      id:this.userId,
      listId: subscriptionId
    }
    if(event){
      this.userservice.addSubscriptionUser(SubscriptionDetail).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('User Subscription Was Successfully', '', this.getConfig());
          this.userservice.getUserSubscription(this.userId, new Date().getTime()).then(data=>{
            if(data['status'] == 'success'){
              this.listSub = data['rows'];
            }
          });
          this.statisticsService.getStatsByUser(this.userId, new Date().getTime()).then(data=>{
            if(data['status'] == 'success'){
              this.newsLetterDetails = data['reports'];
            }
          });
        }
      });
    }
    else{
      this.userservice.unSubscriptionUser(SubscriptionDetail).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('User Was Successfullly Unsubscribed', '', this.getConfig());
          this.userservice.getUserSubscription(this.userId, new Date().getTime()).then(data=>{
            if(data['status'] == 'success'){
              this.listSub = data['rows'];
            }
          });
        }
      });
    }
  }

  ageCheck(){
    let today = new Date();
    let birthDate = new Date(this.userModel.dob);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    if(age < 18){
      return false;
    }
    else{
      return true;
    }
  }

  isNumberKey(evt) {
    let charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
        return false;
    }
    return true;
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
   }
}

import { Routes, RouterModule } from '@angular/router';

// Route for content layout with sidebar, navbar and footer.

export const Full_ROUTES: Routes = [
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  },
  {
    path: 'users',
    loadChildren: './users/users.module#UsersModule'
  },
  {
    path: 'configurations',
    loadChildren: './configurations/configurations.module#ConfigurationsModule'
  },
  {
    path: 'web-dashboard',
    loadChildren: './web-dashboard/web-dashboard.module#WebDashboardModule'
  },
  {
    path: 'groups',
    loadChildren: './groups/groups.module#GroupsModule'
  },
  // {
  //   path: 'calendar',
  //   loadChildren: './calendar/calendar.module#CalendarsModule'
  // },
  // {
  //   path: 'charts',
  //   loadChildren: './charts/charts.module#ChartsNg2Module'
  // },
  // {
  //   path: 'forms',
  //   loadChildren: './forms/forms.module#FormModule'
  // },
  // {
  //   path: 'maps',
  //   loadChildren: './maps/maps.module#MapsModule'
  // },
  // {
  //   path: 'tables',
  //   loadChildren: './tables/tables.module#TablesModule'
  // },
  // {
  //   path: 'datatables',
  //   loadChildren: './data-tables/data-tables.module#DataTablesModule'
  // },
  // {
  //   path: 'uikit',
  //   loadChildren: './ui-kit/ui-kit.module#UIKitModule'
  // },
  // {
  //   path: 'components',
  //   loadChildren: './components/ui-components.module#UIComponentsModule'
  // },
  // {
  //   path: 'pages',
  //   loadChildren: './pages/full-pages/full-pages.module#FullPagesModule'
  // },
  // {
  //   path: 'cards',
  //   loadChildren: './cards/cards.module#CardsModule'
  // },
  {
    path: 'statistics',
    loadChildren: './statistics/statistics.module#StatisticsModule'
  },
  {
  path: 'crews',
  loadChildren: './crews/crews.module#CrewsModule'
  },
  {
  path: 'safaripushconfig',
  loadChildren: './safaripushconfig/safaripushconfig.module#SafaripushconfigModule'
  },
  {
    path: 'transactions',
    loadChildren: './transactions/transactions.module#TransactionsModule'
  },
  // {
  //   path: 'web-engagement',
  //   loadChildren: './web-engagement/web-engagement.module#WebEngagementModule'
  // },
  {
    path: 'roles',
    loadChildren: './roles/roles.module#RolesModule'
  },
  {
    path: 'feedback',
    loadChildren: './feedback/feedback.module#FeedbackModule'
  },
  {
    path: 'lists',
    loadChildren: './lists/lists.module#ListsModule'
  },
  {
    path: 'importlog',
    loadChildren: './importlog/importlog.module#ImportLogModule'
  },
  {
    path: 'events',
    loadChildren: './events/events.module#EventsModule'
  },
  {
    path: 'membership',
    loadChildren: './membership/membership.module#MembershipModule'
  },
  {
    path: 'renewals',
    loadChildren: './renewals/renewals.module#RenewalsModule'
  },
  {
    path: 'campaign',
    loadChildren: './campaign/campaign.module#CampaignModule'
  },
  {
    path: 'emailtemplate',
    loadChildren: './email-template/email-template.module#EmailTemplateModule'
  },
  {
    path: 'loyalty',
    loadChildren: './loyalty/loyalty.module#LoyaltyModule'
  },
  {
    path: 'organization',
    loadChildren: './organization/organization.module#OrganizationModule'
  },
  {
    path: 'subscription',
    loadChildren: './subscription/subscription.module#SubscriptionModule'
  },
  {
    path: 'report',
    loadChildren: './report/report.module#ReportModule'
  },
  // {
  //   path: 'colorpalettes',
  //   loadChildren: './color-palette/color-palette.module#ColorPaletteModule'
  // },
  // {
  //   path: 'chat',
  //   loadChildren: './chat/chat.module#ChatModule'
  // },
  // {
  //   path: 'chat-ngrx',
  //   loadChildren: './chat-ngrx/chat-ngrx.module#ChatNGRXModule'
  // },
  // {
  //   path: 'taskboard',
  //   loadChildren: './taskboard/taskboard.module#TaskboardModule'
  // },
  // {
  //   path: 'taskboard-ngrx',
  //   loadChildren: './taskboard-ngrx/taskboard-ngrx.module#TaskboardNGRXModule'
  // }
];

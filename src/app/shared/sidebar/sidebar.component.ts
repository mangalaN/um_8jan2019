import { Component, OnInit } from '@angular/core';
import { ROUTES, ROUTESLOYALTY, SUPERADMINROUTES, USERROUTES, LISTADMINROUTES } from './sidebar-routes.config';
import { RouteInfo } from "./sidebar.metadata";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { SettingsService } from '../../shared/data/settings.service';

declare var $: any;

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    public loyaltymenuItems: any[];
    public superAdminmenuItems: any[];
    public userMenuItems: any[];
    public listAdminMenuItems: any[];
    public isListAdmin = false;
    public isAdmin = true;
    public isSuperAdmin = false;
    public isUser = false;
    //public loyaltyCheck;
    public users;
    public dashBoardPath;
    public autooneUser;
    public reiwaUser;
    public carferretUser;

    constructor(private router: Router, private route: ActivatedRoute, public settingsservice: SettingsService, public translate: TranslateService) {
        this.users = JSON.parse(localStorage.getItem('currentUser'));
        // this.settingsservice.getSettings().then(data => {
        //   if(data['status'] == 'success'){
        //     this.loyaltyCheck = data['settings'][0].loyaltyCheck;
        //   }
        //   else{
        //     this.loyaltyCheck = false;
        //   }
        // });
    }

    ngOnInit() {
      $.getScript('./assets/js/app-sidebar.js');

      if(this.users){
        if(this.users['username'] === 'autoone' || this.users['username'] === 'reiwa' || this.users['username'] === 'carferret.com.au'){
          if(this.users['username'] === 'autoone'){
            this.autooneUser = true;
          }
          if(this.users['username'] === 'reiwa'){
            this.reiwaUser = true;
          }
          if(this.users['username'] === 'carferret.com.au'){
            this.carferretUser = true;
          }
          this.dashBoardPath = "/web-dashboard/dashboard";
          this.settingsservice.getMenu().then(data => {
            this.menuItems = [];
            this.menuItems = data['menu'][0]['path'];
          });
        }
        else{
          let menuPaths = [];
          this.settingsservice.getMenu().then(data => {
            menuPaths = data['menu'];
            if(this.users['role_id'] == '2'){
                this.isSuperAdmin = true;
                this.isAdmin = false;
                this.isListAdmin = false;
                this.isUser = false;
                this.superAdminmenuItems = SUPERADMINROUTES.filter(menuItem => menuItem);
            }
            else if(this.users['role_id'] == '4'){  // list admin role id
                this.isListAdmin = true;
                this.isAdmin = false;
                this.isUser = false;
                for(let i = 0; i< menuPaths.length; i++){
                  if(menuPaths[i]['menu'] == 'Main Menu'){
                    this.menuItems = menuPaths[i]['path'];
                  }
                }
                this.loyaltymenuItems = this.menuItems;
                localStorage.setItem('isListAdmin', 'true');
            }
            else if(this.users['role_id'] == "3"){
              this.isUser = true;
              this.isSuperAdmin = false;
              this.isAdmin = false;
              this.isListAdmin = false;
              for(let i = 0; i< menuPaths.length; i++){
                if(menuPaths[i]['menu'] == 'User Menu'){
                  this.userMenuItems = menuPaths[i]['path'];
                }
              }
            }
            else if(this.users['role_id'] == "1"){
              this.isUser = false;
              this.isSuperAdmin = false;
              this.isAdmin = true;
              this.isListAdmin = false;
              for(let i = 0; i< menuPaths.length; i++){
                if(menuPaths[i]['menu'] == 'Main Menu'){
                  this.menuItems = menuPaths[i]['path'];
                }
              }
              this.loyaltymenuItems = this.menuItems;
            }
          });
          this.dashBoardPath = "/dashboard/charts";
        }
        //if(this.users['username'] !== 'autoone' && this.users['username'] !== 'reiwa' && this.users['username'] !== 'carferret.com.au'){
          // this.menuItems = ROUTES.filter(menuItem => menuItem);
          //if(this.isAdmin){
            //this.loyaltymenuItems = ROUTESLOYALTY.filter(menuItem => menuItem);
          //}
          //this.superAdminmenuItems = SUPERADMINROUTES.filter(menuItem => menuItem);
          //this.userMenuItems = USERROUTES.filter(menuItem => menuItem);
          //this.listAdminMenuItems = LISTADMINROUTES.filter(menuItem => menuItem);
        //}
      }
    }

}

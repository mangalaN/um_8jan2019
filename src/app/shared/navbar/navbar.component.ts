import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent {
    currentLang = 'en';
    toggleClass = 'ft-maximize';
    public isCollapsed = true;
    public isAdmin = true;
    public autooneUser;
    public pushPlan;
    public upgradePlan = false;
    closeResult;
    name;
    form = {
        'package' : '',
        'subscribers': ''
    };
    @ViewChild('f') floatingLabelForm: NgForm;

    constructor(private modalService: NgbModal,public translate: TranslateService, private router: Router, private route: ActivatedRoute) {
        const browserLang: string = translate.getBrowserLang();
        translate.use(browserLang.match(/en|es|pt|de/) ? browserLang : 'en');
        let users = JSON.parse(localStorage.getItem('currentUser'));
        this.name = users['name'];
        if(users['username'] === 'autoone'){
          this.autooneUser = true;
        }
        else{
          this.autooneUser = false;
          if (users && users['role_id'] != '4' && users['role_id'] != "1" && users['role_id'] != "2"){ //admin / super admin role id
            this.isAdmin = false;
          }
        }
        this.pushPlan = localStorage.getItem('push-plan');
    }

    ChangeLanguage(language: string) {
        this.translate.use(language);
    }

    logOut() {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('currentUserId');
        localStorage.removeItem('oi');
        localStorage.removeItem('subscriptionType');
        localStorage.removeItem('push-plan');
        this.router.navigate(['/pages/login']);
        //this.router.navigate(['login'], { relativeTo: this.route.parent });
    }

    ToggleClass() {
        if (this.toggleClass === 'ft-maximize') {
            this.toggleClass = 'ft-minimize';
        }
        else
            this.toggleClass = 'ft-maximize'
    }
    upgrade(content){
        this.upgradePlan = true;
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    open(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }
    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
        } else {
          return  `with: ${reason}`;
        }
    }
}

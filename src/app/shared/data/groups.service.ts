import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class GroupsService {
    public oi = localStorage.getItem('oi');
    constructor(private http: HttpClient) { }

    addGroups(data){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getGroups.php?action=add&oi='+this.oi+'&q='+new Date().getTime(), { data })
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getGroups(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getGroups.php?action=select&oi='+this.oi+'&q='+new Date().getTime())
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
  editGroup(id){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getGroups.php?action=select&oi='+this.oi+'&q='+new Date().getTime())
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    deleteGroup(id){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getGroups.php?action=select&oi='+this.oi+'&q='+new Date().getTime())
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class StatisticsService {
    public oi = localStorage.getItem('oi');
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getclickCounts(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=links&oi='+this.oi+'&q='+this.timestamp, { id })
            .subscribe(stats => {
                console.log(stats);
                resolve(stats);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getreports(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getStats.php?action=reports&oi='+this.oi+'&q='+this.timestamp)
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getReportById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=reportbyid&oi='+this.oi+'&q='+this.timestamp, { id })
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getEmailDomain(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=emaildomain&oi='+this.oi+'&q='+this.timestamp, { id })
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    gettimeline(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=timeline&oi='+this.oi+'&q='+this.timestamp, { id })
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getLatestReport(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getStats.php?action=latestreport&oi='+this.oi+'&q='+this.timestamp)
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getopenclickcount(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=openclickcount&oi='+this.oi+'&q='+this.timestamp, { id })
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getStatsByUser(uid, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getStats.php?action=getusernewslettercount&oi='+this.oi+'&q='+timestamp, { uid })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getOpenSubscriberList(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getStats.php?action=openList&oi='+this.oi+'&q='+this.timestamp, { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getClickSubscriberList(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getStats.php?action=clickList&oi='+this.oi+'&q='+this.timestamp, { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getUnSubscriberList(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getStats.php?action=unsubscribeList&oi='+this.oi+'&q='+this.timestamp, { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getabtestingReports(timestamp){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getStats.php?action=getabtestingReports&oi='+this.oi+'&q='+timestamp)
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getabtestingReportById(id, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getStats.php?action=getabtestingReportById&oi='+this.oi+'&q='+timestamp, { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getabTestStats(id, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getStats.php?action=getabTestStats&oi='+this.oi+'&q='+timestamp, { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getnormaltimeline(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getStats.php?action=normaltimeline&oi='+this.oi+'&q='+this.timestamp, { id })
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}

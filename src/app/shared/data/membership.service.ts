import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class MembershipService {
    public oi = localStorage.getItem('oi');
    constructor(private http: HttpClient) { }

    getMembership() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getMembership.php?action=get&oi='+this.oi+'&q='+new Date().getTime())
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    getMembershipTime(time){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getMembership.php?action=get&oi='+this.oi+'&q='+time)
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    getMembershipById(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getMembership.php?action=id&oi='+this.oi+'&q='+new Date().getTime(), {id})
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    addMembership(memDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addMembership.php?action=add&oi='+this.oi+'&q='+new Date().getTime(), { memDetail: memDetail })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    updateMembership(memDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addMembership.php?action=update&oi='+this.oi+'&q='+new Date().getTime(), { memDetail })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    deleteMembership(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addMembership.php?action=delete&oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
}

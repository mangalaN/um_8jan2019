import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ImportLogService {

    options = {
        types: ['geocode'],
        componentRestrictions: { country: 'AU' }
    }

    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getImportLog() {
        return new Promise(resolve => {
          //alert(localStorage.getItem('currentUserId'));
        this.http.get('https://click365.com.au/usermanagement/getImportLog.php?user_id='+localStorage.getItem('currentUserId')+'&q='+this.timestamp)
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
                  alert(JSON.stringify(err));
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    filter(whereClause){
    return new Promise(resolve => {
      //alert(localStorage.getItem('currentUserId'));
    this.http.get('https://click365.com.au/usermanagement/getImportLog.php?user_id='+localStorage.getItem('currentUserId')+'&q='+this.timestamp +'&whereClause=' + whereClause)
        .subscribe(lists => {
            console.log(lists);
            resolve(lists);
            }, err => {
              alert(JSON.stringify(err));
           console.log("vbn"+ JSON.stringify(err));
        });
    });
}
getImportLogById(id) {
        return new Promise(resolve => {
          //alert(localStorage.getItem('currentUserId'));
        this.http.get('https://click365.com.au/usermanagement/getImportLog.php?id='+ id +'&q='+this.timestamp)
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
                  alert(JSON.stringify(err));
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getLogDetail(task_id){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getImportLogDetails.php?task_id=' + task_id + '&q='+this.timestamp)
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/deleteImportSchedule.php?id=' + id + '&q='+this.timestamp,{})
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}

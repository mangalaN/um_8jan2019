import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class WebTemplateService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();
    public oi = localStorage.getItem('oi');

insertwebtemplate(push,selectedVal) {
  console.log('PUSH val - ' + JSON.stringify(push));
    return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/webTemplate.php?q='+new Date().getTime()+'&action=insert&oi='+this.oi, { push,selectedVal })
        .subscribe(name => {
            resolve(name);
            }, err => {
           console.log("vbn"+JSON.stringify(err));
        });
    });
}

selectwebtemplate() {
    return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/webTemplate.php?q='+new Date().getTime()+'&action=select&oi='+this.oi, {  })
        .subscribe(name => {
            resolve(name);
            }, err => {
           console.log("vbn"+JSON.stringify(err));
        });
    });
}

edittemplateById(id) {
    return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/webTemplate.php?q='+new Date().getTime()+'&action=edit&oi='+this.oi, { id })
        .subscribe(name => {
            resolve(name);
            }, err => {
           console.log("vbn"+JSON.stringify(err));
        });
    });
}

updatewebtemplate(push,id) {
    return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/webTemplate.php?q='+new Date().getTime()+'&action=update&oi='+this.oi, { push,id })
        .subscribe(name => {
            resolve(name);
            }, err => {
           console.log("vbn"+JSON.stringify(err));
        });
    });
}

  }

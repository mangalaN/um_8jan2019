import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getUsers(storeName,timestamp) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getUsers.php?action=getUsers&oi='+ localStorage.getItem('oi') +'&q='+timestamp, {storeName})
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUsersByStatus(value,storeName,timestamp) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getUsers.php?action=getUsersByStatus&oi='+ localStorage.getItem('oi') +'&q='+timestamp, {value,storeName})
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUser(id, timestamp) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getUserById.php?q='+timestamp+'&action=get&oi='+ localStorage.getItem('oi'),{ id })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUsertime(storeName,timestamp) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getUsers.php?action=getUsers&oi='+ localStorage.getItem('oi') +'&q='+timestamp,{storeName})
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    addUser(userDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addUser.php?q='+new Date().getTime()+'&oi='+ localStorage.getItem('oi'), { userDetail: userDetail })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUserByEmailAndUniId(email,uniqueId) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getUsers.php?q='+new Date().getTime()+'&action=getuserbyemailid&oi='+ localStorage.getItem('oi'), { email,uniqueId })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    editUser(userDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/editUser.php?q='+new Date().getTime()+'&oi='+ localStorage.getItem('oi'), { userDetail })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    deleteUser(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/deleteUser.php?action=delete&q='+new Date().getTime()+'&oi='+ localStorage.getItem('oi'), { id })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUsersSubscribtion(timestamp){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+timestamp+'&action=all&oi='+ localStorage.getItem('oi'))
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUsersSubscriptionList(listId, timestamp){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+timestamp+'&action=list&listId='+listId+'&oi='+ localStorage.getItem('oi'))
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    subscribeUserThroughEdit(listSubid){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/addSubscription.php?q='+new Date().getTime()+'&action=subscribeUserThroughEdit&oi='+ localStorage.getItem('oi'), { listSubid })
          .subscribe(users => {
              //console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    getGroups(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getGroups.php?action=usersgroups&q='+new Date().getTime()+'&oi='+ localStorage.getItem('oi'))
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    moveUser(ToList, FromList){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+new Date().getTime()+'&action=moveuser', { ToList, FromList })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    getUserDetails(uId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getUserById.php?q='+new Date().getTime()+'&action=userdetail&oi='+ localStorage.getItem('oi'), { uId })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    getTimeLine(uId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getUserById.php?q='+new Date().getTime()+'&action=getTimeLine&oi='+ localStorage.getItem('oi'), { uId })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    moveSelectedUser(ToList, FromList, usersId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+new Date().getTime()+'&action=moveselecteduser', { ToList, FromList, usersId })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    unSubscriptionUser(SubscriptionDetail){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/addSubscription.php?q='+new Date().getTime()+'&action=unsub', { SubscriptionDetail })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    addSubscriptionUser(SubscriptionDetail){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/addSubscription.php?q='+new Date().getTime()+'&action=resub', { SubscriptionDetail })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    getUserSubscription(id, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/addSubscription.php?q='+new Date()+'&action=getall&oi='+ localStorage.getItem('oi'), { id })
          .subscribe(users => {
              //console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    sendEmail(id,email,orgId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/sendInvite.php?action=saveinviteduser&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(), { id,email,orgId })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    resendEmail(id,email,uniqueId,orgId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/sendInvite.php?action=sendmail&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(), { id,email,uniqueId,orgId })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    registerSuccessEmail(id,email,name){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/sendInvite.php?action=registerSuccessMail&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(), { id,email,name })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    deactivateUsers(val, id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getUsers.php?action=deactivateusers&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(), { val, id })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUserrColumns(timestamp){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getUsers.php?action=getUserColumn&oi='+ localStorage.getItem('oi')+'&q='+timestamp)
          .subscribe(users => {
              //console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    getSubscribedListsFromEmail(value){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/addSubscription.php?action=getSubscribedListsFromEmail&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(), { value })
          .subscribe(users => {
              //console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    removeblacklisteduser(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/deleteUser.php?action=removeblacklisted&q='+new Date().getTime()+'&oi='+ localStorage.getItem('oi'), { id })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
}

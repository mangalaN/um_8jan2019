import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class TransactionsService {
    constructor(private http: HttpClient) { }
    public timestamp = new Date().getTime();
  public oi = localStorage.getItem('oi');

    getByCsv(link,type='') {
          return new Promise(resolve => {
          this.http.get('https:/click365.com.au/usermanagement/csv2json.php?q='+this.timestamp + '&file=' + link + '&type=' + type)
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
               alert(JSON.stringify(err));
            });
        });
    }
    getByFTP(file, server, username, password) {
        return new Promise(resolve => {
        this.http.get('https:/click365.com.au/usermanagement/ftp2json.php?q=' + this.timestamp + '&file=' + file + '&server=' + server  + '&username=' + username + '&password=' + password)
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
               alert(JSON.stringify(err));
            });
        });
    }
    getByAPI(url, headername, headervalue) {
      let url1 = 'https:/click365.com.au/usermanagement/callAPI.php?q='+this.timestamp + '&link=' + url + '&headername=' + headername  + '&headervalue=' + encodeURIComponent(headervalue);
      return new Promise(resolve => {
      this.http.get(url1)
          .subscribe(data => {
              console.log(data);
              resolve(data);
              //alert(JSON.stringify(data));
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
             alert(JSON.stringify(err));
          });
      });
  }
  getColNames(table) {
        return new Promise(resolve => {
        this.http.get('https:/click365.com.au/usermanagement/getColNames.php?q='+this.timestamp + '&table=' + table)
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
               alert(JSON.stringify(err));
            });
        });
    }
    saveTable(table, col, row) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveTable.php?q='+this.timestamp + '&table=' + table + '&col=' + col,{ row })
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);
                resolve(subscriptionValue);
                //alert(JSON.stringify(subscriptionValue));
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
               alert(JSON.stringify(err));
            });
        });
    }
    saveImport(table, col, row, importData) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getLoyalty.php?action=saveImport&oi='+this.oi+'&q='+this.timestamp + '&table=' + table + '&col=' + col,{ row,importData })
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);
                resolve(subscriptionValue);
                //alert(JSON.stringify(subscriptionValue));
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
               alert(JSON.stringify(err));
            });
        });
    }
    saveSchedule(user_id,srcCols,destCols,table,type,parameters,minute,hour,day,month,weekday,title,task) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveImportSchedule.php?user_id=' + user_id + '&q='+this.timestamp + '&table=' + table + '&task=' + task + '&title=' + title + '&srcCols=' + srcCols + '&destCols=' + destCols + '&type=' + type + '&minute=' + minute + '&hour=' + hour + '&day=' + day + '&month=' + month + '&weekday=' + weekday,{ parameters })
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);
                resolve(subscriptionValue);
                //alert(JSON.stringify(subscriptionValue));
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
               alert(JSON.stringify(err));
            });
        });
    }

    saveImportLog(task_id,start_dt,end_dt,status) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveImportLog.php?q='+this.timestamp + '&task_id=' + task_id + '&start_dt=' + start_dt + '&end_dt=' + end_dt + '&status=' + status,{})
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);
                resolve(subscriptionValue);
                //alert(JSON.stringify(subscriptionValue));
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
               alert(JSON.stringify(err));
            });
        });
    }
}

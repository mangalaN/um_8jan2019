import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class EventMappingService {
    constructor(private http: HttpClient) { }

    getEventMapping(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/saveEventMapping.php?action=get&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    saveMapping(eventMapping){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveEventMapping.php?action=add&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { eventMapping })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    deleteMapping(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveEventMapping.php?action=delete&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { id })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    editMapping(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveEventMapping.php?action=edit&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { id })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    updateMapping(eventMapping){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveEventMapping.php?action=update&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { eventMapping })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
}

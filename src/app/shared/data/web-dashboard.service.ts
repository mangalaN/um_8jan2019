import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class WebDashboardService {
    constructor(private http: HttpClient) { }

    getStats(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getWebStats.php?action=get&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getCities(state){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getwebstatsstate.php?action=state&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { state })
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebUserStats(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/get_web_user_stats.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebDashboardTiles(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/get_web_dash_tiles.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWeekDashboardTiles(){
    return new Promise(resolve => {
    this.http.get('https://click365.com.au/usermanagement/getWeeklyUserReport.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
        .subscribe(stats => {
            console.log(stats);
            resolve(stats);
            }, err => {
           console.log("vbn"+ JSON.stringify(err));
        });
    });
  }

    getWebNotificationReportNonAB(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/get_web_notification_report.php?action=nonab&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebNotificationReportAB(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/get_web_notification_report.php?action=ab&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebNotificationReportABall(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/get_web_notification_report.php?action=aball&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

  getWebNotificationReportByBrowser(campaignId,type){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/get_web_notifchart_bycampain.php?action=browser&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(),{ id: campaignId,type: type })
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebNotificationReportByLocation(campaignId,type){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/get_web_notifchart_bycampain.php?action=location&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(),{ id: campaignId,type: type })
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebNotificationReportByTime(campaignId,type){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/get_web_notifchart_bycampain.php?action=time&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(),{ id: campaignId,type: type })
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebNotificationReportByPlatform(campaignId,type){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/get_web_notifchart_bycampain.php?action=platform&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(),{ id: campaignId ,type: type})
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebNotificationReportByDevice(campaignId,type){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/get_web_notifchart_bycampain.php?action=device&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(),{ id: campaignId,type: type })
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebNotificationReportTbl(campaignId,type){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/get_web_notifchart_bycampain.php?action=tblreport&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(),{ id: campaignId ,type: type})
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getWebNotificationTokenByOrg(){
     return new Promise(resolve => {
     this.http.get('https://click365.com.au/usermanagement/get_tokens_by_org.php?action=get&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
         .subscribe(stats => {
             console.log(stats);
             resolve(stats);
             }, err => {
            console.log("vbn"+ JSON.stringify(err));
         });
     });
   }​

   getWebNotificationTokenByOrgDate(date){
    return new Promise(resolve => {
    this.http.post('https://click365.com.au/usermanagement/get_tokens_by_org.php?action=getByDate&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { date })
        .subscribe(stats => {
            console.log(stats);
            resolve(stats);
            }, err => {
           console.log("vbn"+ JSON.stringify(err));
        });
    });
  }​

   getActiveTokenByOrg(){
     return new Promise(resolve => {
     this.http.get('https://click365.com.au/usermanagement/get_tokens_by_org.php?action=active&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
         .subscribe(stats => {
             console.log(stats);
             resolve(stats);
             }, err => {
            console.log("vbn"+ JSON.stringify(err));
         });
     });
   }

   getInactiveTokenByOrg(){
     return new Promise(resolve => {
     this.http.get('https://click365.com.au/usermanagement/get_tokens_by_org.php?action=inactive&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
         .subscribe(stats => {
             console.log(stats);
             resolve(stats);
             }, err => {
            console.log("vbn"+ JSON.stringify(err));
         });
     });
   }

    getWebTokenStats(){
     return new Promise(resolve => {
     this.http.get('https://click365.com.au/usermanagement/get_tokens_by_org.php?action=statsToken&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
         .subscribe(stats => {
             console.log(stats);
             resolve(stats);
             }, err => {
            console.log("vbn"+ JSON.stringify(err));
         });
     });
   }

  getWebTokenStatsbydate(date){
    return new Promise(resolve => {
    this.http.post('https://click365.com.au/usermanagement/get_tokens_by_org.php?action=statsToken&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), {date})
        .subscribe(stats => {
            console.log(stats);
            resolve(stats);
            }, err => {
           console.log("vbn"+ JSON.stringify(err));
        });
    });
  }

  getValueforTimelineGraphs(campaignId,type){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/get_web_notifchart_bycampain.php?action=timeline&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(),{ id: campaignId,type: type })
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getCampaignCountByOrg(){
       return new Promise(resolve => {
       this.http.get('https://click365.com.au/usermanagement/getCampaignCountByOrg.php?action=nonab&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
           .subscribe(stats => {
               console.log(stats);
               resolve(stats);
               }, err => {
              console.log("vbn"+ JSON.stringify(err));
           });
       });
     }

     getSubscriberCountByOrg(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getSubscriberCountByOrg.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
            .subscribe(stats => {
                console.log(stats);
                resolve(stats);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
      }
      
    sendWinnerNotification(campaignId, type, winnerType){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/sendManualWinnerABNotification.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(),{ id: campaignId, type: type, winnerType: winnerType })
          .subscribe(stats => {
              console.log(stats);
              resolve(stats);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
}

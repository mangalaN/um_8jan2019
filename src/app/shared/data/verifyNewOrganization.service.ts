import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class VerifyNewOrganization {
    constructor(private http: HttpClient) { }

    saveOrganization(organizationDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/userOrganization.php?action=insert&q='+new Date(), { organizationDetail })
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);
                resolve(subscriptionValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    updateUserOrganization(orgId, userId) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/userOrganization.php?action=update&q='+new Date(), { orgId, userId })
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);
                resolve(subscriptionValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}

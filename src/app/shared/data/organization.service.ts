import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class OrganizationService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    saveOrganization(organizationDetail,moduleSettings,userData) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveOrganization.php?q='+new Date(), { organizationDetail,moduleSettings,userData })
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);
                resolve(subscriptionValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    ModuleSettings(moduleSettings, action, orgId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveSettings.php?action='+action+'&oi='+encodeURIComponent(window.btoa(orgId+'_u$0r'))+'&q='+new Date().getTime(), { moduleSettings })
          .subscribe(settingValue => {
              console.log(settingValue);
              resolve(settingValue);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    updateOrganization(organizationDetail){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getOrganization.php?action=updateorganization&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(), { organizationDetail })
            .subscribe(subscriptionValue => {
                console.log(subscriptionValue);
                resolve(subscriptionValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getOrganization(timestamp) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getOrganization.php?action=getorganization&oi='+ localStorage.getItem('oi')+'&q='+timestamp)
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getOrganizationById(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getOrganization.php?action=getorganizationbyid&q='+new Date(), { id })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getAdminsByOrgId(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getOrganization.php?action=getadminsbyorgid&q='+new Date(), { id })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getCharts() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getChartsForAdmin.php?action=getChartsForAdmin&q='+new Date())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getTiles() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getChartsForAdmin.php?action=getTilesForAdmin&q='+new Date())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getListCount() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getChartsForAdmin.php?action=getReportsForAdmin&q='+new Date())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    updateStatus(status, id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getChartsForAdmin.php?action=update&q='+new Date(), {status,id})
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}

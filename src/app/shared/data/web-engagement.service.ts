import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class WebEngagementService {
    constructor(private http: HttpClient) { }
    public timestamp = new Date().getTime();
    public orgId;
    public user = JSON.parse(localStorage.getItem('currentUser'));
    public oi = localStorage.getItem('oi');

    getSegments(timestamp) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/get_segment.php?action=get&oi='+ localStorage.getItem('oi')+'&q='+timestamp)
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    saveWebCampaign(scheduleData,segVal){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?action=saveWebCampaign&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(),{scheduleData,segVal})
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    saveABWebCampaign(scheduleData,tokenAData,tokenBData,segVal){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?action=saveABWebCampaign&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(),{scheduleData,tokenAData,tokenBData,segVal})
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    sendNotification(scheduleData,campaignId){
        console.log('DATA - ' + JSON.stringify(scheduleData));
        return new Promise(resolve => {
        //this.http.post('https://click365.com.au/usermanagement/send_rich_notifications.php?oi='+ localStorage.getItem('oi')+'&q='+timestamp,{safaritoken:tokendata['safaritokens'],nonsafaritoken:tokendata['nonsafaritokens'],title:sendData['floattitle'],body:sendData['floatmessage'],icon:icon,image:sendData['image'],landingURL:sendData['floaturl'] })
        this.http.post('https://click365.com.au/usermanagement/send_rich_notifications.php?oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(),{nonsafaritoken:scheduleData['nonsafaritoken'],title:scheduleData['title'],body:scheduleData['body'],icon:scheduleData['icon'],image:scheduleData['image'],landingURL:scheduleData['landingURL'],safaritoken:scheduleData['safaritoken'],campaignId,type:scheduleData['type'] })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    sendSafariNotification(scheduleData){
        return new Promise(resolve => {
        //this.http.post('https://click365.com.au/usermanagement/send_rich_notifications.php?oi='+ localStorage.getItem('oi')+'&q='+timestamp,{safaritoken:tokendata['safaritokens'],nonsafaritoken:tokendata['nonsafaritokens'],title:sendData['floattitle'],body:sendData['floatmessage'],icon:icon,image:sendData['image'],landingURL:sendData['floaturl'] })
        this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?action=sendSafariNotifications&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(),{ scheduleData })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    updateWebCampaign(id,status,count){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?action=updateWebCampaign&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(),{id,status,count})
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getTokensBySegment(segId){
        console.log('DATA - ' +localStorage.getItem('currentUser')['organization_id']);
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getTokensBySegments.php?oi='+ this.user['organization_id']+'&q='+new Date().getTime(),{segId})
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getAllTokens(segVal){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getTokensBySegments.php?oi='+ this.user['organization_id']+'&q='+new Date().getTime(),{segVal})
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getNotSentTokens(id){
        return new Promise(resolve => {
            this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?action=getNotSentTokens&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(),{id})
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getOpenCount(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?action=getCampaignList&oi='+ this.user['organization_id']+'&q='+new Date().getTime(),{id})
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getPushPlan(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/saveWebCampaign.php?action=getPushPlan&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    /* Sandeep */
    addSegment(mydata){
      return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/add_segment.php?action=add&oi='+this.oi+'&q='+Date.now(), { data: mydata })
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, err => {
          console.log("vbn"+err);
        });
      });
    }

    getSegmentByOrg() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/get_segment.php?action=get&oi='+this.oi+'&q='+Date.now())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }

    // getSegmentById(id,oi) {
    //     return new Promise(resolve => {
    //     this.http.post('https://click365.com.au/usermanagement/get_campaign.php?action=id&oi='+oi+'&q='+Date.now(), { id })
    //         .subscribe(data => {
    //             console.log(data);
    //             resolve(data);
    //             }, err => {
    //            console.log("vbn"+err);
    //         });
    //     });
    // }

    deleteSegment(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/add_segment.php?action=delete&oi='+this.oi+'&q='+Date.now(), { id })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    /* End of Sandeep's Code */

    selectwebtemplate() {
        return new Promise(resolve => {
         this.http.get('https://click365.com.au/usermanagement/webTemplate.php?q='+new Date().getTime()+'&action=select&oi='+this.oi)
            .subscribe(name => {
                resolve(name);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }

    getTemplateByName(tempname) {
        return new Promise(resolve => {
         this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?q='+new Date().getTime()+'&action=getTemplateByName&oi='+this.oi, {tempname})
            .subscribe(name => {
                resolve(name);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }

    createNotification(notifyData){
        return new Promise(resolve => {
         this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?q='+new Date().getTime()+'&action=createNotification&oi='+this.oi, {notifyData})
            .subscribe(name => {
                resolve(name);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getPushAutomation(){
        return new Promise(resolve => {
         this.http.get('https://click365.com.au/usermanagement/saveWebCampaign.php?q='+new Date().getTime()+'&action=getPushAutomation&oi='+this.oi)
            .subscribe(name => {
                resolve(name);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }

    updatePushAutomation(notifyData){
        return new Promise(resolve => {
         this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?q='+new Date().getTime()+'&action=updatePushAutomation&oi='+this.oi, {notifyData})
            .subscribe(name => {
                resolve(name);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }

    getLastCampaign(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/saveWebCampaign.php?action=getLastCampaign&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getTokensCountBySegment(segId){
         console.log('DATA - ' +localStorage.getItem('currentUser')['organization_id']);
         return new Promise(resolve => {
         this.http.post('https://click365.com.au/usermanagement/get_countstokenby_segment.php?oi='+ this.oi+'&q='+new Date().getTime(),{segId})
             .subscribe(data => {
                 console.log(data);
                 resolve(data);
                 }, err => {
                console.log("vbn"+ JSON.stringify(err));
             });
         });
     }

     getCampaignId(id){
       return new Promise(resolve => {
       this.http.post('https://click365.com.au/usermanagement/saveWebCampaign.php?action=getWebCampaignById&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(),{id})
           .subscribe(data => {
               console.log(data);
               resolve(data);
               }, err => {
              console.log("vbn"+ JSON.stringify(err));
           });
       });
   }
     getSegmentById(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/get_segment.php?action=id&oi='+ localStorage.getItem('oi')+'&q='+Date.now(), { id })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
}

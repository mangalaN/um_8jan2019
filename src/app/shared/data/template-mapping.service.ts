import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class TemplateMappingService {
    constructor(private http: HttpClient) { }

    gettemplateMapping(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/saveTemplateMapping.php?action=get&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getNewsTemp(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/saveTemplateMapping.php?action=getemailtemp&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    saveMapping(templateMapping){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveTemplateMapping.php?action=add&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { templateMapping })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getTemplateHTML(template){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getEmailTemplates.php?action=getHTML&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { template })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    deleteMapping(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveTemplateMapping.php?action=delete&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { id })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    editMapping(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveTemplateMapping.php?action=edit&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { id })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    updateMapping(templateMapping){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveTemplateMapping.php?action=update&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { templateMapping })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
}

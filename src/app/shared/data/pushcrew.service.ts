import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class pushcrewService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();
    public oi = localStorage.getItem('oi');

      savePushcrew(push) {
          return new Promise(resolve => {
            this.http.post('https://click365.com.au/usermanagement/savePushCrew.php?q='+new Date().getTime()+'&action=save&oi='+this.oi, { push })
              .subscribe(name => {
                  resolve(name);
                  }, err => {
                 console.log("vbn"+JSON.stringify(err));
              });
          });
      }
  }

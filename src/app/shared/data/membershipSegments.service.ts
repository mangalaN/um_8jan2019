import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class MembershipSegmentsService {
    constructor(private http: HttpClient) { }

    addSegment(mydata){
      return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/add_membership_segment.php?action=add&oi='+localStorage.getItem('oi')+'&q='+Date.now(), { data: mydata })
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, err => {
          console.log("vbn"+err);
        });
      });
    }

    getSegmentByOrg() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/add_membership_segment.php?action=get&oi='+localStorage.getItem('oi')+'&q='+Date.now())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }


    deleteSegment(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/add_membership_segment.php?action=delete&oi='+localStorage.getItem('oi')+'&q='+Date.now(), { id })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    /* End of Sandeep's Code */
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class loyaltyService {
  public oi = localStorage.getItem('oi');
  public timestamp = new Date().getTime();

    constructor(private http: HttpClient) { }

    addloyalty(loyaltydisplay, dynamic, timestamp) {
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/loyalty.php?oi='+this.oi+'&action=add&q='+timestamp, { loyaltydisplay, dynamic })
            .subscribe(name => {
                console.log(name);
                resolve(name);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    addReward(rewardData, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/loyalty.php?oi='+this.oi+'&action=addreward&q='+timestamp, { rewardData })
        .subscribe(name => {
            console.log(name);
            resolve(name);
            }, err => {
           console.log("vbn"+err);
        });
      });
    }
    getReward(timestamp){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/loyalty.php?oi='+this.oi+'&action=getreward&q='+timestamp)
        .subscribe(name => {
            console.log(name);
            resolve(name);
            }, err => {
           console.log("vbn"+err);
        });
      });
    }
    getRewardById(id,timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/loyalty.php?oi='+this.oi+'&action=getrewardbyid&q='+timestamp, {id})
        .subscribe(name => {
            console.log(name);
            resolve(name);
            }, err => {
           console.log("vbn"+err);
        });
      });
    }
    getMemberPoints(timestamp,userId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getLoyalty.php?oi='+this.oi+'&action=getmemberpoints&q='+timestamp, { userId })
        .subscribe(name => {
            console.log(name);
            resolve(name);
            }, err => {
           console.log("vbn"+err);
        });
      });
    }
    updateMemberPoints(timestamp,userId,points){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getLoyalty.php?oi='+this.oi+'&action=updatememberpoints&q='+timestamp, { userId,points })
        .subscribe(name => {
            console.log(name);
            resolve(name);
            }, err => {
           console.log("vbn"+err);
        });
      });
    }
    saveRedeemedPoints(timestamp, redeemedData){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/loyalty.php?oi='+this.oi+'&action=addredeemedpoints&q='+timestamp, { redeemedData })
        .subscribe(name => {
            console.log(name);
            resolve(name);
            }, err => {
           console.log("vbn"+err);
        });
      });
    }
    getLoyalty(timestamp) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getLoyalty.php?oi='+this.oi+'&action=get&q='+timestamp)
            .subscribe(name => {
                console.log(name);
                resolve(name);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    getLoyaltyById(id, timestamp) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getLoyalty.php?oi='+this.oi+'&action=id&q='+timestamp, { id })
            .subscribe(name => {
                console.log(name);
                resolve(name);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    getRuleType() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getLoyalty.php?action=getruletype&q='+this.timestamp)
            .subscribe(name => {
                console.log(name);
                resolve(name);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    getTablewithColumns(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getLoyalty.php?action=gettablewithcolumns&q='+this.timestamp)
          .subscribe(name => {
              console.log(name);
              resolve(name);
              }, err => {
             console.log("vbn"+err);
          });
      });
    }
    getDynamicFields(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getLoyalty.php?action=getdynamicfields&q='+this.timestamp)
          .subscribe(name => {
              console.log(name);
              resolve(name);
              }, err => {
             console.log("vbn"+err);
          });
      });
    }
    deleteloyalty(id, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/loyalty.php?oi='+this.oi+'&action=delete&q='+timestamp, { id })
          .subscribe(name => {
              console.log(name);
              resolve(name);
              }, err => {
             console.log("vbn"+err);
          });
      });
    }
    updateloyalty(loyaltydisplay, dynamic, timestamp) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/loyalty.php?oi='+this.oi+'&action=update&q='+timestamp, { loyaltydisplay, dynamic })
          .subscribe(name => {
              console.log(name);
              resolve(name);
              }, err => {
             console.log("vbn"+err);
          });
        });
    }
    getLoyaltyFilter(filters) {
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getLoyalty.php?oi='+this.oi+'&action=getfilters&q='+new Date().getTime(),{filters})
          .subscribe(name => {
              console.log(name);
              resolve(name);
              }, err => {
             console.log("vbn"+err);
          });
      });
  }
  getActive() {
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getLoyalty.php?oi='+this.oi+'&action=getactive&q='+new Date().getTime())
          .subscribe(name => {
              console.log(name);
              resolve(name);
              }, err => {
             console.log("vbn"+err);
          });
      });
  }
  getInActive() {
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getLoyalty.php?oi='+this.oi+'&action=getinactive&q='+new Date().getTime())
          .subscribe(name => {
              console.log(name);
              resolve(name);
              }, err => {
             console.log("vbn"+err);
          });
      });
  }
}

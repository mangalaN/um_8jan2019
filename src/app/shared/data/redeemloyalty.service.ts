import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class redeemloyaltyService {
    constructor(private http: HttpClient) { }
    public oi = localStorage.getItem('oi');

    getdata() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getRedeemPoints.php?action=get&oi='+ localStorage.getItem('oi') +'&q='+new Date().getTime())
            .subscribe(points => {
                resolve(points);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }


    getdataByuser(loyaltypoint) {
            return new Promise(resolve => {
            this.http.post('https://click365.com.au/usermanagement/getRedeemPoints.php?q='+new Date().getTime()+'&action=getuser&oi='+this.oi, { loyaltypoint })
                .subscribe(name => {
                    resolve(name);
                    }, err => {
                   console.log("vbn"+err);
                });
            });
        }


    updateFilters(loyaltypoint) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getRedeemPoints.php?q='+new Date().getTime()+'&action=filters&oi='+this.oi, { loyaltypoint })
            .subscribe(name => {
                resolve(name);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    updateFiltersbyuser(loyaltypoint,id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getRedeemPoints.php?q='+new Date().getTime()+'&action=filterbyuser&oi='+this.oi, { loyaltypoint,id })
            .subscribe(name => {
                resolve(name);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
}

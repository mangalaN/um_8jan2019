import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class FeedbackService {
    public oi = localStorage.getItem('oi'); 
    //public timestamp = new Date().getTime();
    constructor(private http: HttpClient) {
    }

    addCampaign(mydata){
      return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/add_campaign.php?action=add&oi='+this.oi+'&q='+Date.now(), { data: mydata })
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, err => {
          console.log("vbn"+err);
        });
      });
    }

    getCampaignByOrg() { 
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/get_campaign.php?action=get&oi='+this.oi+'&q='+Date.now())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }

    getCampaignById(id,oi) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/get_campaign.php?action=id&oi='+oi+'&q='+Date.now(), { id })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }

    deleteCampaign(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/add_campaign.php?action=delete&oi='+this.oi+'&q='+Date.now(), { id })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }

    addSubmission(mydata,oi){
        return new Promise(resolve => {
            this.http.post('https://click365.com.au/usermanagement/add_submission.php?action=add&oi='+oi+'&q='+Date.now(), { data: mydata })
            .subscribe(data => {
                console.log(data);
                resolve(data);
            }, err => {
                console.log("vbn"+err);
            });
        });
    }

    getSubmissionsById(myid){
        return new Promise(resolve => {
            this.http.post('https://click365.com.au/usermanagement/get_submissions_id.php?action=id&oi='+this.oi+'&q='+Date.now(), {id: myid})
            .subscribe(data => {
                resolve(data);
            }, err => {
                console.log("vbn"+err);
            });
        });
    }

    getSubmissionsByIdFilter(myid,filters){
        return new Promise(resolve => {
            this.http.post('https://click365.com.au/usermanagement/get_submissions_id.php?action=filter&oi='+this.oi+'&q='+Date.now(), {id: myid,filters: filters})
            .subscribe(data => {
                resolve(data);
            }, err => {
                console.log("vbn"+err);
            });
        });
    }

    getTablewithColumns(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getLoyalty.php?action=gettablewithcolumns&q='+Date.now())
          .subscribe(name => {
              console.log(name);
              resolve(name);
              }, err => {
             console.log("vbn"+err);
          });
      });
    }

    getDynamicFields(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getLoyalty.php?action=getdynamicfields&q='+Date.now())
          .subscribe(name => {
              console.log(name);
              resolve(name);
              }, err => {
             console.log("vbn"+err);
          });
      });
    }

    updateTblMap(id,mydata){
        return new Promise(resolve => {
            this.http.post('https://click365.com.au/usermanagement/add_campaign.php?action=update&id='+id+'&oi='+this.oi+'&q='+Date.now(), {data: mydata})
            .subscribe(data => {
                resolve(data);
            }, err => {
                console.log("vbn"+err);
            });
        });
    }

    saveDashboardChart(chartDetail){
            return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/add_fb_dashboardchart.php?oi='+this.oi+'&action=add&q='+Date.now(), { data: chartDetail})
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }


    getfbDashboardCharts(){
      return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/get_fb_dashboardchat.php?action=get&oi='+this.oi+'&q='+Date.now())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }



    addSegment(mydata){
      return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/add_segment.php?action=add&oi='+this.oi+'&q='+Date.now(), { data: mydata })
        .subscribe(data => {
          console.log(data);
          resolve(data);
        }, err => {
          console.log("vbn"+err);
        });
      });
    }

    getSegmentByOrg() { 
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/get_segment.php?action=get&oi='+this.oi+'&q='+Date.now())
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }

    // getSegmentById(id,oi) {
    //     return new Promise(resolve => {
    //     this.http.post('https://click365.com.au/usermanagement/get_campaign.php?action=id&oi='+oi+'&q='+Date.now(), { id })
    //         .subscribe(data => {
    //             console.log(data);
    //             resolve(data);
    //             }, err => {
    //            console.log("vbn"+err);
    //         });
    //     });
    // }

    deleteSegment(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/add_segment.php?action=delete&oi='+this.oi+'&q='+Date.now(), { id })
            .subscribe(data => {
                console.log(data);
                resolve(data);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }


}

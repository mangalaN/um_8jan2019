import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AddFieldsService {
    public oi = localStorage.getItem('oi');
    public adminId = localStorage.getItem('currentUserId');
    constructor(private http: HttpClient) {}

    getFields(currentTime){
    	let details = {
    		'oi' : localStorage.getItem('oi'),
			'adminId' : localStorage.getItem('currentUserId')
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=get&q='+new Date().getTime(), { details })
            .subscribe(addfields => {
                console.log(addfields);
                resolve(addfields);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getFieldsByTable(tableName){
    	let details = {
    		  'oi' : localStorage.getItem('oi'),
          'adminId' : localStorage.getItem('currentUserId'),
          'tableName': tableName
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=getbytable&q='+new Date().getTime(), { details })
            .subscribe(addfields => {
                console.log(addfields);
                resolve(addfields);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getFieldsById(id, currentTime){
    	let details = {
    		'id': id,
    		'oi' : localStorage.getItem('oi'),
			'adminId' : localStorage.getItem('currentUserId')
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=getId&q='+new Date().getTime(), { details })
            .subscribe(addfields => {
                console.log(addfields);
                resolve(addfields);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addFields(fieldsVal, currentTime){
    	let details = {
    		'fieldsVal': fieldsVal,
    		'oi' : localStorage.getItem('oi'),
			'adminId' : localStorage.getItem('currentUserId')
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=add&q='+new Date().getTime(), { details })
            .subscribe(addfields => {
                console.log(addfields);
                resolve(addfields);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    editFields(fieldsVal, currentTime){
    	let details = {
    		'fieldsVal': fieldsVal,
    		'oi' : localStorage.getItem('oi'),
			'adminId' : localStorage.getItem('currentUserId')
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=edit&q='+new Date().getTime(), { details })
            .subscribe(addfields => {
                console.log(addfields);
                resolve(addfields);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteFields(fieldsVal, currentTime){
    	let details = {
    		'id': fieldsVal,
    		'oi' : localStorage.getItem('oi'),
			'adminId' : localStorage.getItem('currentUserId')
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=delete&q='+new Date().getTime(), { details })
            .subscribe(addfields => {
                console.log(addfields);
                resolve(addfields);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getColumns(tableName){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getUsers.php?action=getUserColumn&oi='+ localStorage.getItem('oi')+'&q='+new Date().getTime(), {tableName})
          .subscribe(columns => {
              //console.log(users);
              resolve(columns);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
}

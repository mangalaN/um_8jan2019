import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class SettingsService {

    public timestamp = new Date().getTime();
    public oi = localStorage.getItem('oi');
    public adminId = localStorage.getItem('currentUserId');

    constructor(private http: HttpClient) { }

    saveSettings(settingDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveSettings.php?action=saveSettings&oi='+this.oi+'&q='+new Date().getTime(), { settingDetail })
            .subscribe(settingValue => {
                console.log(settingValue);
                resolve(settingValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getSettings() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getSettings.php?oi='+this.oi+'&q='+new Date().getTime())
            .subscribe(settingValue => {
                console.log(settingValue);
                resolve(settingValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    updateSettings(settingDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/updateSettings.php?oi='+this.oi+'&q='+new Date().getTime(), { settingDetail })
            .subscribe(settingValue => {
                console.log(settingValue);
                resolve(settingValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    ModuleSettings(moduleSettings, action){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveSettings.php?action='+action+'&oi='+this.oi+'&q='+new Date().getTime(), { moduleSettings })
          .subscribe(settingValue => {
              console.log(settingValue);
              resolve(settingValue);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getMenu(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/saveSettings.php?action=getMenu&oi='+this.oi+'&q='+new Date().getTime())
          .subscribe(settingValue => {
              console.log(settingValue);
              resolve(settingValue);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    addMenu(mainMenu, userMenu, action){
      if(action == 'saveModule'){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveSettings.php?action=addmenu&oi='+this.oi+'&q='+new Date().getTime(), { mainMenu, userMenu })
            .subscribe(settingValue => {
                console.log(settingValue);
                resolve(settingValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
      }
      else{
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveSettings.php?action=updatemenu&oi='+this.oi+'&q='+new Date().getTime(), { mainMenu, userMenu })
            .subscribe(settingValue => {
                console.log(settingValue);
                resolve(settingValue);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
      }
    }
    addMenuOrgId(mainMenu, userMenu, action, orgId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveSettings.php?action=addmenu&oi='+encodeURIComponent(window.btoa(orgId+'_u$0r'))+'&q='+new Date().getTime(), { mainMenu, userMenu })
          .subscribe(settingValue => {
              console.log(settingValue);
              resolve(settingValue);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    /*getEmailTempById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getEmailTemplateById.php?q='+new Date().getTime(), { id })
            .subscribe(emailtem => {
                console.log(emailtem);
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteEmailTempById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/deleteEmailTemplateById.php?q='+new Date().getTime(), { id })
            .subscribe(emailtem => {
                console.log(emailtem);
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }*/
    safariNotification(data){
   return new Promise(resolve => {
   this.http.post('https://click365.com.au/usermanagement/safariPushNotification.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { data })
       .subscribe(emailtem => {
           console.log(emailtem);
           resolve(emailtem);
           }, err => {
          console.log("vbn"+ JSON.stringify(err));
       });
   });
 }
 saveSafariConfig(data,action){
    return new Promise(resolve => {
    this.http.post('https://click365.com.au/usermanagement/safari_config.php?action=' + action + '&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { data })
        .subscribe(emailtem => {
            console.log(emailtem);
            resolve(emailtem);
            }, err => {
           console.log("vbn"+ JSON.stringify(err));
        });
    });
  }
   getSafariConfig(){
    return new Promise(resolve => {
    this.http.get('https://click365.com.au/usermanagement/getSafariConfig.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
        .subscribe(emailtem => {
            console.log(emailtem);
            resolve(emailtem);
            }, err => {
           console.log("vbn"+ JSON.stringify(err));
        });
    });
  }
  }

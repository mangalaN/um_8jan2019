import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class EventsService {

    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getevent(timestamp) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/events.php?action=get&oi='+localStorage.getItem('oi')+'&q='+timestamp)
            .subscribe(events => {
                console.log(events);
                resolve(events);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addevent(events, action) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events.php?action='+action+'&oi='+localStorage.getItem('oi')+'&q='+this.timestamp, { events })
            .subscribe(events => {
                console.log(events);
                resolve(events);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    geteventById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events.php?action=getbyid&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(events => {
                console.log(events);
                resolve(events);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteeventById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events.php?action=delete&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(events => {
                console.log(events);
                resolve(events);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getticket(event_id) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/events_ticket.php?action=get&oi='+localStorage.getItem('oi')+'&q='+this.timestamp + '&event_id=' + event_id)
            .subscribe(tickets => {
                console.log(tickets);
                resolve(tickets);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addticket(tickets, action) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_ticket.php?action='+action+'&oi='+localStorage.getItem('oi')+'&q='+this.timestamp, { tickets })
            .subscribe(tickets => {
                console.log(tickets);
                resolve(tickets);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getticketById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_ticket.php?action=getbyid&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(tickets => {
                console.log(tickets);
                resolve(tickets);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteticketById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_ticket.php?action=delete&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(tickets => {
                console.log(tickets);
                resolve(tickets);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    addlocation(location, action) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_location.php?action='+action+'&oi='+localStorage.getItem('oi')+'&q='+this.timestamp, { location })
            .subscribe(events => {
                console.log(events);
                resolve(events);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getlocationById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_location.php?action=getbyid&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(events => {
                console.log(events);
                resolve(events);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getlocation(event_id) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/events_location.php?action=getbyid&oi='+localStorage.getItem('oi')+'&q='+this.timestamp + '&event_id=' + event_id)
            .subscribe(location => {
                console.log(location);
                resolve(location);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getcontact(event_id) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/events_contact.php?action=get&oi='+localStorage.getItem('oi')+'&q='+this.timestamp + '&event_id=' + event_id)
            .subscribe(contacts => {
                console.log(contacts);
                resolve(contacts);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addcontact(contacts, action) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_contact.php?action='+action+'&oi='+localStorage.getItem('oi')+'&q='+this.timestamp, { contacts })
            .subscribe(contacts => {
                console.log(contacts);
                resolve(contacts);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getcontactById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_contact.php?action=getbyid&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(contacts => {
                console.log(contacts);
                resolve(contacts);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deletecontactById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_contact.php?action=delete&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(contacts => {
                console.log(contacts);
                resolve(contacts);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteImgById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_images.php?action=delete&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(contacts => {
                console.log(contacts);
                resolve(contacts);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getimages(event_id) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/events_images.php?action=get&oi='+localStorage.getItem('oi')+'&q='+this.timestamp + '&event_id=' + event_id)
            .subscribe(contacts => {
                console.log(contacts);
                resolve(contacts);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getsponsor(event_id) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/events_sponsor.php?action=get&oi='+localStorage.getItem('oi')+'&q='+this.timestamp + '&event_id=' + event_id)
            .subscribe(sponsors => {
                console.log(sponsors);
                resolve(sponsors);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addsponsor(sponsors, action) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_sponsor.php?action='+action+'&oi='+localStorage.getItem('oi')+'&q='+this.timestamp, { sponsors })
            .subscribe(sponsors => {
                console.log(sponsors);
                resolve(sponsors);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getsponsorById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_sponsor.php?action=getbyid&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(sponsors => {
                console.log(sponsors);
                resolve(sponsors);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deletesponsorById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/events_sponsor.php?action=delete&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(sponsors => {
                console.log(sponsors);
                resolve(sponsors);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }


}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();
    public orgId;
    public oi = localStorage.getItem('oi');

    login(username, password) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/login.php?q='+this.timestamp, { username: username, password: password })
            .subscribe(user => {
                console.log(user);
                // login successful if there's a jwt token in the response `${config.apiUrl}/users/authenticate`
                if (user && user['status'] == 'success') {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUserId', user['user']['id']);
                    console.log('ST - ' + JSON.stringify(user['user']));
                    localStorage.setItem('currentUser', JSON.stringify(user['user']));
                    localStorage.setItem('subscriptionType', user['user']['subscription_type']);
                    localStorage.setItem('oi', encodeURIComponent(window.btoa(user['user']['organization_id']+'_u$0r')));
                }

                resolve(user);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }

    resetPassword(passwordDetail){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/resetPassword.php', { passwordDetail })
            .subscribe(user => {
                //console.log(user);
                // login successful if there's a jwt token in the response `${config.apiUrl}/users/authenticate`
                // if (user && user['status'] == 'success') {
                //     // store user details and jwt token in local storage to keep user logged in between page refreshes
                //     localStorage.setItem('currentUser', JSON.stringify(user['user']));
                // }

                resolve(user);
                }, err => {
               console.log("vbn"+err);
            });
        });
      }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('currentUserId');
        localStorage.removeItem('oi');
        localStorage.removeItem('subscriptionType');
    }

    addregister(registerDetail) {
          return new Promise(resolve => {
          this.http.post('https://click365.com.au/usermanagement/register.php?oi='+this.oi, { registerDetail })
              .subscribe(user => {
                  console.log(user);
                  // login successful if there's a jwt token in the response `${config.apiUrl}/users/authenticate`
                  // if (user && user['status'] == 'success') {
                  //     // store user details and jwt token in local storage to keep user logged in between page refreshes
                  //     localStorage.setItem('currentUser', JSON.stringify(user['user']));
                  // }

                  resolve(user);
                  }, err => {
                 console.log("vbn"+err);
              });
          });
      }
      
      addsimpleregister(registerDetail){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/simpleregister.php?q='+new Date(), { registerDetail })
            .subscribe(user => {
                console.log(user);
                // login successful if there's a jwt token in the response `${config.apiUrl}/users/authenticate`
                // if (user && user['status'] == 'success') {
                //     // store user details and jwt token in local storage to keep user logged in between page refreshes
                //     localStorage.setItem('currentUser', JSON.stringify(user['user']));
                // }

                resolve(user);
                }, err => {
               console.log("vbn"+err);
            });
        });
      }

    uploadFile(file){
        // const httpOptions = {
        //     headers: new HttpHeaders({
        //         'Content-Type':  'application/json'
        //     })
        // };
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/uploadfile.php?q='+this.timestamp, file)
            .subscribe(user => {
                console.log(user);
                // login successful if there's a jwt token in the response `${config.apiUrl}/users/authenticate`, httpOptions
                resolve(user);
            }, err => {
               console.log("vbn - "+JSON.stringify(err));
            });
        });
    }

    getInvitedUserData(uniqueid){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getOrganization.php?q='+this.timestamp+'&action=getinviteduserbyid&oi='+this.oi, { uniqueid })
            .subscribe(user => {
                resolve(user);
            }, err => {
               console.log("vbn - "+JSON.stringify(err));
            });
        });
    }
    getinvitedusersstatus(timestamp){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getOrganization.php?q='+timestamp+'&action=getinvitedusersstatus&oi='+this.oi)
            .subscribe(user => {
                resolve(user);
            }, err => {
               console.log("vbn - "+JSON.stringify(err));
            });
        });
    }
    getInvitedUsersByEmail(email,timestamp){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getOrganization.php?q='+timestamp+'&action=getinviteduserbyemail&oi='+this.oi,{email})
            .subscribe(user => {
                resolve(user);
            }, err => {
               console.log("vbn - "+JSON.stringify(err));
            });
        });
    }
    getRoleAndOrg(roleId, orgId){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getOrganization.php?q='+this.timestamp+'&action=getroleandorg', { roleId, orgId })
            .subscribe(user => {
                resolve(user);
            }, err => {
               console.log("vbn - "+JSON.stringify(err));
            });
        });
    }
}

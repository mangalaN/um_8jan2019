import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class newsLetterService {
    public oi = localStorage.getItem('oi');
    constructor(private http: HttpClient) { }

    saveNewletter(newsletter) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveNewletter.php?action=normal&oi='+this.oi+'&q='+new Date().getTime(), { newsletter })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getNewletter() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getNewletter.php?action=normal&oi='+this.oi+'&q='+new Date().getTime())
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getNewletterTimeLine(time){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getNewletter.php?action=normal&oi='+this.oi+'&q='+time)
          .subscribe(newsletter => {
              console.log(newsletter);
              resolve(newsletter);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    sendNewletter(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/sendEmail.php?oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    sendTestEmail(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/sendTestEmail.php?oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    editNewsletter(newsletterDetail){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/editNewsletter.php?oi='+this.oi+'&q='+new Date().getTime(), { newsletterDetail })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    copyNewsletter(id){
      return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/copyNewsletter.php?oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    saveabtestingNewletter(newsletter, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveNewletter.php?action=abtesting&oi='+this.oi+'&q='+timestamp, { newsletter })
          .subscribe(newsletter => {
              console.log(newsletter);
              resolve(newsletter);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    sendABTestingNewletter(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/SendABTestingEmail.php?action=test&oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    sendABTestingNewletterwinner(id, winnerRate){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/SendABTestingEmail.php?action=winner&oi='+this.oi+'&q='+new Date().getTime(), { id, winnerRate })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getABTestingNewletter(timestamp){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getNewletter.php?action=abtesting&oi='+this.oi+'&q='+timestamp)
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getABTestingWinner(id, winnerRate){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getNewletter.php?action=abtestingwinner&oi='+this.oi+'&q='+new Date().getTime(), {id, winnerRate})
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    ScheduleNewsletter(details){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveNewletter.php?action=schedule&oi='+this.oi+'&q='+new Date().getTime(), { details })
          .subscribe(newsletter => {
              console.log(newsletter);
              resolve(newsletter);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getabtestingResult(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getNewletter.php?action=getabtestingResult&oi='+this.oi+'&q='+new Date().getTime(), { id })
          .subscribe(newsletter => {
              console.log(newsletter);
              resolve(newsletter);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getFailedCampaigns(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getNewletter.php?action=getfailedcampaign&oi='+this.oi+'&q='+new Date().getTime())
          .subscribe(newsletter => {
              console.log(newsletter);
              resolve(newsletter);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getMembersSegment(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getNewletter.php?action=getSegments&oi='+this.oi+'&q='+new Date().getTime())
          .subscribe(newsletter => {
              console.log(newsletter);
              resolve(newsletter);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getUserMembersSegment(condition){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getNewletter.php?action=getSegmentCondition&oi='+this.oi+'&q='+new Date().getTime(), {condition})
          .subscribe(newsletter => {
              console.log(newsletter);
              resolve(newsletter);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
}

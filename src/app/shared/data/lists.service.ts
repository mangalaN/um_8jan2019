import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ListsService {

    options = {
        types: ['geocode'],
        componentRestrictions: { country: 'AU' } 
    }

    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getlist(timestamp) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/list.php?action=get&oi='+localStorage.getItem('oi')+'&q='+timestamp)
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getNewsletterList(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/list.php?action=getNews&oi='+localStorage.getItem('oi')+'&q='+this.timestamp)
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addlist(lists, action) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/list.php?action='+action+'&oi='+localStorage.getItem('oi')+'&q='+this.timestamp, { lists })
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getlistById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/list.php?action=getbyid&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deletelistById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/list.php?action=delete&oi='+ localStorage.getItem('oi') +'&q='+this.timestamp, { id })
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getUsersByListId(listId){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+this.timestamp+'&action=list&listId='+listId+'&oi='+ localStorage.getItem('oi'))
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ABTestingStatisticsService {
    public oi = localStorage.getItem('oi');
    constructor(private http: HttpClient) { }

    getclickCounts(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=links&oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(stats => {
                console.log(stats);
                resolve(stats);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getreports(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getABTestingStats.php?action=reports&oi='+this.oi+'&q='+new Date().getTime())
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getReportById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=reportbyid&oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getabtestingEmailDomain(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=emaildomain&oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    gettimeline(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=timeline&oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getLatestReport(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getABTestingStats.php?action=latestreport&oi='+this.oi+'&q='+new Date().getTime())
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getopenclickcount(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=openclickcount&oi='+this.oi+'&q='+new Date().getTime(), { id })
            .subscribe(reports => {
                console.log(reports);
                resolve(reports);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    getStatsByUser(uid ){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=getusernewslettercount&oi='+this.oi+'&q='+new Date().getTime(), { uid })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getOpenSubscriberList(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=openList&oi='+this.oi+'&q='+new Date().getTime(), { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getClickSubscriberList(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=clickList&oi='+this.oi+'&q='+new Date().getTime(), { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getUnSubscriberList(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=unsubscribeList&oi='+this.oi+'&q='+new Date().getTime(), { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getabtestingReports(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getABTestingStats.php?action=getabtestingReports&oi='+this.oi+'&q='+new Date().getTime())
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getabtestingReportById(id ){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=getabtestingReportById&oi='+this.oi+'&q='+new Date().getTime(), { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getabTestStats(id ){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getABTestingStats.php?action=getabTestStats&oi='+this.oi+'&q='+new Date().getTime(), { id })
          .subscribe(reports => {
              console.log(reports);
              resolve(reports);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class EmailTemplateService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();
    defaultTemplate = '<table bgcolor="#F0F0F0" border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody> <tr> <td align="center" bgcolor="#ffffff" style="background-color: #ffffff;" valign="top"> <br> <table border="0" cellpadding="0" cellspacing="0" style="width: 600px;background: #ffffff;border: 2px solid #b7b7b7;"> <tbody> <tr style="background: #1e1e1e;"> <td height="20">&nbsp;</td> </tr> <tr> <td align="center" style="background: #1e1e1e;color:#ffffff;">[LOGO]</td></tr> <tr style="background: #fff;"> <td align="left" style="padding: 12px 24px 12px 24px;"> <p style="font-family:Verdana, Geneva, sans-serif;font-size: 24px;color: #000;">Hello [NAME],</p> </td> </tr> <tr> <td align="left" style="background-color: #ffffff;padding: 0px 24px 0px 24px;"> <p style="font-family:Verdana, Geneva, sans-serif;font-size: 14px;line-height: 20px;    margin: 0;text-align: left;color: #000;"> <br> </p> <p style="font-family:Verdana,Geneva, sans-serif;font-size: 14px;line-height: 20px;text-align: left;color: #000;"> <br> </p> </td> </tr> <tr></tr> <tr style="background: #fff;"> <td bgcolor="#07253f" style="background: #ffffff;padding: 15px 18px;-webkit-border-radius: 4px;border-radius: 4px;"> <br> </td> </tr> <tr style="background: #fff;"> <td height="35">&nbsp;</td> </tr> <tr> <td style="border-bottom: 1px solid #DDDDDD;background: #fff;">&nbsp;</td> </tr> <tr style="background: #1e1e1e;"><td> <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"> <tbody> <tr> <td align="left" style="font-family:Verdana, Geneva, sans-serif;" valign="top"> <p style="text-align: left; color: #ffffff;text-align: center; font-size: 12px; font-weight: normal; line-height: 20px;">&copy;[DATE] [COMPANY NAME]</p> </td> <td width="30">&nbsp;</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table>';

    getDefaultTemplate() {
        return this.defaultTemplate;
    }
    getEmailTemp() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getEmailTemplates.php?action=get&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
            .subscribe(emailtem => {
                console.log(emailtem);
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getEmailTempTimeStamp(time) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getEmailTemplates.php?action=get&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
            .subscribe(emailtem => {
                console.log(emailtem);
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addEmailTemp(emTemDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addEmailTemplate.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { emTemDetail })
            .subscribe(emailtem => {
                console.log(emailtem);
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getEmailTempById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getEmailTemplateById.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { id })
            .subscribe(emailtem => {
                console.log(emailtem);
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    updateEmailTemp(emTemupdateDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/updateEmailTemplate.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { emTemupdateDetail })
            .subscribe(emailtem => {
                console.log(emailtem);
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteEmailTempById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/deleteEmailTemplateById.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { id })
            .subscribe(emailtem => {
                console.log(emailtem);
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getNewsTemp(time){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getEmailTemplates.php?action=news&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    getUniqueId(value){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getEmailTemplates.php?action=unique&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { value })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    apiKeys() {
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/randomString.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
          .subscribe(settingValue => {
              console.log(settingValue);
              resolve(settingValue);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
  }

secretkey(data){
 return new Promise(resolve => {
 this.http.post('https://click365.com.au/usermanagement/secretkeys.php?oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { data })
     .subscribe(emailtem => {
         console.log(emailtem);
         resolve(emailtem);
         }, err => {
        console.log("vbn"+ JSON.stringify(err));
     });
 });
}
getEmailTempByName(name){
      return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getEmailTemplates.php?action=getByName&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { name })
         .subscribe(emailtem => {
             console.log(emailtem);
             resolve(emailtem);
             }, err => {
            console.log("vbn"+ JSON.stringify(err));
         });
      });
    }

    addEmailAutomation(emailData){
     return new Promise(resolve => {
     this.http.post('https://click365.com.au/usermanagement/emailAutomation.php?action=add&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { emailData })
         .subscribe(emailtem => {
             console.log(emailtem);
             resolve(emailtem);
             }, err => {
            console.log("vbn"+ JSON.stringify(err));
         });
     });
    }

    getEmailAutomation(){
     return new Promise(resolve => {
     this.http.get('https://click365.com.au/usermanagement/emailAutomation.php?action=get&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
         .subscribe(emailtem => {
             console.log(emailtem);
             resolve(emailtem);
             }, err => {
            console.log("vbn"+ JSON.stringify(err));
         });
     });
    }

    deleteEmailAutomation(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/emailAutomation.php?action=delete&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { id })
          .subscribe(emailtem => {
              console.log(emailtem);
              resolve(emailtem);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getEmailAutomationById(id){
     return new Promise(resolve => {
     this.http.post('https://click365.com.au/usermanagement/emailAutomation.php?action=getById&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime(), { id })
         .subscribe(emailtem => {
             console.log(emailtem);
             resolve(emailtem);
             }, err => {
            console.log("vbn"+ JSON.stringify(err));
         });
     });
    }

    emailEvents(){
      return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getEmailTemplates.php?action=event&oi='+localStorage.getItem('oi')+'&q='+new Date().getTime())
        .subscribe(emailtem => {
          console.log(emailtem);
          resolve(emailtem);
          }, err => {
            console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
}

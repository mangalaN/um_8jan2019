import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class click365webpushService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();
    public oi = localStorage.getItem('oi');

      saveclick365webpush(push) {
          return new Promise(resolve => {
            this.http.post('https://click365.com.au/usermanagement/saveclick365webpush.php?q='+new Date().getTime()+'&action=save&oi='+this.oi, { push })
              .subscribe(name => {
                  resolve(name);
                  }, err => {
                 console.log("vbn"+JSON.stringify(err));
              });
          });
      }

      selectclick365webpush() {
    return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveclick365webpush.php?q='+new Date().getTime()+'&action=select&oi='+this.oi, {  })
        .subscribe(name => {
            resolve(name);
            }, err => {
           console.log("vbn"+JSON.stringify(err));
        });
    });
}

updateclick365webpush(push) {
    return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/saveclick365webpush.php?q='+new Date().getTime()+'&action=update&oi='+this.oi, { push })
        .subscribe(name => {
            resolve(name);
            }, err => {
           console.log("vbn"+JSON.stringify(err));
        });
    });
}
  }

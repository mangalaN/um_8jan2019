import { Component, OnInit,ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { FormControl, FormGroup, FormBuilder,Validators, NgForm } from '@angular/forms';
import { crewsService } from '../shared/data/crews.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

@Component({
  selector: 'app-crews',
  templateUrl: './crews.component.html',
  styleUrls: ['./crews.component.scss']
})
export class CrewsComponent implements OnInit {
  crewsForm : FormGroup;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;
  @ViewChild(DatatableComponent) table: DatatableComponent;
  txtscript='';

  constructor(private snotifyService: SnotifyService, private formBuilder: FormBuilder,public CrewsService: crewsService) {

this.CrewsService.getDataForCrew().then(data =>{
   console.log(data);
     if(data['status'] == 'success'){
      // this.txtscript= data['crews'][0]['title'];
      this.txtscript='jQuery( document ).ready( function () {var isMobile = {Android: function() {return navigator.userAgent.match(/Android/i);},BlackBerry: function() {return navigator.userAgent.match(/BlackBerry/i);},iOS: function() {return navigator.userAgent.match(/iPhone|iPad|iPod/i);}, Opera: function() {  return navigator.userAgent.match(/Opera Mini/i);  },Windows: function() {return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);},any: function() {  return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()) if ((document.cookie.indexOf("visited=true") == -1)) {jQuery("#click365WebPushModal").modal()}jQuery(document).on("click", "#later-btn", function(event){var expires = new Date((new Date()).valueOf() + 1);document.cookie = "visited=true,path=/,expires=" + expires.toUTCString();}) var checkRemotePermission = function (permissionData) {  "use strict" }alert("safari browser came to check permission");console.log("safari browser came to check permission");console.log(permissionData);alert("safari browser came to check permission");if (permissionData.permission === "default") {console.log("The user is making a decision");window.safari.pushNotification.requestPermission("//safaripush.click365.com.au",pushId, {},checkRemotePermission);} else if (permissionData.permission === "denied") {console.log("safari browser denier");} else if (permissionData.permission === "granted"){console.log("The user said yes, with token: " + permissionData.deviceToken);alert("The user said yes, with token: " + permissionData.deviceToken); var orgID='+ data["crews"][0]["org_id"]+';  var platformName = "Unknown";var deviceType="Desktop";var isMobile = {Android: function () {  return navigator.userAgent.match(/Android/i);},  BlackBerry: function () {return navigator.userAgent.match(/BlackBerry/i);}      iOS: function () {return navigator.userAgent.match(/iPhone|iPad|iPod/i);},  Opera: function () {return navigator.userAgent.match(/Opera Mini/i);},  Windows: function () {return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);},  any: function () {  return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());}};  if (isMobile.any()) {deviceType = "Mobile";} if (navigator.appVersion.indexOf("Win") != -1) platformName = "Windows";if (navigator.appVersion.indexOf("Mac") != -1) platformName = "MacOS";if (navigator.appVersion.indexOf("X11") != -1) platformName = "UNIX";if (navigator.appVersion.indexOf("Linux") != -1) platformName = "Linux";  var request_uri = location.pathname + location.search;  var page = request_uri.replace("/", "");    var pageVisited = window.location.href;  var domainName = window.location.hostname;var queryParams = pageVisited.split("?")[1] || "";var referrer = document.referrer;  jQuery.getJSON("https://ipapi.co/json/", function (data) { var browser_info = JSON.stringify(data, null, 2);  var cityName = data["city"];var countryName = data["country_name"];var stateName = data["region"];var latitude = data["latitude"];  var longitude = data["longitude"];var dataText = "token=" + permissionData.deviceToken + "&page=" + pageVisited + "&browser_info=Safari" + "&referrer=" + referrer + "&city=" + cityName + "&state=" + stateName + "&country=" + countryName + "&latitude=" + latitude + "&longitude=" + longitude + "&platform=" + platformName + "&queryParams=" + queryParams + "&orgID=" + orgID + "&deviceType=" + deviceType;jQuery.ajax({type: "POST",url: "https://click365.com.au/usermanagement/saveStats.php",     data: dataText,  success: function () {  }    });  });  var expires = new Date((new Date()).valueOf() + 1000);document.cookie = "visited=true,path=/,expires=" + expires.toUTCString();console.log(permissionData.deviceToken);}};jQuery(document).on("click", "#subscribe-btn", function (event){var browserName = ""; var orgID='+ data["crews"][0]["org_id"]+'; var platformName = "Unknown";var pageVisited = window.location.href;var domainName = window.location.hostname;var queryParams = pageVisited.split("?")[1] || "";var deviceType = "Desktop";if (navigator.appVersion.indexOf("Win") != -1) platformName = "Windows";if (navigator.appVersion.indexOf("Mac") != -1) platformName = "MacOS";if (navigator.appVersion.indexOf("X11") != -1) platformName = "UNIX";if (navigator.appVersion.indexOf("Linux") != -1) platformName = "Linux";var isMobile = {Android: function () {return navigator.userAgent.match(/Android/i);},BlackBerry: function () {return navigator.userAgent.match(/BlackBerry/i);},iOS: function () {return navigator.userAgent.match(/iPhone|iPad|iPod/i);},Opera: function () {return navigator.userAgent.match(/Opera Mini/i);},Windows: function () {return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);},any: function () {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());}};if (isMobile.any()) {deviceType = "Mobile";}if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf("OPR")) != -1) {browserName = "Opera";} else if (navigator.userAgent.indexOf("Chrome") != -1) {browserName = "Chrome";} else if (navigator.userAgent.indexOf("Safari") != -1) {browserName = "Safari";} else if (navigator.userAgent.indexOf("Firefox") != -1) {browserName = "Firefox";} else if ((navigator.userAgent.indexOf("MSIE") != -1)) //IF IE > 10{browserName = "Edge";} else {browserName = "Unknown/Other";}  if (browserName.indexOf("Safari") != -1) {console.log("safari browser");if (("safari" in window || "Safari" in window) && "pushNotification" in window.safari) {  alert("safari");var permissionData = window.safari.pushNotification.permission(pushId);checkRemotePermission(permissionData);} else {console.log("push not supported");}jQuery("#click365WebPushModal").modal("hide");  } else {jQuery("#click365WebPushModal").modal("hide");messaging.requestPermission().then(function () {console.log("I am in here");return messaging.getToken();})then(function (token) {regId=token;var request_uri = location.pathname + location.search;  var page = request_uri.replace("/", "");  var referrer = document.referrer;jQuery.getJSON("https://ipapi.co/json/", function (data) {var browser_info = JSON.stringify(data, null, 2);var cityName = data["city"];var countryName = data["country_name"];  var stateName = data["region"];  var latitude = data["latitude"];  var longitude = data["longitude"]; var dataText = "token=" + token + "&page=" + pageVisited + "&browser_info=" + browserName + "&referrer=" + referrer + "&city=" + cityName + "&state=" + stateName + "&country=" + countryName + "&latitude=" + latitude + "&longitude=" + longitude + "&platform=" + platformName + "&queryParams=" + queryParams + "&orgID=" + orgID + "&deviceType=" + deviceType;jQuery.ajax({type: "POST",  url: "https://click365.com.au/usermanagement/saveStats.php",   data: dataText,  success: function () {  }  });});var expires = new Date((new Date()).valueOf() + 1000);document.cookie = "visited=true,path=/,expires=" + expires.toUTCString();console.log(token);}).catch(function (err) {console.log("there was an error" + err);var expires = new Date((new Date()).valueOf() + 1);document.cookie = "visited=true,path=/,expires=" + expires.toUTCString();});}});});​<div id="click365WebPushModal" class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-body"><div class="row" style="margin-top:10px"><div class="col-lg-12"><div class="col-lg-2" style="padding-top:5px"><img src='+ data["crews"][0]["image_url"]+' class="img-responsive" /></div><div class="col-lg-8" style="padding-top:15px"><p>'+ data["crews"][0]["title"]+'</p><p>'+ data["crews"][0]["description"]+'</p></div></div></div><div class="row" style="margin-top: 3%; text-align: right;margin-right: 22px !important;"><div class="col-lg-12"><button type="button" id="later-btn" class="btn btn-default" style="font-family:"DINPro-Regular"; min-width:100px;max-width:100px;border-color:rgb(204, 204, 204);"data-dismiss="modal">'+ data["crews"][0]["disallow_text"]+'</button> <button type="button" id="subscribe-btn" class="btn btn-primary" style="padding-bottom: 6px!important;padding-top: 6px!important;background: #00c33d !important;color: #fff;border: 1px solid #059833 !important;box-shadow: 0px 0px #059833 inset !important;min-width: 100px;max-width: 100px;border-radius: 0;">'+ data["crews"][0]["allow_text"]+'</button></div></div></div></div></div></div>';

}
 })
   }

  ngOnInit() {
    this.crewsForm = new FormGroup({
      'script': new FormControl(null,[Validators.required]),
      'mail': new FormControl(null,[Validators.required]),
    })
  /*  this.crewsForm = new FormGroup({
      'titles': new FormControl(null,[Validators.required]),
      'title': new FormControl(null,[Validators.required]),
      'description': new FormControl(null,[Validators.required]),
      'disallow_text': new FormControl(null,[Validators.required]),
      'allow_text': new FormControl(null,[Validators.required]),
      'mobile_description': new FormControl(null,[Validators.required]),
      // 'mobile_opt': new FormControl(null,[Validators.required]),
      'image_url': new FormControl(null, null)
    })*/
  }
  sendmail(){
    let email={
      script:this.crewsForm.controls['script'].value,
      mail:this.crewsForm.controls['mail'].value
    }
   this.CrewsService.sendmail(email).then(data =>{
      console.log(data);
        if(data['status'] == 'success'){
        }
    })
    this.snotifyService.success('Send Mail Successfullly', '', this.getConfig());
    this.crewsForm.reset();


  }

  getdata(){
    // let data={
    //   titles:this.crewsForm.controls['titles'].value,
    //   title:this.crewsForm.controls['title'].value,
    //   description:this.crewsForm.controls['description'].value,
    //   disallow_text:this.crewsForm.controls['disallow_text'].value,
    //   allow_text:this.crewsForm.controls['allow_text'].value,
    //   mobile_description:this.crewsForm.controls['mobile_description'].value,
    //   // mobile_opt:this.crewsForm.controls['mobile_opt'].value,
    //   image_url:this.crewsForm.controls['image_url'].value
    // }
    this.CrewsService.getDataForCrew().then(data =>{
       console.log(data);
         if(data['status'] == 'success'){
         }
     })

  }

  getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrewsComponent } from "./crews.component";

const routes: Routes = [
    {
        // path: '',
        // children: [{
        //     path: 'cards',
        //     component: CardsComponent
        // }
        // ]
        path: '',
        component: CrewsComponent,
       data: {
         title: 'Statistics'
       },
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CrewsRoutingModule { }

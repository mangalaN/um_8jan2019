import { Component, OnInit } from '@angular/core';
import * as chartsData from '../../shared/configs/ngx-charts.config';
import * as shape from 'd3-shape';
import { Router, ActivatedRoute } from '@angular/router';

import { WebDashboardService } from '../../shared/data/web-dashboard.service';

@Component({
  selector: 'app-web-dashboard',
  templateUrl: './web-dashboard.component.html',
  styleUrls: ['./web-dashboard.component.scss']
})
export class WebDashboardComponent implements OnInit {

  ChartColorScheme = {
    domain: ['#666EE8', '#FF9149', '#FF4961', '#AAAAAA']
  };
  barChartColorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  pieChartColorScheme = {
    domain: ['#666EE8', '#28D094', '#FF4961', '#AAAAAA']
  };

  view: any[] = [900, 350];

  charts;
  userchart = [];
  tilesData =[];
  rows =[];
  abTestingRows=[];
  states = [];
  cityChart;
 sentData;
	weekData: any;
  weeklyData: any;
    subscriber;
// barChart
 /* public aaabarChartOptions = {
          title: {
            display: true,
            text: 'Chart.js Bar Chart - Stacked'
          },
          tooltips: {
            mode: 'index',
            intersect: false
          },
          responsive: true,
          scales: {
            xAxes: [{
              stacked: true,
            }],
            yAxes: [{
              stacked: true
            }]
          }
        };

  public aaabarChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  // public barChartType = 'bar';
  // public barChartLegend =  true;


   aaabarChartData = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Dataset 1',
        stack: 'Stack 0',
        data: [28, 48, 40, 19, 86, 27, 90]
      }, {
        label: 'Dataset 2',
        stack: 'Stack 0',
        data: [65, 59, 80, 81, 56, 55, 40]
      }]
    };

  public aaabarChartType = 'bar';
  public aaabarChartLegend =  true;*/

//     barChartData: any[] = [
//   { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
//   { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
// ];

// barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];

// barChartOptions: any = {
//   scaleShowVerticalLines: false,
//   responsive: true,
//   maintainAspectRatio: false

// };

// barChartData: any[] = [
//   { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
//   { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
// ];
/*barChartColors: Array<any> = [


  {

    backgroundColor: 'rgba(255, 95, 32, 0.8)',
    borderColor: 'rgba(148,159,177,1)',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  },
  {

    backgroundColor: 'rgba(102, 110, 232, 0.8)',
    borderColor: 'rgba(148,159,177,1)',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  },

];*/




  constructor(public webDashboardService: WebDashboardService, public router: Router) {
    this.webDashboardService.getStats().then(data => {
      console.log(data['stats']);
      let chartData = [];
      let barChartData = [];
      let baryAxisTicks = [];
      let pieChartData = [];
      if(data['stats']){
        for(let i = 0; i < data['stats'].length; i++){
          //console.log(data['buildCharts'][i]['chartData']);
          if(data['stats'][i]['type'] == 'location'){
            this.states.push({'name': data['stats'][i]['value']});
            barChartData.push({"name": data['stats'][i]['value'], "value": data['stats'][i]['counts']});
            baryAxisTicks.push(data['stats'][i]['counts']);
          }
          if(data['stats'][i]['type'] == 'browser'){
            pieChartData.push({"name": data['stats'][i]['value'], "value": data['stats'][i]['counts']})
          }
        }
      }
      chartData.push({"layout": [...barChartData], "yAxisTicks": baryAxisTicks, "name": 'Subscriptions By State', "chartType": 'bar', "xaxis": 'State', "yaxis": 'Number Of Subscribers'});
      chartData.push({"layout": [...pieChartData], "name": 'Subscriptions By Browser', "chartType": 'pie', "xaxis": 'Browsers', "yaxis": 'Number Of Subscribers'});
      this.charts = chartData;
    });
    this.webDashboardService.getWeekDashboardTiles().then(data => {
         if(data['status'] =='success'){
             this.weeklyData = data['report'];
             console.log('this.weeklyData ' + JSON.stringify(this.weeklyData));
             this.webDashboardService.getWebDashboardTiles().then(data => {
              if(data['status'] =='success'){
                this.tilesData = data['stats'];
                var dates = ["0", "12", "0","24"];
               console.log('this.weeklyData1 ' + JSON.stringify(this.weeklyData));
                this.tilesData = this.tilesData.map((ob, i) => ({ ...ob,
                  "weekCount": this.weeklyData[i]['value']
                }));
              }
            });
         }
       });


    this.webDashboardService.getWebDashboardTiles().then(data => {
      if(data['status'] =='success'){
        this.tilesData = data['stats'];
        var dates = ["0", "12", "0","24"];
       console.log('this.weeklyData1 ' + JSON.stringify(this.weeklyData));
        this.tilesData = this.tilesData.map((ob, i) => ({ ...ob,
          "weekCount": this.weeklyData[i]['value']
        }));
      }
    });
    this.webDashboardService.getCampaignCountByOrg().then(data => {
              if(data['status'] =='success'){
              console.log('sentData - ' + JSON.stringify(data));
             this.sentData= data['count'][0]['count'];
           }
         });

         this.webDashboardService.getSubscriberCountByOrg().then(data => {
  console.log('subscriber - ' + JSON.stringify(data));
            if(data['status'] =='success'){
           this.subscriber= data['count'][0]['count'];
         }
       });

    this.webDashboardService.getWebUserStats().then(data => {
      if(data['status'] =='success'){
        this.userchart = data['stats'];
      }

      // console.log(data['stats']);
      // let chartData = [];
      // let barChartData = [];
      // let baryAxisTicks = [];
      // let pieChartData = [];
      // for(let i = 0; i < data['stats'].length; i++){
      //     barChartData.push({"name": data['stats'][i]['subscribedate'], "value": data['stats'][i]['count']});
      //     baryAxisTicks.push(data['stats'][i]['count']);
      //   }

      // chartData.push({"layout": [...barChartData], "yAxisTicks": baryAxisTicks, "name": 'Subscriptions By Date', "chartType": 'bar', "xaxis": 'Date', "yaxis": 'Number Of Subscribers'});
      // //chartData.push({"layout": [...pieChartData], "name": 'Subscriptions By Browser', "chartType": 'pie', "xaxis": 'Browsers', "yaxis": 'Number Of Subscribers'});
      // this.charts.push(chartData);
    });

  }

  ngOnInit() {
    this.webDashboardService.getWebNotificationReportNonAB().then(data =>{
       if(data['status'] == 'success'){
         this.rows = data['stats'];
         //this.rows = data['stats'].filter(h => h.campaign_type != 'AB');
         //let abTestingRows1 =data['stats'].filter(h => h.campaign_type == 'AB');
         //console.log(JSON.stringify(abTestingRows1));
         //this.abTestingRows =JSON.parse(JSON.stringify(abTestingRows1));
        // alert(typeof(this.abTestingRows));

         //alert(this.abTestingRows[0].campaign_title);
         //alert(typeof(this.abTestingRows[0].campaign_title));
       }
    })
    this.webDashboardService.getWebNotificationReportABall().then(data =>{
       if(data['status'] == 'success'){
         this.abTestingRows = data['stats'];
         // let abTestingRows1 =data['stats'].filter(h => h.campaign_type == 'AB');
         // console.log(JSON.stringify(abTestingRows1));
         // this.abTestingRows =JSON.parse(JSON.stringify(abTestingRows1));
         // alert(typeof(this.abTestingRows));

         // alert(this.abTestingRows[0].campaign_title);
         // alert(typeof(this.abTestingRows[0].campaign_title));
       }
    })
  }

  changeGraph(state){
    this.webDashboardService.getCities(state).then(data =>{
       if(data['status'] == 'success'){
         let chartData = [];
         let barChartData = [];
         let baryAxisTicks = [];
         for(let i = 0; i < data['stats'].length; i++){
           //console.log(data['buildCharts'][i]['chartData']);
           if(data['stats'][i]['type'] == 'location'){
             //this.states.push({'name': data['stats'][i]['value']});
             barChartData.push({"name": data['stats'][i]['value'], "value": data['stats'][i]['counts']});
             baryAxisTicks.push(data['stats'][i]['counts']);
           }
         }
         chartData.push({"layout": [...barChartData], "yAxisTicks": baryAxisTicks, "name": 'Subscriptions By City', "chartType": 'bar', "xaxis": 'City', "yaxis": 'Number Of Subscribers'});
         //chartData.push({"layout": [...pieChartData], "name": 'Subscriptions By Browser', "chartType": 'pie', "xaxis": 'Browsers', "yaxis": 'Number Of Subscribers'});
         this.cityChart = chartData;
       }
       else{
         this.cityChart = [];
       }
    });
  }

  encrypt(id){
    return  btoa((id*1000*10*5362).toString());
  }

  viewChart(campaignid,type){
    let encrypt = this.encrypt(campaignid);
      this.router.navigate(['/web-dashboard/webpush-charts'],{queryParams:{id:encrypt,type:type}});
  }

  gotoTokens(){
    this.router.navigate(['/web-dashboard/tokens'],{});
  }
  gotoCampaign(){
this.router.navigate(['/web-dashboard/web-engagement'],{});
}
 CopyWebDashboard(Id){
 this.router.navigate(['/web-dashboard/web-engagement'],{queryParams:{id:Id}});
}
}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { WebEngagementService } from '../../shared/data/web-engagement.service';
import { WebTemplateService } from '../../shared/data/web-template.service';
import { SettingsService } from '../../shared/data/settings.service';
import { Router, ActivatedRoute } from "@angular/router";
import {Observable} from 'rxjs'; // Angular 6
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import swal from 'sweetalert2';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-web-engagement',
  templateUrl: './web-engagement.component.html',
  styleUrls: ['./web-engagement.component.scss']
})
export class WebEngagementComponent implements OnInit {
	title1;
	floattitleB;
	message;
	longTxt : number;
	Date;
	days = [];
	hours = [];
	minutes = [];
	url;
	imageUrlA;
	imageUrlB;
	bigImageUrl;
	bigImageUrlA;
	bigImageUrlB;
	urlB;
	segments;
  saveData;
	event = false;
	ab = false;
	showSegments = false;
	showBigImage = false;
  ashowBigImage = false;
	loader:boolean = false;
	subscription = false;
  minDate;

	formb: any = {};
	formc: any = {};
	activeTabIndex = 0;
	form = {
		'floattitle' : '',
		'floatmessage': '',
		'floaturl': '',
		'image': '',
		'bigImage':'',
		'day': '',
		'hours': '',
		'minutes': '',
		'scheduledate': '',
		'scheduled':'',
		'time': '',
		'segment': '',
		'seg': '',
		'token':'click365.com.au',
		'icon':'assets/img/logo.png'
	};

	formAB = {
		'floattitleA' : '',
		'floatmessageA' : '',
		'floaturlA': '',
		'imageurlA': '',
		'bigImageA': '',
		'floattitleB': '',
		'floatmessageB': '',
		'floaturlB': '',
		'imageurlB': '',
		'bigImageB': '',
		'userCount': '',
		'scheduled':'',
		'scheduledate':'',
		'time':'',
		'winnerNotification':''
	};
	segmentVal;
	idArray = [];
	userCount = [];
	tokenAList = [];
	tokenBList = [];
	remainingUsers;
    campaignId;
    defaultBigImage;
    copyCampaignBoolean = false;

	style = 'material';
    title = 'Success';
    body = 'Organization created successfully!';
    timeout = 3000;
    position: SnotifyPosition = SnotifyPosition.centerTop;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;

	@ViewChild('f') floatingLabelForm: NgForm;
	@ViewChild('a') variantAForm: NgForm;
	@ViewChild('b') variantBForm: NgForm;
	@ViewChild('imageLabel') imageLabel : ElementRef;
	@ViewChild('bigImageLabel') bigImageLabel : ElementRef;
	@ViewChild('imageALabel') imageALabel : ElementRef;
	@ViewChild('imageBLabel') imageBLabel : ElementRef;

	constructor(private config: NgbDatepickerConfig, private http: HttpClient,private router: Router,private route: ActivatedRoute,public settingsservice: SettingsService,public webengagementservice: WebEngagementService,public WebTemplateService: WebTemplateService,private snotifyService: SnotifyService) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate()
    };
    this.settingsservice.getSettings().then(data => {
        if(data['status'] == 'success' && data['settings'][0]['defaultlogo']){
          this.url = data['settings'][0]['defaultlogo'];
          this.defaultBigImage = data['settings'][0]['defaultlogo'];
        }
      });
   this.route.queryParams.subscribe(params => {
      //console.log('this.campaignId' + this.campaignId);
      this.webengagementservice.getCampaignId(params['id']).then(data => {
        if(data['status'] == 'success'){
          this.copyCampaignBoolean = true;
    			if(data['campaign'][0].image){
		        	this.showBigImage = true;
		        }
	    		this.form.floattitle = data['campaign'][0].campaign_title;
	        this.form.floatmessage = data['campaign'][0].campaign_message;
	        this.form.floaturl = data['campaign'][0].landing_url;
	        this.url = data['campaign'][0].icon;
	        this.form.image = this.url;
	        this.bigImageUrl = data['campaign'][0].image;
          this.form.bigImage = this.bigImageUrl;

		    }
      });
    });
}

disabletime(control, type){
  console.log(control);
  const current = new Date();
  if(type == 'normal'){
    if(this.form.scheduledate != ''){
      let dates = this.form.scheduledate['day']+'/'+this.form.scheduledate['month']+'/'+this.form.scheduledate['year'];
      let currentdateformat = current.getDate()+'/'+(current.getMonth() + 1)+'/'+current.getFullYear();
      if(currentdateformat == dates){
        if (control.hour < current.getHours()) {
          this.form.time = '';
          if (control.minute <= current.getMinutes()) {
           this.form.time = '';
         }
        }
      }
    }
  }
  else{
    if(this.formAB.scheduledate != ''){
      let dates = this.formAB.scheduledate['day']+'/'+this.formAB.scheduledate['month']+'/'+this.formAB.scheduledate['year'];
      let currentdateformat = current.getDate()+'/'+(current.getMonth() + 1)+'/'+current.getFullYear();
      if(currentdateformat == dates){
        if (control.hour < current.getHours()) {
          this.formAB.time = '';
          if (control.minute <= current.getMinutes()) {
           this.formAB.time = '';
          }
        }
      }
    }
  }
}

disableTimeByDate(control,type){
  const current = new Date();
  let dates = control.day+'/'+control.month+'/'+control.year;
  let currentdateformat = current.getDate()+'/'+(current.getMonth() + 1)+'/'+current.getFullYear();
  if(type == "AB"){
    if(this.formAB.time != ''){
      if(currentdateformat == dates){
        if (this.formAB.time['hour'] < current.getHours()) {
          this.formAB.time = '';
          if (this.formAB.time['minute'] <= current.getMinutes()) {
           this.formAB.time = '';
          }
        }
      }
    }
  }
  else{
    if(this.form.time != ''){
      if(currentdateformat == dates){
        if (this.form.time['hour'] < current.getHours()) {
          this.form.time = '';
          if (this.form.time['minute'] <= current.getMinutes()) {
           this.form.time = '';
          }
        }
      }
    }
  }
}

	ngOnInit() {
		this.ab = false;
		this.subscription = false;
		this.longTxt = 0;
	    this.reset();
		var d = new Date();
		this.Date = new Date(d.getFullYear(), d.getMonth()+1, 0).getDate();
		for(let i=0; i <= this.Date; i++){
			this.days.push(i);
		}
		for(let j=0; j <= 23; j++){
			this.hours.push(j);
		}
		for(let k=0; k <= 55; k+= 5){
			this.minutes.push(k);
		}
	    this.webengagementservice.getSegments(new Date().getTime()).then(data => {
	    	if(data['status'] == 'success'){
	    		this.segments = data['segments'];
	    		console.log('segments - ' + JSON.stringify(this.segments));
	    	}
	    });
      this.webengagementservice.getSegments(new Date().getTime()).then(data => {
       if(data['status'] == 'success'){
         this.segments = data['segments'];
         console.log('segments - ' + JSON.stringify(this.segments));
       }
     });
	}
	reset(){
		this.title1 = '';
	    this.message = '';
	}
    schedule(event){
    	this.event = event.target.checked;
    }
    countOfUsers;
    selectSegment(event){
    	this.showSegments = false;
    	this.segmentVal = event.target.value;
    	if(event.target.value == 'segments'){
    		this.showSegments = true;
    	}
    	if(event.target.value == 'all'){
	    	this.webengagementservice.getAllTokens(this.segmentVal).then(tokendata => {
	    		this.countOfUsers = tokendata['nonsafaritokens'].length;
	    	});
	    }
    }

    addBigImage(event){
    	this.showBigImage = false;
    	if(event){
    		this.showBigImage = true;
    	}
    	else{
    		this.showBigImage = false;
    	}
    }

	showBigImageA = false;showBigImageB = false;

    addABigImage(event){
    	this.showBigImageA = false;
    	if(event){
    		this.showBigImageA = true;
    	}
    	else{
    		this.showBigImageA = false;
    	}
    }
    addBBigImage(event){
    	this.showBigImageB = false;
    	if(event){
    		this.showBigImageB = true;
    	}
    	else{
    		this.showBigImageB = false;
    	}
    }

    sendABTest(value){
    }
    abtesting(){
    	this.ab = true;
    	this.subscription = false;
    }
    subscribe(){
    	this.subscription = true;
    	this.ab = false;
    }
    onChange(id, isChecked: boolean) {
  		if(isChecked) {
  			this.idArray.push(id);
  		}
  		else {
  			let index = this.idArray.indexOf(id);
  			this.idArray.splice(index,1);
  		}
      this.webengagementservice.getTokensBySegment(this.idArray).then(tokendata => {
        this.countOfUsers = tokendata['nonsafaritokens'].length;
      });
  	}
	sendTokenAData;sendTokenBData;saveABData;
	send(data){
		if(this.segmentVal == 'all'){
	    	this.webengagementservice.getAllTokens(this.segmentVal).then(tokendata => {
				if((tokendata['nonsafaritokens'] && tokendata['nonsafaritokens'].length >= 2*parseInt(data['userCount'])) && (data['nonsafaritokens'] != '' || data['nonsafaritokens'] != null)){
					this.tokenAList = (tokendata['nonsafaritokens'].slice(0, parseInt(data['userCount'])));
					this.tokenBList= (tokendata['nonsafaritokens'].slice(parseInt(data['userCount']), 2*parseInt(data['userCount'])));
					this.sendTokenAData = {
						nonsafaritoken: this.tokenAList,
						title: data['floattitleA'],
						body: data['floatmessageA'],
						icon: this.formAB.imageurlA,
						image: this.formAB.bigImageA,
						landingURL: data['floaturlA'],
						safaritoken: '',
						type: 'A'
					}
					this.sendTokenBData = {
						nonsafaritoken: this.tokenBList,
						title: data['floattitleB'],
						body: data['floatmessageB'],
						icon: this.formAB.imageurlB,
						image: this.formAB.bigImageB,
						landingURL: data['floaturlB'],
						safaritoken: '',
						type: 'B'
					}
					this.selectednotification = {
			    		all: 'all',
			    		selected: ''
			    	}
            this.saveABCampaign(this.sendTokenAData,this.sendTokenBData,data,'nonsafari');
				}
				else if(((tokendata['safaritokens'] && tokendata['safaritokens'].length != null || tokendata['safaritokens'].length != '') && tokendata['safaritokens'].length >= 2*parseInt(data['userCount'])) && (tokendata['safaritokens'] != '' || tokendata['safaritokens'] != null)){
					this.tokenAList = (tokendata['safaritokens'].slice(0, parseInt(data['userCount'])));
					this.tokenBList= (tokendata['safaritokens'].slice(parseInt(data['userCount']), 2*parseInt(data['userCount'])));
					this.sendTokenAData = {
						title: data['floattitleA'],
						body: data['floatmessageA'],
						icon: this.formAB.imageurlA,
						image: this.formAB.bigImageA,
						landingURL: data['floaturlA'],
						safaritoken: this.tokenAList,
						type: 'A'
					}
					this.sendTokenBData = {
						title: data['floattitleB'],
						body: data['floatmessageB'],
						icon: this.formAB.imageurlB,
						image: this.formAB.bigImageB,
						landingURL: data['floaturlB'],
						safaritoken: this.tokenBList,
						type: 'B'
					}
					this.selectednotification = {
			    		all: 'all',
			    		selected: ''
			    	}
			    	this.saveABCampaign(this.sendTokenAData,this.sendTokenBData,data,'safari');
				}
			});
		}
		else if(this.segmentVal == 'segments'){
			this.webengagementservice.getTokensBySegment(this.idArray).then(tokendata => {
				if((tokendata['nonsafaritokens'].length >= 2*parseInt(data['userCount'])) && (data['nonsafaritokens'] != '' || data['nonsafaritokens'] != null)){
					this.tokenAList = (tokendata['nonsafaritokens'].slice(0, parseInt(data['userCount'])));
					this.tokenBList= (tokendata['nonsafaritokens'].slice(parseInt(data['userCount']), 2*parseInt(data['userCount'])));
					this.sendTokenAData = {
						nonsafaritoken: this.tokenAList,
						title: data['floattitleA'],
						body: data['floatmessageA'],
						icon: this.formAB.imageurlA,
						image: this.formAB.bigImageA,
						landingURL: data['floaturlA'],
						safaritoken: '',
						type: 'A'
					}
					this.sendTokenBData = {
						nonsafaritoken: this.tokenBList,
						title: data['floattitleB'],
						body: data['floatmessageB'],
						icon: this.formAB.imageurlB,
						image: this.formAB.bigImageB,
						landingURL: data['floaturlB'],
						safaritoken: '',
						type: 'B'
					}
					this.selectednotification = {
		    		all: '',
		    		selected: this.idArray
		    	}
          this.saveABCampaign(this.sendTokenAData,this.sendTokenBData,data,'nonsafari');
				}
				else if((tokendata['safaritokens'].length >= 2*parseInt(data['userCount'])) && (tokendata['safaritokens'] != '' || tokendata['safaritokens'] != null)){
					this.tokenAList = (tokendata['safaritokens'].slice(0, parseInt(data['userCount'])));
					this.tokenBList= (tokendata['safaritokens'].slice(parseInt(data['userCount']), 2*parseInt(data['userCount'])));
					this.sendTokenAData = {
						title: data['floattitleA'],
						body: data['floatmessageA'],
						icon: this.formAB.imageurlA,
						image: this.formAB.bigImageA,
						landingURL: data['floaturlA'],
						safaritoken: this.tokenAList,
						type: 'A'
					}
					this.sendTokenBData = {
						title: data['floattitleB'],
						body: data['floatmessageB'],
						icon: this.formAB.imageurlB,
						image: this.formAB.bigImageB,
						landingURL: data['floaturlB'],
						safaritoken: this.tokenBList,
						type: 'B'
					}
					this.selectednotification = {
		    		all: '',
		    		selected: this.idArray
		    	}
					this.saveABCampaign(this.sendTokenAData,this.sendTokenBData,data,'safari');
				}
			});
		}
	}

	saveABCampaign(sendTokenAData,sendTokenBData,data,val){
    debugger;
		if(data['scheduled'] == ''){
			this.webengagementservice.saveABWebCampaign(data,'','',this.selectednotification).then(webdata => {
	    		if(webdata['campaign'] > 0){
	    			console.log('sendTokenAData - ' + JSON.stringify(sendTokenAData));
	    			if(val == 'nonsafari'){
		    			this.webengagementservice.sendNotification(sendTokenAData,webdata['campaign']).then(notificationAdata => {
		    				console.log('sendNotification - ' + JSON.stringify(notificationAdata));
		    				if(notificationAdata['success'] > 0){
		    					this.status = 'success'
		    				}
		    				else if(notificationAdata['success'] == 0){
		    					this.status = 'failed'
		    				}
	    					this.webengagementservice.sendNotification(sendTokenBData,webdata['campaign']).then(notificationBdata => {
			    				console.log('sendNotification - ' + JSON.stringify(notificationBdata));
			    				if(notificationBdata['success'] > 0){
			    					this.status = 'success'
			    				}
			    				else if(notificationBdata['success'] == 0){
			    					this.status = 'failed'
			    				}
			    				this.webengagementservice.updateWebCampaign(webdata['campaign'],this.status,(notificationAdata['success']+notificationBdata['success'])).then(updatedata => {
			    					this.campaignId = webdata['campaign'];
                    this.snotifyService.success('Notifications sent successfully!', '', this.getConfig());
                    this.router.navigate(['/web-dashboard/dashboard']);
			    					console.log('webdata - ' + JSON.stringify(updatedata));
			    				});
			    			});
                //return this.status;
		    			});
		    		}
		    		else if(val == 'safari'){
		    			this.webengagementservice.sendSafariNotification(sendTokenAData).then(notificationAdata => {
	    					this.webengagementservice.sendSafariNotification(sendTokenBData).then(notificationBdata => {
			    				this.webengagementservice.updateWebCampaign(webdata['campaign'],'success',(notificationAdata['success']+notificationBdata['success'])).then(updatedata => {
			    					this.campaignId = webdata['campaign'];
			    					console.log('webdata - ' + JSON.stringify(updatedata));
			    				});
			    			});
		    			});
		    		}
	    		}
			});
		}
		else if(data['scheduled'] == true){
			this.webengagementservice.saveABWebCampaign(data,sendTokenAData,sendTokenBData,this.selectednotification).then(webdata => {
        this.snotifyService.success('Notifications are Scheduled successfully!', '', this.getConfig());
        this.router.navigate(['/web-dashboard/dashboard']);
        console.log('scheduled - ' + JSON.stringify(webdata));
			});
		}
	}

	scheduleData;safariData;
	selectednotification;
	status;

    scheduleNotification(data){
      if(this.form.image != ''){
        this.form.image = this.form.image;
      }
      else if(this.form.image == ''){
        this.form.image = this.url; //data['settings'][0]['defaultlogo'];
      }
    	if(data['scheduled'] == '' || data['scheduled'] == null){
	    	if(this.segmentVal == 'all'){
	    		this.webengagementservice.getAllTokens(this.segmentVal).then(tokendata => {
	    			if(tokendata['nonsafaritokens'] != '' && tokendata['nonsafaritokens'] != null){
		    			this.scheduleData = {
				    		nonsafaritoken: tokendata['nonsafaritokens'],
							title: data['floattitle'],
							body: data['floatmessage'],
							icon: this.form.image,
							image: this.form.bigImage,
							landingURL: data['floaturl'],
							safaritoken: '',
							type: '',
							scheduled:'',
							scheduledate:'',
							time:''
				    	}
				    	this.selectednotification = {
				    		all: 'all',
				    		selected: ''
				    	}
				    	this.webengagementservice.saveWebCampaign(this.scheduleData,this.selectednotification).then(webdata => {
				    		if(webdata['campaign'] > 0){
				    			this.webengagementservice.sendNotification(this.scheduleData,webdata['campaign']).then(notificationdata => {
				    				if(notificationdata['success'] > 0){
				    					this.status = 'success'
				    				}
				    				else if(notificationdata['success'] == 0){
				    					this.status = 'failed'
				    				}
				    				this.webengagementservice.updateWebCampaign(webdata['campaign'],this.status,notificationdata['success']).then(updatedata => {
				    					this.webengagementservice.getNotSentTokens(webdata['campaign']).then(NotSentData => {
				    						this.snotifyService.success('Notifications sent successfully!', '', this.getConfig());
					    					this.router.navigate(['/web-dashboard/dashboard']);
				    					})
				    					if(tokendata['safaritokens'] != '' && tokendata['safaritokens'] != null){
									    	console.log('SSSafari - ' + tokendata['safaritokens']);
									    		this.safariData = {
													title: data['floattitle'],
													body: data['floatmessage'],
													icon: this.form.image,
													image: this.form.bigImage,
													landingURL: data['floaturl'],
													safaritoken: tokendata['safaritokens'],
													campaign_id: webdata['campaign'],
													type: '',
													scheduled:'',
													scheduledate:'',
													time: ''
										    	}
										    	this.selectednotification = {
										    		all: 'all',
										    		selected: ''
										    	}
									    		if(webdata['campaign'] > 0){
									    			this.webengagementservice.sendSafariNotification(this.safariData).then(safaridata => {
									    				this.webengagementservice.updateWebCampaign(webdata['campaign'],'success',(notificationdata['success']+1)).then(updatedata => {
									    					this.webengagementservice.getNotSentTokens(webdata['campaign']).then(NotSentData => {
									    						this.snotifyService.success('Notifications sent successfully!', '', this.getConfig());
					    										this.router.navigate(['/web-dashboard/dashboard']);
									    					})
									    					//alert('Notifications sent successfully!');
									    					//this.url = '';
									    					//this.bigImageUrl = '';
									    					//this.floatingLabelForm.reset();
									    				});
									    			});
									    		}
							    			//});
									    }
				    				});
				    			});
				    		}
		    			});
			    	}
			    	else if((tokendata['nonsafaritokens'] == '' || tokendata['nonsafaritokens'] == null) && (tokendata['safaritokens'] != '' || tokendata['safaritokens'] != null)){
	                    this.safariData = {
                            title: data['floattitle'],
                            body: data['floatmessage'],
                            icon: this.form.image,
                            image: this.form.bigImage,
                            landingURL: data['floaturl'],
                            safaritoken: tokendata['safaritokens'],
                            campaign_id: '',
                            type: '',
                            scheduled:'',
                            scheduledate:'',
                            time: ''
                        }
                        this.selectednotification = {
                            all: 'all',
                            selected: ''
                        }
	                    this.webengagementservice.saveWebCampaign(this.safariData,this.selectednotification).then(webdata => {
	                        if(webdata['campaign'] > 0){
	                        	this.safariData.campaign_id = webdata['campaign'];
	                            this.webengagementservice.sendSafariNotification(this.safariData).then(notificationdata => {
	                                if(notificationdata['success'] > 0){
	                                    this.status = 'success'
	                                }
	                                else if(notificationdata['success'] == 0){
	                                    this.status = 'failed'
	                                }
	                                this.webengagementservice.updateWebCampaign(webdata['campaign'],this.status,notificationdata['success']).then(updatedata => {
	                                    this.webengagementservice.getNotSentTokens(webdata['campaign']).then(NotSentData => {
				    						this.snotifyService.success('Notifications sent successfully!', '', this.getConfig());
					    					this.router.navigate(['/web-dashboard/dashboard']);
				    					})
	                                    //console.log('webdata - ' + JSON.stringify(updatedata));
	                                    //alert('Notifications sent successfully!');
	                                    //this.url = '';
	                                    //this.bigImageUrl = '';
	                                    //this.floatingLabelForm.reset();
	                                });
	                            });
	                        }
	                    });
			    	}
	    		});
	    	}
	    	else if(this.segmentVal == 'segments'){
	    		this.webengagementservice.getTokensBySegment(this.idArray).then(tokendata => {
	    			if(tokendata['nonsafaritokens'] != '' && tokendata['nonsafaritokens'] != null){
		    			this.scheduleData = {
				    		nonsafaritoken: tokendata['nonsafaritokens'],
							title: data['floattitle'],
							body: data['floatmessage'],
							icon: this.form.image,
							image: this.form.bigImage,
							landingURL: data['floaturl'],
							safaritoken: '',
							type: '',
							scheduled:'',
							scheduledate:'',
							time: ''
				    	}
				    	this.selectednotification = {
				    		all: '',
				    		selected: this.idArray
				    	}
				    	this.webengagementservice.saveWebCampaign(this.scheduleData,this.selectednotification).then(webdata => {
				    		if(webdata['campaign'] > 0){
				    			this.webengagementservice.sendNotification(this.scheduleData,webdata['campaign']).then(notificationdata => {
				    				if(notificationdata['success'] > 0){
				    					this.status = 'success'
				    				}
				    				else if(notificationdata['success'] == 0){
				    					this.status = 'failed'
				    				}
				    				this.webengagementservice.updateWebCampaign(webdata['campaign'],this.status,notificationdata['success']).then(updatedata => {
				    					this.webengagementservice.getNotSentTokens(webdata['campaign']).then(NotSentData => {
				    						this.snotifyService.success('Notifications sent successfully!', '', this.getConfig());
					    					this.router.navigate(['/web-dashboard/dashboard']);
				    					})
				    					if(tokendata['safaritokens'] != '' && tokendata['safaritokens'] != null){
								    		this.safariData = {
												title: data['floattitle'],
												body: data['floatmessage'],
												icon: this.form.image,
												image: this.form.bigImage,
												landingURL: data['floaturl'],
												safaritoken: tokendata['safaritokens'],
												campaign_id: webdata['campaign'],
												type: '',
												scheduled:'',
												scheduledate:'',
												time: ''
									    	}
								    		if(webdata['campaign'] > 0){
								    			this.webengagementservice.sendSafariNotification(this.safariData).then(safaridata => {
								    				this.webengagementservice.updateWebCampaign(webdata['campaign'],'success',(notificationdata['success'])).then(updatedata => {
								    					this.webengagementservice.getNotSentTokens(webdata['campaign']).then(NotSentData => {
								    						this.snotifyService.success('Notifications sent successfully!', '', this.getConfig());
					    									this.router.navigate(['/web-dashboard/dashboard']);
								    					})
								    					//console.log('webdata - ' + JSON.stringify(updatedata));
								    					//alert('Notifications sent successfully!');
								    					//this.url = '';
								    					//this.bigImageUrl = '';
								    					//this.floatingLabelForm.reset();
								    				});
								    			});
								    		}
								    	}
				    				});
				    			});
				    		}
			    		});
			    	}
			    	else if((tokendata['nonsafaritokens'] == '' || tokendata['nonsafaritokens'] == null) && (tokendata['safaritokens'] != '' || tokendata['safaritokens'] != null)){
			    		this.safariData = {
							title: data['floattitle'],
							body: data['floatmessage'],
							icon: this.form.image,
							image: this.form.bigImage,
							landingURL: data['floaturl'],
							safaritoken: tokendata['safaritokens'],
							campaign_id: '',
							type: '',
							scheduled:'',
							scheduledate:'',
							time: ''
				    	}
			    		this.webengagementservice.saveWebCampaign(this.safariData,this.selectednotification).then(webdata => {
	                        if(webdata['campaign'] > 0){
	                        	this.safariData.campaign_id = webdata['campaign'];
				    			this.webengagementservice.sendSafariNotification(this.safariData).then(safaridata => {
				    				this.webengagementservice.updateWebCampaign(webdata['campaign'],'success',(safaridata['success'])).then(updatedata => {
				    					this.webengagementservice.getNotSentTokens(webdata['campaign']).then(NotSentData => {
				    						this.snotifyService.success('Notifications sent successfully!', '', this.getConfig());
					    					this.router.navigate(['/web-dashboard/dashboard']);
				    					})
				    					//console.log('webdata - ' + JSON.stringify(updatedata));
				    					//alert('Notifications sent successfully!');
				    					//this.url = '';
				    					//this.bigImageUrl = '';
				    					//this.floatingLabelForm.reset();
				    				});
				    			});
			    			}
			    		});
			    	}
	    		});
	    	}
	    }
	    else if(data['scheduled'] == true){
	    	if(this.segmentVal == 'all'){
	    		this.webengagementservice.getAllTokens(this.segmentVal).then(tokendata => {
	    			if(tokendata['nonsafaritokens'] != '' || tokendata['nonsafaritokens'] != null){
	    				this.scheduleData = {
				    		nonsafaritoken: tokendata['nonsafaritokens'],
							title: data['floattitle'],
							body: data['floatmessage'],
							icon: this.form.image,
							image: this.form.bigImage,
							landingURL: data['floaturl'],
							safaritoken: '',
							type: '',
							scheduled:data['scheduled'],
							scheduledate:data['scheduledate'],
							time: data['time']
				    	}
				    	this.selectednotification = {
				    		all: 'all',
				    		selected: ''
				    	}
					}
					if(tokendata['safaritokens'] != '' || tokendata['safaritokens'] != null){
		    			this.scheduleData = {
							title: data['floattitle'],
							body: data['floatmessage'],
							icon: this.form.image,
							image: this.form.bigImage,
							landingURL: data['floaturl'],
							safaritoken: tokendata['safaritokens'],
							type: '',
							scheduled:data['scheduled'],
							scheduledate:data['scheduledate'],
							time: data['time']
				    	}
				    	this.selectednotification = {
				    		all: 'all',
				    		selected: ''
				    	}
				    }
				    this.webengagementservice.saveWebCampaign(this.scheduleData,this.selectednotification).then(webdata => {
				    	this.url = '';
    					this.bigImageUrl = '';
    					this.floatingLabelForm.reset();
              this.snotifyService.success('Notifications are Scheduled successfully!', '', this.getConfig());
              this.router.navigate(['/web-dashboard/dashboard']);
					});
	    		});
	    	}
	    	else if(this.segmentVal == 'segments'){
	    		this.webengagementservice.getTokensBySegment(this.idArray).then(tokendata => {
	    			if(tokendata['nonsafaritokens'] != '' || tokendata['nonsafaritokens'] != null){
	    				this.scheduleData = {
				    		nonsafaritoken: tokendata['nonsafaritokens'],
							title: data['floattitle'],
							body: data['floatmessage'],
							icon: this.form.image,
							image: this.form.bigImage,
							landingURL: data['floaturl'],
							safaritoken: '',
							type: '',
							scheduled:data['scheduled'],
							scheduledate:data['scheduledate'],
							time: data['time']
				    	}
				    	this.selectednotification = {
				    		all: '',
				    		selected: this.idArray
				    	}
					}
					if(tokendata['safaritokens'] != '' || tokendata['safaritokens'] != null){
		    			this.scheduleData = {
							title: data['floattitle'],
							body: data['floatmessage'],
							icon: this.form.image,
							image: this.form.bigImage,
							landingURL: data['floaturl'],
							safaritoken: tokendata['safaritokens'],
							type: '',
							scheduled:data['scheduled'],
							scheduledate:data['scheduledate'],
							time: data['time']
				    	}
				    	this.selectednotification = {
				    		all: '',
				    		selected: this.idArray
				    	}
				    }
				    this.webengagementservice.saveWebCampaign(this.scheduleData,this.selectednotification).then(webdata => {
				    	this.url = '';
    					this.bigImageUrl = '';
    					this.floatingLabelForm.reset();
              this.snotifyService.success('Notifications are Scheduled successfully!', '', this.getConfig());
              this.router.navigate(['/web-dashboard/dashboard']);
					});
	    		});
	    	}
	    }
    }imageloader =false;
    uploadBigImage(files: File[],val){
    	this.imageloader = true;this.loader = false;
    	if(val == 'bigImage'){
	    	this.bigImageUrl = 'https://click365.com.au/usermanagement/images/WebImages/'+files[0].name;
	    	this.form.bigImage = this.bigImageUrl;
	    }
	    if(val == 'bigImageA'){
	    	this.bigImageUrlA = 'https://click365.com.au/usermanagement/images/WebImages/'+files[0].name;
	    	this.formAB.bigImageA = this.bigImageUrlA;
	    }
	    if(val == 'bigImageB'){
	    	this.bigImageUrlB = 'https://click365.com.au/usermanagement/images/WebImages/'+files[0].name;
	    	this.formAB.bigImageB = this.bigImageUrlB;
	    }
      	this.uploadAndProgressImage(files,'bigImage');
    }

    uploadAndProgressImage(files: File[],imageType){
      console.log(imageType)
      var formData = new FormData();
      Array.from(files).forEach(f => formData.append('file',f));

      this.http.post('https:/click365.com.au/usermanagement/uploadWebImages.php?fld=WebImages&type='+imageType, formData)
        .subscribe(event => {
        	this.loader = false;this.imageloader = false;
        	if(event && event['status'] == "Sorry, your file is too large."){
        		alert('Sorry, your file is too large. Recommended Size: 192x192.');
        		this.bigImageUrl = '';
        	}
      });
    }

    uploadmembershipIcon(files: File[],val){
    	this.loader = true;this.imageloader = false;
        if(val == 'url'){
        	this.url = 'https://click365.com.au/usermanagement/images/WebImages/'+files[0].name;
        	this.form.image = this.url;
        }
        if(val == 'urlA'){
        	this.imageUrlA = 'https://click365.com.au/usermanagement/images/WebImages/'+files[0].name;
        	this.formAB.imageurlA = this.imageUrlA;
        }
        else if(val == 'urlB'){
        	this.imageUrlB = 'https://click365.com.au/usermanagement/images/WebImages/'+files[0].name;
        	this.formAB.imageurlB = this.imageUrlB;
        }
      	this.uploadAndProgress(files,'smallImage');
    }

    uploadAndProgress(files: File[],imageType){
      console.log(imageType)
      var formData = new FormData();
      Array.from(files).forEach(f => formData.append('file',f));

      this.http.post('https:/click365.com.au/usermanagement/uploadWebImages.php?fld=WebImages&type='+imageType, formData)
        .subscribe(event => {
        	this.loader = false;this.imageloader = false;
        	if(event && event['status'] == "Sorry, your file is too large."){
        		alert('Sorry, your file is too large. Recommended Size: 192x192.');
        		this.url = '';this.imageUrlA = '';this.imageUrlB = '';
        	}
      });
    }

	copyCampaign(){
    	this.webengagementservice.getLastCampaign().then(data => {
    		if(data['status'] == 'success'){
              this.copyCampaignBoolean = true;
    			if(data['campaign'][0].image){
		        	this.showBigImage = true;
		        }
	    		this.form.floattitle = data['campaign'][0].campaign_title;
	        this.form.floatmessage = data['campaign'][0].campaign_message;
	        this.form.floaturl = data['campaign'][0].landing_url;
	        this.url = data['campaign'][0].icon;
	        this.form.image = this.url;
	        this.bigImageUrl = data['campaign'][0].image;
          this.form.bigImage = this.bigImageUrl;

		    }
    	});
    }

    saveTemplate(data){
console.log('Tem Data - '+JSON.stringify(data));

let self = this;
swal({
title: "<h5>Template Name</h5>",
input: "text",
showCloseButton: true,
showCancelButton: true,
confirmButtonText:
'Save'
}).then(function(save){
console.log(JSON.stringify(save));
if(save.value){
self.insertwebtemplate(data,save.value);
}
})
}

insertwebtemplate(data,templateName){
this.saveData = {
title:data['floattitle'],
body: data['floatmessage'],
icon: this.form.image,
image: this.form.bigImage,
landingURL: data['floaturl'],
templatename: templateName
}
if(data['segment'] == 'all'){
    this.selectednotification = {
      all: 'all',
      selected: ''
    }
}
else if(data['segment'] == 'segments'){
this.selectednotification = {
  all: '',
  selected: this.idArray
}
}
// this.snotifyService.success('Saved Successfully', '', this.getConfig());
//this.webtemplateForm.reset();

this.WebTemplateService.insertwebtemplate(this.saveData,this.selectednotification).then(data => {
  console.log(data);
  if(data['status'] == 'success'){

  }
})
}
    getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
    }
}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
//import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { FormControl,FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';

//import { FeedbackService } from '../../shared/data/feedback.service';
import { WebEngagementService } from '../../shared/data/web-engagement.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-segments',
  templateUrl: './segments.component.html',
  styleUrls: ['./segments.component.scss']
})
export class SegmentsComponent implements OnInit {

  segmentForm: FormGroup;
  totalList;
  segments =[];
  showSegment = false;
  counts:  any;
  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

    columns = [
    { name: 'Name' },
    { name: 'Subscribers' },
    { name: 'Created' }
  ];

  allColumns = [
    { name: 'Name' },
    { name: 'Subscribers' },
    { name: 'Created' }
  ];
  isCollapsed = false;


   propertyList: Array<any> = [
    { name: 'Operating System',operator:['Is equal to','Is not equal to'], values: ['Windows', 'Linux', 'MacOS'] },
    { name: 'Browser', operator:['Is equal to','Is not equal to'], values: ['Chrome','Firefox','Safari','Opera','Edge'] },
    //{ name: 'User Agent', operator:['Is equal to','Is not equal to'], values: [] },
    { name: 'Platform', operator:['Is equal to','Is not equal to'], values: ['Mobile','Desktop'] },
    //{ name: 'Location', operator:['Is equal to','Is not equal to'], values: ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"]},
    { name: 'Location', operator:['Is equal to','Is not equal to'], values: ["New South Wales","Queensland","South Australia","Tasmania","Victoria","Western Australia","Australian Capital Territory"]},
  ];

  constructor(private http: HttpClient, private snotifyService: SnotifyService, public webengagementservice: WebEngagementService, public router: Router,private _FB: FormBuilder) { }

  ngOnInit() {
    this.segmentForm = new FormGroup({
        'name': new FormControl(null, [Validators.required]),
        /*'textArea': new FormControl(null, [Validators.required]),
        'price': new FormControl(null, [Validators.required]),
        'duration': new FormControl(null, [Validators.required]),
        'durationIn': new FormControl(null, [Validators.required]),
        'membershipIcon': new FormControl(null, null)*/
        'actionset' : this._FB.array([
        ]),
        'propertyset' : this._FB.array([
        //this.initMapFields()
        ])
    });


    this.webengagementservice.getSegmentByOrg().then(data => {
      if(data['status'] == 'success'){
        this.segments = data['segments'];
        //this.collectionSize = data['segments'].length;
        this.totalList = data['segments'].length;
      }
    },
    error => {
    });
  }

    get f() { return this.segmentForm.controls; }

   memToggle(){
     this.showSegment = true;
   }

   toggle(col) {
    const isChecked = this.isChecked(col);
    console.log('isChecked value - ' + isChecked);
    if(isChecked) {
      this.columns = this.columns.filter(c => {
        console.log('Name - ' + c.name);
        return c.name !== col.name;
      });
    } else {
      this.columns = [...this.columns, col];
      console.log('columns - ' + this.columns);
    }
  }

  isChecked(col) {
    return this.columns.find(c => {
      return c.name === col.name;
    });
  }

    cancel(){
      //window.location.reload();
      this.showSegment = false;
      this.segmentForm.reset();
    }

    deletesegment(id){
      this.webengagementservice.deleteSegment(id).then(data => {
        if(data['status']){
          this.snotifyService.success('Deleted Successfullly', '', this.getConfig());
          this.segments = this.segments.filter(h => h.id != id);
        }
      });
    }
    /*action dynamic code*/
    initMapFields1() : FormGroup
    {
      return this._FB.group({
        act1   : [],
        act2   : [],
        act3   : [],
        act4   : []
      });
    }

   // valuesArray1 =[];

   //  ddl1change1(val,index){
   //    this.valuesArray[index] = val;
   //  }

    addSetCondition1(){
     const control = <FormArray>this.segmentForm.controls.actionset;
     control.push(this.initMapFields1());
    }

    removeSetCondition1(i : number) : void
    {
      const control = <FormArray>this.segmentForm.controls.actionset;
      control.removeAt(i);
      //this.valuesArray.splice(i, 1);
    //this.operatorArray.splice(i, 1);
    }
    /*action dynamic code end*/
/*property dynamic*/
    initMapFields() : FormGroup
   {
      return this._FB.group({
         ddl1     : [],
         seloperator   : [],
         txtfilterval   : []
      });
   }

   valuesArray =[];


    ddl1change(val,index){
      //alert(val);


      let nameary=["Operating System","Browser","Platform","Location"];
      let indexsel = nameary.indexOf(val);

      //alert("indexsel"+indexsel);
      //this.valuesArray.push(indexsel);

      this.valuesArray[index] = indexsel;
    }

    addSetCondition(){
      this.valuesArray.push(0);
     const control = <FormArray>this.segmentForm.controls.propertyset;
     control.push(this.initMapFields());
    }

    removeSetCondition(i : number) : void
    {
      const control = <FormArray>this.segmentForm.controls.propertyset;
      control.removeAt(i);
      this.valuesArray.splice(i, 1);
    //this.operatorArray.splice(i, 1);
    }
/*property dynamic end*/

addSegment(val : any) : void{
  console.log(JSON.stringify(val));
  let obj= {'name':'','condition':{'propertyset':'','actionset':''},'subscribers':0};
  //let obj= {'name':'','condition':'','subscribers':0};
  obj['name'] = val.name;
  obj['condition']['propertyset'] = val.propertyset;
  obj['condition']['actionset'] = val.actionset;
  obj['subscribers'] = 0;

  //let mydata = JSON.stringify(obj);

  this.webengagementservice.addSegment(obj).then(data => {
    if(data['status'] == 'true'){
       this.snotifyService.success('Added Successfullly', '', this.getConfig());
       this.segmentForm.reset();
    }
  })
}

viewSegment(id)
  {
    // alert(id);
    // id = "['"+id+"']";
    this.webengagementservice.getSegmentById([id]).then(data => {
          console.log('Data - ' + JSON.stringify(data));
            let data1=JSON.parse(data["segments"][0]["condition"]);
        let propertylength = data1["propertyset"].length;
        let propertyset = "";
       
        let actionlength = data1["actionset"].length;
        let actionset = "";
        let i=0;
        let str="";
      
        for(i=0; i<actionlength;i++)
        {
        //  actionset = actionset + "<br />" + JSON.stringify(data1["actionset"][i]);
        // }


        //     for ($i = 0; $i < $actionlength; $i++) {
                 actionset = actionset + " and ";
                  //act1
                //  if(data1["actionset"][i]['act1'] == 'tokenin'){
                //   actionset = actionset + "Clicked";
                // }
                // if(data1["actionset"][i]['act1'] == 'tokennotin'){
                //   actionset = actionset + "Not Clicked";
                // }
              
               
                if(data1["actionset"][i]['act2'] == 'event'){
                  actionset = actionset + " action";//"event";
                //   $str .= "event";//"event";
                }
                 if(data1["actionset"][i]['act2'] == 'category'){
                    actionset = actionset + " category";
                    // $str .= "category";
                 }

                if(data1["actionset"][i]['act2'] == 'pageurl'){
                   actionset = actionset + "Page URL";
                }
                if(data1["actionset"][i]['act2'] == 'refferingurl'){
                    actionset = actionset + "Reffering URL";
                }
             
           
                   //act3
                 if(data1["actionset"][i]['act3'] == 'isequalto'){
                     actionset = actionset + " is equal to ";
                }
                
                if(data1["actionset"][i]['act3'] == 'contains'){
                     actionset = actionset + " contains ";
                     actionset = actionset + data1["actionset"][i]['act4'];
                }
                else{
                     actionset = actionset + data1["actionset"][i]['act4'];
                }
              
             }
          // actionset = str;
          // if(propertylength > 0)
          // {
          //  str="";
          // }
        for(i=0; i<propertylength;i++)
        {
          propertyset = propertyset + " and ";
            if(data1["propertyset"][i]['ddl1'] == 'Operating System'){
                   propertyset = propertyset + "Platform";
                }
                if(data1["propertyset"][i]['ddl1'] == 'Browser'){
                     propertyset = propertyset + "Browser info";
                }
                if(data1["propertyset"][i]['ddl1'] == 'Platform'){
                     propertyset = propertyset + "Device Type";
                }
                if(data1["propertyset"][i]['ddl1'] == 'Location'){
                     propertyset = propertyset + "State";
                }
                
                //DDL2
                 if(data1["propertyset"][i]['seloperator'] == 'isequalto'){
                     propertyset = propertyset + " is equal to ";
                }
                
                if(data1["propertyset"][i]['seloperator'] == 'isnotequalto'){
                     propertyset = propertyset + " is not equal to ";
                }
                
                //ddl3
                propertyset = propertyset + data1["propertyset"][i]['txtfilterval'];
             }
          //propertyset = str;
        

        if(actionset != "" || propertyset != "")
        {   
          if(actionset != "")
          {
            actionset = "<b>Actions : </b>" + actionset.substring(5,actionset.length);
          }
          if(propertyset != "")
          {
            propertyset = "<b>Properties : </b>" + propertyset.substring(5,propertyset.length);
          }
          
          swal({
        title: 'Segment',
        //type: 'info',
        html:'<div align="left">' + actionset + '<br />' + propertyset ,// +
          // '<b>data </b><br />' + actionset + '<br />',
         
        showCloseButton: true,
        showCancelButton: false,
        focusConfirm: false,
        confirmButtonText:
          'Ok'
       })
        }
        else
        {
              swal("No Segment","","info");
      
        }
  
      });
  }
  
    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }
    showId;
           getTokenCounts(id,event){
             console.log('ID in - ' + id);
             this.webengagementservice.getTokensCountBySegment(id).then(data => {
                if(data['status'] == 'success'){
                   this.counts = data['tokens'];
                   this.showId = id;
                 }
           });
         }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebNotificationsComponent } from './web-notifications.component';

describe('WebNotificationsComponent', () => {
  let component: WebNotificationsComponent;
  let fixture: ComponentFixture<WebNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebNotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

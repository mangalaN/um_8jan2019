import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl, FormArray, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder} from '@angular/forms';

import { WebEngagementService } from '../../shared/data/web-engagement.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import swal from 'sweetalert2';

@Component({
  selector: 'app-web-notifications',
  templateUrl: './web-notifications.component.html',
  styleUrls: ['./web-notifications.component.scss']
})

export class WebNotificationsComponent implements OnInit {
	notificationform: FormGroup;
	template;scheduleData;selectednotification;
	savetemplateDetails;segments;control;

	style = 'material';
    title = 'Success';
    body = 'Organization created successfully!';
    timeout = 3000;
    position: SnotifyPosition = SnotifyPosition.centerTop;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;

    segmentForm: FormGroup;
    totalList;
    // segments =[];
    showSegment = false;
    counts:  any;

    columns = [
    { name: 'Name' },
    { name: 'Subscribers' },
    { name: 'Created' }
  ];

  allColumns = [
    { name: 'Name' },
    { name: 'Subscribers' },
    { name: 'Created' }
  ];
  isCollapsed = false;


   propertyList: Array<any> = [
    { name: 'Operating System',operator:['Is equal to','Is not equal to'], values: ['Windows', 'Linux', 'MacOS'] },
    { name: 'Browser', operator:['Is equal to','Is not equal to'], values: ['Chrome','Firefox','Safari','Opera','Edge'] },
    // { name: 'User Agent', operator:['Is equal to','Is not equal to'], values: [] },
    { name: 'Reffering URL', operator:['Is equal to','Is not equal to'], values: [] },
    { name: 'Page URL', operator:['Is equal to','Is not equal to'], values: [] },
    { name: 'Platform', operator:['Is equal to','Is not equal to'], values: ['Mobile','Desktop'] }
    //{ name: 'Location', operator:['Is equal to','Is not equal to'], values: ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"]},
    ];
   numbers:Array<any> = [];
   showTable;
   showForm;
    p: number = 1;
    listData;
    // totalList;
    previousPage: any;
    pageSize: number = 10;
    collectionSize;
    event = null;
    immediately = null;
    duration = null;
	span = null;
	web_template = null;
    constructor(private formBuilder: FormBuilder,private _FB: FormBuilder, public webengagementservice: WebEngagementService, private snotifyService: SnotifyService) 
    {
    	 this.numbers = Array.from({length:59},(v,k)=>k+1);
	}
	viewSegment(id)
	{
		// alert(id);
		// id = "['"+id+"']";
		this.webengagementservice.getSegmentById([id]).then(data => {
    	  	console.log('Data - ' + JSON.stringify(data));
    	  		let data1=JSON.parse(data["segments"][0]["condition"]);
    	  let propertylength = data1["propertyset"].length;
    	  let propertyset = "";
    	 
    	  let actionlength = data1["actionset"].length;
    	  let actionset = "";
    	  let i=0;
    	  let str="";
    	
    	  for(i=0; i<actionlength;i++)
    	  {
    	  // 	actionset = actionset + "<br />" + JSON.stringify(data1["actionset"][i]);
    	  // }


    	  //     for ($i = 0; $i < $actionlength; $i++) {
                 actionset = actionset + " and ";
                  //act1
                 if(data1["actionset"][i]['act1'] == 'tokenin'){
                  actionset = actionset + "Clicked";
                }
                if(data1["actionset"][i]['act1'] == 'tokennotin'){
                  actionset = actionset + "Not Clicked";
                }
              
               
                if(data1["actionset"][i]['act2'] == 'event'){
                  actionset = actionset + " action";//"event";
                //   $str .= "event";//"event";
                }
                 if(data1["actionset"][i]['act2'] == 'category'){
                    actionset = actionset + " category";
                    // $str .= "category";
                 }
           
                   //act3
                 if(data1["actionset"][i]['act3'] == 'isequalto'){
                     actionset = actionset + " is equal to ";
                }
                
                if(data1["actionset"][i]['act3'] == 'contains'){
                     actionset = actionset + " contains ";
                     actionset = actionset + data1["actionset"][i]['act4'];
                }
                else{
                     actionset = actionset + data1["actionset"][i]['act4'];
                }
              
             }
         	// actionset = str;
         	// if(propertylength > 0)
         	// {
         	// 	str="";
         	// }
    	  for(i=0; i<propertylength;i++)
    	  {
    	  	propertyset = propertyset + " and ";
    	  	  if(data1["propertyset"][i]['ddl1'] == 'Operating System'){
                   propertyset = propertyset + "Platform";
                }
                if(data1["propertyset"][i]['ddl1'] == 'Browser'){
                     propertyset = propertyset + "Browser info";
                }
                if(data1["propertyset"][i]['ddl1'] == 'Platform'){
                     propertyset = propertyset + "Device Type";
                }
                if(data1["propertyset"][i]['ddl1'] == 'Location'){
                     propertyset = propertyset + "State";
                }
                
                  if(data1["propertyset"][i]['ddl1'] == 'Page URL'){
                   propertyset = propertyset + "Page URL";
                }
                if(data1["propertyset"][i]['ddl1'] == 'Reffering URL'){
                    propertyset = propertyset + "Reffering URL";
                }
                
                //DDL2
                 if(data1["propertyset"][i]['seloperator'] == 'isequalto'){
                     propertyset = propertyset + " is equal to ";
                }
                
                if(data1["propertyset"][i]['seloperator'] == 'isnotequalto'){
                     propertyset = propertyset + " is not equal to ";
                }
                
                if(data1["propertyset"][i]['seloperator'] == 'contains'){
                     propertyset = propertyset + " contains ";
                }
                //ddl3
                propertyset = propertyset + data1["propertyset"][i]['txtfilterval'];
             }
    	  	//propertyset = str;
    	  

    	  if(actionset != "" || propertyset != "")
    	  { 	
    	  	if(actionset != "")
    	  	{
    	  		actionset = "<b>Events : </b>" + actionset.substring(5,actionset.length);
    	    }
    	    if(propertyset != "")
    	  	{
    	  		propertyset = "<b>Properties : </b>" + propertyset.substring(5,propertyset.length);
    	    }
    	  	
    	  	swal({
			  title: 'Segment',
			  //type: 'info',
			  html:'<div align="left">' + actionset + '<br />' + propertyset ,// +
			    // '<b>data </b><br />' + actionset + '<br />',
			   
			  showCloseButton: true,
			  showCancelButton: false,
			  focusConfirm: false,
			  confirmButtonText:
			    'Ok'
			 })
    	  }
    	  else
    	  {
    	  	  	swal("No Segment","","info");
    	
    	  }
	
	  // alert(JSON.stringify(data));
    	});
	}
	cancel()
	{
		this.showForm = false;
		this.showTable = true;
	}
	change_status(id,status)
	{
			this.savetemplateData = {
						status: status,
						id: id
					};
				
		this.webengagementservice.updatePushAutomation(this.savetemplateData).then(data => {
			// alert(JSON.stringify(data));
			this.snotifyService.success('Status Updated Successfully!', '', this.getConfig());
			for (var i in this.listData) {
	          if (this.listData[i].id === id) {
	              this.listData[i].status=status;
	            }
	          }
   		});
			
	}
  	ngOnInit() {
   //    this.segmentForm = new FormGroup({
   //        'name': new FormControl(null, [Validators.required]),
   //        /*'textArea': new FormControl(null, [Validators.required]),
   //        'price': new FormControl(null, [Validators.required]),
   //        'duration': new FormControl(null, [Validators.required]),
   //        'durationIn': new FormControl(null, [Validators.required]),
   //        'membershipIcon': new FormControl(null, null)*/
   //        'actionset' : this._FB.array([
   //        ]),
   //        'propertyset' : this._FB.array([
   //        //this.initMapFields()
   //        ])
   //    });

	  // 	let formControls = {
	  // 		radio: 'F',
			// duration: ['', Validators.required],
			// span: ['', Validators.required],
			// template: ['', Validators.required],
			// // segment: ['', Validators.required],
			// event: ['', Validators.required],
			// notifications: this.formBuilder.array([
			// this.createItem(1),
			// ]),
	  //   }
	  //   this.notificationform = this.formBuilder.group(formControls);

	  //   this.webengagementservice.selectwebtemplate().then(data => {
	  //   	if(data['status'] == 'success'){
	  //   		this.template = data['webtemplate'];
	  //   	}
	  //   	console.log('Data - ' + JSON.stringify(data['webtemplate']));
	  //   });
	  //   // this.webengagementservice.getSegments(new Date().getTime()).then(data => {
	  //   // 	if(data['status'] == 'success'){
	  //   // 		this.segments = data['segments'];
	  //   // 		console.log('segments - ' + JSON.stringify(this.segments));
	  //   // 	}
	  //   // });
	    this.getData();
	
	  	this.showForm = false;
    	this.showTable = true;
   	}

  	get f(){ return this.notificationform.controls; }

getData()
{
    this.webengagementservice.getPushAutomation().then(data => {
    	  	console.log('Data - ' + JSON.stringify(data));
	  
        if(data['status'] == 'success'){
            this.listData = data['lists'];
          //  alert(JSON.stringify(this.listData));
            // for(let i = 0; i < this.listData.length; i++){
            //   let encodeJSON = JSON.parse(this.listData[i]['parameters']);
            //   //alert(encodeJSON);
            //   if(encodeJSON != null)
            //     this.listData[i]['parameters']= encodeJSON['link'];
            // }
            this.collectionSize = data['lists'].length;
            this.totalList = data['lists'].length;
        }
    });
}
    loadPage(page: number) {
        if (page !== this.previousPage) {
          this.previousPage = page;
          //this.loadData();
        }
    }
new()
{
    this.event = null;
    this.immediately = null;
    this.duration = null;
	this.span = null;
	this.web_template = null;
 
	this.showForm = true;
	this.showTable = false;

	this.segmentForm = new FormGroup({
          'name': new FormControl(null, [Validators.required]),
          /*'textArea': new FormControl(null, [Validators.required]),
          'price': new FormControl(null, [Validators.required]),
          'duration': new FormControl(null, [Validators.required]),
          'durationIn': new FormControl(null, [Validators.required]),
          'membershipIcon': new FormControl(null, null)*/
          'actionset' : this._FB.array([
          ]),
          'propertyset' : this._FB.array([
          //this.initMapFields()
          ])
      });

	  	let formControls = {
	  		radio: 'F',
			duration: [null, Validators.required],
			span: [null, Validators.required],
			template: [null, Validators.required],
			// segment: ['', Validators.required],
			event: [null, Validators.required],
			notifications: this.formBuilder.array([
			this.createItem(1),
			]),
	    }
	    this.notificationform = this.formBuilder.group(formControls);
this.notificationform.reset();
	    this.webengagementservice.selectwebtemplate().then(data => {
	    	if(data['status'] == 'success'){
	    		this.template = data['webtemplate'];
	    	}
	    	console.log('Data - ' + JSON.stringify(data['webtemplate']));
	    });
	    // this.webengagementservice.getSegments(new Date().getTime()).then(data => {
	    // 	if(data['status'] == 'success'){
	    // 		this.segments = data['segments'];
	    // 		console.log('segments - ' + JSON.stringify(this.segments));
	    // 	}
	    // });
}
  	createItem(index): FormGroup {
	    /*return this.formBuilder.group({
	      immediately : 'F'
	    });*/
	    console.log('Index = ' + index);
	    //this.indexValue++;
	    let i = 'immediately'+index;
	    let d = 'duration'+index;
	    let s = 'span'+index;
	    let t = 'template'+index;
	    return this.formBuilder.group({
	      i : 'F',
	      d: '',
	      s: '',
	      t: ''
	    });
  	}
  	get invoiceparticularsArray(): FormArray{
		return this.notificationform.get('notificationform') as FormArray;
	}
  	addInvoiceParticulars(index){
  		this.control = <FormArray>this.notificationform.controls.notifications;
     	this.control.push(this.createItem(index));
	    // let fg = this.formBuilder.group(new FormControl());
	    // this.invoiceparticularsArray.push(this.createItem(index));
	}
	deleteInvoiceParticulars(idx: number) {
	    this.control.removeAt(idx);
	}
templateData: any;safariData;savetemplateData;
	send(){
		if(this.f.radio.value == 'now'){
			this.savetemplateData = {
				value: this.f.radio.value,
				template: this.f.template.value,
				duration: '',
				status: 'sent'
			};
			console.log('createNotification - ' + JSON.stringify(this.savetemplateData));
			this.webengagementservice.createNotification(this.savetemplateData).then(data => {
				console.log('createNotification - ' + JSON.stringify(this.savetemplateData));
			});
			this.webengagementservice.getTemplateByName(this.f.template.value).then(data => {
				this.templateData = data['campaign'];
			});
			/*this.webengagementservice.getAllTokens('all').then(tokendata => {
				if(tokendata['nonsafaritokens'] != '' && tokendata['nonsafaritokens'] != null){
					this.scheduleData = {
			    		nonsafaritoken: tokendata['nonsafaritokens'],
						title: this.templateData[0]['campaign_title'],
						body: this.templateData[0]['campaign_message'],
						icon: this.templateData[0]['icon'],
						image: this.templateData[0]['image'],
						landingURL: this.templateData[0]['landing_url'],
						safaritoken: '',
						type: '',
						scheduled:'',
						scheduledate:'',
						time:''
			    	}
			    	this.selectednotification = {
			    		all: 'all',
			    		selected: ''
			    	}
			    	console.log('ScheduleData - ' + JSON.stringify(this.scheduleData));
			    	this.webengagementservice.saveWebCampaign(this.scheduleData,this.selectednotification).then(webdata => {
			    		if(webdata['campaign'] > 0){
			    			this.webengagementservice.sendNotification(this.scheduleData,webdata['campaign']).then(notificationdata => {
			    				console.log('sendNotification - ' + JSON.stringify(notificationdata));
			    				if(notificationdata['success'] > 0){
			    					this.status = 'success'
			    				}
			    				else if(notificationdata['success'] == 0){
			    					this.status = 'failed'
			    				}
			    				this.webengagementservice.updateWebCampaign(webdata['campaign'],this.status,notificationdata['success']).then(updatedata => {
			    					this.webengagementservice.getNotSentTokens(webdata['campaign']).then(NotSentData => {
			    						console.log('NotSentData ' + JSON.stringify(NotSentData));
			    					})
			    					if(tokendata['safaritokens'] != '' && tokendata['safaritokens'] != null){
								    	console.log('SSSafari - ' + tokendata['safaritokens']);
								    		this.safariData = {
												title: data['floattitle'],
												body: data['floatmessage'],
												icon: this.form.image,
												image: this.form.bigImage,
												landingURL: data['floaturl'],
												safaritoken: tokendata['safaritokens'],
												campaign_id: webdata['campaign'],
												type: '',
												scheduled:'',
												scheduledate:'',
												time: ''
									    	}
									    	this.selectednotification = {
									    		all: 'all',
									    		selected: ''
									    	}
								    		if(webdata['campaign'] > 0){
								    			this.webengagementservice.sendSafariNotification(this.safariData).then(safaridata => {
								    				this.webengagementservice.updateWebCampaign(webdata['campaign'],'success',(notificationdata['success']+1)).then(updatedata => {
								    					this.webengagementservice.getNotSentTokens(webdata['campaign']).then(NotSentData => {
								    						console.log('NotSentData ' + JSON.stringify(NotSentData));
								    					})
								    					alert('Notifications sent successfully!');
								    				});
								    			});
								    		}
						    			//});
								    }
			    				});
			    			});
			    		}
	    			});
				}
			});*/
		}

		//alert(this.f.radio.value);
	}

	create(index,id,segment_name){
		if((document.getElementById("immediately"+index) as HTMLInputElement).value == 'after'){
    	this.webengagementservice.getTemplateByName((document.getElementById("template"+index) as HTMLInputElement).value).then(data => {
				this.templateData = data['campaign'];
				this.savetemplateDetails = {
					nonsafaritoken: '',
					title: data['campaign'][0]['campaign_title'],
					body: data['campaign'][0]['campaign_message'],
					icon: data['campaign'][0]['icon'],
					image: data['campaign'][0]['image'],
					landingURL: data['campaign'][0]['landing_url'],
					safaritoken: '',
					type: '',
					scheduled:'',
					scheduledate:'',
					time:'',
          			by_segment: id,
        };
				this.selectednotification = {
					all: '',
					selected: ''
				};
				this.webengagementservice.saveWebCampaign(this.savetemplateDetails,this.selectednotification).then(webdata => {
					this.savetemplateData = {
						value: (document.getElementById("immediately"+index) as HTMLInputElement).value,
						template: (document.getElementById("template"+index) as HTMLInputElement).value,
						duration: (document.getElementById("duration"+index) as HTMLInputElement).value + ' ' + (document.getElementById("span"+index) as HTMLInputElement).value,
						status: 'Pending',
						data: {
							title: data['campaign'][0]['campaign_title'],
							body: data['campaign'][0]['campaign_message'],
							icon: data['campaign'][0]['icon'],
							image: data['campaign'][0]['image'],
							landingURL: data['campaign'][0]['landing_url'],
							campaign_id: webdata['campaign']
						},
						segment: segment_name,//(document.getElementById("segment"+index) as HTMLInputElement).value,
            			by_segment: id,
						event: (document.getElementById("event"+index) as HTMLInputElement).value
					};
					this.webengagementservice.createNotification(this.savetemplateData).then(data => {
						console.log('createNotification - ' + JSON.stringify(this.savetemplateData));
						document.getElementById('create_'+index).setAttribute("style", "pointer-events:none; cursor: not-allowed;");
						this.snotifyService.success('Automation Scheduled Successfully', '', this.getConfig());
						this.notificationform.reset();
					  
						this.showForm = false;
						this.showTable = true;
						this.getData();
		  			    // swal("Success!","Automation Scheduled Successfully!","success");
         		// 		window.location.reload();

						//this.notificationform.reset();
						// document.getElementById('create_'+index).style.pointerEvents = "none";
						// document.getElementById('create_'+index).style.cursor = "not-allowed";
						//document.getElementById('create_'+index).prop('disabled',true);//pointer-events: none;
					});
				});

			});


			/*console.log('createNotification - ' + JSON.stringify(this.savetemplateData));
			this.webengagementservice.createNotification(this.savetemplateData).then(data => {
				console.log('createNotification - ' + JSON.stringify(this.savetemplateData));
				document.getElementById('create_'+index).style.pointerEvents = "none";
				document.getElementById('create_'+index).style.cursor = "not-allowed";
				//document.getElementById('create_'+index).prop('disabled',true);//pointer-events: none;
			});*/

			/*this.webengagementservice.saveWebCampaign(this.savetemplateDetails,this.selectednotification).then(webdata => {
				console.log('Save Campaign - ' + JSON.stringify(webdata));
			});*/


		}
	}

  /*action dynamic code*/
  initMapFields1() : FormGroup
  {
    return this._FB.group({
      act1   : [],
      act2   : [],
      act3   : [],
      act4   : []
    });
  }

 // valuesArray1 =[];

 //  ddl1change1(val,index){
 //    this.valuesArray[index] = val;
 //  }

  addSetCondition1(){
   const control = <FormArray>this.segmentForm.controls.actionset;
   control.push(this.initMapFields1());
  }

  removeSetCondition1(i : number) : void
  {
    const control = <FormArray>this.segmentForm.controls.actionset;
    control.removeAt(i);
    //this.valuesArray.splice(i, 1);
  //this.operatorArray.splice(i, 1);
  }
  /*action dynamic code end*/
/*property dynamic*/
  initMapFields() : FormGroup
 {
    return this._FB.group({
       ddl1     : [],
       seloperator   : [],
       txtfilterval   : []
    });
 }

 valuesArray =[];


  ddl1change(val,index){
    //alert(val);


    let nameary=["Operating System","Browser","Reffering URL","Page URL","Platform","Location"];
    let indexsel = nameary.indexOf(val);

    //alert("indexsel"+indexsel);
    //this.valuesArray.push(indexsel);

    this.valuesArray[index] = indexsel;
  }

  addSetCondition(){
    this.valuesArray.push(0);
   const control = <FormArray>this.segmentForm.controls.propertyset;
   control.push(this.initMapFields());
  }

  removeSetCondition(i : number) : void
  {
    const control = <FormArray>this.segmentForm.controls.propertyset;
    control.removeAt(i);
    this.valuesArray.splice(i, 1);
  //this.operatorArray.splice(i, 1);
  }
/*property dynamic end*/

addSegment(val, index)
{
	if((document.getElementById("immediately"+index) as HTMLInputElement).value == '')
	{
		this.immediately = '';
	}
	if((document.getElementById("event"+index) as HTMLInputElement).value == '')
	{
		this.event = '';
	}
	if((document.getElementById("duration"+index) as HTMLInputElement).value == '')
	{
		this.duration = '';
	}
	if((document.getElementById("span"+index) as HTMLInputElement).value == '')
	{
		this.span = '';
	}
  	if((document.getElementById("template"+index) as HTMLInputElement).value == '')
	{
		this.web_template = '';
	}
	
  //alert("immediately=" + (document.getElementById("immediately"+index) as HTMLInputElement).value + "event=" + (document.getElementById("event"+index) as HTMLInputElement).value + "duration=" + (document.getElementById("duration"+index) as HTMLInputElement).value + "span=" + (document.getElementById("span"+index) as HTMLInputElement).value + "template=" + (document.getElementById("template"+index) as HTMLInputElement).value);
  if((document.getElementById("immediately"+index) as HTMLInputElement).value == '' || (document.getElementById("event"+index) as HTMLInputElement).value == '' || (document.getElementById("duration"+index) as HTMLInputElement).value == '' || (document.getElementById("span"+index) as HTMLInputElement).value == '' || (document.getElementById("template"+index) as HTMLInputElement).value == '')
  {
   // swal("Error!","Please Fill All the required fields","error");
    return;
  }
console.log(JSON.stringify(val));
let obj= {'name':'','condition':{'propertyset':'','actionset':''},'subscribers':0};
//let obj= {'name':'','condition':'','subscribers':0};
obj['name'] = '';
obj['condition']['propertyset'] = val.propertyset;
obj['condition']['actionset'] = val.actionset;
obj['subscribers'] = 0;

//let mydata = JSON.stringify(obj);

this.webengagementservice.addSegment(obj).then(data => {
  if(data['status'] == 'true'){
    // alert(JSON.stringify(data));
    this.create(index,data['id'],'');

     // this.snotifyService.success('Added Successfullly', '', this.getConfig());
     this.segmentForm.reset();
  }
})

}

viewNotify(data)
{
  let data1=JSON.parse(data);
  let img="";
  if(data1.image)
  {
  	img = "'<b>Big Image : </b><img src=' + img + ' /><br /></div>',";
  }

	swal({
  title: 'Notification Data',
  //type: 'info',
  html:'<div align="left">' +
    '<b>Title : </b>' + data1.title + '<br />' +
    '<b>Message : </b>' + data1.body + '<br />' +
    '<b>Landing Page URL : </b>' + data1.landingURL + '<br />' +
    '<b>Icon : </b><img src=' + data1.icon + ' /><br />' +
    img,
  showCloseButton: true,
  showCancelButton: false,
  focusConfirm: false,
  confirmButtonText:
    'Ok'
 })
}
	getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
    }
}
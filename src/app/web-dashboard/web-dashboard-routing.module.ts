import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WebDashboardComponent } from "./web-dashboard/web-dashboard.component";
import { WebpushChartsComponent } from "./webpush-charts/webpush-charts.component";
import { SafaripushconfigComponent } from "../safaripushconfig/safaripushconfig.component";

import { WebEngagementComponent } from "./web-engagement/web-engagement.component";
import { WebTemplateComponent } from "./web-template/web-template.component";
import { ViewTokensComponent } from "./view-tokens/view-tokens.component";
import { SegmentsComponent } from "./segments/segments.component";
import { WebNotificationsComponent } from './web-notification/web-notifications.component';

const routes: Routes = [
    {
        path: '',
        children: [
        {
        	path: 'dashboard',
        	component: WebDashboardComponent,
        	data: {
        		title: 'Web Dashboard'
        	}
        },
        {
        	path: 'webpush-charts',
        	component: WebpushChartsComponent,
        	data: {
        		title: 'Charts'
        	}
        },
        {
            path: 'web-engagement',
            component: WebEngagementComponent,
            data: {
              title: 'Web-Engagement'
            }
        },
        {
            path: 'segments',
            component: SegmentsComponent,
            data: {
              title: 'Segments'
            }
        },
        {
            path: 'web-notification',
            component: WebNotificationsComponent,
            data: {
              title: 'Web Notification'
            }
        },
        {
            path: 'tokens',
            component: ViewTokensComponent,
            data: {
              title: 'Tokens'
            }
        },
        {
      path: 'web-template',
      component: WebTemplateComponent,
      data: {
        title: 'Web-Template'
      }
  },
        {
          path: 'safaripushconfig',
          component: SafaripushconfigComponent,
          data:{
            title: 'Safari Push Config'
          }
        }
      ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class WebDashboardRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { WebDashboardRoutingModule } from "./web-dashboard-routing.module";
import { WebDashboardComponent } from "./web-dashboard/web-dashboard.component";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxPaginationModule } from 'ngx-pagination';
import { WebpushChartsComponent } from './webpush-charts/webpush-charts.component';
import { WebEngagementComponent } from "./web-engagement/web-engagement.component";
import { WebTemplateComponent } from "./web-template/web-template.component";
import { ViewTokensComponent } from "./view-tokens/view-tokens.component";
import { SegmentsComponent } from './segments/segments.component';
import { SafaripushconfigComponent } from "../safaripushconfig/safaripushconfig.component";

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularHighchartsChartModule } from 'angular-highcharts-chart';
import { WebNotificationsComponent } from './web-notification/web-notifications.component';
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
    imports: [
        CommonModule,
        NgxChartsModule,
        WebDashboardRoutingModule,
        NgxDatatableModule,
        NgxPaginationModule,
        NgbModule,
        MatchHeightModule,
        FormsModule,
        ReactiveFormsModule,
        AngularHighchartsChartModule
        ,UiSwitchModule
],
    declarations: [
        WebDashboardComponent,
        WebpushChartsComponent,
        WebEngagementComponent,
        WebTemplateComponent,
        ViewTokensComponent,
        SegmentsComponent,
        WebNotificationsComponent,
        SafaripushconfigComponent
   ]
})
export class WebDashboardModule { }

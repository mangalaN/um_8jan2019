import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
// import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';


 import {WebTemplateService } from '../../shared/data/web-template.service';
 import { SettingsService } from '../../shared/data/settings.service';

import swal from 'sweetalert2';


@Component({
  selector: 'app-web-template',
  templateUrl: './web-template.component.html',
  styleUrls: ['./web-template.component.scss']
})
export class WebTemplateComponent implements OnInit {
  segments =[];
  totalList;
  collectionSize;
  saveData;
  form = {
		'floattitle' : '',
		'floatmessage': '',
		'floaturl': '',
		'image': '',
		'bigImage':'',
		'day': '',
		'hours': '',
		'minutes': '',
		'scheduledate': '',
		'scheduled':'',
		'time': '',
		'segment': '',
		'seg': '',
		'token':'click365.com.au',
		'icon':'assets/img/logo.png'
	};
  selectednotification;
  idArray = [];

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  showBigImage = false;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;


  columns = [
    { name: 'Event' },
    { name: 'Template Mapping' },
    { name: 'Action' }
  ];
  @ViewChild(DatatableComponent) table: DatatableComponent;
  url;
  loader:boolean = false;
  bigImageUrl;
showTable = true;
templateID=0;
sitedir;
  constructor(private http: HttpClient,private snotifyService: SnotifyService,public webtemplateservice: WebTemplateService,public settingsservice: SettingsService, public router: Router) { }

  ngOnInit() {
    this.webtemplateservice.selectwebtemplate().then(data =>{
      if(data['webtemplate']){
        this.segments = data['webtemplate'];
        this.collectionSize = data['webtemplate'].length;
        this.totalList = data['webtemplate'].length;
      }
    });
    	this.settingsservice.getSettings().then(data => {
         console.log('data - ' + data['settings'][0]['sitedir']);
         if(data['status'] == 'success'){
           this.sitedir = data['settings'][0]['sitedir'];
         }
      });


  }


  updatewebtemplate(){
    this.saveData = {
    title:this.form.floattitle,
    body:  this.form.floatmessage,
    icon:this.url,
    image: this.bigImageUrl,
    landingURL: this.form.floaturl,
  }
  this.snotifyService.success('Updted Successfully', '', this.getConfig());
  this.showTable = true;
    this.webtemplateservice.updatewebtemplate(this.saveData,this.templateID).then(data =>{
      console.log(data);
      if(data['status'] == 'success'){
        this.webtemplateservice.selectwebtemplate().then(data =>{
          if(data['webtemplate']){
            this.segments = data['webtemplate'];
            this.collectionSize = data['webtemplate'].length;
            this.totalList = data['webtemplate'].length;
          }
         })
        // this.form.floattitle = data['webtemplate'][0]['title'];
        // this.form.floatmessage = data['webtemplate'][0]['body'];
        // this.form.image = data['webtemplate'][0]['icon'];
        // this.form.bigImage = data['webtemplate'][0]['image'];
        // this.form.floaturl = data['webtemplate'][0]['landingURL'];
      }
    })
  }


 edittemplateById(id){
   this.showTable = false;
   this.templateID=id;
   this.webtemplateservice.edittemplateById(id).then(data =>{
     console.log('Tem val - ' + JSON.stringify(data));
     this.form.floattitle = data['webtemplate'][0]['campaign_title'];
     this.form.floatmessage = data['webtemplate'][0]['campaign_message'];
    // this.form.image = data['webtemplate'][0]['icon'];
    console.log('Image URL - ' + data['webtemplate'][0]['icon']);
     this.url = data['webtemplate'][0]['icon'];
    this.bigImageUrl = data['webtemplate'][0]['image'];
     this.form.floaturl = data['webtemplate'][0]['landing_url'];

     //this.saveData['title'].setValue(data['webtemplate'][0]['campaign_title']);
      //this.saveData = data[''][0]['campaign_title'];
      // this.saveData = data[''][0]['campaign_message'];
      // this.saveData = data[''][0]['landing_url'];
      // this.saveData = data[''][0]['image'];
      // this.saveData = data[''][0]['icon'];
      //this.segments = data['webtemplate'][0]['campaign_title'];
    })
    // this.router.navigate(['/web-dashboard/web-engagement'],{queryParams:{id:id}});
   /*this.webtemplateservice.edittemplate(id).then(data => {
     if(data['status'] == 'success'){

     }
   })*/
 }
 // updatetemplate(){
 //   this.webtemplateservice.updatetemplate().then(data => {
 //
 //
 //   })
 // }

 cancel(){
   this.showTable = true;
 }
 addBigImage(event){
   this.showBigImage = false;
   if(event){
     this.showBigImage = true;
   }
   else{
     this.showBigImage = false;
   }
 }imageloader =false;
 uploadBigImage(files: File[]){
   	this.imageloader = true;this.loader = false;
      // this.bigImageUrl = 'https://click365.com.au/usermanagement/images/UploadwebImages/'+files[0].name;
    //  this.bigImageUrl = 'https://click365.com.au/usermanagement/images/UploadwebImages/usermanagement/'+files[0].name;
      if(this.sitedir != '' && this.sitedir != null){
        this.bigImageUrl ='https://click365.com.au/usermanagement/images/UploadwebImages/'+this.sitedir+'/'+files[0].name;
      }
      else if(this.sitedir == '' || this.sitedir == null){
        this.bigImageUrl = 'https://click365.com.au/usermanagement/images/UploadwebImages/'+files[0].name;
      }
       console.log('dxcvdx - ' + this.bigImageUrl);
       this.uploadAndProgress(files,'bigImage');
 }
 uploadmembershipIcon(files: File[]){
   this.loader = true;this.imageloader = false;
      // this.url = 'https://click365.com.au/usermanagement/images/UploadwebImages/'+files[0].name;
      //this.url = 'https://click365.com.au/usermanagement/images/UploadwebImages/usermanagement/'+files[0].name;
      if(this.sitedir != '' && this.sitedir != null){
        this.url ='https://click365.com.au/usermanagement/images/UploadwebImages/'+this.sitedir+'/'+files[0].name;
      }
      else if(this.sitedir == '' || this.sitedir == null ){
        this.url = 'https://click365.com.au/usermanagement/images/UploadwebImages/'+files[0].name;
      }
       console.log('dxcvdx - ' + this.url);
       this.uploadAndProgress(files,'smallImage');
 }

 uploadAndProgress(files: File[],imageType){
     console.log(imageType)
   var formData = new FormData();
   Array.from(files).forEach(f => formData.append('file',f));
   this.http.post('https:/click365.com.au/usermanagement/uploadTemplateImages.php?fld=UploadwebImages/usermanagement&type='+imageType, formData)
     .subscribe(event => {
       this.loader = false;this.imageloader = false;
       if(event && event['status'] == "Sorry, your file is too large."){
         alert('Sorry, your file is too large. Recommended Size: 192x192.');
         this.url = '';this.bigImageUrl = '';
       }
   });
 }
 getConfig(): SnotifyToastConfig {
   this.snotifyService.setDefaults({
       global: {
           newOnTop: this.newTop,
           maxAtPosition: this.blockMax,
           maxOnScreen: this.dockMax,
       }
   });
   return {
       bodyMaxLength: this.bodyMaxLength,
       titleMaxLength: this.titleMaxLength,
       backdrop: this.backdrop,
       position: this.position,
       timeout: this.timeout,
       showProgressBar: this.progressBar,
       closeOnClick: this.closeClick,
       pauseOnHover: this.pauseHover
   };
 }

  // viewwebtemplate() {
  //   this.webtemplateservice.selectwebtemplate().then(data => {
  //     swal({
  //         title: '<b>WebTemplate Detail</b>',
  //         type: 'info',
  //         html:
  //         '<table class="viewOrg"><tbody><tr></tr>' +
  //         '<tr><th>EVENT</th><td>'+data['webtemplate'][0].campaign_title+'</td></tr>' +
  //         '<tr><th>TEMPLATE NAME</th><td>'+data['webtemplate'][0].template_name+'</td></tr>' +
  //         '</tbody></table><br />',
  //         showCloseButton: true,
  //         confirmButtonText:
  //         '<i class="fa fa-thumbs-up"></i> Okay!'
  //     })
  //   });
  // }
}

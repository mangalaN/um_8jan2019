import { Component, OnInit } from '@angular/core';
import * as chartsData from '../../shared/configs/ngx-charts.config';
import * as shape from 'd3-shape';
import * as Highcharts from 'highcharts';
import { WebDashboardService } from '../../shared/data/web-dashboard.service';
import { Router, ActivatedRoute } from "@angular/router";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { AngularHighchartsChartModule } from 'angular-highcharts-chart';
import swal from 'sweetalert2';

@Component({
	selector: 'app-webpush-charts',
	templateUrl: './webpush-charts.component.html',
	styleUrls: ['./webpush-charts.component.scss']
})
export class WebpushChartsComponent implements OnInit {

	ChartColorScheme = {
		domain: ['#666EE8', '#FF9149', '#FF4961', '#AAAAAA']
	};
	browserChart=[];
	locationChart=[];
	timelineChart=[];
	deviceChart=[];
	platformChart=[];
  chartOptions;
  timeCharts;
  Highcharts = Highcharts;

	abTestStats=[];
	isABtestingReport:boolean =false;

	winnerType;
	successSent = false;
	winnerSending;

	style = 'material';
  title = 'Success';
  body = 'Organization created successfully!';
  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  constructor(public webDashboardService: WebDashboardService,
  	private snotifyService: SnotifyService,
  	private router: Router,
  	private route: ActivatedRoute) {
  	this.route.queryParams.subscribe(params => {
      this.webDashboardService.getValueforTimelineGraphs(params['id'],params['type']).then(data => {
        if(data['status'] =='success'){
          this.timeCharts = data['stats'];
          let senddate = this.timeCharts[0].createdDate.split('-');
          let timeval = this.timeCharts[0].time.split(':');
          let month = new Date(this.timeCharts[0].createdDate).getUTCMonth();
          let chartData = [];
          let receivedData = [];
          let clickedData = [];
          console.log('data - ' + JSON.stringify(this.timeCharts));
          for(var i = 0; i < this.timeCharts.length; i++){
            if(this.timeCharts[i]['type'] == 'received'){
              receivedData.push([this.timeCharts[i].datetimeval, this.timeCharts[i].counts]);
            }
            if(this.timeCharts[i]['type'] == 'clicked'){
              clickedData.push([this.timeCharts[i].datetimeval, this.timeCharts[i].counts]);
            }

          }
          Highcharts.setOptions({
                    time: {
                        timezoneOffset: parseInt(timeval[0]) * 60
                    }
                });
                this.chartOptions = {
                  credits: {
                        enabled: false
                      },
                  title: {
                      text: 'Rate'
                  },
                  chart: {
                    type: 'spline'
                  },
                    xAxis: {
                    labels: {
                        enabled: false
                    },
                     type: 'datetime'
                  },

                  series: [{
                      data: receivedData,
                      pointStart: Date.UTC((senddate[0]), month, (senddate[2])),
                      pointInterval: 10000 * 60,
                      name:'Received'
                  },
                  {
                      data: clickedData,
                      pointStart: Date.UTC((senddate[0]), month, (senddate[2])),
                      pointInterval: 10000 * 60,
                      name:'Clicked'
                  }]
                };
        }
      });
		  if(params['type']=='AB'){
		    this.isABtestingReport = true;
		  }
    	this.getTbltestingReport(params['id'],params['type']);
	 		/*chart1 end*/
	 		this.getBrowserChart(params['id'],params['type']);
	 		this.getLocationChart(params['id'],params['type']);
	 		this.getTimelineChart(params['id'],params['type']);
	 		this.getPlatformChart(params['id'],params['type']);
	 		this.getDeviceChart(params['id'],params['type']);
	 	});
  }

  ngOnInit() {

  }

	sendABNotification(){
		this.route.queryParams.subscribe(params => {
			this.webDashboardService.sendWinnerNotification(params['id'],params['type'],this.winnerType).then(data => {
				if(data['status'] =='success'){
					this.snotifyService.success('Remaining Notifications sent successfully!', '', this.getConfig());
					this.successSent = true;
					this.webDashboardService.getValueforTimelineGraphs(params['id'],params['type']).then(data => {
		        if(data['status'] =='success'){
		          this.timeCharts = data['stats'];
		          let senddate = this.timeCharts[0].createdDate.split('-');
		          let timeval = this.timeCharts[0].time.split(':');
		          let month = new Date(this.timeCharts[0].createdDate).getUTCMonth();
		          let chartData = [];
		          let receivedData = [];
		          let clickedData = [];
		          console.log('data - ' + JSON.stringify(this.timeCharts));
		          for(var i = 0; i < this.timeCharts.length; i++){
		            if(this.timeCharts[i]['type'] == 'received'){
		              receivedData.push([this.timeCharts[i].datetimeval, this.timeCharts[i].counts]);
		            }
		            if(this.timeCharts[i]['type'] == 'clicked'){
		              clickedData.push([this.timeCharts[i].datetimeval, this.timeCharts[i].counts]);
		            }

		          }
		          Highcharts.setOptions({
		                    time: {
		                        timezoneOffset: parseInt(timeval[0]) * 60
		                    }
		                });
		                this.chartOptions = {
		                  credits: {
		                        enabled: false
		                      },
		                  title: {
		                      text: 'Rate'
		                  },
		                  chart: {
		                    type: 'spline'
		                  },
		                    xAxis: {
		                    labels: {
		                        enabled: false
		                    },
		                     type: 'datetime'
		                  },

		                  series: [{
		                      data: receivedData,
		                      pointStart: Date.UTC((senddate[0]), month, (senddate[2])),
		                      pointInterval: 10000 * 60,
		                      name:'Received'
		                  },
		                  {
		                      data: clickedData,
		                      pointStart: Date.UTC((senddate[0]), month, (senddate[2])),
		                      pointInterval: 10000 * 60,
		                      name:'Clicked'
		                  }]
		                };
		        }
		      });
				  if(params['type']=='AB'){
				    this.isABtestingReport = true;
				  }
		    	this.getTbltestingReport(params['id'],params['type']);
			 		/*chart1 end*/
			 		this.getBrowserChart(params['id'],params['type']);
			 		this.getLocationChart(params['id'],params['type']);
			 		this.getTimelineChart(params['id'],params['type']);
			 		this.getPlatformChart(params['id'],params['type']);
			 		this.getDeviceChart(params['id'],params['type']);
				}
			});
		});
}

  getBrowserChart(id,type){
  	this.webDashboardService.getWebNotificationReportByBrowser(id,type).then(data => {
  		if(data['status'] =='success'){
  			this.browserChart = data['stats'];
  		}
  	});
  }
  getLocationChart(id,type){
  	  	this.webDashboardService.getWebNotificationReportByLocation(id,type).then(data => {
  		if(data['status'] =='success'){
  			this.locationChart = data['stats'];
  		}
  	});
  }
  getTimelineChart(id,type){
	  	this.webDashboardService.getWebNotificationReportByTime(id,type).then(data => {
		if(data['status'] =='success'){
			this.timelineChart = data['stats'];
		}
	});
}
    getPlatformChart(id,type){
	  	this.webDashboardService.getWebNotificationReportByPlatform(id,type).then(data => {
		if(data['status'] =='success'){
			this.platformChart = data['stats'];
		}
	});
}
    getDeviceChart(id,type){
  	  	this.webDashboardService.getWebNotificationReportByDevice(id,type).then(data => {
  		if(data['status'] =='success'){
  			this.deviceChart = data['stats'];
  		}
  	});
  }

    getTbltestingReport(id,type){
    this.webDashboardService.getWebNotificationReportTbl(id,type).then(data => {
      if(data['status'] =='success'){
        this.abTestStats = data['stats'];
				this.winnerType = this.abTestStats[0]['winnertype'];
				this.winnerSending = this.abTestStats[0]['winnerSending'];
				if(this.abTestStats[0]['status'] == 'sent'){
					this.successSent = true;
				}
				else{
					this.successSent = false;
				}
      }
    });
  }

	getConfig(): SnotifyToastConfig {
			this.snotifyService.setDefaults({
					global: {
							newOnTop: this.newTop,
							maxAtPosition: this.blockMax,
							maxOnScreen: this.dockMax,
					}
			});
			return {
					bodyMaxLength: this.bodyMaxLength,
					titleMaxLength: this.titleMaxLength,
					backdrop: this.backdrop,
					position: this.position,
					timeout: this.timeout,
					showProgressBar: this.progressBar,
					closeOnClick: this.closeClick,
					pauseOnHover: this.pauseHover
			};
	}

 // getWebNotificationReportByBrowser
//getWebNotificationReportByLocation
//getWebNotificationReportByTime

}

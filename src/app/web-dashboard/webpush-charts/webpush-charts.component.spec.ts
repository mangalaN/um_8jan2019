import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebpushChartsComponent } from './webpush-charts.component';

describe('WebpushChartsComponent', () => {
  let component: WebpushChartsComponent;
  let fixture: ComponentFixture<WebpushChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebpushChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebpushChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

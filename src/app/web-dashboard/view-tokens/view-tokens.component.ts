import { Component, OnInit } from '@angular/core';
import { WebDashboardService } from '../../shared/data/web-dashboard.service';
import * as chartsData from '../../shared/configs/ngx-charts.config';
import * as shape from 'd3-shape';
import { NgbDateStruct, NgbDatepickerI18n, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormArray, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-view-tokens',
  templateUrl: './view-tokens.component.html',
  styleUrls: ['./view-tokens.component.scss']
})
export class ViewTokensComponent implements OnInit {
  ChartColorScheme = {
    domain: ['#666EE8', '#FF9149', '#FF4961', '#AAAAAA']
  };
  barChartColorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  pieChartColorScheme = {
    domain: ['#666EE8', '#28D094', '#FF4961', '#AAAAAA']
  };

  charts;
  userchart = [];
  tilesData =[];
  rows =[];
  abTestingRows=[];
  states = [];
  cityChart;

  filterModule = {
    'startDate':'',
    'endDate':''
  }
  filterForm: FormGroup;
  columns = [
    { name: 'Browser' },
    { name: 'Location' },
    { name: 'State' },
    { name: 'Device' },
    { name: 'Token' },
  ];

  allColumns = [
    { name: 'Browser' },
    { name: 'Location' },
    { name: 'State' },
    { name: 'Device' },
    { name: 'Token' },
  ];

 isCollapsed = false;
 showCol = false;
 error:any={isError:false,errorMessage:''};
 parsedStartDate;
 parsedEndDate;
  constructor(public webDashboardService: WebDashboardService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.webDashboardService.getWebNotificationTokenByOrg().then(data =>{
       if(data['status'] == 'success'){
         this.rows = data['rdata'];
       }
    });
    this.filterForm = this.formBuilder.group({
        startDate :['',null],
        endDate :['',null]
    });
    this.webDashboardService.getWebTokenStats().then(data =>{
       if(data['status'] != 0){
         this.userchart = data['stats'];
                  
       }
       else{
         this.userchart = [];
       }
    });
    this.webDashboardService.getWebTokenStatsbydate(this.filterModule).then(data =>{
       if(data['status'] != 0){
         this.userchart = data['stats'];
                  
       }
       else{
         this.userchart = [];
       }
    });
  }
  toggle(col) {
    const isChecked = this.isChecked(col);
    console.log('isChecked value - ' + isChecked);
    if(isChecked) {
      this.columns = this.columns.filter(c => {
        console.log('Name - ' + c.name);
        return c.name !== col.name;
      });
    } else {
      this.columns = [...this.columns, col];
      console.log('columns - ' + this.columns);
    }
  }

  isChecked(col) {
    return this.columns.find(c => {
      return c.name === col.name;
    });
  }

  getColumns() {
    this.showCol = !this.showCol;

    // CHANGE THE NAME OF THE BUTTON.
    if(this.showCol)
      this.showCol = true;
    else
      this.showCol = false;
  }

  getFilters(){
      this.webDashboardService.getWebTokenStatsbydate(this.filterModule).then(data =>{
       if(data['status'] != 0){
         this.userchart = data['stats'];
                  
       }
       else{
         this.userchart = [];
       }
    });
    this.webDashboardService.getWebNotificationTokenByOrgDate(this.filterModule).then(data =>{
       if(data['status'] == 'success'){
         this.rows = data['rdata'];
       }
       else{
         this.rows = [];
       }
    });
  }

  getActive(){
    this.webDashboardService.getActiveTokenByOrg().then(data =>{
      if(data['status'] == 'success'){
        console.log('Came here');
        this.rows = data['rdata'];
      }
      else{
        this.rows = [];
      }
    });
  }
  getInactive(){
    this.webDashboardService.getInactiveTokenByOrg().then(data =>{
      if(data['status'] == 'success'){
        console.log('Came here');
        this.rows = data['rdata'];
      }
      else{
        this.rows = [];
      }
   });
 }
 endDate(){
     this.error = '';
    //if(this.filterForm.controls.endDate.value){
      this.parsedStartDate = this.filterForm.controls.startDate.value['year']+'-'+this.filterForm.controls.startDate.value['month']+'-'+this.filterForm.controls.startDate.value['day'];
      this.parsedEndDate = this.filterForm.controls.endDate.value['year']+'-'+this.filterForm.controls.endDate.value['month']+'-'+this.filterForm.controls.endDate.value['day'];
      if(Date.parse(this.parsedStartDate) > Date.parse(this.parsedEndDate)){
        this.error={isError:true,errorMessage:'End date should be greater than Start date'}
      }
}
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTokensComponent } from './view-tokens.component';

describe('ViewTokensComponent', () => {
  let component: ViewTokensComponent;
  let fixture: ComponentFixture<ViewTokensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTokensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

import { SubscriptionService } from '../shared/data/subscription.service';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
	personalForm: FormGroup;
	public subscriptionList;
	public subscribers = [];
	public submitted: boolean = false;
	rows;
	public viewForm: boolean = false;
	columns = [
        { prop: 'type' },
        { prop: 'noOfCampaign' },
        { prop: 'noOfMembers' }
    ];

    @ViewChild(DatatableComponent) table: DatatableComponent;
	
	constructor(public subscriptionService: SubscriptionService) {
		this.subscriptionService.getSubscription(new Date().getTime()).then(data => {
			this.subscriptionList = data['subscription'];
			if(data['subscription']){
				for (let i = 0; i < data['subscription'].length; i++) {
					let array = {
						type: data['subscription'][i]['type'],
						noOfCampaign: data['subscription'][i]['campaign'],
						noOfMembers: data['subscription'][i]['members']
					}
					this.subscribers.push(array);
				}
				this.rows = this.subscribers;
			}
		});
	}

	ngOnInit() {
		this.personalForm = new FormGroup({
			'type': new FormControl(null, [Validators.required]),
			'campaign': new FormControl(null, [Validators.required]),
			'members': new FormControl(null, [Validators.required])
	    }, {updateOn: 'blur'});
	}
	
	addSubscription(){
		this.viewForm = true;
		this.personalForm.reset();
	}

	cancel(){
		this.viewForm = false;
		this.personalForm.reset();
	}

	saveSubscription(){
		this.submitted = true;
		let subscriptionDetail = {
			'type': this.personalForm.controls['type'].value,
			'campaign': this.personalForm.controls['campaign'].value,
			'members': this.personalForm.controls['members'].value
		}
		this.subscriptionService.saveSubscription(subscriptionDetail).then(data => {
			if(data['status']){
				console.log('Came here!');
				this.submitted = false;
				this.viewForm = false;
				this.subscribers = [];
				this.rows = [];
				this.subscriptionService.getSubscription(new Date().getTime()).then(data => {
					this.subscriptionList = data['subscription'];
					if(data['subscription']){
						for (let i = 0; i < data['subscription'].length; i++) {
							let array = {
								type: data['subscription'][i]['type'],
								noOfCampaign: data['subscription'][i]['campaign'],
								noOfMembers: data['subscription'][i]['members']
							}
							this.subscribers.push(array);
						}
						this.rows = this.subscribers;
					}
				});
			}
		});
	}
}

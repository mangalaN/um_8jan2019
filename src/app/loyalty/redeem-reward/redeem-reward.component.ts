import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {NgbCarousel, NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { loyaltyService } from '../../shared/data/loyalty.service';
import { MembershipService } from '../../shared/data/membership.service';
import { loyaltypointService } from '../../shared/data/loyaltypoint.service';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import {Observable} from "rxjs";

const URL = 'https:/click365.com.au/usermanagement/uploadLogo.php?fld=CompanyLogo';

@Component({
  selector: 'app-redeem-reward',
  templateUrl: './redeem-reward.component.html',
  providers: [NgbCarouselConfig],
  styleUrls: ['./redeem-reward.component.scss']
})
export class RedeemRewardComponent implements OnInit {
	isAdmin: boolean = true;
	status: boolean = false;
	user;
	userName;
	loyaltyPoints;
	points = [];
	expirydateArray = [];
	showTable: boolean = true;
	rewImage: boolean = false;
	loyaltyDisplay;
	rows = [];
    temp = [];
    exportData = [];
    logo = [];
    rewarddata;
    rewardDisplay;
    columns = [
      { name: 'Reward Name' },
      { name: 'Reward Image' },
      { name: 'Additional Images' },
      { name: 'Points Value' },
      { name: 'Membership Type' },
      { name: 'Start Time' },
      { name: 'End Time' }
  	];

  	endTime;additionalImages = [];
	error:any={isError:false,errorMessage:''};

    timeout = 3000;
	position: SnotifyPosition = SnotifyPosition.centerTop;
	progressBar = true;
	closeClick = true;
	newTop = true;
	backdrop = -1;
	dockMax = 8;
	blockMax = 6;
	pauseHover = true;
	titleMaxLength = 15;
	bodyMaxLength = 80;
	members = [];
	slideIndex = 1;images;
	image;
	addImage;
	urls = [];

    rewardForm: FormGroup;
    @ViewChild(DatatableComponent) table: DatatableComponent;
    @ViewChild('companylogoLabel') companylogoLabel : ElementRef;
    @ViewChild('rewardImages') rewardImages : ElementRef;
    @ViewChild('myCarousel') myCarousel: NgbCarousel;

	config = {
		displayKey:"description",
		search:true,
		height: 'auto',
		placeholder:'Select',
		customComparator: ()=>{},
		limitTo: this.members.length,
		moreText: 'more',
		noResultsFound: 'No results found!',
		searchPlaceholder:'Search',
		searchOnKey: 'name'
	}
	class;index;

	public uploader:FileUploader = new FileUploader({
		url: URL,
	    isHTML5: true
	});

	constructor(config: NgbCarouselConfig, private snotifyService: SnotifyService, private LoyaltyService: loyaltyService, public membershipservice: MembershipService, private http: HttpClient, private formBuilder: FormBuilder,private route: ActivatedRoute, public router: Router, public LoyaltypointService: loyaltypointService) {
		config.interval = 10000;
		config.wrap = false;
    	config.keyboard = false;

    	this.user = JSON.parse(localStorage.getItem('currentUser'));
		if(this.user['role_id'] != "1" && this.user['role_id'] != "2"){
			this.isAdmin = false;
		}
		this.LoyaltyService.getLoyalty(new Date().getTime()).then(data =>{
			this.loyaltyPoints = data['loyalty'];
			for (let i = 0; i < data['loyalty'].length; i++) {
			  this.points[data['loyalty'][i]['id']] = data['loyalty'][i]['points'];
			  this.expirydateArray[data['loyalty'][i]['id']] = data['loyalty'][i]['expiryDate'];
			}
		})

		this.LoyaltyService.getReward(new Date().getTime()).then(data =>{
			this.rewardDisplay = data['reward'];
		});
	}

	ngOnInit() {
		this.rewardForm = this.formBuilder.group({
			rewardName: ['', Validators.required],
			description: ['', Validators.required],
			rewardImage: ['', Validators.required],
			rewardAddImage: ['', null],
			points: ['', Validators.required],
			memType: ['', Validators.required],
			startTime: ['', Validators.required],
			endTime: ['', Validators.required],
			status: ['']
		});

		this.membershipservice.getMembership().then(data => {
			if(data['membership']){
				for(let i = 0; i < data['membership'].length; i++){
					this.members.push(data['membership'][i].plans);
				}
			}
		});
	}

	addReward(){
		this.rewarddata = {
	        rewardName: this.rewardForm.controls.rewardName.value,
	        description: this.rewardForm.controls.description.value,
	        rewardImage: this.companylogoLabel.nativeElement.innerHTML,
	        rewardAddImage: JSON.stringify(this.logo),
	        points: this.rewardForm.controls.points.value,
	        memType: this.rewardForm.controls.memType.value,
	        startTime: this.rewardForm.controls.startTime.value,
	        endTime: this.rewardForm.controls.endTime.value,
	        status: this.rewardForm.controls.status.value
	    };
	    this.LoyaltyService.addReward(this.rewarddata, new Date().getTime()).then(data => {
	    	this.LoyaltyService.getReward(new Date().getTime()).then(data =>{
				this.rewardDisplay = data['reward'];
				this.showTable = true;
				this.rewardForm.reset();
			});
	    	this.snotifyService.success('Updated Successfullly', '', this.getConfig());
	    });
	}

	uploadSubmit(val, event){
    	this.rewImage = false;
    	this.addImage = false;
		for (let i = 0; i < this.uploader.queue.length; i++) {
			let fileItem = this.uploader.queue[i]._file;
			if(fileItem.size > 10000000){
				alert("Each File should be less than 10 MB of size.");
				return;
			}
		}
		for (let j = 0; j < this.uploader.queue.length; j++) {
			let data = new FormData();
			let fileItem = this.uploader.queue[j]._file;
			if(val == 'rewardImage'){
				this.rewImage = true;
				this.companylogoLabel.nativeElement.innerHTML = fileItem.name;
				this.image=this.uploader.queue[j]._file.name;
			}
			else if(val == 'rewardAddImage'){
				this.addImage = true;this.rewImage = true;
				this.urls.push("https://click365.com.au/usermanagement/images/CompanyLogo/"+this.uploader.queue[j]._file.name);
				this.logo.push(this.uploader.queue[j]._file.name);
				this.rewardImages.nativeElement.innerHTML = this.logo;
			}
			data.append('file', fileItem);
			data.append( 'dataType', this.rewardForm.controls.rewardAddImage.value);
			this.uploadFile(data).subscribe(data => {
				if (data.type === HttpEventType.UploadProgress) {
				} 
				else if (event instanceof HttpResponse) {
				}
			});
		}
        this.uploader.clearQueue();
  	}

	uploadFile(data: FormData): Observable<any> {
		return this.http.post('https:/click365.com.au/usermanagement/uploadLogo.php?fld=CompanyLogo', data);
	}

	cancel(){
		this.showTable = true;
		this.rewardForm.reset();
	}

	toggle() {
      this.showTable = false;
    }

    updateStatus(event){
    	//alert(event);
    	this.status = event;
    }

    endDate(){
    	this.error = '';
	    if ((Date.parse(this.rewardForm.controls.startTime.value) >= Date.parse(this.rewardForm.controls.endTime.value))) {
	        //alert("End date should be greater than Start date");
	        //this.endTime = '';
	        this.error={isError:true,errorMessage:'End date should be greater than Start date'};
	    }
    }

	getConfig(): SnotifyToastConfig {
		this.snotifyService.setDefaults({
		global: {
		    newOnTop: this.newTop,
		    maxAtPosition: this.blockMax,
		    maxOnScreen: this.dockMax,
		}
		});
		return {
			bodyMaxLength: this.bodyMaxLength,
			titleMaxLength: this.titleMaxLength,
			backdrop: this.backdrop,
			position: this.position,
			timeout: this.timeout,
			showProgressBar: this.progressBar,
			closeOnClick: this.closeClick,
			pauseOnHover: this.pauseHover
		};
	}

}

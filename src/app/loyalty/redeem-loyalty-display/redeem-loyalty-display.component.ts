import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import * as XLSX from 'xlsx';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import 'jspdf-autotable';


import { redeemloyaltyService } from '../../shared/data/redeemloyalty.service';


@Component({
  selector: 'app-redeem-loyalty-display',
  templateUrl: './redeem-loyalty-display.component.html',
  styleUrls: ['./redeem-loyalty-display.component.scss']
})
export class RedeemLoyaltyDisplayComponent implements OnInit {
	rows = [];
  temp = [];
  exportData = [];


    // Table Column Titles
    columns = [
        { prop: 'Name' },
        { prop: 'Points Redeemed' },
        { prop: 'Remaining Points' },
        { prop: 'Redeemed On' }
    ];
    loyaltypointform = {
      'loyalty': '',
      'pointsredeemed': ''
    };
    popupModel = {
      'dp': '',
      'dp1': ''
    };
    @ViewChild(DatatableComponent) table: DatatableComponent;
    isAdmin = false;
    userId;
    isSuperAdmin = false;

	constructor(public RedeemloyaltyService: redeemloyaltyService) {
    let users = JSON.parse(localStorage.getItem('currentUser'));
    if(users){
      if(users['role_id'] == '2'){
        this.isSuperAdmin = true;
      }
      if(users['role_id'] == '1'){
        this.isAdmin = true;
      }
      if(users['role_id'] == '3'){
        this.userId = localStorage.getItem('currentUserId');
      }
    }
    if(this.isAdmin){
      this.RedeemloyaltyService.getdata().then(data => {
        console.log(data['loyalPoints']);
        this.rows = data['loyalPoints'];
        this.exportData = data['loyalpoints'];
      });
    }
    else{
      this.RedeemloyaltyService.getdataByuser(this.userId).then(data => {
        console.log(data['loyalPoints']);
        this.rows = data['loyalPoints'];
        this.exportData = data['loyalpoints'];
      });
    }
	}

	ngOnInit() {
	}

  updateFilters() {
      let stDate = this.popupModel.dp['year']+'-'+this.popupModel.dp['month']+'-'+this.popupModel.dp['day'];
      //this.popupModel.dp = stDate;
      let endDate = this.popupModel.dp1['year']+'-'+this.popupModel.dp1['month']+'-'+this.popupModel.dp1['day'];
      //this.popupModel.dp1 = endDate;
      let datesValue = {
        'dp': stDate,
        'dp1': endDate
      }
      if(this.isAdmin){
      this.RedeemloyaltyService.updateFilters(datesValue).then(data => {
        console.log(data);
        if(data['status'] == 'success'){
          this.rows = data['loyalPoints'];
          this.exportData = data['loyalpoints'];
        }
        else{
          this.rows=[];
          this.exportData=[];
        }
      });
      }
      else{
        this.RedeemloyaltyService.updateFiltersbyuser(datesValue,this.userId).then(data => {
          console.log(data['loyalPoints']);
          if(data['status'] == 'success'){
          this.rows = data['loyalPoints'];
          this.exportData = data['loyalpoints'];
        }
        else{
          this.rows=[];
          this.exportData=[];
        }
        });
      }
     }


	ExportToPdf(){
		console.log(this.exportData);
		var doc = new jspdf();
		var col = ['Name', 'Points Redeemed', 'Remaining Points', 'Redeemed On'];
		var rows = [];
		this.exportData.forEach(element => {
			var temp = [element.name, element.points_used, element.remainingPoints, element.date];
			rows.push(temp);
		});
		doc.autoTable(col, rows, { startY: 10 });
		doc.save('renewal_'+ new Date().getTime() +'.pdf');
	}

	ExportTOExcel(){
		const workBook = XLSX.utils.book_new();
		const workSheet = XLSX.utils.json_to_sheet(this.exportData);

		XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
		XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.xlsx');
	}

	ExportTOCsv(){
		const workBook = XLSX.utils.book_new();
		const workSheet = XLSX.utils.json_to_sheet(this.exportData);
		const csv = XLSX.utils.sheet_to_csv(workSheet);

		XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
		XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.csv');
	}
}

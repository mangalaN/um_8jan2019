import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemLoyaltyDisplayComponent } from './redeem-loyalty-display.component';

describe('RedeemLoyaltyDisplayComponent', () => {
  let component: RedeemLoyaltyDisplayComponent;
  let fixture: ComponentFixture<RedeemLoyaltyDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedeemLoyaltyDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedeemLoyaltyDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

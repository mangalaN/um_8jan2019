import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {FormControl, FormArray, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder} from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { NgbDateStruct, NgbDatepickerI18n, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import * as _moment from 'moment-timezone';
import * as XLSX from 'xlsx';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import 'jspdf-autotable';

import { loyaltyService } from '../../shared/data/loyalty.service';
import { MembershipService } from '../../shared/data/membership.service';
import { AddFieldsService } from '../../shared/data/addFields.service';

@Component({
  selector: 'app-loyalty-points',
  templateUrl: './loyalty-points.component.html',
  styleUrls: ['./loyalty-points.component.scss']
})

export class LoyaltyPointsComponent implements OnInit {
  public loyaltydisplay: any;
  showTable = true;
  submitted = false;
  percentage: boolean = false;
  absolute: boolean = false;
  eventBased: boolean = false;
  referral: boolean = false;
  rows = [];
  temp = [];
  exportData = [];
  loyaltydata;
  selectfield = '';
  selectmatchType = '';
  selectfield1 = [];
  selectmatchType1 = [];
  addvalue1 = [];
  // Table Column Titles
  columns = [
    { name: 'Rule Type' },
    { name: 'Membership Type' },
    { name: 'Name' },
    { name: 'Description' },
    //{ name: 'Points' },
    { name: 'Max Points' },
    { name: 'Start Date' },
    { name: 'End Date' },
    { name: 'Status' }
  ];
  
  columns1 = [
    { name: 'Rule' },
    { name: 'Membership' },
    { name: 'Name' },
    { name: 'Description' },
    { name: 'Max Points' },
    { name: 'Start Date' },
    { name: 'End Date' }
  ];

  allColumns = [
    { name: 'Rule' },
    { name: 'Membership' },
    { name: 'Name' },
    { name: 'Description' },
    { name: 'Max Points' },
    { name: 'Start Date' },
    { name: 'End Date' }
  ];

 isCollapsed = false;
  loyaltyform: FormGroup;
  invoiceForm: FormGroup;
  filterForm: FormGroup;
  loyaltyDetails = {
    'ruleType': '',
    'ruleVal': '',
    'referer': '',
    'referee': '',
    'memType': '',
    'name': '',
    'description': '',
    'points': '',
    'maxpoints': '',
    'id': '',
    'startDate': '',
    'endDate': '',
    'field': '',
    'matchType': '',
    'addvalue': '',
    'updatestatus': ''
  };
  endDate1: NgbDateStruct;
  loyaltyStatus = {
    'status': '',
    'id': '',
    'updatestatus': ''
  };
  editLoyalty = false;
  loyaltyId = 0;
  selectval = '';

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;
  currentDate = new Date();
  members = [];
  status;
  selectedStatus = [];
  loyaltyrules;selectedMemType;
  event;
  tableValues = [];
  tableName;
  result:any[];
  dynamicFields = [];
  extraFields = [];
  checkextrafields: boolean = false;
  error:any={isError:false,errorMessage:''};
  endTime: NgbTimeStruct;

  startDate: NgbDateStruct;
  startTime: NgbTimeStruct;
  extrafield;
  extramatchType;
  extraaddvalue;
  selectedtableName;
  selectedName;
  savedtable;
  savedextratable = [];
  indexValue = 0;
  editID;dateField: boolean = false;
  parsedStartDate;parsedEndDate;parsedStartTime;
  parsedEndTime;tab;extratab;

  filterModule = {
    'startDate':'',
    'endDate':'',
    'filterMemberType':''
  }
  filterMembership = [];

  @ViewChild(DatatableComponent) table: DatatableComponent;
  config = {
    displayKey:"description",
    search:true,
    height: 'auto',
    placeholder:'Select',
    customComparator: ()=>{},
    limitTo: this.members.length,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder:'Search',
    searchOnKey: 'name'
  }
  configfilter = {
    displayKey:"description",
    search:true,
    height: 'auto',
    placeholder:'Select',
    customComparator: ()=>{},
    limitTo: this.filterMembership.length,
    moreText: 'more',
    noResultsFound: 'No results found!',
    searchPlaceholder:'Search',
    searchOnKey: 'name'
  }

   // declarations for add fields
  addField = false;
  addFields = [];
  FieldsValue = [];

  constructor(private snotifyService: SnotifyService, public addFieldsService: AddFieldsService, public membershipservice: MembershipService, private formBuilder: FormBuilder, public loyaltyservice: loyaltyService, public router: Router) {
    this.membershipservice.getMembership().then(data => {
      if(data['membership']){
        //this.filterMembership = data['membership'];
        for(let i = 0; i < data['membership'].length; i++){
          this.members.push(data['membership'][i].plans);
          this.filterMembership.push(data['membership'][i].plans);
        }
        this.filterMembership = [...this.filterMembership];
      }
      else{
        this.filterMembership = [];
      }
    });
    this.status = [
      { name: 'Active', value: '1' },
      { name: 'Inactive', value: '2' },
      { name: 'Status-complete-or-active', value: '3' },
    ];
    this.status.map((item) => this.selectedStatus.push(item));
  }

  ngOnInit() {
    let formControls = {
      ruleType: ['', Validators.required],
      ruleVal: ['', null],
      referer: ['', null],
      referee: ['', null],
      memType: ['', Validators.required],
      description: ['', Validators.required],
      name: ['', Validators.required],
      maxpoints: ['', null],
      status: ['', null],
      startDate: ['', Validators.required],
      startTime: ['', Validators.required],
      endDate1: ['', Validators.required],
      endTime: ['', Validators.required],
      field: ['', Validators.required],
      matchType: ['', Validators.required],
      addvalue: ['', Validators.required],
      fields: this.formBuilder.array([
        //this.createItem(),
      ]),
    }
    this.filterForm = this.formBuilder.group({
        startDate :['',null],
        endDate :['',null],
        filterMemberType :['',null]
    });

    this.checkextrafields = false;
    this.event = ['Signup', 'Referral', 'Newsletter Signup', 'First Purchase/Transaction'];
    this.invoiceForm = this.formBuilder.group({
      'name': ['', Validators.required],
      'invoiceparticulars': this.formBuilder.array([])
    });
    this.loyaltyservice.getLoyalty(new Date().getTime()).then(data => {
      this.loyaltydisplay = data['loyalty'];
      this.exportData = data['loyalty'];
    },
    error => {
    });

    this.loyaltyservice.getRuleType().then(data => {
      this.loyaltyrules = data['loyalty'];
    });
    this.loyaltyservice.getDynamicFields().then(data => {
      this.extraFields = data['table'];
    });
    this.loyaltyservice.getTablewithColumns().then(data => {
      this.loyaltyservice.getDynamicFields().then(data => {
        this.extraFields = data['table'];
      });
      if(data['status'] == "success"){
        for(var i = 0; i < this.extraFields.length; i++){
          data['table'].push(this.extraFields[i]);
        }
        this.tableValues = data['table'];
        var groups = new Set(this.tableValues.map(item => item.table_name));
        this.result = [];
        groups.forEach(g => {
          this.result.push({
            name: g,
            values: this.tableValues.filter(i => i.table_name === g)
          })
        })
      }
    });
    // extra fields Code
    this.addFieldsService.getFieldsByTable('loyalty').then(data=>{
      if(data['status'] == 'success'){
        //addFields
        for(let i=0; i< data['fields'].length; i++){
          let fields = JSON.parse(data['fields'][i]['fields']);
            this.addFields.push({
              'label': fields['label'],
              'datatype':fields['datatype'],
              'length':fields['length'],
              'mandatory':fields['mandatory'],
              'validation':fields['validation'],
              'validationMsg':fields['validationMsg'],
              'fieldType':fields['fieldType'],
              'fieldName':fields['fieldName'],
              'id':data['fields'][i]['id']
            });
            if(fields['mandatory']){
              formControls[fields['fieldName']] = ['',Validators.required];
            }
            else{
              formControls[fields['fieldName']] = ['',null];
            }
        }
        this.loyaltyform = this.formBuilder.group(formControls);
        this.addField = true;
      }
      else{
        this.loyaltyform = this.formBuilder.group(formControls);
        this.addField = false;
      }
    });
  }
  // extra fields Code
  fieldValueChange(id, value, fieldName){
    if(this.FieldsValue.length > 0){
      let checkValue = false;
      for(let i = 0; i < this.FieldsValue.length; i++){
        if(this.FieldsValue[i].id == id){
          this.FieldsValue[i].value = value;
          this.FieldsValue[i].fieldName = fieldName;
          checkValue = true;
        }
      }
      if(!checkValue){
        this.FieldsValue.push({"id": id, "value": value, "fieldName": fieldName});
      }
    }
    else{
      this.FieldsValue.push({"id": id, "value": value, "fieldName": fieldName});
    }
  }
  // extra fields Code
  isNumberKey(evt) {
      let charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
          return false;
      }
      return true;
    }

  get f(){ return this.loyaltyform.controls; }

  createItem(index): FormGroup {
    this.indexValue++;
    let f = 'field'+index;
    let m = 'matchType'+index;
    let a = 'addvalue'+index;
    return this.formBuilder.group({
      f : '',
      m: '',
      a: ''
    });
  }

  get invoiceparticularsArray(): FormArray{
    return this.loyaltyform.get('fields') as FormArray;
  }
  
   columntoggle(col) {
    const isChecked = this.isChecked(col);
    console.log('isChecked value - ' + isChecked);
    if(isChecked) {
      this.columns1 = this.columns1.filter(c => {
        console.log('Name - ' + c.name);
        return c.name !== col.name; 
      });
    } else {
      this.columns1 = [...this.columns1, col];
      console.log('columns - ' + this.columns1);
    }
  }

  isChecked(col) {
    return this.columns1.find(c => {
      return c.name === col.name;
    });
  }

  addInvoiceParticulars(index){
    let fg = this.formBuilder.group(new FormControl());
    this.invoiceparticularsArray.push(this.createItem(index));
  }

  deleteInvoiceParticulars(idx: number) {
    this.invoiceparticularsArray.removeAt(idx);
    this.dynamicFields.splice(idx, 1);
    this.indexValue--;
    let fieldValue = this.dynamicFields;
    if(fieldValue.length > 0){
      this.indexValue = 0;
      this.selectfield1=[];
      this.selectmatchType1=[];
      this.addvalue1=[];
      this.dynamicFields=[];
      this.loyaltyform.setControl('fields', new FormArray([]));
      for(let i = 0; i < fieldValue.length; i++){
        this.selectfield1.push(fieldValue[i].field);
        this.selectmatchType1.push(fieldValue[i].matchType);
        this.addvalue1.push(fieldValue[i].addvalue);
        this.dynamicFields.push({'field':fieldValue[i].field,'matchType':fieldValue[i].matchType,'addvalue':fieldValue[i].addvalue, 'table': fieldValue[i].table});
        this.addInvoiceParticulars(this.dynamicFields.length);
      }
    }
  }

  toggle(){
    this.refValue = '';
    this.referral = false;
    this.loyaltyform.reset();
    this.loyaltyform.setControl('fields', new FormArray([]));
    this.selectfield = '';
    this.selectmatchType = '';
    this.selectfield1=[];
    this.selectmatchType1=[];
    this.addvalue1=[];
    this.dynamicFields=[];
    this.indexValue = 0;
    this.showTable = false;
    this.editLoyalty = false;
  }

  onChangeStatus(status,id) {
    this.loyaltyStatus.status = status;
    this.loyaltyStatus.id = id;
    this.loyaltyStatus.updatestatus = 'true';
    this.loyaltyservice.updateloyalty(this.loyaltyStatus, '', new Date().getTime()).then(data => {
    });
  }

  selected(event){
    this.selectedMemType = event;
  }

  endDate(){
    this.error = '';
    if(this.loyaltyform.controls.endTime.value){
      this.parsedStartDate = this.loyaltyform.controls.startDate.value['year']+'-'+this.loyaltyform.controls.startDate.value['month']+'-'+this.loyaltyform.controls.startDate.value['day'];
      this.parsedEndDate = this.loyaltyform.controls.endDate1.value['year']+'-'+this.loyaltyform.controls.endDate1.value['month']+'-'+this.loyaltyform.controls.endDate1.value['day'];
      this.parsedStartTime = this.loyaltyform.controls.startTime.value['hour']+':'+this.loyaltyform.controls.startTime.value['minute'];
      this.parsedEndTime = this.loyaltyform.controls.endTime.value['hour']+':'+this.loyaltyform.controls.endTime.value['minute'];
      if(Date.parse(this.parsedStartDate+' '+this.parsedStartTime) >= Date.parse(this.parsedEndDate+' '+this.parsedEndTime)){
        this.error={isError:true,errorMessage:'End date should be greater than Start date'};
      }
    }
  }

  addloyalty(val) {
    this.refValue = '';
    this.referral = false;
    this.dynamicFields = [];
    this.submitted = true;
    if (this.loyaltyform.invalid) {
      this.submitted = false;
      return;
    }
    if(!this.editLoyalty){
      this.loyaltydata = {
        ruleType: this.f.ruleType.value,
        ruleVal: this.f.ruleVal.value,
        referer: this.f.referer.value,
        referee: this.f.referee.value,
        memType: this.f.memType.value,
        name: this.f.name.value,
        description: this.f.description.value,
        maxpoints: this.f.maxpoints.value,
        id: this.loyaltyId,
        startDate: this.f.startDate.value,
        startTime: this.f.startTime.value,
        endDate1: this.f.endDate1.value,
        endTime: this.f.endTime.value,
        field: this.f.field.value,
        matchType: this.f.matchType.value,
        addvalue: this.f.addvalue.value,
        addfields: JSON.stringify(this.FieldsValue)
      };
      this.dynamicFields.push({'field':this.f.field.value,'matchType':this.f.matchType.value,'addvalue':this.f.addvalue.value,'table': this.selectedtableName});

      for(var i = 0; i < this.indexValue; i++){
        let f = (document.getElementById("field"+i) as HTMLInputElement).value;
        let m = (document.getElementById("matchType"+i) as HTMLInputElement).value;
        let a = (document.getElementById("addvalue"+i) as HTMLInputElement).value;
        this.dynamicFields.push({'field':f,'matchType':m,'addvalue':a,'table': this.selectedName});
      }
      this.loyaltyservice.addloyalty(this.loyaltydata, this.dynamicFields, new Date().getTime()).then(data => {
        if(data['status'] == "success"){
          this.percentage = false;
          this.absolute = false;
          this.eventBased = false;
          this.referral = false;
          this.snotifyService.success('Saved Successfullly', '', this.getConfig());
          this.loyaltydisplay = [];
          this.exportData = [];
          this.loyaltyservice.getLoyalty(new Date().getTime()).then(data => {
            this.loyaltydisplay = data['loyalty'];
            this.exportData = data['loyalty'];
          });
          this.showTable = true;
          this.submitted = false;
          this.loyaltyform.reset();
          this.loyaltyform.setControl('fields', new FormArray([]));
          this.selectfield = '';
          this.selectmatchType = '';
        }
        else if(data['status'] == "Failed"){
        }
      },
      error => {
      });
    }
    else{
      this.loyaltydata = {
        ruleType: this.f.ruleType.value,
        ruleVal: this.f.ruleVal.value,
        referer: this.f.referer.value,
        referee: this.f.referee.value,
        memType: this.f.memType.value,
        name: this.f.name.value,
        description: this.f.description.value,
        maxpoints: this.f.maxpoints.value,
        id: this.loyaltyId,
        startDate: this.f.startDate.value,
        startTime: this.f.startTime.value,
        endDate1: this.f.endDate1.value,
        endTime: this.f.endTime.value,
        field: this.f.field.value,
        matchType: this.f.matchType.value,
        addvalue: this.f.addvalue.value,
        updatestatus: 'false',
        addfields: JSON.stringify(this.FieldsValue)
      };
      if(this.selectedtableName != undefined){
        this.tab = this.selectedtableName;
      }
      else if(this.selectedtableName == undefined){
        this.tab = this.savedtable;
      }

      this.dynamicFields.push({'field':this.f.field.value,'matchType':this.f.matchType.value,'addvalue':this.f.addvalue.value,'table':this.tab});
      for(var i = 0; i < this.indexValue; i++){
        if(this.selectedName != undefined){
          this.extratab = this.selectedName;
        }
        else if(this.selectedName == undefined){
          this.extratab = this.savedextratable[i];
        }
        let f = (document.getElementById("field"+i) as HTMLInputElement).value;
        let m = (document.getElementById("matchType"+i) as HTMLInputElement).value;
        let a = (document.getElementById("addvalue"+i) as HTMLInputElement).value;
        this.dynamicFields.push({'field':f,'matchType':m,'addvalue':a, 'table': this.extratab});
      }
      this.loyaltyservice.updateloyalty(this.loyaltydata, this.dynamicFields, new Date().getTime()).then(data => {
        if(data['status'] == "success"){
          this.percentage = false;
          this.absolute = false;
          this.eventBased = false;
          this.referral = false;
          this.loyaltyform.reset();
          this.loyaltyform.setControl('fields', new FormArray([]));
          this.selectfield = '';
          this.selectmatchType = '';
          this.snotifyService.success('Updated Successfullly', '', this.getConfig());
          this.loyaltydisplay = [];
          this.exportData = [];
          this.loyaltyservice.getLoyalty(new Date().getTime()).then(data => {
            if(data['status'] == "success"){
              this.loyaltydisplay = data['loyalty'];
              this.exportData = data['loyalty'];
            }
            else{
              this.loyaltydisplay = [];
              this.exportData = [];
            }
          });
          this.showTable = true;
          this.editLoyalty = false;
          this.loyaltyId = 0;
          this.submitted = false;
          this.loyaltyform.reset();
        }
        else if(data['status'] == "Failed"){
        }
      },
      error => {
      });
    }
  }

  selectChange(ev){
    const selectedIndex = ev.target.selectedIndex;
    const optGroupLabel = ev.target.options[selectedIndex].parentNode.getAttribute('label');
    this.selectedtableName = optGroupLabel;
  }

  selectedField(ev){
    const selectedIndex = ev.target.selectedIndex;
    const optGroupLabel = ev.target.options[selectedIndex].parentNode.getAttribute('label');
    this.selectedName = optGroupLabel;
  }

  onChange(event){
    this.refValue = '';
    if(event == 'Percentage'){
      this.percentage = true;
      this.absolute = false;
      this.eventBased = false;
      this.referral = false;
    }
    else if(event == 'Absolute'){
      this.absolute = true;
      this.percentage = false;
      this.eventBased = false;
      this.referral = false;
    }
    else if(event == 'Event Based'){
      this.eventBased = true;
      this.absolute = false;
      this.percentage = false;
      this.referral = false;
    }
    else if(event == 'Double your points'){
      this.eventBased = false;
      this.absolute = false;
      this.percentage = false;
      this.referral = false;
    }
    else if(event == 'Referral'){
      this.eventBased = false;
      this.absolute = false;
      this.percentage = false;
      this.referral = true;
    }
  }

  refValue = '';datetime;
  referralType(event){
    this.refValue = event;
  }

  editloyalty(id){
    this.editID = id;
    this.loyaltyform.reset();
    this.loyaltyform.setControl('fields', new FormArray([]));
    this.selectfield = '';
    this.selectmatchType = '';
    this.selectfield1 = [];
    this.selectmatchType1 = [];
    this.addvalue1 = [];
    this.loyaltyservice.getLoyaltyById(id, new Date().getTime()).then(data => {
      this.showTable = false;
      this.editLoyalty = true;
      this.loyaltyId = id;
      this.refValue = data['loyalty'][0].rule_value;

      if(data['status'] == 'success'){
        for(let k = 0; k < data['loyalty'][0]['fields_value'].length; k++){
          if(k == 0){
            this.savedtable = data['loyalty'][0]['fields_value'][0]['table'];
          }
          else if(k >= 1){
            this.savedextratable.push(data['loyalty'][0]['fields_value'][k]['table']);
          }
        }
        let year = (new Date(data['loyalty'][0].endDate)).getFullYear();
        let month = (new Date(data['loyalty'][0].endDate)).getMonth() + 1;
        let day = (new Date(data['loyalty'][0].endDate)).getUTCDate();
        let hour = (new Date(data['loyalty'][0].endDate)).getHours();
        let minute = (new Date(data['loyalty'][0].endDate)).getMinutes();
        let year1 = (new Date(data['loyalty'][0].startDate)).getFullYear();
        let month1 = (new Date(data['loyalty'][0].startDate)).getMonth() + 1;
        let day1 = (new Date(data['loyalty'][0].startDate)).getUTCDate();
        let hour1 = (new Date(data['loyalty'][0].startDate)).getHours();
        let minute1 = (new Date(data['loyalty'][0].startDate)).getMinutes();
        if(data['loyalty'][0].rule_type == 'Percentage'){
          this.percentage = true;
          this.absolute = false;
          this.eventBased = false;
          this.referral = false;
        }
        else if(data['loyalty'][0].rule_type == 'Absolute'){
          this.absolute = true;
          this.percentage = false;
          this.eventBased = false;
          this.referral = false;
        }
        else if(data['loyalty'][0].rule_type == 'Event Based'){
          this.eventBased = true;
          this.absolute = false;
          this.percentage = false;
          this.referral = false;
        }
        else if(data['loyalty'][0].rule_type == 'Referral'){
          this.eventBased = false;
          this.absolute = false;
          this.percentage = false;
          this.referral = true;
        }
        this.selectval = data['loyalty'][0].rule_type;
        this.loyaltyDetails.ruleType = data['loyalty'][0].rule_type;
        this.loyaltyDetails.ruleVal = data['loyalty'][0].rule_value;
        this.loyaltyDetails.referer = data['loyalty'][0].referer;
        this.loyaltyDetails.referee = data['loyalty'][0].referee;
        this.loyaltyDetails.memType = data['loyalty'][0].mem_type;
        this.loyaltyDetails.name = data['loyalty'][0].name;
        this.loyaltyDetails.description = data['loyalty'][0].description;
        this.loyaltyDetails.points = data['loyalty'][0].points;
        this.loyaltyDetails.maxpoints = data['loyalty'][0].maxpoints;
        this.loyaltyDetails.startDate = data['loyalty'][0].startDate;
        this.loyaltyDetails.endDate = data['loyalty'][0].endDate;
        this.startDate = {year: year1, month: month1, day: day1};
        this.startTime = {hour: hour1, minute: minute1, second: 30};
        this.endDate1 = {year: year, month: month, day: day};
        this.endTime = {hour: hour, minute: minute, second: 30};
        this.loyaltyDetails.field = data['loyalty'][0]['fields_value'][0].field;
        this.loyaltyDetails.matchType = data['loyalty'][0]['fields_value'][0].matchType;
        this.loyaltyDetails.addvalue = data['loyalty'][0]['fields_value'][0].addvalue;
        this.loyaltyDetails.updatestatus = 'false';
        let fieldValue = data['loyalty'][0]['fields_value'];
        let val = fieldValue[0].addvalue;
        this.loyaltyDetails.id = data['loyalty'][0].id;

        this.loyaltyform.controls['ruleType'].setValue(data['loyalty'][0].rule_type);
        this.loyaltyform.controls['ruleVal'].setValue(data['loyalty'][0].ruleVal);
        this.loyaltyform.controls['memType'].setValue(data['loyalty'][0].mem_type);
        this.loyaltyform.controls['name'].setValue(data['loyalty'][0].name);
        this.loyaltyform.controls['description'].setValue(data['loyalty'][0].description);
        this.loyaltyform.controls['maxpoints'].setValue(data['loyalty'][0].maxpoints);
        this.loyaltyform.controls['field'].setValue(fieldValue[0].field);
        this.loyaltyform.controls['matchType'].setValue(fieldValue[0].matchType);
        this.loyaltyform.controls['addvalue'].setValue(val);
        this.selectfield = fieldValue[0].field;
        this.selectmatchType = fieldValue[0].matchType;
        if(fieldValue.length >= 2){
          for(let i = 1; i < fieldValue.length; i++){
            this.selectfield1.push(fieldValue[i].field);
            this.selectmatchType1.push(fieldValue[i].matchType);
            this.addvalue1.push(fieldValue[i].addvalue);
            this.dynamicFields.push({'field':fieldValue[i].field,'matchType':fieldValue[i].matchType,'addvalue':fieldValue[i].addvalue, 'table': fieldValue[i].table});
            this.addInvoiceParticulars(i-1);
          }
        }
        //extra fields Code
        this.FieldsValue = data['loyalty'][0].extraFieldsValue;
        for(let j = 0; j < this.addFields.length; j++){
          for(let i = 0; i < this.FieldsValue.length; i++){
            if(this.addFields[j].id == this.FieldsValue[i].id){
              this.addFields[j].valuesBind = this.FieldsValue[i].value;
            }
          }
        }
      }
      else{
      }
    },
    error => {
    });
  }

  changeField(){
    this.dateField = true;
  }

  deleteloyalty(id){
    this.loyaltyservice.deleteloyalty(id, new Date().getTime()).then(data => {
      if(data['status'] == "success"){
        this.snotifyService.success('Deleted Successfullly', '', this.getConfig());
        this.loyaltydisplay = [];
        this.exportData = [];
        this.loyaltyservice.getLoyalty(new Date().getTime()).then(data => {
          if(data['status'] == "success"){
            this.loyaltydisplay = data['loyalty'];
            this.exportData = data['loyalty'];
          }
          else{
            this.loyaltydisplay = [];
            this.exportData = [];
          }
        });
        this.submitted = false;
        this.loyaltyform.reset();
      }
      else if(data['status'] == "Failed"){
      }
    },
    error => {
    });
  }

  cancel(){
    this.showTable = true;
    this.editLoyalty = false;
    this.referral = false;
    this.refValue = '';
    this.loyaltyId = 0;
    this.loyaltyform.reset();
    this.loyaltyform.setControl('fields', new FormArray([]));
    this.selectfield = '';
    this.selectmatchType = '';
    this.submitted = false;
    this.loyaltyservice.getLoyalty(new Date().getTime()).then(data => {
      if(data['status'] == "success"){
        this.loyaltydisplay = data['loyalty'];
        this.exportData = data['loyalty'];
      }
      else{
        this.loyaltydisplay = [];
        this.exportData = [];[];
      }
    });
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
      global: {
        newOnTop: this.newTop,
        maxAtPosition: this.blockMax,
        maxOnScreen: this.dockMax,
      }
    });
    return {
      bodyMaxLength: this.bodyMaxLength,
      titleMaxLength: this.titleMaxLength,
      backdrop: this.backdrop,
      position: this.position,
      timeout: this.timeout,
      showProgressBar: this.progressBar,
      closeOnClick: this.closeClick,
      pauseOnHover: this.pauseHover
    };
  }


  //filters columnMode
  getFilters(){
  if(this.filterForm.controls.endDate.value){
    this.parsedStartDate = this.filterForm.controls.startDate.value['year']+'-'+this.filterForm.controls.startDate.value['month']+'-'+this.filterForm.controls.startDate.value['day'];
    this.parsedEndDate = this.filterForm.controls.endDate.value['year']+'-'+this.filterForm.controls.endDate.value['month']+'-'+this.filterForm.controls.endDate.value['day'];
    if(Date.parse(this.parsedStartDate) >= Date.parse(this.parsedEndDate)){
      this.error={isError:true,errorMessage:'End date should be greater than Start date'};
      this.loyaltydisplay = [];
      this.exportData = [];
    }
    else{

        this.error = '';
      this.loyaltyservice.getLoyaltyFilter(this.filterModule).then(data => {
        if(data['status'] == "success"){
          this.loyaltydisplay = data['loyalty'];
          this.exportData = data['loyalty'];
        }
        else{
          this.loyaltydisplay = [];
          this.exportData = [];
        }

      });
    }
  }
  else{
    this.error = '';
  this.loyaltyservice.getLoyaltyFilter(this.filterModule).then(data => {
    if(data['status'] == "success"){
      this.loyaltydisplay = data['loyalty'];
      this.exportData = data['loyalty'];
    }
    else{
      this.loyaltydisplay = [];
      this.exportData = [];
    }

  });
  }

  }

    reset(){
    this.filterForm.reset();
    this.filterForm.markAsPristine();
    this.filterForm.markAsUntouched();

    this.filterForm.controls['filterMemberType'].reset();
    this.membershipservice.getMembership().then(data => {
      if(data['membership']){
        //this.filterMembership = data['membership'];
        for(let i = 0; i < data['membership'].length; i++){
          this.members.push(data['membership'][i].plans);
          this.filterMembership.push(data['membership'][i].plans);
        }
        this.filterMembership = [...this.filterMembership];
      }
      else{
        this.filterMembership = [];
      }
    });
    this.loyaltyservice.getLoyalty(new Date().getTime()).then(data => {
      if(data['status'] == "success"){
        this.loyaltydisplay = data['loyalty'];
        this.exportData = data['loyalty'];
      }
      else{
        this.loyaltydisplay = [];
        this.exportData = [];[];
      }
    });
    //document.getElementsByName['filterMemberType'].innerHTML = '';
    //this.selectedMemType=[];
    //this.modelProperty = [];
  }

  getActive(){
    this.loyaltyservice.getActive().then(data => {
      if(data['status'] == "success"){
        this.loyaltydisplay = data['loyalty'];
        this.exportData = data['loyalty'];
      }
      else{
        this.loyaltydisplay = [];
        this.exportData = [];
      }
        this.filterForm.reset();
    });
  }
  getInactive(){
    this.loyaltyservice.getInActive().then(data => {
      if(data['status'] == "success"){
        this.loyaltydisplay = data['loyalty'];
        this.exportData = data['loyalty'];
      }
      else{
        this.loyaltydisplay = [];
        this.exportData = [];
      }
        this.filterForm.reset();
    });
  }
  ExportToPdf(){
    console.log(this.exportData);
    var doc = new jspdf();
    var col = ['Rule Type', 'Membership Type', 'Name', 'Description', 'Max Points', 'Start Date','End Date','Action'];
    var rows = [];
    this.exportData.forEach(element => {
      var temp = [element.rule, element.membership, element.name, element.description, element.maxpoints, element.startDate,element.endDate,element.status];
      rows.push(temp);
    });
    doc.autoTable(col, rows, { startY: 10 });
    doc.save('renewal_'+ new Date().getTime() +'.pdf');
  }

  ExportTOExcel(){
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(this.exportData);

    XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
    XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.xlsx');
  }

  ExportTOCsv(){
    const workBook = XLSX.utils.book_new();
    const workSheet = XLSX.utils.json_to_sheet(this.exportData);
    const csv = XLSX.utils.sheet_to_csv(workSheet);

    XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
    XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.csv');
  }
}

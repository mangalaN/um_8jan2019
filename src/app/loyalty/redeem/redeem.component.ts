import { Component, OnInit } from '@angular/core';
import { loyaltyService } from '../../shared/data/loyalty.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import swal from 'sweetalert2';

@Component({
  selector: 'app-redeem',
  templateUrl: './redeem.component.html',
  styleUrls: ['./redeem.component.scss']
})
export class RedeemComponent implements OnInit {
	rewardDisplay;
	redeemeddata;
	redeemed: boolean = false;
	user;
	memberPoints;remainingPoints;memType;

	style = 'material';
    title = 'Snotify title!';
    body = 'Lorem ipsum dolor sit amet!';
    timeout = 3000;
    position: SnotifyPosition = SnotifyPosition.centerCenter;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;

  constructor(private snotifyService: SnotifyService, private LoyaltyService: loyaltyService) {
  	this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngOnInit() {
  	this.LoyaltyService.getReward(new Date().getTime()).then(data =>{
		this.rewardDisplay = data['reward'];
        this.memType = data['reward'][0].mem_type.replace(/[\[\]\"\"']+/g,'');
	});
	this.LoyaltyService.getMemberPoints(new Date().getTime(), this.user['id']).then(data =>{
		this.memberPoints = data['points'];
		this.remainingPoints = data['points'][0]['points'];
	});

  }

  redeem(points,id){
  	this.redeemeddata = {
		transaction_id: 1,
		member_id: this.user['id'],
		points_used: points,
		paid: '',
		paymentinfo: ''
    }

  	if(parseInt(this.remainingPoints) >= parseInt(points) && parseInt(this.remainingPoints) != 0){
  		this.LoyaltyService.updateMemberPoints(new Date().getTime(), this.user['id'], (this.remainingPoints - points)).then(data =>{
  			this.LoyaltyService.saveRedeemedPoints(new Date().getTime(), this.redeemeddata).then(redeemeddata =>{
  				const { timeout, closeOnClick, ...config } = this.getConfig();
  				this.snotifyService.confirm('Are you sure to redeem the Reward?', 'Confirm', {
		            ...config,
		            buttons: [
		                { text: 'Yes', action: (toast) => {this.redeemed = id;this.snotifyService.remove(toast.id);swal("Success", "Redeemed Successfully!", "success");} },
		                { text: 'No', action: (toast) => { console.log('Clicked: Close'); this.snotifyService.remove(toast.id); }, bold: true }
		            ]
		        });
  			});
  		});
  	}

  	else if(parseInt(points) > parseInt(this.remainingPoints)){
  		alert('Not Enough Points');
  	}
  }
  
  viewDetails(id){

  	this.LoyaltyService.getRewardById(id,new Date().getTime()).then(data =>{
  		let self = this;
    		swal({
		        title: '<b>Reward Detail</b>',
		        type: 'info',
		        html:
		        '<table class="viewOrg"><tbody><tr></tr>' +
		        '<tr><th>REWARD NAME</th><td>'+data['reward'][0].reward_name+'</td></tr>' +
		        '<tr><th>REWARD DESCRIPTION</th><td>'+data['reward'][0].reward_description+'</td></tr>' +
		        '<tr><th>REWARD IMAGE</th><td><img src='+data['reward'][0].reward_image+'></td></tr>' +
		        '<tr><th>POINTS</th><td>'+data['reward'][0].points_value+'</td></tr>' +
		        '<tr><th>MEMBERSHIP TYPE</th><td><span class="badge badge-primary">'+data['reward'][0].mem_type.replace(/[\[\]\"\"']+/g,'')+'</span></td></tr>' +
		        '<tr><th>START TIME</th><td>'+data['reward'][0].start_time+'</td></tr>' +
		        '<tr><th>END TIME</th><td>'+data['reward'][0].end_time+'</td></tr>' +
		        '</tbody></table><br />',
		        showCloseButton: true,
		        confirmButtonText:
		        'Redeem'
		    }).then(function(submit){
		    	//console.log(JSON.stringify(submit));
		    	if(submit.value){
		    		self.redeem(data['reward'][0].points_value,id);
		    	}
		    })
    	});
  }

  getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
    }

}

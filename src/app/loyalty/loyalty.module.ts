import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { LoyaltyRoutingModule } from "./loyalty-routing.module";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { SelectDropDownModule } from 'ngx-select-dropdown';

import { MembershipComponent } from "../membership/membership.component";
import { RenewalsComponent } from "../renewals/renewals.component";
import { TransactionsComponent } from "../transactions/transactions.component";
import { LoyaltyComponent } from "./loyalty/loyalty.component";
import { LoyaltyPointsComponent } from "./loyalty-points/loyalty-points.component";
import { LoyaltyDisplayComponent } from "./loyalty-display/loyalty-display.component";
import { RedeemRewardComponent } from "./redeem-reward/redeem-reward.component";
import { RedeemComponent } from "./redeem/redeem.component";
import { RedeemLoyaltyDisplayComponent } from "./redeem-loyalty-display/redeem-loyalty-display.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { UiSwitchModule } from 'ngx-ui-switch';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';

@NgModule({
    imports: [
        CommonModule,
        LoyaltyRoutingModule,
        NgxDatatableModule,
        Ng2SmartTableModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NgSelectModule,
        SelectDropDownModule,
        UiSwitchModule,
        FileUploadModule,
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot()
    ],
    declarations: [
        MembershipComponent,
        RenewalsComponent,
        TransactionsComponent,
        LoyaltyComponent,
        LoyaltyPointsComponent,
        LoyaltyDisplayComponent,
        RedeemRewardComponent,
        RedeemComponent,
        RedeemLoyaltyDisplayComponent
    ]
})
export class LoyaltyModule { }

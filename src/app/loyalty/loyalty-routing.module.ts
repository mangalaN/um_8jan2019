import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MembershipComponent } from "../membership/membership.component";
import { RenewalsComponent } from "../renewals/renewals.component";
import { TransactionsComponent } from "../transactions/transactions.component";
import { LoyaltyPointsComponent } from "./loyalty-points/loyalty-points.component";
import { LoyaltyComponent } from "./loyalty/loyalty.component";
import { LoyaltyDisplayComponent } from "./loyalty-display/loyalty-display.component";
import { RedeemRewardComponent } from "./redeem-reward/redeem-reward.component";
import { RedeemComponent } from "./redeem/redeem.component";
import { RedeemLoyaltyDisplayComponent } from "./redeem-loyalty-display/redeem-loyalty-display.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'membership',
        component: MembershipComponent,
        data: {
          title: 'Membership'
        }
      },
      {
        path: 'renewals',
        component: RenewalsComponent,
        data: {
          title: 'Renewals'
        }
      },
      {
        path: 'transactions',
        component: TransactionsComponent,
        data: {
          title: 'Transactions'
        }
      },
      {
        path: 'loyalty-points',
        component: LoyaltyPointsComponent,
        data: {
          title: 'Loyalty Points'
        }
      },
      {
        path: 'loyalty',
        component: LoyaltyComponent,
        data: {
          title: 'Loyalty'
        }
      },
      {
        path: 'loyalty-display',
        component: LoyaltyDisplayComponent,
        data: {
          title: 'Loyalty Display'
        }
      },
      {
        path: 'redeem-reward',
        component: RedeemRewardComponent,
        data: {
          title: 'Redeem Rewards'
        }
      },
      {
        path: 'redeem-loyalty-display',
        component: RedeemLoyaltyDisplayComponent,
        data: {
          title: 'Loyalty Display'
        }
      },
      {
        path: 'redeem',
        component: RedeemComponent,
        data: {
          title: 'Redeem'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoyaltyRoutingModule { }
